#!/bin/bash

# generate a grid distribution defined by the Möbius transformation (1+x)/(1-x+s).
# The overall form is
#
#   r(x) = (rmax-rmin)/2  (1+x)/(1+s(1-x))  , s<infinity  and |x|<=1
#
# NOTE: s is here inversly defined to zeta used in the TDCIS literature.

n=$1
shift
rmin=$1
shift
rmax=$1
shift
s=$1

if [ ${#n} == 0 ] ; then n=2 ; fi
if [ ${#rmax} == 0 ] ; then rmax=1 ; fi
if [ ${#rmin} == 0 ] ; then rmin=0 ; fi
if [ ${#s} == 0 ] ; then s=0 ; fi

file="grid.${rmin}_${rmax}_${s}_${n}.dat"

# generate grid distribution with gnuplot
gpfile=$(mktemp)
ftmp=$(mktemp)
#echo $gpfile

# generate gnuplot program
cat > $gpfile << EOF
f(r1,r2,s,x)=(r2-r1)/2. *(1.+x)/(1.+s*(1.-x)) + r1
set table "$ftmp"
set sample $n+1
set format x '%.5e'
set format y '%.5e'
p [-1:1] f($rmin,$rmax,$s,x)
EOF

# generate grid in file
gnuplot $gpfile

# remove overhead output of gnuplot
sed -e '/^#/d' -e '/^$/d' -e 's/i//g' $ftmp > $file

# remove temporary files
rm $gpfile $ftmp
