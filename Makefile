#!/bin/sh
#
# This makefile generates all executables in the TDCIS package
# Source files are stored in "./src", compiled and executable file
# are stored in "./bin". All executables in "./bin" have a soft
# link into main directory ".".
#
# Please use only the make flags: 'noarpack', "default", "all", "hfonly", "tdcisonly",
# "matrixonly", "densityonly", "install", and "clean".
#
# NOTE: If you don't have ARAPCK use the make option 'noarpack' !!!
#
# The program svnversion is needed to keep the project svn revision
# number up-to-date. In order to do that a module accessing this
# number will be created on the fly.
#
# svn info: $Id: Makefile 1835 2015-07-15 06:08:51Z spabst $
# ------------------------------------------------------------------------


# variables
#----------
# path to source files
SRC=src
BIN=bin

# module lists
M_VAR=$(BIN)/Constants.o $(BIN)/Pulse_type.o $(BIN)/Grid_type.o              \
      $(BIN)/SplineInterpol.o  $(BIN)/Legendre.o $(BIN)/SphericalHarmonics.o \
      $(BIN)/VarSystem.o $(BIN)/VarWfct.o $(BIN)/VarMatrix.o 

M_BASIC=$(BIN)/AngMom.o $(BIN)/basics_append.o $(BIN)/basics.o $(BIN)/inout.o    \
        $(BIN)/Pulse.o $(BIN)/Reduce_lapack.o $(BIN)/EigSlv.o $(BIN)/FEM_basis.o \
	$(BIN)/orbital.o $(BIN)/analytic_integrator.o  $(BIN)/gq_integrator.o    \
        $(BIN)/Grid.o $(BIN)/General.o $(BIN)/Read.o $(BIN)/Write.o

M_TDCIS=$(BIN)/EOM.o  $(BIN)/ReadMatrices.o $(BIN)/TDCIS.o $(BIN)/DiagCIS.o     \
        $(BIN)/PropMethods.o $(BIN)/TDCIS_program.o

M_MATRIX=$(BIN)/CoulombMatrix.o  $(BIN)/PHMatrices.o      \
         $(BIN)/FEM_Hamiltonian.o $(BIN)/FEM-DVR_Hamil.o $(BIN)/Hamiltonian.o      \
		 $(BIN)/Matrix_program.o

M_HF=$(BIN)/FEM_Hamiltonian.o $(BIN)/FEM-DVR_Hamil.o $(BIN)/Hamiltonian.o  \
     $(BIN)/CoulombMatrix.o \
     $(BIN)/HF.o $(BIN)/HF_program.o

M_DENSITY=$(BIN)/ReadMatrices.o $(BIN)/Density.o $(BIN)/Density_program.o

M_SVN=$(BIN)/svn.o

# compiler
# --------
NVCC=nvcc # CUDA compiler

#default: 
#---------
COMP=ifort
CPU=unknown  # =intel: enable SSE3 features
MODE=openmp
ARPACK=-larpack  #NOTE: comment out this line if ARAPCK library shouldn't be used
DEBUG=no

# libraries
MKL= -mkl=sequential
ifeq ($(MODE),gpu)
LIBx=  -lcublas -lcudart
FLAGx2= -DCUBLAS
M_GPU=$(BIN)/fortran_thunking.o

else ifeq ($(MODE),openmp)

# openflag changes for ifort 16 and newer
ifeq ($(shell if test `ifort -v 2>&1 | cut -d ' ' -f 3 | cut -c -2` -gt 15 ; then echo 1 ; else echo 0 ; fi),1)
FLAGx2= -qopenmp -DOPENMP
else
FLAGx2= -openmp -DOPENMP
endif
MKL= -mkl=parallel
endif

ifneq ($(ARPACK),)
  FLAG_ARPACK= -DARPACK
endif

SYS=_lp64
LIB= -L$(HOME)/lib $(LIBx) $(ARPACK) -lmkl_blas95${SYS} -lmkl_lapack95${SYS} $(MKL)


FLAGx=-xSSE2 -axSSE2   # -align array64byte 
ifeq ($(CPU),intel)  # SSE3 is not supported with AMD CPUs
  FLAGx=-xSSE3 -axSSE3 # -align array64byte
endif

# debugging
ifeq ($(DEBUG),yes)
  # for command-line debugging
  # -> don't check for uninit variable as intent(in) and using the variable creates errors
  FLAGy=-g -O0  -WB -traceback -p -debug all  -qopt-report=4  -check all -init=snan -check nouninit  #-check bounds 
else ifeq ($(DEBUG),intel)
  # for intel inspector 
  FLAGy=-g -O0  -check none  
else
  # normal mode
  FLAGy=$(FLAGx)  -diag-disable warn -O2 -fp-model precise  
endif

FLAGx3= -fpp  $(FLAG_ARPACK)  -module ${BIN}
FLAGS=$(FLAGy) $(FLAGx2) $(FLAGx3)
# -g -check all # or -ipo (for multiple file optimization) 
# -p or -pg : profiling with gprof

ifeq ($(COMP),gfortran)
  #assume gfortran version 4.4 or higher
  #-------------------------------------
  LIB= $(LIBx) -lblas -llapack $(ARPACK)
  FLAGS= -J$(BIN) -O3
endif

# --- main programs/options ----

all : init standard 

standard :  hf.x matrix.x tdcis.x density.x

hf : init hf.x 

tdcis : init tdcis.x 

matrix : init matrix.x 

density : init density.x 

noarpack : 
	@make ARPACK=

# ------------------------------------
# --- compile-source-code routines ---
# ------------------------------------

# --- programs ---
# program moduls must be recompiled when svn module changed
# otherwise svn number doesn't get updated in the program
$(BIN)/%_program.o: $(SRC)/%_program.f90 $(M_SVN)
	@echo "  Compiling "$<
	@$(COMP) $(FLAGS) -c $< -o $@ $(LIB)

# --- GPU CUDA Bindungs ---
$(BIN)/fortran_thunking.o: gpu/fortran_thunking.c 
	@echo "  Compiling "$<
	@cd gpu; ls
	@$(NVCC) -c $< -o $@
	@cd ..

# --- modules ---
$(BIN)/%.o: $(SRC)/%.f90
	@echo "  Compiling "$<
	@$(COMP) $(FLAGS) -c $< -o $@ $(LIB)


# --- hf ---
hf.x: $(M_VAR) $(M_BASIC) $(M_SVN) $(M_HF)
	@echo ""
	@echo " Assemble HF program"
	@echo " -------------------"
	@echo ""
	@$(COMP) $(FLAGS)  $^ -o $(BIN)/$@  $(LIB)
	@ln -sf $(BIN)/$@ .

# --- matrix ---
matrix.x: $(M_VAR) $(M_BASIC) $(M_SVN) $(M_MATRIX)
	@echo ""
	@echo " Assemble Matrix program"
	@echo " -----------------------"
	@echo ""
	@$(COMP) $(FLAGS)   $^ -o $(BIN)/$@  $(LIB)
	@ln -sf $(BIN)/$@ .

# --- tdcis ---
tdcis.x: $(M_VAR) $(M_BASIC) $(M_SVN) $(M_TDCIS) $(M_GPU)
	@echo ""
	@echo " Assemble Propagation (TDCIS) program"
	@echo " ------------------------------------"
	@echo ""
	@$(COMP) $(FLAGS)   $^ -o $(BIN)/$@  $(LIB)
	@ln -sf $(BIN)/$@ .

# --- density ---
density.x : $(M_VAR) $(M_BASIC) $(M_SVN) $(M_DENSITY) 
	@echo ""
	@echo " Assemble Density program"
	@echo " ------------------------"
	@echo ""
	@$(COMP) $(FLAGS)  $^ -o $(BIN)/$@  $(LIB)
	@ln -sf $(BIN)/$@ .


# ------------------------------------
#   ----- auxiliary routines -----
# ------------------------------------


clean: uninstall remove

rm : remove

remove:
	rm -f bin/*.o bin/*.mod bin/*.x 
	rm -f *.o *.mod *.x


install :
	@list=$$(ls bin/*.x 2> /dev/null); \
	if [ $${#list} -ne 0 ] ; then       \
	  for i in $$list ; do ln -sf $$(pwd)/$$i $$HOME/bin/ ; \
	  done;                            \
	fi

uninstall: 
	@cd bin; list=$$(ls *.x 2> /dev/null); \
        if [ $${#list} != 0 ] ; then \
          for i in $$list ; do rm -f $$HOME/bin/$$i ; done; \
        fi 


init: binfolder versioncheck

binfolder : 
	@mkdir -p $(BIN)

versioncheck : 
	@# create svn.f90 file with global version number
	-@( ver="m_projectrev='$$(git rev-parse --short HEAD)'";             \
	echo "module svn" > svn.f90 ;                          \
	echo "  implicit none" >> svn.f90;                     \
	echo "  character(100),parameter:: $$ver" >> svn.f90 ; \
	echo "end module svn" >> svn.f90 )

	@# compare with old svn.f90 file when it exists
	@(if [ -e $(SRC)/svn.f90 ] ; then                 \
	   newversion="$$(diff -q svn.f90 $(SRC)/svn.f90 )" ; \
	   if [ $${#newversion} -ne 0 ] ; then       \
	     echo " update module containing svn-revision number"; \
	     mv svn.f90 $(SRC)/ ;                \
	   else                                  \
	     #echo " svn module is up-to-date";   \
	     rm svn.f90 ;                        \
	   fi ;                                  \
	  else                                   \
	    echo " create module to access project revision number"; \
	    mv svn.f90 $(SRC)/ ;                 \
	  fi                                     \
	)

.PHONY : init install uninstall binfolder versioncheck rm remove standard default
