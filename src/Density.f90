module Density
  !
  !**PURPOSE
  !  -------
  !  Contains subroutines calculating spatial densities
  !
  !**DESCRIPTION
  !  -----------
  !  This module contians the following (sbr) and functions(fct):
  !
  !  -> (sbr) store_density_loc :
  !                          calculate spatial density of
  !                          excited electron/ionic hole and store it in a file
  !  -> (sbr) store_reduced_density_noloc :
  !                          calculate nonlocal spatial reduced density of
  !                          excited electron/ionic hole and store it in a file
  !  -> (sbr) wfct_n2r :     transforms the wavefunction from a function of the 
  !                          radial index n to a function of the radius r 
  !                                  wfct(n) -> wfct(r)
  !  -> (sbr) wfct_l2theta: transforms the wavefunction from a function of the 
  !                         angular momentum l,m to a function of the angles 
  !                         theta,phi
  !                                    wfct(l,m) -> wfct(theta,phi)
  !  -> (sbr) elec_spectrum:
  !                         calculate photoelectron spectrum using Volkov states
  !                         for electrons that are far a away from the ion
  !                          -> angle and energy resoved
  !
  !**AUTHOR
  !  ------
  !  (c) and written by Stefan Pabst
  !
  !**VERSION
  !  -------
  !  svn info: $Id: Density.f90 2176 2015-12-16 04:13:40Z spabst $
  !----------------------------------------------------------------------
  !
  use constants, dp=>dblprec
  use VarSystem, only : m_program
  implicit none

contains

  subroutine store_density_loc( a_file , a_type , a_wfct , a_orbital , a_angular )
    !
    !**PURPOSE
    !  Calculate local 3D density of excited electron/ionic hole
    !**DESCRIPTION
    !  First the radial quantum number is converted into the real space radius r.
    !  For each 3D angle (theta,phi) the radial wavefunction is determined
    !  and stored in a block.
    !
    !**INPUT/OUTPUT
    !    a_file  : CHARACTER - IN
    !              Name root of the file containing the spatial wavefunction.
    !              File ending carries the info "ion" for ionic hole or "elec" for
    !              full excited electron wavefunction.
    !              For subchannels (only a_type="e") the ending contains the channel info.
    !    a_type  : CHARACTER - IN
    !              = "p" - 1-particle density is calculated (= e - i + interf. w/ HF GS)
    !              = "e" - excited electron wavefunction is calculated
    !              = "i" - ionic hole wavefunction is calculated
    !    a_wfct  : T_WFCT - IN
    !              Object containing the CI coefficients
    !    a_orbital : T_ORBITAL - IN
    !              Object containing the radial orbitals
    !    a_spherical : T_SPHERICAL - IN
    !              Object containing spherical harmonics informations
    !
    !
    use grid_type
    use VarSystem, only : m_program,m_nmax,m_lmax,m_rskip,m_SOcoupl &
                         ,m_nelec,m_SOcoupl
    use VarWfct, only   : t_wfct,t_orbital                          &
                         ,m_i1p1hclass,m_ioccact                    &
                         ,m_phr=>m_id1p1hclassrev
    use VarMatrix, only : m_olap
    use SphericalHarmonics, only : t_spherical,y_lm=>get_spherical_harmonics_obj
    use general, only  : printvariables, printinfo
    use orbital, only  : get_orbital_radial
    use inout, only    : opendir,find_openpipe
    implicit none

    character(*), intent(in) :: a_file
    character, intent(in) :: a_type
    type(t_wfct), intent(in) :: a_wfct
    type(t_orbital), intent(in) :: a_orbital(0:,0:)
    type(t_spherical), intent(in) :: a_angular


    type(t_grid), pointer :: grid =>null()
    ! coef - store n -> r converted coefficient
    ! dens - store radial density for each channel (for a given 3D angle)
    complex(dp),allocatable :: coeff(:,:,:)
    complex(dp),allocatable :: idm(:,:),horb(:,:)
    real(dp),allocatable :: dens(:,:)

    real(dp), pointer :: r => null()
    integer :: i,j,nf,ihole,jhole
    integer :: npoints
    integer :: n,l,m,ims,iphi,itheta
    integer :: fid
    complex(dp) :: caux,raux

    character(clen) :: fend

    ! check type
    ! ----------
    select case(a_type)
    case("i")
      fend="ion"
    case("e")
      fend="elec"
    case("p")
      fend="1p"
    case default
      stop " ERROR(store_dens): only ionic ('i'), excited electronic ('e'), or 1-particle ('p') wavefunction"
    end select

    if ( size(a_orbital,1)<m_nmax+1  .or.  size(a_orbital,2)<m_lmax+1 )  then
      stop " ERROR(store_dens): size of orbital object is too small"
    end if

    if ( size(a_wfct%alpha_ai,1)<m_nmax+1  .or.  &
         size(a_wfct%alpha_ai,2)<m_lmax+1  .or.  &
         size(a_wfct%alpha_ai,3)<m_i1p1hclass    &
       )  then
      stop " ERROR(store_dens): size of coefficient object is too small"
    end if


    if ( .not.associated(a_orbital(0,0)%grid) )  &
       stop " ERROR(store_dens): grid not assigned"
    grid => a_orbital(0,0)%grid
    npoints = grid%npoints


    ! allocate fields
    select case(a_type)
    case("e","p")
      allocate( coeff(npoints,0:m_lmax,m_i1p1hclass) )
      ! convert radial index n in radial coordiante
      call wfct_n2r(coeff,a_wfct%alpha_ai,a_orbital)
    end select
    select case(a_type)
    case("p","i")
      allocate( idm(m_ioccact,m_ioccact) )
      idm = a_wfct%idm
      allocate( horb(npoints,m_i1p1hclass) )
      horb = ZERO

      ! add together uncorrected idm (from wfct) and 
      ! idm correction (from a_wfct%idm)
      do i=1,m_i1p1hclass
        ihole = m_phr(i)%idhole
        do j=1,m_i1p1hclass
          jhole = m_phr(j)%idhole
          
          ! m^L_a = m^L_b <-> m^L_i = m^L_j (with/without SO coupling)
          if (m_phr(i)%ml/=m_phr(j)%ml) cycle
          ! m^S_a = m^S_b (with SO coupling)
          if (m_phr(i)%ms/=m_phr(j)%ms) cycle

          !idm(ihole,jhole) = idm(ihole,jhole) + sum( coeff(:,:,i)*conjg(coeff(:,:,j)) )
          do l=0,m_lmax
            idm(ihole,jhole) = idm(ihole,jhole)   &
              + dot_product( a_wfct%alpha_ai(:,l,j) , matmul(m_olap(:,:,l),a_wfct%alpha_ai(:,l,i))  )
          end do
        end do
      end do 
    end select

    ! allocate density array <- store radial wfct w.r.t. in spatial representation
    select case(a_type)
    case("e")
      allocate( dens(npoints,m_ioccact) )
      dens  = ZERO
    case("i","p")
      allocate( dens(npoints,1) )
      dens = ZERO
    end select

    ! open file and write header info
    ! -------------------------------
    nf = m_ioccact
    if ( a_type == "i" .or. a_type=="p" .or. m_ioccact==1 ) nf = 0

    ! header information
    fid = find_openpipe()
    call opendir(fID,file=trim(a_file)//"_"//trim(fend))
    call printinfo(m_program,fID,'#')
    call printvariables(1,fID,'#','#ORB')
    write(fID,'("#",/,"##T time [a.u.] = ",ES19.10E3)') a_wfct%time
    select case (a_type)
    case("p")
      write(fID,'(a,/,a)') "#","#column:  r  |  theta  |  phi  |  rho"
    case("i")
      write(fID,'(a,/,a)') "#","#column:  r  |  theta  |  phi  |  rho_ion"
    case("e")
      write(fID,'(a,/,a)') "#","#column:  r  |  theta  |  phi  |  rho_elec&
                           &  |  rho_i , i - ionic channel (see above for &
                           &the channel order)"
    end select


    ! determine and write wfct
    ! ------------------------
    ! loop through angles
    do iphi=1,a_angular%nphi
      do itheta=1,a_angular%ntheta

        ! get radial density for given set of angles
        dens = zero


        ! excited electron
        ! ----------------
        if_elec:if ( a_type=="e" .or. a_type=="p" ) then
          lp_channel:do i=1,m_i1p1hclass
            m = m_phr(i)%ml
            ! needed for spin-orbit couping in occupied orbitals
            ihole = m_phr(i)%idhole
            if ( a_type=="p" ) ihole=1  ! <- add all up
            
            do l=abs(m),m_lmax  ! L >= m_l
              dens(:,ihole) = dens(:,ihole)                           &
                            + abs(y_lm(a_angular,l,m,itheta,iphi))**2 &
                              * abs(coeff(:,l,i))**2
            end do
          end do lp_channel
        end if if_elec


        ! ionic hole
        ! ------------
        if_ion:if (a_type=="i" .or. a_type=="p") then

          ! setup hole orbitals (horb)
          horb = ZERO
          do i=1,m_i1p1hclass
            m = m_phr(i)%ml
            l = m_phr(i)%l
            n = m_phr(i)%n

            horb(:,i) = get_orbital_radial(a_orbital(n,l))   &
                      * y_lm(a_angular,l,m,itheta,iphi)
            
            if ( m_SOcoupl ) then
              ims = int( (ONE+m_phr(i)%ms)/TWO )
              horb(:,i) = m_phr(i)%cleb(ims) * horb(:,i)
            end if
          end do


          ! build hole density
          do i=1,m_i1p1hclass
            ihole = m_phr(i)%idhole
            do j=1,m_i1p1hclass
              jhole = m_phr(j)%idhole

              caux = idm(ihole,jhole)
              if ( m_phr(i)%ms/=m_phr(j)%ms ) cycle
              if ( caux==ZERO  ) cycle
              
              ! substracte hole density from 1-particle density
              if ( a_type=="p" )   then
                caux = -caux  
              end if
              
              dens(:,1) = dens(:,1) + dble( caux*horb(:,j)*conjg(horb(:,i)) )
            end do


            ! add active occupied orbitals
            if ( a_type=="p" ) then
              m = m_phr(i)%ml
              
              if ( mod(m_nelec,2) == 1 ) then
                raux = ONE
              elseif (m_SOcoupl .or. m==0 ) then  ! SOC or ml==0 (2 elec. per spatial orbital)
                raux = TWO
              elseif (m/=0) then  ! no SOC and ml>0 (4 elec. per spatial orbital)
                raux = FOUR
              end if

              dens(:,1) = dens(:,1) + raux*abs(horb(:,i))**2
            end if
          end do

        end if if_ion

    
        ! interference between 1p1h config. and HGF ground state
        if_phgs:if ( a_type=="p" ) then 
          do i=1,m_i1p1hclass
            n = m_phr(i)%n
            m = m_phr(i)%ml
            j = m_phr(i)%l
            
            if ( mod(m_nelec,2) == 1 ) then
              raux = ONE
            elseif (m_SOcoupl .or. m==0 ) then  ! SOC or ml==0 (2 elec. per spatial orbital)
              raux = sqrt(TWO)
            elseif (m/=0) then  ! no SOC and ml>0 (4 elec. per spatial orbital)
              raux = TWO
            end if
            

            do l=abs(m),m_lmax  ! L >= m_l
              dens(:,1) = dens(:,1) + TWO*raux*dble( horb(:,i)                &
                             * conjg(a_wfct%alpha_0)                          &
                             * coeff(:,l,i) * y_lm(a_angular,l,m,itheta,iphi) &
                          )
            end do
          end do
        end if if_phgs


        ! store local density in file
        ! ---------------------------
        do n=1,npoints,m_rskip
          r => grid%r(n)
          write(fId,'(99ES14.5)')                                &
             r , a_angular%theta(itheta) , a_angular%phi(iphi)   &
             , sum(dens(n,:))/r**2, ( dens(n,i)/r**2 , i=1,nf )
        end do

        write(fId,*)
      end do
      write(fId,*)
    end do

    ! close files
    close(fId)

  end subroutine store_density_loc



  subroutine store_reduced_density_nonloc( a_file , a_type , a_mode          &
                                         , a_wfct , a_orbital , a_angular )
    !
    !**PURPOSE
    !  Calculate non-local 1D reduced density of excited electron/ion
    !
    !**DESCREIPTION
    !  Calculate the spatial wavefunction and then perform necessary reductions.
    !  First block in file is the local (diagonal) parts. Second block contains
    !  non-local information.
    !
    !**INPUT/OUTPUT
    !
    !    a_file  : CHARACTER - IN
    !              Name root of the file containing the spatial wavefunction.
    !              File ending carries the info "ion" for ionic hole or "elec" for
    !              full excited electron wavefunction.
    !              For subchannels (only a_type="e") the ending contains the channel info.
    !    a_type  : CHARACTER - IN
    !              = "e" - excited electron wavefunction is calculated
    !              = "i" - ionic hole wavefunction is calculated (not implented yet)
    !    a_mode  : CHARACTER - IN
    !              = "r" - non-local radial density (perform angular integration)
    !              = "a" - non-local angular(theta) density (perform radial integration)
    !    a_wfct  : T_WFCT - IN
    !              Object containing the CI coefficients
    !    a_orbital : T_ORBITAL - IN
    !              Object containing the radial orbitals
    !    a_spherical : T_SPHERICAL - IN
    !              Object containing spherical harmonics informations
    !
    use Grid_type
    use inout, only     : find_openpipe,opendir
    use varSystem, only : m_lmax
    use VarWfct, only   : t_wfct,t_orbital,m_i1p1hclass
    use SphericalHarmonics, only : t_spherical
    use general, only   : printvariables,printinfo
    implicit none

    character(*), intent(in) :: a_file
    character, intent(in) :: a_type,a_mode
    type(t_wfct), intent(in) :: a_wfct
    type(t_orbital), intent(in) :: a_orbital(0:,0:)
    type(t_spherical), intent(in),target :: a_angular

    type(t_grid), pointer :: grid => null() 
    real(dp), pointer :: theta(:) => null()
    integer :: nrpoints , ntpoints
    integer :: i,l,ir1,ir2,it1,it2
    integer :: fid
    complex(dp),allocatable :: coeff(:,:,:),dens(:,:,:)
    character(clen) :: fend


    ! check type
    select case(a_type)
    case("e")
      fend="elec"
    case("i")
      fend="ion"
      write(STDERR,'(a)') " ionic reduced non-local density is not implemented yet"
      stop
    case default
      stop " ERROR(RDM-nonlocal): invalid type"
    end select


    ! header information
    fid = find_openpipe()
    call opendir(fID,file=trim(a_file)//"_"//trim(fend))
    call printinfo(m_program,fID,'#')
    call printvariables(1,fID,'#','#ORB')
    write(fID,'("#",/,"##T time [a.u.] = ",ES19.10E3)') a_wfct%time

    select case (a_type)
    case("i")
      write(fID,'(a,/,a)') "#","# non-local ionic reduced density"
    case("e")
      write(fID,'(a,/,a)') "#","# non-local electron reduced density"
    end select

    select case (a_mode)
    case("r")
      write(fID,'(a,/,a)') "# -> radial reduced density"   &
                          ,"#column:  r  |  r'  |  rho(r,r')"
    case("a")
      write(fID,'(a,/,a)') "# -> angular reduced density (also integrated &
                                 over the angle phi)"                     &
                          ,"#column:  theta  |  theta'  |  rho(theta,theta')"
    case default
      write(STDERR,'(a)') " ERROR(RDM-nonlocal): invalid mode"
      stop
    end select


    if ( .not.associated(a_orbital(0,0)%grid) )   &
       stop " ERROR(store_dens): grid not assigned"
    grid => a_orbital(0,0)%grid
    nrpoints = grid%npoints
    
    theta => a_angular%theta
    ntpoints = a_angular%ntheta


    ! wfct fields
    allocate( coeff(nrpoints,0:m_lmax,m_i1p1hclass) )    
    ! convert n in r index
    call wfct_n2r(coeff,a_wfct%alpha_ai,a_orbital)

    
    ! ------------------------------
    ! ------------------------------
    select case(a_mode)
    case("r")
      allocate( dens(nrpoints,nrpoints,1) )
      dens = ZERO

      ! build reduced density matrix
      do i=1,m_i1p1hclass
        do l=0,m_lmax
          do ir1=1,nrpoints
            dens(ir1,:,1) = dens(ir1,:,1) + coeff(ir1,l,i)*conjg(coeff(:,l,i))
          end do
        end do
      end do

      ! print matrix
      ! ------------
      ! local part
      do ir1=1,nrpoints
        write(fid,'(99ES14.4)')  grid%r(ir1),dens(ir1,ir1,1)/grid%r(ir1)**2
      end do
      write(fId,'(2/)')
      
      ! non-local part
      do ir1=1,nrpoints
      do ir2=1,nrpoints
        write(fid,'(99ES14.4)')  grid%r(ir1),grid%r(ir2),dens(ir1,ir2,1)/grid%r(ir1)/grid%r(ir2)
      end do
      write(fId,*)
      end do 
    
    
    ! ------------------------------
    case("a")
    ! ------------------------------
    
      ! convert l,m into theta,phi=0
      call move_alloc(coeff,dens)
      allocate( coeff(nrpoints,ntpoints,m_i1p1hclass) )    
      call wfct_l2theta(coeff,dens,a_angular)

      deallocate(dens)
      allocate( dens(ntpoints,ntpoints,1) )
      dens = ZERO

      ! build reduced density matrix
      do i=1,m_i1p1hclass
        do it1=1,ntpoints
        do it2=1,ntpoints
          dens(it1,it2,1) = dens(it1,it2,1)     &
                          + dot_product(coeff(:,it2,i),coeff(:,it1,i))
        end do
        end do
      end do
      
      ! print matrix
      ! ------------
      ! local part
      do it1=1,ntpoints
        write(fid,'(99ES14.4)')  theta(it1),dens(it1,it1,1)
      end do
      write(fId,'(2/)')
      
      ! non-local part
      do it1=1,ntpoints
      do it2=1,ntpoints
        write(fid,'(99ES14.4)')  theta(it1),theta(it2),dens(it1,it2,1)
      end do
      write(fId,*)
      end do 
    end select

  end subroutine store_reduced_density_nonloc


  ! -----------------------------------------------------
  ! -----------------------------------------------------
  ! -----------------------------------------------------
  ! -----------------------------------------------------      


  subroutine wfct_n2r( a_wfct , a_coeff , a_orbital )
    !
    !  Convert index n into radial coordinate r
    !
    use VarSystem, only : m_lmax,m_nmax
    use varWfct,   only : t_orbital
    use orbital,   only : get_orbital_radial
    implicit none
    complex(dp), intent(out) :: a_wfct(:,:,:)
    complex(dp), intent(in)  :: a_coeff(:,:,:)    
    type(t_orbital), intent(in) :: a_orbital(0:,0:)

    integer :: i,l,n,nr
    complex,allocatable :: alpha(:,:,:)


    ! check sizes
    ! ------------
    ! r grid points
    if ( .not.associated(a_orbital(0,0)%grid) )  &
        stop " ERROR(wfct_n2r): no grid information"

    nr = size(a_orbital(0,0)%grid%r)
    if ( nr < 1 ) stop " ERROR(wfct_n2r): grid size < 1"

    ! radial quantum number
    if ( size(a_wfct,1)/=nr .or. size(a_coeff,1)-1/=m_nmax )           &
        stop " ERROR(wfct_n2r): size inconsistency in r,n indicies"

    ! angular quantum number
    if ( size(a_wfct,2)/=size(a_coeff,2) .or. size(a_wfct,2)-1<m_lmax  &
         .or. size(a_orbital,2)-1<m_lmax                               &
       ) &
        stop " ERROR(wfct_n2r): size inconsistency in l index"

    ! 1p1h-classes
    if ( size(a_wfct,3)<size(a_coeff,3) )     &
        stop " ERROR(wfct_n2r): size inconsistency in the hole index"

    
    a_wfct = zero

    ! convert
    do i=1,size(a_wfct,3)
      do l=0,m_lmax
        do n=0,m_nmax
          if ( .not.allocated(a_orbital(n,l)%wfct) ) then
            stop " ERROR(wfct_n2r): radial wfct. not assigned"
          end if
          
          a_wfct(:,l+1,i) = a_wfct(:,l+1,i)     &
                          + a_coeff(n+1,l+1,i) * get_orbital_radial(a_orbital(n,l))
        end do
      end do
    end do
  end subroutine wfct_n2r


  subroutine wfct_l2theta( a_wfct , a_coeff , a_angular )
    !
    !  Convert angular momentum index l into angular distribution theta
    !
    use VarSystem, only : m_lmax
    use VarWfct,   only : m_phr=>m_id1p1hclassrev
    use SphericalHarmonics, only : t_spherical,y_lm=>get_spherical_harmonics_obj
    implicit none
    complex(dp), intent(out) :: a_wfct(:,:,:)
    complex(dp), intent(in)  :: a_coeff(:,:,:)    
    type(t_spherical), intent(in) :: a_angular

    integer :: i,l,m,nr,it,nt
    complex,allocatable :: alpha(:,:,:)



    ! check sizes
    ! -----------
    nr = size(a_wfct,1)
    nt = a_angular%ntheta

    ! radial indicies
    if ( size(a_wfct,1)/=size(a_coeff,1) )    &
        stop " ERROR(wfct_l2theta): size inconsistency in r index"

    ! angular indicies
    if ( size(a_wfct,2)/=nt .or. size(a_coeff,2)<m_lmax+1 ) &
        stop " ERROR(wfct_l2theta): size inconsistency in l index"

    ! 1p1h-index
    if ( size(a_wfct,3)/=size(a_coeff,3) )     &
        stop " ERROR(wfct_l2theta): size inconsistency in the hole index"

 
    a_wfct = ZERO
    
    ! convert
    do i=1,size(a_wfct,3)
      m = m_phr(i)%ml
      do it=1,nt
        do l=0,m_lmax
          a_wfct(:,it,i) = a_wfct(:,it,i)       &
                         + a_coeff(:,l+1,i) * y_lm(a_angular,l,m,it,1)
        end do
      end do
    end do
  end subroutine wfct_l2theta


  ! -----------------------------------------------------
  ! -----------------------------------------------------
  ! -----------------------------------------------------
  ! -----------------------------------------------------    

  
  subroutine store_elec_spectrum(a_file,a_pes,a_energy,a_theta,a_phi,a_mode)
    !
    !  a_mode  : defines whether total, angular, energy, or angle-and-energy
    !            resolved photoelectron spectrum is written to a file
    !              = 1 : Angle and energy resolved spectrum
    !              = 2 : Energy resolved spectrum
    !              = 3 : Angle resolved spectrum
    !              = 4 : Fully integrated spectrum
    !
    use inout, only : find_openpipe
    use general, only : printinfo,printvariables
    implicit none

    real(dp) :: a_pes(:,:,:,:)
    real(dp),intent(in) :: a_energy(3),a_theta(3),a_phi(3)
    character, intent(in) :: a_mode
    character(clen), intent(in) :: a_file
    real(dp) :: denergy,dtheta,dphi
    integer :: fid
    integer :: ni,ne,nt,np
    integer :: ii,ie,it,ip
    real(dp) :: e,p,t


    fid = find_openpipe()
  
    ne = size(a_pes,1)
    nt = size(a_pes,2)
    np = size(a_pes,3)
    ni = size(a_pes,4)

    ! to avoid division by zero when only one gridpoint should be calculated
    denergy = (a_energy(2)-a_energy(1)) /(nint(a_energy(3))-1)
    dtheta  = (a_theta(2)-a_theta(1))  /(nint(a_theta(3))-1)
    dphi    = (a_phi(2)-a_phi(1))      /(nint(a_phi(3))-1)
    if (ne==1) denergy = ONE
    if (nt==1) dtheta = ONE
    if (np==1) dphi = ONE
    
    ! multiply sin(theta) for angular integration (volume element)
    if (a_mode == "2" .or. a_mode == "4" ) then  
      forall ( it=1:nt ) a_pes(:,it,:,:) = a_pes(:,it,:,:) *dphi*dtheta    &
                                         * sin(a_theta(1)+ (it-1)*dtheta)
    end if
    if (a_mode == "3".or. a_mode == "4" ) then
      a_pes = a_pes *denergy
    end if
                                       

    ! open file
    open(fid,file=a_file)
    ! write header informations
    write(fid,'(a)') "# photolectron spectrum (PES)"
    call printinfo(m_program,fID,'#')
    call printvariables(ao_fid=fID,ao_str='#')

    select case(a_mode)
    case("1")
      write(fid,'(a)') "# column: energy |  theta | phi | PES_total &
                      &| PES_i (i = 1p-1h class [incl. spin of photoelectron])"
    do ip=1,np
      do it=1,nt
        do ie=1,ne
          e = a_energy(1)   + (ie-1)*denergy
          t = a_theta(1) + (it-1)*dtheta
          p = a_phi(1)   + (ip-1)*dphi
          write(fid,'(99ES16.6)') e,t,p,sum(a_pes(ie,it,ip,:)),(a_pes(ie,it,ip,ii),ii=1,ni)
        end do
        write(fid,*)
      end do
    end do


    case("2") ! -------------------------------------------------------
    write(fid,'(a)') "# column: energy  |  PES_total  &
                      &| PES_i  (i = 1p-1h class [incl. spin of photoelectron])"
 
    do ie=1,ne
      e = a_energy(1)   + (ie-1)*denergy
      write(fid,'(99ES16.6)')   e,sum(a_pes(ie,:,:,:)),(sum(a_pes(ie,:,:,ii)),ii=1,ni)
    end do

    case("3") ! ------------------------------------------------------- 	
    write(fid,'(a)') "# column: theta  |  phi  |  PES_total  &
                      &| PES_i  (i = 1p-1h class [incl. spin of photoelectron])"
    do ip=1,np
      do it=1,nt
      t = a_theta(1) + (it-1)*dtheta
      p = a_phi(1)   + (ip-1)*dphi
      write(fid,'(99ES16.6)')   t,p,sum(a_pes(:,it,ip,:)) &
                             ,(sum(a_pes(:,it,ip,ii)),ii=1,ni)
      end do
      write(fid,*)
    end do
  
  
    case("4") ! -------------------------------------------------------
    write(fid,'(a)') "# column: PES_total  &
                  &| PES_i  (i = 1p-1h class [incl. spin of photoelectron])"
    write(fid,'(99ES16.6)')   sum(a_pes(:,:,:,:)),(sum(a_pes(:,:,:,ii)),ii=1,ni)
    end select
  
  ! close file
  close(fid)
  
  end subroutine store_elec_spectrum
 



  subroutine elec_spectrum_spl(a_pes,a_wfcts,a_orbital,a_afield,a_energy,a_theta,a_phi)
    use pulse_type
    use grid_type
    use varwfct, only   : t_wfct,t_orbital,m_i1p1hclass,m_id1p1hclassrev  &
                         ,m_moamax
    use varsystem, only : m_lmax,m_nmax,m_LMgauge,m_lgauge,m_vgauge
    use pulse, only  : getpulse
    use basics, only : sbessel, sbess_deriv1
    use orbital, only: get_orbital_radial
    use sphericalharmonics
    implicit none

    real(dp), intent(out),target :: a_pes(:,:,:,:)
    type(t_wfct), intent(in) :: a_wfcts(:)
    type(t_orbital), intent(in) :: a_orbital(0:,0:)
    type(t_pulse), intent(in) :: a_afield(:)
    real(dp), intent(in),dimension(3) :: a_theta,a_phi,a_energy

    integer :: ie,it,ip,ii,ir
    real(dp) :: p(3),A(3),radius_p,theta_p,phi_p
    real(dp) :: radius_pa,theta_pa,phi_pa
    real(dp) :: time
    integer :: l,m,nr,la,itime,n,i
    complex(dp) :: volkov_phase, ei
    real(dp)    :: tmax,dt

    complex(dp),allocatable :: ylm(:,:),radial(:,:)
    real(dp), allocatable :: bess(:,:)
    real(dp) :: fac, bessel

    type(t_grid), pointer :: grid => null()
    complex(dp),allocatable :: pes(:) ,partial(:,:)   


    grid => a_orbital(0,0)%grid
    nr =  grid%npoints  
    if ( nr < 1 )  stop "ERROR(elec_spectrum): number of grid points <1"

    if ( grid%type=="FEM" )  stop " ERROR(espectrum_split): FEM is not implemented"


    allocate( ylm(0:m_lmax,0:m_moamax) , partial(0:m_lmax,1:m_i1p1hclass) )
    ylm = ZERO
    partial = zero ! contains the contribution without ylm

    allocate( bess(nr,0:m_lmax) )
    bess = ZERO

    allocate( radial(0:m_nmax,0:m_lmax))
    radial = ZERO

    allocate(pes(m_i1p1hclass))
    pes = ZERO

    ! find max. time 
    ! -------------- 
    ! for time integration needed for calculating the 
    ! Volkov phase S(ti,p,A) = exp( -i \int_-ti^infty (p-A)^2/2  dt )
    ! For t>tmax the Volkov phase is the same for all parts of the wavefunction
    ! therefore it is only needed to integrate up to tmax.
    
    ! largest time wavefunction is projected onto Volkov states
    time = maxval(a_wfcts(:)%time) 
    tmax = time
    do i=1,size(a_afield)
      if ( a_afield(i)%rdfile) then
        tmax = max(tmax,maxval(a_afield(i)%time)+a_afield(i)%t0)
      else
        tmax = max(tmax,a_afield(i)%t0 + 5.0D0*a_afield(i)%duration)
      end if   
    end do

    ! loop over all possible 3D momenta
    lp_energy:do ie=1,nint(a_energy(3))
      
      ! avoid division by zero
      radius_p = a_energy(1)
      if (a_energy(3) /= 1)   radius_p = a_energy(1)  + (ie-1)*(a_energy(2)-a_energy(1)) &
                                                        / (nint(a_energy(3))-1)
      
      ! conversion of energy to momentum
      radius_p = sqrt(TWO*radius_p)
 
      ! for velocity form the momentum is p and the quantities 
      ! bess, radial are time-independent
      if(m_LMgauge==m_vgauge) then
        ! initialize all spherical bessel function j_l(pr) 
        ! - convert spherical bessel function into the Gauss-Lobatto coefficients
        !   see Eq. 43 and below in Greenman, PRA 82, 023406 (2010)
        !$omp parallel do private(ir,l) default(shared) 
        do ir=1,nr
          do l=0,m_lmax
            bess(ir,l) = sbessel( radius_p*grid%r(ir) , l ) 
          end do
        end do
        !$omp end parallel do
        
        if ( grid%type=="PSG" )  then
          forall(l=0:m_lmax)   bess(:,l) = bess(:,l) / grid%A2u * grid%r(:)
        elseif ( grid%type=="DVR" )  then
         ! DVR: A2u stores the radial volume element (use trapeziodal rule)
          forall(l=0:m_lmax)   bess(:,l) = bess(:,l) * grid%A2u * grid%r(:)
        end if


        ! perform radial integral  /int dr  r * u_[n,l](r) * j_l(pr)
        fac = ONE/grid%norm**2
        select case (grid%type) 
        case("PSG")
          !$omp parallel do private(n,l) default(shared)
          do n=0,m_nmax
            do l=0,m_lmax
              radial(n,l) = fac*sum( bess(:,l) * a_orbital(n,l)%wfct )
            end do
          end do
          !$omp end parallel do

        case("DVR")
          do n=0,m_nmax
            do l=0,m_lmax
              radial(n,l) = fac*sum(bess(:,l)*get_orbital_radial(a_orbital(n,l)))
            end do
          end do
        end select
       
      end if

      write(STDOUT,'(a,ES14.5)') " electron energy [a.u.] = "  &
             , a_energy(1) + (ie-1)*(a_energy(2)-a_energy(1)) / (nint(a_energy(3))-1)


    lp_theta:do it=1,nint(a_theta(3))
    lp_phi:do ip=1,nint(a_phi(3))
      
      pes = ZERO

      ! avoid division by zero
      if (a_theta(3) /= 1) then    
        theta_p  = a_theta(1)+ (it-1)*(a_theta(2)-a_theta(1))/(nint(a_theta(3))-1)
      else 
        theta_p  = a_theta(1)
      end if
      if (a_phi(3) /= 1) then
        phi_p    = a_phi(1)  + (ip-1)*(a_phi(2)-a_phi(1))    /(nint(a_phi(3))-1)
      else 
        phi_p    = a_phi(1)
      end if
 
      ! electron momentum in cartesian coordinates
      p(1) = radius_p * sin(theta_p) * cos(phi_p)
      p(2) = radius_p * sin(theta_p) * sin(phi_p)
      p(3) = radius_p * cos(theta_p)
 
      ! for velocity form the conserved momentum is p and the quantity
      ! ylm is time-independent
      if(m_LMgauge==m_vgauge) then        
        ! calculate Y_lm(Omega_p)
        forall(l=0:m_lmax, m=0:m_moamax) &
          ylm(l,m) = get_spherical_harmonics(l,m,theta_p,phi_p)        
      end if
 
 
      ! loop over all splitting times t_i
      lp_time:do itime=1,size(a_wfcts)
        
        time = a_wfcts(itime)%time
        ! get A(t_i)  (linearly polarized in z-direction)
        A = ZERO
        A(3) = getpulse(time,a_afield)
        

        ! calculate volkov phase
        if (itime==1) then
          volkov_phase = get_volkovphase(time,tmax,a_afield,p)
        else
          volkov_phase = volkov_phase - get_volkovphase(a_wfcts(itime-1)%time,time,a_afield,p)
        end if
 
        if_length:if (m_LMgauge==m_lgauge) then
          ! find p-A
          radius_pa = sqrt(sum( (p-A)**2 ))
          phi_pa    = phi_p
          theta_pa  = acos( (p(3)-A(3))/radius_pa  )
 
          ! calculate Y_lm(Omega_[p-A(t_i)])
          !$omp parallel do private(l,m) default(shared) 
          do l=0,m_lmax
            do m=0,m_moamax
              ylm(l,m) = get_spherical_harmonics(l,m,theta_pa,phi_pa)
            end do
          end do
          !$omp end parallel do
 
          ! initialize all spherical bessel function j_l(|p-A|r) 
          ! for a given |p-A(t_i)|
          ! - convert spherical bessel function into the Gauss-Lobatto coefficients
          !   see Eq. 43 and below in Greenman, PRA 82, 023406 (2010)          
          !$omp parallel do private(ir,l) default(shared) 
          do ir=1,nr
            do l=0,m_lmax
              bess(ir,l) = sbessel( radius_pa*grid%r(ir) , l )
            end do
          end do
          !$omp end parallel do

          if ( grid%type=="PSG" )  then
            ! PSG: represent bessel function in PSG basis
            forall(l=0:m_lmax)   bess(:,l) = bess(:,l) / grid%A2u * grid%r(:)
          elseif ( grid%type=="DVR" )  then
            ! DVR: A2u stores the radial volume element
            forall(l=0:m_lmax)   bess(:,l) = bess(:,l) * grid%A2u * grid%r(:)
          end if


          ! perform radial integral  /int dr  r * u_[n,l](r) * j_l(|p-A|r)
          fac = ONE/grid%norm**2
          select case (grid%type) 
          case("PSG")
            do n=0,m_nmax
              do l=0,m_lmax
                radial(n,l) = fac*sum( bess(:,l) * a_orbital(n,l)%wfct  )
              end do
            end do

          case("DVR")
            do n=0,m_nmax
              do l=0,m_lmax
                radial(n,l) = fac*sum(bess(:,l)*get_orbital_radial(a_orbital(n,l)))
              end do
            end do
          end select
        end if if_length

        ! add spectrum of the current time stamp
        do ii=1,m_i1p1hclass
          m  = m_id1p1hclassrev(ii)%ml
          ei = m_id1p1hclassrev(ii)%energy
          ! time evolution of the hole after the time of splitting (volkov phase of the hole)
          ei = exp(-IMG *ei*time)  

          do l=0,m_lmax
            pes(ii) = pes(ii) + (-IMG)**mod(l,4) * exp(-IMG*volkov_phase)*ei* ylm(l,m)  & 
                              * sum(radial(:,l)*a_wfcts(itime)%alpha_ai(:,l,ii))
          end do
        end do

      end do lp_time

      ! store electron spectrum for a given 3D-momentum
      a_pes(ie,it,ip,:) = TWO/PI * radius_p * abs(pes(:))**2
      pes = ZERO

    end do lp_phi
    end do lp_theta

    end do lp_energy

  end subroutine elec_spectrum_spl



  subroutine elec_spectrum_tsurf(a_pes,a_wfcts,a_afield,a_energy,a_theta,a_phi)
    use pulse_type
    use grid_type
    use varwfct, only : t_wfct,t_orbital,m_i1p1hclass,m_id1p1hclassrev  &
                       ,m_moamax
    use varsystem, only : m_lmax,m_tsurf_r,m_grid,m_LMgauge,m_lgauge, m_vgauge 
    use sphericalharmonics
    use AngMom
    use pulse, only : getpulse
    use basics, only : sbessel, sbess_deriv1
    implicit none

    real(dp), intent(out),target :: a_pes(:,:,:,:)
    type(t_wfct), intent(in) :: a_wfcts(:)
    type(t_pulse), intent(in) :: a_afield(:)
    real(dp), intent(in),dimension(3) :: a_theta,a_phi,a_energy

    integer :: ie,it,ip,ii,ir
    real(dp) :: p(3),A(3),radius_p,theta_p,phi_p
    real(dp) :: radius_pa,theta_pa,phi_pa
    real(dp) :: time
    integer :: l,la,m,nr,itime,i
    complex(dp) :: volkov_phase,ei,caux
    real(dp)    :: tmax,dt
    complex(dp),allocatable :: ylm(:,:),op(:,:),clc(:,:)
    real(dp), allocatable :: bess(:,:), time_av(:)
    real(dp) :: fac, bessel
    complex(dp),pointer :: pes(:)
 
    nr =  m_grid%npoints  
    if ( nr < 1 )  stop "ERROR(elec_spectrum): number of grid points <1"

    ! set radius of sphere for flux measurement, everything evaluated at ir
    ir = minloc(abs(m_grid%r-m_tsurf_r),dim=1)


    allocate(clc(1:m_lmax,0:m_lmax))
    clc = ZERO

    allocate( ylm(0:m_lmax,0:m_moamax) )
    ylm = ZERO

    ! bess contains the bessel function and the first derivative for every l
    allocate( bess(2,0:m_lmax) )
    bess = ZERO

    allocate( op(0:m_lmax,1:m_i1p1hclass) )
    op = ZERO

    allocate(pes(m_i1p1hclass))
    pes = ZERO

    ! find max. time 
    ! -------------- 
    ! for time integration needed for calculating the 
    ! Volkov phase S(ti,p,A) = exp( -i \int_-ti^infty (p-A)^2/2  dt )
    ! For t>tmax the Volkov phase is the same for all parts of the wavefunction
    ! therefore it is only needed to integrate up to tmax.
    
    ! largest time wavefunction is projected onto Volkov states
    time = maxval(a_wfcts(:)%time) 
    tmax = time
    do i=1,size(a_afield)
      if ( a_afield(i)%rdfile) then
        tmax = max(tmax,maxval(a_afield(i)%time)+a_afield(i)%t0)
      else
        tmax = max(tmax,a_afield(i)%t0 + 5.0D0*a_afield(i)%duration)
      end if   
    end do


    pes = ZERO
    
    dt = (a_wfcts(2)%time-a_wfcts(1)%time)

    ! loop over all possible 3D momenta
    do ie=1,nint(a_energy(3))
      write(STDOUT,'(a,ES14.5)') " electron energy [a.u.] = "  &
           , a_energy(1) + (ie-1)*(a_energy(2)-a_energy(1)) / (nint(a_energy(3))-1)
    do it=1,nint(a_theta(3))
    do ip=1,nint(a_phi(3))

    ! avoid division by zero
    if (a_energy(3) /= 1) then
      radius_p = a_energy(1)+(ie-1)*(a_energy(2)-a_energy(1))/(nint(a_energy(3))-1)
    else  
      radius_p = a_energy(1)
    end if
    radius_p = sqrt(TWO*radius_p)

    if (a_theta(3) /= 1) then    
      theta_p  = a_theta(1)+(it-1)*(a_theta(2)-a_theta(1))/(nint(a_theta(3))-1)
    else 
      theta_p  = a_theta(1)
    end if
    if (a_phi(3) /= 1) then
      phi_p    = a_phi(1) + (ip-1)*(a_phi(2)-a_phi(1)) /(nint(a_phi(3))-1)
    else 
      phi_p    = a_phi(1)
    end if

    ! electron momentum in cartesian coordinates
    p(1) = radius_p * sin(theta_p) * cos(phi_p)
    p(2) = radius_p * sin(theta_p) * sin(phi_p)
    p(3) = radius_p * cos(theta_p)

    ! in velocity gauge ylm, clebsch, bess are time-independent
    if (m_lmgauge == m_vgauge) then

      !omp parallel do private(la,l,m) shared(clebsch,clc1,clc)
      ! y_lm(k)
      ! -> l index refers always to the largest angular momentum max(l,la)
      ! -> m quantum number is always positive 
      do l=1,m_lmax
        do m=0,m_lmax
          clc(l,m) = sqrt((TWO*l+1)*(TWO*(l-1)+1))  * (-IMG)**l                        &
                   * cleb(2*l,2*m,2,0 , 2*(l-1),2*m) * cleb(2*l,0,2,0 , 2*(l-1),0)  &
                   * get_spherical_harmonics(l,m,theta_pa,phi_pa)
        end do
      end do
      !omp end parallel do

      ! for velocity form the momentum is p
      radius_pa = radius_p
      phi_pa    = phi_p
      theta_pa  = theta_p

      ! calculate Y_lm(Omega_[p-A(t_i)])
      forall(l=0:m_lmax,m=0:m_moamax)    &
        ylm(l,m) = get_spherical_harmonics(l,m,theta_pa,phi_pa)

      ! - calculate spherical Bessel functions and their first derivative  
      !   of order l for fixed radius r_c=r_absorb
      forall(l=0:m_lmax)   &
        bess(1,l) = sbessel( radius_pa * m_grid%r(ir) , l)

      forall(l=0:m_lmax)   &
        bess(2,l) = sbess_deriv1(radius_pa*m_grid%r(ir),l)     

    end if


    ! loop over all times t_i fori integration
    time_loop: do itime=1, size(a_wfcts)
      time = a_wfcts(itime)%time

      ! get A(t_i)  (linearly polarized in z-direction)
      A = ZERO
      A(3) = getpulse(time,a_afield)


      ! calculate volkov phase
      if (itime==1) then
        volkov_phase = get_volkovphase(time,tmax,a_afield,p)
      else
        volkov_phase = volkov_phase - get_volkovphase(a_wfcts(itime-1)%time,time,a_afield,p)
      end if


      if (m_LMgauge==m_lgauge) then
        ! find p-A
        radius_pa = sqrt(sum( (p-A)**2 ))
        phi_pa    = phi_p
        theta_pa  = acos( (p(3)-A(3))/radius_pa  )

        ! calculate Y_lm(Omega_[p-A(t_i)])
        forall(l=0:m_lmax,m=0:m_moamax)    &
          ylm(l,m) = get_spherical_harmonics(l,m,theta_pa,phi_pa)

        ! - calculate spherical Bessel functions and their first derivative  
        !   of order l for fixed radius r_c=r_absorb
        forall(l=0:m_lmax)   &
          bess(1,l) = sbessel( radius_pa * m_grid%r(ir) , l)

        forall(l=0:m_lmax)   &
          bess(2,l) = sbess_deriv1(radius_pa*m_grid%r(ir),l)     
      end if
        
      ! calculate the operator parts not involving the vector potential  
      forall(ii=1:m_i1p1hclass, l=0:m_lmax)                                           &
        op(l,ii) = (  (-bess(1,l)+ 0.5D0* m_grid%r(ir) *radius_pa* bess(2,l) )        &
                      *a_wfcts(itime)%alpha_ai(0,l,ii)                                &
                    - 0.5D0*m_grid%r(ir) * bess(1,l) *a_wfcts(itime)%alpha_ai(1,l,ii) &
                   )                                                                  &
                   * (-IMG)**l* ylm(l,m_id1p1hclassrev(ii)%ml)

      do ii=1,m_i1p1hclass
        m = m_id1p1hclassrev(ii)%ml
        ei = m_id1p1hclassrev(ii)%energy
        ei = exp( -IMG*ei*time)   ! remove phase evolution of the hole state
        
        do l=0,m_lmax
          
          pes(ii) = pes(ii) + exp(-IMG*volkov_phase)* op(l,ii)* dt *ei    

          if (m_LMgauge==m_vgauge) then
            ! dipole coupling from A*p (only exists in the velocity gauge)
            caux = ZERO
            do la=l-1,l+1,2
              if ( la<0 .or. la>m_lmax) cycle
              caux = caux + bess(1,la) * clc(max(la,l),m)
            end do
            pes(ii) = pes(ii) - exp(-IMG*volkov_phase) * dt*ei * a_wfcts(itime)%alpha_ai(0,l,ii)     &
                      * m_grid%r(ir) * A(3)/THREE * caux
          end if
        end do

      end do
    end do time_loop 

    a_pes(ie,it,ip,:) = TWO/PI *radius_p * abs(pes(:))**2
    pes = ZERO

    end do  
    end do
    end do
  end subroutine elec_spectrum_tsurf



  pure real(dp) function get_volkovphase(a_t,a_tmax,a_afield,a_p) result(volkov)
    !**INPUT
    !  -----
    !  a_t      : time wavefunction is projected on to Volkov states 
    !               -> initial time of integration
    !  a_tmax   : time wavefunction hits the detector 
    !               -> max. time of integration
    !  a_afield : vector potential (function of time)
    !  a_p      : canonical momentum (time independent)
    !               -> pulse is linearly polarized
    !                      (p-A)**2 = p_x**2 + p_y**2 + (p_z-A)**2
    !                  
    !**OUTPUT
    !  ------
    !  volkov
    !
    use Varsystem, only : m_LMgauge, m_vgauge
    use pulse_type
    use pulse, only : getpulse
    implicit none
    real(dp), intent(in) :: a_t,a_tmax,a_p(3)
    type(t_pulse), intent(in) :: a_afield(:)
  
    real(dp) :: dt
    real(dp),allocatable :: time(:),afield(:)
    integer :: i,nt
   

    volkov = ZERO
    
    ! volkov phase is zero, integration boundaries are equal
    if ( a_t >= a_tmax) return 
  
    ! find time steps for integration
    dt = 100.
    do i=1,size(a_afield)
      if ( a_afield(i)%rdfile ) then
        dt = min( dt , (a_afield(i)%time(2)-a_afield(i)%time(1))/TWO )
      else
        dt = min( dt , a_afield(i)%duration/40.D0 , 2*pi/a_afield(i)%frequency/40.d0)
      end if
    end do
    
    ! make a_tmax-a_t a multiple of dt
    nt = ceiling( (a_tmax-a_t)/dt )  
    dt = (a_tmax-a_t)/nt
    allocate(time(1:nt))
    forall(i=1:nt) time(i) = a_t + (i-1)*dt 
    
    allocate(afield(1:nt))
    forall(i=1:nt) afield(i) = getpulse(time(i),a_afield)
  

    if ( m_LMgauge==m_vgauge) then
      ! don't add the A**2  because it doesn't appear in the TDCIS equations 
      ! for velcoity gauge (it is already transformed away)
      volkov = sum( sum(a_p**2)      - 2*a_p(3)*afield )/TWO *dt
    else
      volkov = sum( sum(a_p(1:2)**2) + (a_p(3)-afield)**2 )/TWO *dt
    end if
  end function get_volkovphase







  subroutine Hlike_spec_pwvs(a_wfcts)
    ! 
    ! calculate and store partial-wave and energy distribution(z-direction) of wfcts in a_wfcts
    ! -> assume H-like system (no residual ulomb interaction, H_1=0, and only one occupied orbital)
    !
    use varsystem, only : m_lmax,m_nmax,m_fwfct
    use varwfct, only : t_wfct,m_angular,m_orbital
    use inout
    use sphericalharmonics, only : get_spherical_harmonics
    implicit none
    type(t_wfct), intent(in) :: a_wfcts(:)

    character(clen) :: ffile
    integer :: l,i,n,fid

    real(dp) :: espec(0:m_nmax,0:m_lmax) , pwvs(0:m_lmax)
    real(dp) :: aux

    fid = find_openpipe()

    ! for each wfct find partial-wave distribution and energy spectrum (assume H_1=0)
    do i=1,size(a_wfcts)
      write(ffile,*) i
      ffile=trim(m_fwfct)//'.ana.'//trim(adjustl(ffile))

      espec(:,:) = abs(a_wfcts(i)%alpha_ai(:,:,1))**2

      forall(l=0:m_lmax)
        pwvs(l) = sum(espec(:,l))
      end forall
        
      ! write  
      open(fid,file=ffile)
        ! partial waves
        do l=0,m_lmax
          write(fid,'(i4,ES15.5)') l,pwvs(l)
        end do
        
        ! new block
        write(fid,'(2/)')
        
        ! write sepctrum
        do l=0,m_lmax
          aux = get_spherical_harmonics(l,0,ZERO,ZERO)
          
          do n=0,m_nmax
            write(fid,*)  dble(m_orbital(n,l)%energy) , espec(n,l)*aux**2
          end do
        end do

      close(fid)
    end do

  end subroutine Hlike_spec_pwvs


end module Density
