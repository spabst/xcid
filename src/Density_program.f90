program density_program
  ! PURPOSE
  ! -------
  ! The program density can derive various density functions.
  !
  ! DESCRIPTION
  ! -----------
  ! After the input and initialization of all necessary parameters,
  ! density functions (like photoelectron density,2-time densities)
  ! are calculated depending on the specified mode defined by the
  ! input flag. The results are written in a file. The file name is
  ! internally defined. 
  ! 
  ! USAGE
  ! -----
  ! density.x [input.file] [mode]
  !
  ! input.file : Input file is a formatted fortran input.
  !              This input file can be also used to run all other 
  !              programs included in this program package. 
  !
  ! mode = 1  : local 1-particle density matrix
  !              -> 1.1 : of excited electron
  !              -> 1.2 : of ionic hole
  !      = 2  : non-local 1-particle density matrix of excited electron
  !             hole index is kept and the particle index is 
  !             converted to spatial coordinates 
  !              -> 2.1 : reduced radial density matrix
  !              -> 2.2 : redcued angular density matrix
  !      = 3  : calculate photoelectron spectrum
  !              -> 3.1 : angle and energy resolved
  !              -> 3.2 : only energy resolved
  !              -> 3.3 : only angular resolved
  !              -> 3.4 : totally integrated spectrum
  !
  ! Version info: $Id: Density_program.f90 2127 2015-11-11 17:02:15Z spabst $
  !
  use constants , dp => dblprec
  use basics, only : timeinfo
  use VarSystem , only : g_program=>m_program,m_svnrev
  use svn
  implicit none
  character(clen) :: g_mode
  integer :: g_tp(8,2)=0

  call date_and_time(values=g_tp(:,1))



  ! Density/Correlation program is running
  g_program = 3

  ! pass on-the-fly determined global svn version
  ! to module "general"
  m_svnrev = m_projectrev


  ! preparation
  ! -----------
  call prepare

  call selection(g_mode)

  ! final time
  ! -----------
  call date_and_time(values=g_tp(:,2))
  call timeinfo(g_tp)


  ! ####################################################
contains
  ! ####################################################


  subroutine selection( a_mode )
    use varsystem, only : m_fout,m_tskip,m_fdens,m_pes,m_fpes,m_afield  &
                         ,m_energy,m_theta,m_phi,m_lmax,m_absorb,m_tsurf
    use varwfct,   only : m_wfcts,m_orbital,m_angular,m_i1p1hclass
    use density,   only : store_density_loc             &
                         ,store_reduced_density_nonloc  &
                         ,elec_spectrum_spl,elec_spectrum_tsurf,store_elec_spectrum &
                         ,Hlike_spec_pwvs
    use basics,    only : numberformat
    use inout,     only : find_openpipe
    implicit none

    character(*), intent(in) :: a_mode

    character(clen), parameter :: line=" -------------------------------------------------"
    integer :: i,n
    real(dp) :: tmax
    character(clen) :: file


    ! header
    select case(a_mode)
      case("1.0")
        write(STDOUT,'(2/,a,/,a)')  "  Local 1-particle density",trim(line)
      case("1.1")
        write(STDOUT,'(2/,a,/,a)')  "  Local density of excited electron",trim(line)
      case("1.2")
        write(STDOUT,'(2/,a,/,a)')  "  Local density of ionic hole",trim(line)
      case("2.1")
        write(STDOUT,'(2/,a,/,a)')  "  Non-local, radial density of excited electron",trim(line)
      case("2.2")
        write(STDOUT,'(2/,a,/,a)')  "  Non-local, angular density of excited electron",trim(line)
      case("3.1")
        write(STDOUT,'(2/,a,/,a)')  "  Angle and energy resolved photoelectron spectrum",trim(line)
      case("3.2")
        write(STDOUT,'(2/,a,/,a)')  "  Energy resolved photoelectron spectrum",trim(line)
      case("3.3")
        write(STDOUT,'(2/,a,/,a)')  "  Angle resolved photoelectron spectrum",trim(line)
      case("3.4")
        write(STDOUT,'(2/,a,/,a)')  "  Integrated photoelectron spectrum",trim(line)
      case("4")
        write(STDOUT,'(2/,a,/,a)')  "  Analyze Wavefunction",trim(line)
      case default
        write(STDERR,*) " ERROR : Invalid mode option ",trim(a_mode)    &
                      , "(SHOULD HAVE OCCURRED EARLIER)"
        stop
    end select


   select case(a_mode(1:1))
   case("1","2")
    n = size(m_wfcts)
    tmax = max( abs(m_wfcts(1)%time) , abs(m_wfcts(n)%time) )
    lp_time:do i=1,n

      ! only take every m_tskip time step
      if ( mod(i-1,m_tskip) /= 0 )  cycle
      write(STDOUT,'(a,'//numberformat("i",ONE*n)//",a,"//numberformat("f",tmax)//')')  &
          "  time step ",i," = ",m_wfcts(i)%time

      ! find file name
      write(file,*)  (i-1)/m_tskip

      select case(a_mode)
      case("1.0")
        write(file,'(2a)')  trim(m_fdens)//"-loc_",trim(adjustl(file))
        write(STDOUT,'(2a)') "    -> store in file... ",trim(file)
        call store_density_loc(file,"p",m_wfcts(i),m_orbital,m_angular)

      case("1.1")
        write(file,'(2a)')  trim(m_fdens)//"-loc_",trim(adjustl(file))
        write(STDOUT,'(2a)') "    -> store in file... ",trim(file)
        call store_density_loc(file,"e",m_wfcts(i),m_orbital,m_angular)

      case("1.2")
        write(file,'(2a)')  trim(m_fdens)//"-loc_",trim(adjustl(file))
        write(STDOUT,'(2a)') "    -> store in file... ",trim(file)
        call store_density_loc(file,"i",m_wfcts(i),m_orbital,m_angular)

      case("2.1")
        write(file,'(2a)')  trim(m_fdens)//"-r-nonloc_",trim(adjustl(file))
        call store_reduced_density_nonloc(file,"e","r",m_wfcts(i),m_orbital,m_angular)

      case("2.2")
        write(file,'(2a)')  trim(m_fdens)//"-a-nonloc_",trim(adjustl(file))
        write(STDOUT,'(2a)') "    -> store in file... ",trim(file)        
        call store_reduced_density_nonloc(file,"e","a",m_wfcts(i),m_orbital,m_angular)
    end select
    end do lp_time


   case("3")
  
     allocate(m_pes( nint(m_energy(3)) , nint(m_theta(3)) , nint(m_phi(3)) , m_i1p1hclass ))
     m_pes = ZERO
     
 
     ! method of calculating electron spectra
     ! -> if t-surf is set it is used also when splitting is used as a absorber
     ! -> if the splitting method should be used m_tsurf has to be turned off
     if ( m_tsurf ) then
       call elec_spectrum_tsurf(m_pes,m_wfcts,m_afield,m_energy,m_theta,m_phi)
     else if ( m_absorb == "SPL" ) then
       call elec_spectrum_spl(m_pes,m_wfcts,m_orbital,m_afield,m_energy,m_theta,m_phi)
     end if
 
     call store_elec_spectrum(m_fpes,m_pes,m_energy,m_theta,m_phi,a_mode(3:3))
     deallocate(m_pes)
     
     ! append time information
     call date_and_time(values=g_tp(:,2))
     i = find_openpipe()
     open(i,file=m_fpes,status="old",position="append")
     call timeinfo(g_tp,i,"#")
     close(i)
   
  
   case("4")
     
     call Hlike_spec_pwvs(m_wfcts)

  end select


  end subroutine selection


  ! -----------------------------------------------------------------


  subroutine prepare
    ! read in all parameters -> check them 
    use VarSystem, only :  m_lmax,m_nmax,m_ecut                          &
                          ,m_focc,m_fout,m_fwfct,m_forb,m_fenergy        &
                          ,m_fwfctsplit, m_split_write,m_ftsurf,m_tsurf  &
                          ,m_grid, m_theta, m_phi                        &
                          ,m_folap

    use VarWfct, only   : m_lomax, m_moamax, m_actoccorb, m_idoccactrev  &
                         ,m_orbital, m_angular 

    use VarMatrix, only : m_olap
    use ReadMatrices, only : get_L0operator

    use general,only    : printinfo,init_idoccrev                            &
                         ,assign_active_orbitals,assign_active_orbitals_file &
                         ,defaultvalue,printvariables
                         
    use read
    use grid, only  : initialize_grid
    use pulse, only : read_pulse

    use sphericalharmonics, only : init_spherical
    use inout, only : find_openpipe,find_allfiles
    implicit none

    complex(dp), allocatable :: aux(:,:)
    character(clen) :: input,file,froot
    logical :: ex
    integer :: i


    ! print header
    ! ------------
    call printinfo(g_program,ao_header=-1)


    ! read first flag argument
    call getarg(1,input)

    ! show help
    ! ---------
    if (input=="-v") then
      call printhelp
      stop
    end if


    ! read mode
    ! ---------
    inquire(file=input,exist=ex)
    if ( .not. ex ) then
      g_mode = input
    else
      call getarg(2,g_mode)
    end if


    ! read global variables
    !-----------------------
    ! standard valued 
    call defaultvalue
    ! read input file
    call readSystemPara(input)


    ! print parameters 
    call printinfo(g_program,ao_header=0)


    ! initialize grid
    ! ---------------
    call initialize_grid(m_grid)


    ! read orbital informations
    ! --------------------------
    ! define occupied and active orbitals
    print *
    print *, "reading files"
    print *, "-------------"
    print *, "reading occupation file... ",trim(m_focc)
    call readoccupied(m_focc)



    ! read in complex orbital energy
    ! ------------------------------
    print *, "reading energy file... ",trim(m_fenergy)
    call readenergy(m_fenergy)


    ! read radial orbitals
    ! ---------------------
    ! -> calculate overlap rather than using overlap matrix (not needed to read)
    select case (g_mode)
    case("1.2")
       print *, "reading orbital file... ",trim(m_forb)
       ! need only orbitals up to l=lomax
       call readorb(m_forb,m_orbital,m_grid,m_lomax)
       
       ! read wavefunction (set by m_storewfct)
       ! which is not CAP-correccted
       froot=trim(m_fwfct)//"_*"

    case("1.0","1.1","2.1","2.2")


       ! need all orbitals up to l=lmax
       !  -> identical to call readorb(forb,forbc)
       print *, "reading orbital file... ",trim(m_forb)
       call readorb(m_forb,m_orbital,m_grid,m_lmax)
       

       ! read wavefunction (set by m_storewfct)
       ! which is not CAP-correccted
       froot=trim(m_fwfct)//"_*"

    case("3.1","3.2","3.3","3.4")
       if (m_tsurf) then
         ! need all orbitals up to l=lmax
         ! not CI coefficients are stored in "*.tsurf" but value and 1. radial derivative of wavefunction
         m_nmax = 1  ! wfct and derivative are read/needed
         froot=trim(m_ftsurf)//"_*"
         ! read absorbed wavefunction
       else if (m_split_write) then
         ! need all orbitals up to l=lmax
         !  -> identical to call readorb(forb,forbc)
         print *, "reading orbital file... ",trim(m_forb)
         call readorb(m_forb,m_orbital,m_grid,m_lmax)
         ! read absorbed wavefunction
         froot=trim(m_fwfctsplit)//"_*"
       else
         stop " ERROR: no spectral method defined"
       end if

    case("4")
      froot=trim(m_fwfct)//"_*"

    case default
      write(STDERR,*) " ERROR : Invalid mode option ",trim(g_mode)
      call printhelp
      stop
    end select
   

    select case (g_mode)
    case("1.0","1.2")      
      call get_L0Operator(m_folap,m_olap)
    end select


    ! assign active orbitals
    if ( trim(m_actoccorb) == "-1" ) then

      ! find a temporary file
      i = find_openpipe()
      open(i,status="scratch")
      inquire(i,name=input)
      close(i)

      ! write list of files into input
      call find_allfiles( trim(froot) , trim(input) , "b" )
      
      ! find one file (contains information about active orbitals)
      open(i,name=trim(input),status='old')
      read(i,'(a)') file
      close(i)

      call assign_active_orbitals_file(file)
    else
      call assign_active_orbitals(m_actoccorb)
    end if


    ! initialize idoccactrev
    !  - all orbital information should be read before calling this routine
    call init_idoccrev

    ! print important running parameter
    call printvariables


    select case (g_mode)
    case("3.1","3.2","3.3","3.4")
      ! read wavefunction for all times
      write(STDOUT,'(/,2a)') " reading wfct-file... ",trim(froot)

      if (m_tsurf) then
        call read_tsurf(trim(froot))
      else if (m_split_write) then
        call read_wfcts(trim(froot))
      end if

    case default
      ! read wavefunction for all times
      write(STDOUT,'(/,2a)') " reading wfct-file... ",trim(froot)
      call read_wfcts(trim(froot))
    end select

     

    ! initial spherical harmonics
    ! ---------------------------
    select case (g_mode)
    case("1.0","1.1","1.2","2.2")
      write(STDOUT,'(/,a)') " prepare angular momenta"
      call init_spherical(m_angular,m_lmax,m_moamax,m_theta,m_phi)
    end select

    ! prepare pulse object
    call read_pulse

  end subroutine prepare


  ! -----------------------------------------------------------------


  subroutine read_wfcts(a_froot)
    !
    !  Find all files containing wfct information
    !  and sort them according to time
    !
    use VarSystem, only : m_nmax,m_lmax
    use VarWfct, only : t_wfct,m_wfcts,wfct_init   &
                       ,m_ioccact,m_i1p1hclass
    use read, only    : read_backup
    use inout, only   : find_openpipe,find_allfiles,flength
    implicit none

    character(*), intent(in) :: a_froot

    character(clen) :: file,ftmp
    type(t_wfct) :: wfct
    integer :: i,j,ntime,fid


    ! find all files
    ! ---------------
    fid = find_openpipe()
    open(fid,status='scratch')  ! get some random file name
    inquire(fid,name=ftmp)
    call find_allfiles(trim(a_froot),ftmp,"b")
    ntime=flength(ftmp)


    if ( ntime < 1 ) stop " ERROR: no file [fout.wfct_*] found"


    ! allocate wfct field
    ! -------------------
    if ( allocated(m_wfcts) ) deallocate(m_wfcts)
    allocate( m_wfcts(ntime) )

    ! read all files
    ! ---------------
    lp_file:do i=1,ntime

      ! allocate alpha-coefficient and idm fields
      call wfct_init(m_wfcts(i),m_nmax,m_lmax,m_i1p1hclass,m_ioccact)

      read(fid,'(a)') file
      write(STDOUT,*) " -> read file... ",trim(file)

      call read_backup( file , m_wfcts(i) )
    end do lp_file

    close(fid,status='delete')


    ! order m_wfcts w.r.t. increasing time
    do i=1,ntime
      do j=1,ntime
        if ( m_wfcts(i)%time >= m_wfcts(j)%time ) cycle

        wfct = m_wfcts(i)
        m_wfcts(i) = m_wfcts(j)
        m_wfcts(j) = wfct
      end do
    end do

  end subroutine read_wfcts


  ! -----------------------------------------------------------------


  subroutine read_tsurf(a_froot)
    !
    !  Find all files containing wfct and its derivative 
    !
    use VarSystem, only : m_nmax,m_lmax
    use VarWfct, only : t_wfct,m_wfcts,wfct_init   &
                       ,m_ioccact,m_i1p1hclass
    use inout, only   : find_openpipe,find_allfiles,flength,readfile_fcts
    implicit none

    character(*), intent(in) :: a_froot

    character(clen) :: ffile,ftmp
    type(t_wfct) :: wfct
    integer :: i,j,k
    integer :: nfile,nlen
    integer :: fid,fid2

    real(dp), allocatable :: t(:),fcts(:,:)

    ! find all files
    ! ---------------
    fid = find_openpipe()
    open(fid,status='scratch')  ! get some random file name
    inquire(fid,name=ftmp)
    call find_allfiles(trim(a_froot),ftmp,"b")
    nfile=flength(ftmp)
    

    if ( nfile < 1 ) stop " ERROR: no file [fout.tsurf_T*] found"


    ! read all files
    ! ---------------
    lp_file:do i=1,nfile


      read(fid,'(a)') ffile


      ! allocate wfct field
      ! -------------------
      if ( i==1 ) then
        nlen = flength(ffile)
        allocate( m_wfcts(nlen) )
        allocate( t(nlen) , fcts(nlen,4*(m_lmax+1) ) )
        t = ZERO
        fcts = ZERO

        ! allocate alpha-coefficient and idm fields
        do j=1,nlen
          call wfct_init(m_wfcts(j),m_nmax,m_lmax,m_i1p1hclass,m_ioccact)
        end do
      elseif ( i>m_i1p1hclass ) then
        cycle
      end if


        write(STDOUT,*) " -> read t-surf file... ",trim(ffile)
      fid2 = find_openpipe()
      open(fid2,file=ffile)  
      t=ZERO
      fcts=ZERO
      call readfile_fcts( fid2 , t, fcts , "#" , 2 , 4*(m_lmax+1)+1  )  
      close(fid2)            

      do j=1,nlen
        m_wfcts(j)%time = t(j)
        m_wfcts(j)%alpha_ai(0,0:m_lmax,i) = fcts(j,1:4*(m_lmax+1):4) + IMG*fcts(j,2:4*(m_lmax+1):4)
        m_wfcts(j)%alpha_ai(1,0:m_lmax,i) = fcts(j,3:4*(m_lmax+1):4) + IMG*fcts(j,4:4*(m_lmax+1):4)
      end do


    end do lp_file

    close(fid,status='delete')



  end subroutine read_tsurf


  ! ----------------------------------------


  subroutine printhelp
    implicit none

    write(STDOUT,*)
    write(STDOUT,*) " usage: density.x [input.file / -v] [mode]"
    write(STDOUT,*) " -------------------------------------------"
    write(STDOUT,*)
    write(STDOUT,*) "  mode =  1 : Local Density"
    write(STDOUT,*) "   -> mode = 1.0  : 1-particle local density (inactive electrons substracted)"
    write(STDOUT,*) "   -> mode = 1.1  : Excited Electron"
    write(STDOUT,*) "   -> mode = 1.2  : Ionic Hole (not implemented)"
    write(STDOUT,*)
    write(STDOUT,*) "  mode =  2 : Reduced Non-Local Density Matrix (of excited electron)"
    write(STDOUT,*) "   -> mode = 2.1  :  Radial Density [ rho(r,r') ]"
    write(STDOUT,*) "   -> mode = 2.2  :  Angular Density [ rho(theta,theta') ]"
    write(STDOUT,*) "  mode =  3  : Photoelectron Spectrum"
    write(STDOUT,*) "   -> mode = 3.1  :  Angle and energy resolved spectrum"
    write(STDOUT,*) "   -> mode = 3.2  :  Energy resolved spectrum"
    write(STDOUT,*) "   -> mode = 3.3  :  Angle resolved spectrum"
    write(STDOUT,*) "   -> mode = 3.4  :  Fully integrated spectrum"
    write(STDOUT,*) "  mode =  4  : analyze wfct (partial wave, energy (H-like system))"
    write(STDOUT,*)
  end subroutine printhelp
 
end program density_program
