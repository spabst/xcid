module VarWfct
  !
  !**PURPOSE
  !  -------
  !  Collection of variables directly related to the wavefunction
  !
  !**AUTHOR
  !  ------
  !  (c) and written by Stefan Pabst
  !
  !**VERSION
  !  -------
  !  svn info: $Id: VarWfct.f90 1658 2015-05-08 21:46:13Z spabst $
  !-----------------------------------------------------------------------
  !
  use constants,only : dblprec,clen
  use grid_type
  use sphericalharmonics, only : t_spherical
  implicit none


  ! ---------
  ! | TYPES | 
  ! ---------

  ! store spatial,energy, and quantum number information of a orbital
  type t_orbital
     ! n_basis : number of basis function
     !           = no. grid points for pseudo-spec. grid method 
     !           = 3*(no. grid points)-2 for FEM   !NOTE: end points r=0,m_Rmax count too
     integer :: nbasis = 0  

     type(t_grid),pointer :: grid => null()   ! grid object 
     complex(dblprec),allocatable :: wfct(:)  ! coefficients of orbital  wavefunction
     complex(dblprec) :: energy = ZERO        ! HF-orbital energy
     
     ! orbital infos
     ! -------------
     ! n - radial quant. no.
     ! l - angular momentum
     
     ! occupation info (/= 0 if it is an occupied orbital)
     ! ---------------------------------------------------
     ! occ - no. of electrons in (n,l) shell (0...4l+2)
     integer :: n = -1   &
               ,l = -1   &
               ,occ = 0   
     
     ! spin-orbit splitted energies
     ! -> J=L-\+S, S=0.5
     complex(dblprec) :: energy_so(0:1) = ZERO ! J=L-\+S
  end type t_orbital
 

  ! store channel information
  type t_idrev
    ! j,mj are stored with twice its value to be able to use integer
    integer :: n = 0
    integer :: l = 0
    integer :: j = 0
    integer :: mj = 0
    integer :: ml = 0
    integer :: ms = 0
    integer :: idhole = 0
    ! clebsch-gordan coefficient, which on gets from the SO coupling 
    ! (1. order pertubation theory) <L;S|J> for the spin up/down part
    real(dblprec) :: cleb(0:1) = ZERO ! ms=-1/2,1/2
    complex(dblprec) :: energy = ZERO
  end type t_idrev


  ! store all wavefunction informations
  type t_wfct
    ! currrent time
    real(dp) :: time=zero

    ! m_alpha_ai:
    !  -> index structure (N_a,L_a,ID_i)
    !  -> ID_i : ID of 1p1h class
    !  -> m_alpha0 = alpha_0 and m_alpha contains all alpha^a_i
    !     see Eq. 1 in Greenman, PRA 82, 023406 (2011)
    !  
    complex(dp) :: alpha_0=ZERO
    complex(dp), allocatable :: alpha_ai(:,:,:)

    ! idm - uncorrected matrix
    !  -> Eq. 17 in Greenman, PRA 82, 023406 (2011)
    ! idmcorr - correction (due to absorption)
    !  -> Eq. 26 in Greenman, PRA 82, 023406 (2011)
    ! idmcorrL - contributions to idmcorr are ang. mom. (L) resolved  
    complex(dp), allocatable :: idm(:,:),idmcorr(:,:),idmcorrL(:,:,:)
    logical :: store_IDMcorrL = .false.
  end type t_wfct
  
  ! ------------------------------------------------------------
  ! ------------------------------------------------------------
  ! ------------------------------------------------------------


  ! Wavefunction CIS coefficients
  ! -----------------------------
  ! - m_wfct : stores CIS wfct
  ! - m_wfctprev : needed for propagation mode: 2. order finite differencing
  !   -> see Eq. 53 in Greenman, PRA 82, 023406 (2011)
  ! - m_wfctref : is a reference CIS state (e.g., needed for correlation function)
  ! - m_wfctsplit : wfct resulting from applying the split operator stored in m_split
  !
  type(t_wfct), target :: m_wfct,m_wfctprev,m_wfctref,m_wfctsplit

  ! m_ephase : contains phase factor exp(-i dt e_{a,i})
  !            -> index structure : 
  !                +  0...nmax,0...lmax,1...i1p1hclass
  !                +  first two indicies refer to excited electron
  !                +  3. index is the ID of 1p1h-channel (hole index + spin of excited electron)
  complex(dblprec),allocatable,target :: m_ephase(:,:,:)

  ! CIS wfct for each time (used in program density)
  type(t_wfct), allocatable :: m_wfcts(:)


  ! orbital information
  ! -------------------
  type(t_orbital),allocatable,target :: m_orbital(:,:)

  ! angular information
  ! --------------------
  ! spherical harmonics object 
  !  -> contains angular grid and spherical harmoncis Y^l_m(theta,phi)
  type(t_spherical),save :: m_angular

  ! parameter connected to the occupied orbitals
  ! ---------------------------------------------
  ! m_nomax  : max. radial number of occupied orbitals
  ! m_lomax  : max. occupied ang. mom.
  ! m_moamax : max. M of active occupied orbitals
  ! m_mmax   : max. M of active orbitals 
  !            -> w/ spin-orbit and interchannel interaction 
  !               a spin-flip can increase m^L to m^J_i+1
  !             -> mmmax=moamax+1 
  integer :: m_lomax,m_nomax,m_moamax,m_mmax

  ! m_ioccnl : number of occupied (n,l)-shells
  !  -> m_occ,m_nomaxl,m_noaminl have the length m_ioccnl
  ! m_occnl(:,i): i=1 : list of radial number nr of occupied (n,l) pairs
  !               i=2 : list of ang. mom. L of occupied (n,l) pairs
  ! m_nomaxl : max. occupied radial number n for a given l<=lomax
  ! m_noaminl: min. active occupied radial number n for a given l<=lomax
  integer :: m_ioccnl
  integer, allocatable :: m_occnl(:,:),m_nomaxl(:),m_noaminl(:)

  ! m_actoccorb : List of active occupied orbitals (NLM) orbitals,
  !                where N is the principal quantum number (shell period) 
  !                and NOT the radial quantum number, are given in the 
  !                format NL_M. When "_M" is omitted, all M orbitals in 
  !                the NL shell are activated. 
  !                Spin orbitals (NLJM) are in the format NLJ_M, 
  !                with J and M are given in the form 0.5, 1.5, ... .
  !                M must be non-negative, since symmetries are exploited
  !                such that only M>=0 are needed. When "_M" is omitted, 
  !                all M orbitals in the NLJ shell are activated.
  !                It is possible to define ranges like 1s-3p.
  !                The word "END"  refers to the highest occupied orbital.
  !                Elements are sperated by ":".
  character(clen),target :: m_actoccorb
  ! m_orbformat: format of orbital statements in m_actoccorb
  !              = "principal" : orbitals are stated in terms of the principal 
  !                              quantum number
  !              = "radial"    : orbitals are stated in terms of the radial
  !                              quantum number
  !               ->  default : "principal"
  character(clen),parameter :: m_orbformat_p="principal"
  character(clen),parameter :: m_orbformat_r="radial"
  character(clen),save :: m_orbformat=m_orbformat_p

  
  ! m_ioccall : number of occupied orbitals (frozen and active) in (n,l,m)
  !             representation
  !
  ! m_ioccact : number of active occupied orbitals {use +-M^L(M^J) symmetry}
  !             -> with    SO-coupling : M^J>0
  !             -> without SO-coupling : M^L>=0
  !
  ! m_i1p1hclass:
  !             number of active 1p1h classes {use +-M^L(M^J) symmetry}
  !             only act. occupied orbitals are counted that have M^J>0
  !             -> without SO coupling:
  !                same as m_ioccact
  !             -> with SO coupling: 
  !                for each occupied orbital i there are up to 2 possibilities
  !                for (M^L_a,M^S_a) to fulfill : M^J_i = M^L_a + M^S_a
  integer :: m_ioccact,m_ioccall,m_i1p1hclass
  ! m_idoccallrev,m_idoccactrev,m_id1p1hclassrev 
  !  -> retrieve from ID all physical properties
  type(t_idrev), allocatable :: m_idoccallrev(:),m_idoccactrev(:)   &
                               ,m_id1p1hclassrev(:)

  ! m_fmoact,m_fmoact_so : 
  !  -> determine active occupied orbitals without,with spin-orbit coupling
  !  -> index structure : (N,L,M) and (N,L,MJ,J)
  !      - m_fmoact   : M-index : -L:L 
    ! fmoact_so(n,l,m_j-.5,j-l+.5)
  !      - m_fmoact_so: MJ-index : MJ-0.5  (range: 0:L)
  !                     J-index  : J-L+0.5 (range: 0:1)
  logical, allocatable :: m_fmoact(:,:,:),m_fmoact_so(:,:,:,:)


contains

  subroutine wfct_init(a_this,ao_nmax,ao_lmax,ao_n1p1h,ao_nocc,ao_Lresolved)
    ! initialize/reset t_wfct object
    implicit none
    type(t_wfct), intent(inout) :: a_this

    integer, intent(in), optional :: ao_nmax,ao_lmax,ao_n1p1h,ao_nocc
    logical, intent(in), optional :: ao_Lresolved

    ! reset
    ! -----
    a_this%time = ZERO
    a_this%alpha_0 = ZERO
    a_this%store_IDMcorrL = .false.
    if ( allocated(a_this%alpha_ai) )  deallocate(a_this%alpha_ai)
    if ( allocated(a_this%idm) )       deallocate(a_this%idm)
    if ( allocated(a_this%idmcorr) )   deallocate(a_this%idmcorr)

    ! WFCT
    ! ----
    if ( present(ao_nmax).and.present(ao_lmax).and.present(ao_n1p1h) ) then
      allocate(a_this%alpha_ai(0:ao_nmax,0:ao_lmax,ao_n1p1h))
      a_this%alpha_ai = ZERO
    end if

    ! IDM
    ! ---
    if ( present(ao_nocc) ) then
      allocate(a_this%idm(ao_nocc,ao_nocc))
      allocate(a_this%idmcorr(ao_nocc,ao_nocc))
      a_this%idm = ZERO
      a_this%idmcorr = ZERO

      ! set L-resolved IDM-correction
      if ( present(ao_Lresolved) ) then
        if ( ao_Lresolved ) then
          if ( .not.present(ao_lmax) )   stop "wfct_init: no Lmax supplied"
          a_this%store_IDMcorrL = .true.
          allocate(a_this%idmcorrL(ao_nocc,ao_nocc,0:ao_lmax))
          a_this%idmcorrL = ZERO
        end if
      end if
    end if
  end subroutine wfct_init


  function wfct_getidm( a_wfct ) Result(idm)
    ! calculate corrected IDM
    implicit none
    type(t_wfct), intent(in) :: a_wfct
    complex(dp), allocatable :: idm(:,:)

    integer :: nocc

    nocc = size(a_wfct%idm,1)

    allocate(idm(nocc,nocc))
    idm = a_wfct%idm + a_wfct%idmcorr
  end function


end module VarWfct
