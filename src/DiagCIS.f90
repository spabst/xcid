module DiagCIS
  use Constants, dp=>dblprec
  implicit none

contains

  subroutine Action_Hamiltonian( a_n, a_init, a_final , a_para )
    use VarSystem, only : m_nmax,m_lmax, m_energy,m_diagcis
    use VarWfct, only   : m_i1p1hclass
    use eom, only : derivative
    implicit none

    integer, intent(in) :: a_n
    complex(dp), intent(in) :: a_init(a_n)
    complex(dp), intent(out) :: a_final(a_n)
    real(dp), intent(in) :: a_para

    integer :: shap(3)
    complex(dp), allocatable :: vec(:,:,:,:)
    complex(dp) :: vec0(2) , energy


    ! determine size of long-format array
    shap(1) = m_nmax + 1
    shap(2) = m_lmax + 1
    shap(3) = m_i1p1hclass


    if ( a_n-1 > product(shap) )    &
      stop " ERROR(Action_Hamiltonian): size inconsistency"


    allocate( vec(shap(1),shap(2),shap(3),2) )
    vec = ZERO

    ! convert format from short to long format
    call convert(.true.,a_init,vec(:,:,:,1),vec0(1))

    ! perform:  |a_final> = H |a_init>      
    ! ----------------------------------
    call derivative( vec0(1),vec(:,:,:,1)  &
                    ,a_para                &
                    ,vec0(2),vec(:,:,:,2)  &
                   )

    ! shift energy spectrum by -( m_energy(1) + IMG * m_energy(2) )
    if (m_diagcis) then
      energy = m_energy(1)+IMG*m_energy(2)
      vec0(2) = vec0(2) - energy * vec0(1)
      vec(:,:,:,2) = vec(:,:,:,2) - energy * vec(:,:,:,1)
    end if

    ! convert format back from long to short format
    call convert(.false.,a_final,vec(:,:,:,2),vec0(2))

  end subroutine Action_Hamiltonian




  subroutine convert(a_mode,a_short,a_long,ao_long0)
    !
    ! DESCRIPTION
    ! -----------
    ! convert (ao_long,a_long) in the m_alpha-format (3-index alpha_ai array+alpha_0) 
    ! into 1D vector a_short and vice versa
    !
    ! -> a_short does not contain unphysical entries for virtual orbital a
    !    which exist in a_long but are never accessed (to keep 3D array rectangular)
    !    e.g.: a could refer to an occupied orbital or to orbitals with 
    !          too high orbital energies
    !
    ! INPUT/OUTPUT
    ! ------------
    !  a_mode  : convert short->long/long->short (=TRUE/FALSE)
    !  a_short : 1D format of CIS coeff. without spurious entries
    !  a_long  : 3D alpha_ai array in 3D+scalar format of CIS coeff.
    !  a_long0 : scalar alpha_0 in 3D+scalar format of CIS coeff.
    use VarSystem, only : m_nmaxl,m_lmax,m_nmax
    use VarWfct, only   : m_lomax,m_nomaxl,m_i1p1hclass
    implicit none
    complex(dp) :: a_short(1:),a_long(0:,0:,1:)
    complex(dp), optional :: ao_long0
    
    ! a_mode = F : a_short will be assigned
    !        = T : a_long will be assigned
    logical,intent(in) :: a_mode  
    integer :: i,l,nmin,nmax,pos1,pos2
    integer :: len_short

    integer :: shap(3)


    ! determine shape of long-format array
    shap(1) = m_nmax + 1
    shap(2) = m_lmax + 1
    shap(3) = m_i1p1hclass


    len_short = size(a_short)
    if ( any(shape(a_long)/=shap) ) &
      stop 'ERROR(convert): invalid long CIS format'

    if (a_mode) then
      a_long = ZERO
    else
      a_short = ZERO
    end if

    ! convert HF ground state part
    if ( present(ao_long0) ) then
      if (a_mode) then
        ao_long0 = a_short(1)
      else
        a_short(1) = ao_long0
      end if
    end if

    ! convert alpha_ai part
    pos1=2
    do i=1,shap(3)
      do l=0,shap(2)-1
        ! find n-range for given l
        nmin = 0
        if ( l <= m_lomax ) nmin=m_nomaxl(l)+1
        nmax = m_nmaxl(l)


        pos2 = pos1 + nmax-nmin   ! end position of current (n,l,i) block
        if ( pos2 > len_short )  then
          stop " ERROR(convert): short CIS format is too short"
        end if

        if (a_mode) then
          a_long(nmin:nmax,l,i) = a_short(pos1:pos2)
        else
          a_short(pos1:pos2) = a_long(nmin:nmax,l,i)
        end if
        pos1 = pos2 + 1  ! update starting position of next (n,l,i) block 
      end do
    end do
  end subroutine convert




  subroutine store_CIS_spectrum( a_E , a_eval , ao_evec )
    use VarSystem, only : m_fout,m_nmax,m_lmax, m_storewfct
    use varwfct, only : t_wfct , wfct_init , m_i1p1hclass , m_1p1hr=>m_id1p1hclassrev
    use varmatrix, only : m_olap
    use inout,only : opendir,find_openpipe
    use general,only : printinfo,printvariables
    use write, only : write_backup
    implicit none

    real(dp), intent(in) :: a_E
    complex(dp), intent(in) :: a_eval(:),ao_evec(:,:)

    optional :: ao_evec

    character(clen) :: fname,fname2
    integer :: i,j,fID,fID2

    integer :: shap(3),io,l
    type(t_wfct) :: wfct
    complex(dp) :: caux,norm


    ! check size consistency
    if ( present(ao_evec) ) then
      if ( size(a_eval) /= size(ao_evec,2) )  stop &
         ' ERROR(store_CIS_spectrum): number of eigenvalues /= number of eigenvectors'
    end if



    shap(1) = m_nmax + 1
    shap(2) = m_lmax + 1
    shap(3) = m_i1p1hclass

    call wfct_init(wfct,m_nmax,m_lmax,m_i1p1hclass)
    wfct%alpha_ai = ZERO
    

    ! write eigenvalues 
    ! ------------------
    fID = find_openpipe()
    write(fname,'(ES12.4E2)')  a_E
    fname="E"//adjustl(fname)
    fname = trim(m_fout)//".diag_"//trim(fname)
    call opendir(fID,trim(fname))  ! save ion density matrix
    call printinfo(2,fID,'#')
    call printvariables(1,fID,'#')
    write(fID,'(a,ES15.5E2)')  "##EFL ", a_E
    write(fID,'(a,2(/,a))')  "#"                                             &
     ,"# This stores the eigenvalues and eigenvectors of the CIS Hamiltonian &
     &for a given electric field (specified above)"                          &
     ,"# COLUMNS: eigenvalues , state index"
    

    do i=1,size(a_eval)      
      write(fID,'(2ES18.7E3,i9)')  a_eval(i) , i
    end do
    ! close pipe to file
    close(fID)


    ! store wavefunction
    ! ------------------
    if_wfct:if ( present(ao_evec) .and. m_storewfct ) then
      do  i=1,size(a_eval)
        
        ! file name
        write(fname2,'(i)')   i
        fname2 = trim(fname)//".wfct_"//trim(adjustl(fname2))
 

        ! store wavefunction
        call convert(.true.,ao_evec(:,i),wfct%alpha_ai,wfct%alpha_0)
        call write_backup( trim(fname2), wfct , ao_energy=a_eval(i) )


        ! store partial wave information of each state
        write(fname2,'(i)')   i
        fname2 = trim(fname)//".pwvs_"//trim(adjustl(fname2))
        fID2 = find_openpipe()
        open(fID2,file=trim(fname2))
        
        
        write(fID2,"(a,2 ES18.10)")  "##ENERGY ",a_eval(i)
        
        write(fID2,'(a)')  "# 1p-1h ID, no, lo, lv, probability"

          
          write(fID2,'(4i4,99ES18.10)')  -1,-1,-1,-1,abs(wfct%alpha_0)**2

          ! find total norm 
          norm = abs(wfct%alpha_0)**2
          do io=1,m_i1p1hclass
            do l=0,m_lmax
               caux = dot_product(                                     &
                       wfct%alpha_ai(:,l,io)                           &
                      ,matmul(m_olap(:,:,l),wfct%alpha_ai(:,l,io))     &
                     )
                norm = norm + abs(caux)
            end do
          end do
 
          ! print norm of partial channels
          do io=1,m_i1p1hclass
            do l=0,m_lmax
              
              caux = dot_product(                                      &
                       wfct%alpha_ai(:,l,io)                           &
                      ,matmul(m_olap(:,:,l),wfct%alpha_ai(:,l,io))     &
                     )
              
              if ( abs(caux)>1.d-10 )  then
                write(fID2,'(4i4,99ES18.10)')     &
                  io,m_1p1hr(io)%n,m_1p1hr(io)%l,l,abs(caux)/abs(norm)
              end if
            end do
          end do
 
        
        close(fID2)
      end do
    end if  if_wfct

  end subroutine store_CIS_spectrum




  subroutine diag_CIS_Ham
    !
    !  Routine in charge of diagonalizing the full CIS Hamiltonian
    !  -----------------------------------------------------------
    use VarSystem, only : m_diagCIS,m_diagField,m_diagnev,m_nmaxl,m_lmax  &
                         ,m_backup
    use VarWfct,   only : m_nomaxl,m_i1p1hclass,m_wfct
    use Eigslv,   only  : diag=>get_DCGenSelect_sbr , diagmat=>get_DCGenSelect_mat, eigensolver
    implicit none

    integer  :: nmax
    complex(dp), allocatable :: eval(:),evec(:,:),snorm(:,:)
    complex(dp), allocatable :: olap0(:,:),vaux0(:,:),evaux0(:),olap1(:,:),vaux1(:,:)
    real(dp) :: E
    integer  :: i,j,jj,l,ne

    character(2), parameter :: which="SM"   ! S/R (smallest/largest)  + R/I/M  (real/imaginary/magnitude
 
    ! number of all alpha coefficients
    ! --------------------------------
    ! sum(m_nmaxl)-sum(m_nomaxl) : number of virtual orbitals
    nmax = 1 + m_i1p1hclass * (sum(m_nmaxl+1)-sum(m_nomaxl+1))
    
    allocate( eval(m_diagnev) , evec(nmax,m_diagnev) )
    eval = ZERO
    evec = ZERO

    allocate( olap0(m_diagnev,m_diagnev), olap1(m_diagnev,m_diagnev) )
    olap0 = zero
    olap1 = zero
    allocate( evaux0(m_diagnev) )
    evaux0 = zero
    allocate(vaux0(nmax,m_diagnev),vaux1(nmax,m_diagnev))
    vaux0 = zero
    vaux1 = zero
    allocate( snorm(m_diagnev,2) )
    snorm = ZERO



    ! calculate the field-free eigenvectors and normalize them
    ! --------------------------------------------------------
      write(STDOUT,'(/,a)')  ' diagonalize field-free CIS Hamiltonian'
    if ( m_backup%read ) then
      ! convert current CIS wfct into short format
      call convert(.false.,vaux1(:,1),m_wfct%alpha_ai,m_wfct%alpha_0)
      ! use state in file m_backup%file_read as initial state for ARPACK routine
      call diag(nmax,m_diagnev,Action_Hamiltonian,evaux0,vaux0,ao_ivector=vaux1(:,1)   &
                ,ao_which=which,ao_info=0,ao_para=ZERO)
    else
      ! use random initial state
      call diag(nmax,m_diagnev,Action_Hamiltonian,evaux0,vaux0,ao_which=which,ao_info=0,ao_para=ZERO)
    end if

    ! normalize state
    !TMP,TODO: hermitian norm should be taken here. 
    !          temporarily just the absolute square is taken (even though basis is not othogonal)
    do j=1,size(vaux0,2)
      snorm(j,1) = ONE/sqrt(sum(vaux0(:,j)**2))
    end do

    ! setting field-free state for the first field iteration
    ! ------------------------------------------------------
    vaux1 = vaux0    
 
    ! intermediate time point
    !call date_and_time(values=g_tp(:,2))
  

    ! -------------------------
    ! loop over all E-fields
    ! -------------------------
    if ( m_diagField(2)==ZERO )  then
      ne = 0
    else
      ne = nint( ( m_diagField(3)-m_diagField(1) ) / m_diagField(2) )
    end if

    lp_Efield: do i=0,ne
      E = m_diagField(1) + i*m_diagField(2)
      write(STDOUT,*)  'E =', E
   

      if ( E==ZERO ) then

        ! do nothing field-free Hamiltonian is already diagonalized
        eval = evaux0
        evec = vaux0

      else

        ! routines for the partial diagonalization with arpack
        ! ----------------------------------------------------
        call diag(nmax,m_diagnev,Action_Hamiltonian,eval,evec,ao_which=which,ao_info=0,ao_para=E )

        !TMP,TODO: hermitian norm should be taken here. 
        !          temporarily just the absolute square is taken (even though basis is not othogonal)
        do j=1,size(vaux0,2)
          snorm(j,2) = ONE / sqrt(sum(evec(:,j)**2))
        end do

      
        ! overlap between the states of the previous and following field step
        ! -------------------------------------------------------------------
        do j=1,size(olap0,2)
          do jj=1,size(olap0,2)
            olap1(j,jj) = sum(vaux1(:,j)*evec(:,jj)) * snorm(j,1)*snorm(jj,2)
          end do
        end do
 
        ! sort eigenvalues and -vectors according to the largest overlap to previous field step
        ! -------------------------------------------------------------------------------------
        call sortol(olap1,eval,evec)
        vaux1 = evec
        snorm(:,1) = snorm(:,2)
        
 
        ! overlap between field-free states and states with field
        ! -------------------------------------------------------
        ! do j=1,size(olap0,2)
        !  do jj=1,size(olap0,2)
        !    olap0(j,jj) = sum(vaux0(:,j)*evec(:,jj))
        !  end do
        ! end do
        ! sort eigenvalues and -vecctors according to their magnitude
        ! call sort(eval, evec)
      
      end if


      ! store the E-field value, the eigenvalues and -vectors in data_E*.diag files
      ! ---------------------------------------------------------------------------
      call store_CIS_spectrum(E, eval, evec)
     
    end do lp_Efield

  end subroutine diag_CIS_Ham



  subroutine sortol(a_olap,a_d,a_v)
  ! sorts the entries of the table evec according to the maximal overlap between the states
  ! of previous and following e-field
   
   implicit none
   complex(dp) :: a_olap(:,:), a_d(:), a_v(:,:)
   complex(dp) :: p
   complex(dp), allocatable :: vec(:),olap(:)
   integer  :: i,l

 
   allocate( vec(size(a_v,1)) , olap(size(a_olap,1)) )

   do i = 1,size(a_d)
      l = maxloc(abs(a_olap(i,:)),dim=1)
      if ( abs(a_olap(i,l)) < 0.5D0 ) cycle

      ! switch l-th entry with i-th entry 
      ! -> in eigenvector and eigenvalue arrays
      p = a_d(l)
      a_d(l) = a_d(i)
      a_d(i) = p
      vec = a_v(:,l)
      a_v(:,l) = a_v(:,i) 
      a_v(:,i) = vec

      ! -> in the overlap matrix
      olap = a_olap(:,i)
      a_olap(:,i) = a_olap(:,l)
      a_olap(:,l) = olap
   end do

  end subroutine sortol


  subroutine sort( a_d, a_v)
!  sorts the entries of the array d and table v in increasing order
!  and returns them sorted correspondingly
   implicit none 
   complex(dp) :: a_d(:), a_v(:,:)
   
   complex(dp) :: p
   complex(dp), allocatable :: vec(:)
   integer  :: i,j


   allocate( vec(size(a_v,1)) ) 

   do i=1,size(a_d)
     do j=i+1,size(a_d)
       if ( dble(a_d(j)) < dble(a_d(i)) ) then
         p = a_d(j)
         a_d(j) = a_d(i)
         a_d(i) = p

         vec = a_v(:,j)
         a_v(:,j) = a_v(:,i)
         a_v(:,i) = vec
       end if 
     end do
   end do
  end subroutine sort 


end module DiagCIS
