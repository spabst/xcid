module VarMatrix
  !
  !**PURPOSE
  !  -------
  !  Collection of variables directly related to the operators.
  !
  !**AUTHOR
  !  ------
  !  (c) and written by Stefan Pabst
  !
  !**VERSION
  !  -------
  !  svn info: $Id: VarMatrix.f90 1458 2015-01-25 06:06:02Z spabst $
  !-----------------------------------------------------------------------
  !
  use constants
  implicit none

  ! ---------
  ! | TYPES | 
  ! ---------
  
  ! coulomb matrix
  !  -> store <Phi^a_i|H_1|Phi^b_j> involving v_ajbi and v_ajib 
  !     for a given ij pair
  !     - internal naming: v1=a , v2=b , o1=i , o2=j
  type t_mat_coulomb  
    ! matrix elements
    ! ---------------
    ! -> index structure : n_v2, n_v1, idx of l_v2 [1..nlb], l_v1
    complex(dblprec), allocatable :: matrix(:,:,:,:)

    ! angular momenta of active occupied orbital 
    ! o1 and o2, i.e., l_o1=lo1 and l_o2=lo2
    ! ---------------------------
    integer :: lo1=-1,lo2=-1   ! -1 : undefined
    
    ! elements containing l_v2 information
    ! -----------------------------------
    integer :: nlv2=0  ! number of possible l_v2
    ! l_v2 : contains all possible ang. mom. of orbital v2
    !   -> 1. index : idx of l_v2 (1...nlv2)  
    !   -> 2. index : l_v1 (0...lprx)
    integer, allocatable :: lv2(:,:)  
  end type t_mat_coulomb



  ! 1p1h matrices
  ! -------------
  !  -> use hermitian inner product
  complex(dblprec),allocatable,target :: m_olap(:,:,:),m_split(:,:,:)            &
                                        ,m_tsurf_mat(:,:,:)
  complex(dblprec),allocatable,target :: m_dipacc(:,:,:,:), m_dipherm(:,:,:,:)   &
                                        ,m_momherm(:,:,:,:)

  
  ! -> use symmetric inner product
  !
  !  -> m_dipij,m_dipab,m_dipai can be used for dipole operator or the momentum operator
  !  -> dipole moment is symmetric w.r.t symmetric inner product
  !  -> momentum operator p_z is antisymmetric w.r.t symmetric inner product
  !  -> it matters which index refers to the bra and ket state
  !       m_dipij(i1,i2) := z_(i1,i2)
  !       m_dipab(n_b,n_a,l_b,l_a) := z_(a,b)
  !       m_dipai(n_a,l_a,i) := z_(a,i)
  complex(dblprec),allocatable,target :: m_dipij(:,:),m_dipai(:,:,:),m_dipab(:,:,:,:)
  

  ! m_weight : Cleb-Gordan coeff. factor
  !   -> use Wigner-Eckart theorem for dipole operators
  !      => store only m=0 component  !   
  real(dblprec),allocatable :: m_mweight(:,:)

  ! 2p2h matrices: Coulomb operator <Phi^a_i|H_1|Phi^b_j>
  ! index structur: (j,i) 
  !  ->  j and i refer to the 1p1h-class (hole + spin of exc. electron)
  type(t_mat_coulomb), allocatable, target :: m_coulomb(:,:)
  ! index structur: (n1,n2,l1,l2,i1,i2)
  !  -> i1,i2 are 1p1h-pairs (N,L,M^L {,J,M^J,M^S})
  !     - J,M^J,M^S : when SO-coupling is active
  !!OLD complex(dblprec),allocatable,target :: m_coudir(:,:,:,:,:,:)  &
  !!OLD     ,m_couexc(:,:,:,:,:,:)
  

  ! Coulomb approximation : 1/||r_1-r_2|| -> 1/||r|| when l>lprx
  !  -> index structur : (n1,n2,l) <- 1p1h operator
  !  -> l1=l2, i1=i2
  !  -> only exchange term survives 
  complex(dblprec),allocatable,target :: m_couexcdiag(:,:,:)


  ! FEM Matrices
  ! ------------
  ! m_fem_ov   : overlap matrix
  ! m_femdvr_trafo : 
  !              transformation matrix between FEM and DVR basis sets
  real(dblprec),allocatable :: m_fem_ov(:,:) , m_femdvr_trafo(:,:)

  ! use for FEM and DVR
  ! --------------------
  ! m_fem_ke   : kynetic energy matrix
  ! m_fem_u    : local potential matrix (-Z/r - l*(l+1)/2r^2 + CAP )
  ! m_fem_dipsymm: dipole moment (complex symmetric)
  ! m_fem_dipacc : dipole acceleration
  complex(dblprec),allocatable :: m_fem_ke(:,:),   &
                                  m_fem_u(:,:,:),  &
                                  m_fem_dipole(:,:),  &
                                  m_fem_gradient(:,:) 

   ! expectation values (need separate arrays for when ECS is used)
   ! as mapping derivate needs to be conjugated and lead to distinct different matrices
   complex(dblprec), allocatable ::  m_fem_ev_dipacc(:,:)    &
                                   , m_fem_ev_dipole(:,:)    &
                                   , m_fem_ev_gradient(:,:)  

  

contains

  elemental subroutine init_coulomb( a_this , a_lo1 , a_lo2 , a_lmax , a_nmax )
    !
    ! setting up coulomb matrix for a given combination of holes o1,o2 (or i,j)
    !
    implicit none
    type(t_mat_coulomb), intent(inout) :: a_this
    integer, intent(in) :: a_lo1,a_lo2,a_lmax,a_nmax

    logical :: allow
    integer :: i,lv1,lv2,nlv2
    integer :: lcmin,lcmax

    if ( allocated(a_this%matrix) .or. allocated(a_this%lv2)   &
         .or. a_this%nlv2>0                                     &
       ) then
      call reset_coulomb(a_this)
    end if

    ! assign angular momentum of occupied orbitals
    a_this%lo1 = a_lo1
    a_this%lo2 = a_lo2

    ! find max. number of possible lv2 for a set of lv1=0...a_lmax
    !  -> lv2 is for a given set of lo1,lo2,lv1 only even or only odd
    !  -> nlv2 <= 1 + a_lmax/2  (lv2 can't be larger than a_lmax)
    !  -> coulomb exchange and direct terms: max. lo1+lo2+1 possibilites for lv2
    !     exterms are lv1+lo1+lo2 and lv1-lo1-lo2
    !  -> use the smaller of the numbers
    nlv2 = 1 + min( a_lmax/2 , a_lo1+a_lo2 )
    a_this%nlv2 = nlv2

    ! allocate arrays
    allocate( a_this%matrix(0:a_nmax,0:a_nmax,nlv2,0:a_lmax) )
    a_this%matrix = ZERO

    allocate( a_this%lv2(nlv2,0:a_lmax) )
    a_this%lv2 = -1  ! -1: means not assigned
    
    ! find possible lv2 for each lv1  (lo1,lo2 always fixed)
    do lv1=0,a_lmax
      
      i=0
      do lv2=mod(lv1+a_lo1+a_lo2,2),a_lmax,2
        ! run only over partity-conserving l
        allow = .false.

        ! check if it is possible in direct coulomb term
        ! -> do this by checking wether lo1,lv1 can couple to the same L
        !    as lo2,lv2
        lcmax = lv1+a_lo1
        lcmin = abs(lv1-a_lo1)
        if ( lcmax>=abs(lv2-a_lo2) .and. lcmin<=a_lo2+lv2 )  allow=.true.
             
        ! check if it is possible in exchange coulomb term
        ! -> do this by checking wether lo1,lo2 can couple to the same L
        !    as lv1,lv2
        lcmax = a_lo1+a_lo2
        lcmin = abs(a_lo1-a_lo2)
        if ( lcmax>=abs(lv1-lv2) .and. lcmin<=lv1+lv2 )  allow=.true.
        
        ! not allowed -> check next entry
        if (.not.allow)  cycle
        
        ! next possible lv2
        ! -----------------
        i=i+1

        if ( i>nlv2 ) then
          ! premature exit
          ! --------------
          ! too many possibilities 
          ! -> unassign everything and exit
          deallocate(a_this%matrix,a_this%lv2)
          a_this%nlv2 = 0
          return
        end if

        a_this%lv2(i,lv1) = lv2
      end do
    end do
  end subroutine init_coulomb


  elemental subroutine reset_coulomb(a_this)
    ! reset 
    implicit none
    type(t_mat_coulomb),intent(out) :: a_this

    a_this%nlv2 = 0
    a_this%lo1  = -1
    a_this%lo2  = -1
    
    if ( allocated(a_this%matrix) )  deallocate(a_this%matrix)
    if ( allocated(a_this%lv2)    )  deallocate(a_this%lv2)
  end subroutine reset_coulomb

end module VarMatrix


