module reduce_lapack
  ! --------------------------------------------------------------------------
  ! PURPOSE
  ! -------
  ! AUTHOR INFORMATION
  ! ------------------
  !  (c) and written by Arina Sytcheva, February 2013
  !
  !
  ! ----------------------------------------------------------------------
  !
  implicit none
  integer,parameter :: dp = kind(1.D0) , STDERR = 0 , STDOUT = 6
  real(dp),parameter :: ONE=1.D0,ZERO=0.D0
  complex(dp),parameter :: IMG= (ZERO, ONE)
  
  interface reduce_to_standard
    module procedure reduce_real, reduce_cmplx
  end interface

  interface reduce_decomposed
    module procedure reduce_decomposed_real, reduce_decomposed_cmplx
  end interface

contains 

  subroutine cholesky_decomp(a_O,a_U)

    ! Performs Cholesky decomposition for a symmetric positive-defined matrix
    !
    ! Lapack routine dpotrf is used:
    ! -----------------------------
    !
    ! INPUT / OUTPUT
    !-----------------
    !
    ! a_O     : (in) positive-defined matris
    !           normally overlap matrix  
    ! a_U     : (out) the upper triangular matrix containing the Cholesky factor U 
    !
    
    implicit none
    real(dp),intent(in)    :: a_O(:,:)
    real(dp),intent(inout)   :: a_U(:,:)
    
!   variables needed for Cholesky decomposition (LAPACK function dpotrf)
    character(len=1),parameter :: uplo = 'u'
    integer :: n
    real(dp),allocatable :: b(:,:)
    integer :: info

    n = size(a_O,1)
    
    if ( n < 1 ) then
      write(STDERR,*) " ERROR(cholesky_decomp): matrix order < 1 =>&
           & nothing to do"
      stop
    end if

    ! check dimensions
    if ( n /= size(a_O,2) ) stop &
         " ERROR(cholesky_decomp): a_O matrix is not a square matrix"


!   perform Cholesky decomposition

    allocate(b(n,n))
    b(1:n,1:n) = a_O(1:n,1:n)
    
    call dpotrf(uplo      &
               ,n         &
               ,b         &
               ,n         &
               ,info      &
                )
 

    if (info .ne. 0 ) then 
          print *, "ERROR(cholesky_decomp): Cholesky decomposition ended with info.", info
          stop
    end if

    a_U(1:n,1:n) = b(1:n,1:n) 

  end subroutine cholesky_decomp

!---------------------------------------------
!
!
!----------------------------------------------

  subroutine reduce_decomposed_real(a_H, a_U)

    ! Computes all eigenvalues, and optionally, all eigenvectors of a real generalized
    ! symmetric-defined eigenvalue problem of the form
    !
    !    H x = E O x
    ! 
    ! Lapack routine DSPGV is used:
    ! -----------------------------
    !
    ! INPUT / OUTPUT
    !-----------------
    ! a_H     : (in/out) 
    !           IN : matrix to be transformed
    !           OUT: transformed matrix inv(U^T)*H*inv(U)
    !
    ! a_U     : (in) 
    !           IN : upper triangular matrix, the result of Cholesky_decomp_real
    ! 
    ! 
    
    implicit none
    real(dp),intent(inout) :: a_H(:,:)
    real(dp),intent(in)    :: a_U(:,:)
    
    character(len=1),parameter :: uplo = 'u'
    integer :: n
    real(dp),allocatable :: b(:,:)
    integer :: info

!   additional variables needed for reduction (LAPACK function dsygst)
    integer,parameter     :: itype = 1 
    real(dp),allocatable           :: a(:,:)

    integer :: i,j

!    print *, "Enetered reduction module."
    n = size(a_H,1)

    if ( n < 1 ) then
      write(STDERR,*) " ERROR(Reduce_lapack: reduce_decomposed_real): matrix order < 1 =>&
           & nothing to do"
      stop
    end if

    ! check dimensions
    if ( n /= size(a_H,2) ) stop &
         " ERROR(Reduce_lapack: reduce_decomposed_real): a_H matrix is not a square matrix"
    
    if ( n /= size(a_U,1) ) stop &
         " ERROR(Reduce_lapack: reduce_decomposed_real): wrong size of a_U matrix "

    if ( n /= size(a_U,2) ) stop &
         " ERROR(Reduce_lapack: reduce_decomposed_real): a_U matrix is not a square matrix"


    allocate(b(n,n))
    b(1:n,1:n) = a_U(1:n,1:n)
        
!   reduce the generalized eigenvalue problem to the standard one
    allocate(a(n,n))
    a(1:n,1:n) = a_H(1:n,1:n)
    
    call dsygst(itype             &
                ,uplo             &
                ,n                &
                ,a                &
                ,n                &
                ,b                &
                ,n                &
                ,info             &
                )

   if (info .ne. 0 ) then 
          print *, "error (Reduce_lapack: reduce_real): eigenvalue problem reduction ended with info.", info
   end if

   a_H(1:n,1:n) = a(1:n,1:n)

! transform the eigen-vectors back to the original basis


   end subroutine reduce_decomposed_real

!---------------------------------------------
!
!
!----------------------------------------------
    
    subroutine reduce_real(a_H, a_O)

    ! Computes all eigenvalues, and optionally, all eigenvectors of a real generalized
    ! symmetric-defined eigenvalue problem of the form
    !
    !    H x = E O x
    ! 
    ! Lapack routine DSPGV is used:
    ! -----------------------------
    !
    ! INPUT / OUTPUT
    !-----------------
    ! a_H     : (in/out) 
    !           IN : matrix to be diagonalized
    !           OUT: right eigenstates (if wanted) ordered as eigenvalue
    !                second matrix index is vector index
    ! a_O     : (in/out) 
    !           IN : right-handside matrix
    !           OUT: 
    ! 
    ! a_eval : (out) 
    !           Eigenvalues ordered with increasing 
    !
    
    implicit none
    real(dp),intent(inout)    :: a_H(:,:)
    real(dp),intent(in)    :: a_O(:,:)
    
!   variables needed for Cholesky decomposition (LAPACK function dpotrf)
    character(len=1),parameter :: uplo = 'u'
    integer :: n
    real(dp),allocatable :: b(:,:)
    !integer :: ldb
    integer :: info

!   additional variables needed for reduction (LAPACK function dsygst)
    integer,parameter     :: itype = 1 
    real(dp),allocatable           :: a(:,:)

    integer :: i,j

    n = size(a_H,1)

    if ( n < 1 ) then
      write(STDERR,*) " ERROR(Reduce_lapack: reduce_real): matrix order < 1 =>&
           & nothing to do"
      stop
    end if

    ! check dimensions
    if ( n /= size(a_H,2) ) stop &
         " ERROR(Reduce_lapack: reduce_real): a_H matrix is not a square matrix"
    
    if ( n /= size(a_O,1) ) stop &
         " ERROR(Reduce_lapack: reduce_real): wrong size of a_O matrix "

    if ( n /= size(a_O,2) ) stop &
         " ERROR(Reduce_lapack: reduce_real): a_O matrix is not a square matrix"

!    print *, "Reduce_lapack: reduce_real:  size check was done" 

!   perform Cholesky decomposition
    !ldb = n
    allocate(b(n,n))
    b(1:n,1:n) = a_O(1:n,1:n)
    
    call dpotrf(uplo      &
               ,n         &
               ,b         &
               ,n         &
               ,info      &
                )
 

    if (info .ne. 0 ) then 
          print *, "error (Reduce_lapack: reduce_real): Cholesky decomposition ended with info.", info
          stop
    end if

!   reduce the generalized eigenvalue problem to the standard one
    allocate(a(n,n))
    a(1:n,1:n) = a_H(1:n,1:n)
    
    call dsygst(itype             &
                ,uplo             &
                ,n                &
                ,a                &
                ,n                &
                ,b                &
                ,n                &
                ,info             &
                )

   if (info .ne. 0 ) then 
          print *, "error (Reduce_lapack: reduce_real): eigenvalue problem reduction ended with info.", info
   end if

   a_H(1:n,1:n) = a(1:n,1:n)

   end subroutine reduce_real

!---------------------------------------------
!
!
!----------------------------------------------
  subroutine reduce_decomposed_cmplx(a_H, a_U)

    !
    ! reduces H X = lambda L^{+} L X eigenvalue problem to
    ! the standard form: 
    !      H (out) = L H L^{+}
    ! where L = a_U - upper triangular matrix
    !

    implicit none
    complex(dp),intent(inout)    :: a_H(:,:)
    real(dp),intent(inout)    :: a_U(:,:)
    
!   variables needed for Cholesky decomposition (LAPACK function dpotrf)
    character(len=1),parameter :: uplo = 'u'
    integer :: n
    complex(dp),allocatable :: b(:,:)
    integer :: info

!   additional variables needed for reduction (LAPACK function dsygst)
    integer,parameter     :: itype = 1 
    complex(dp),allocatable           :: a(:,:)
    
    integer :: i,j

    n = size(a_H,1)

    if ( n < 1 ) then
      write(STDERR,*) " ERROR(Reduce_lapack: reduce_decomposed_cmplx): matrix order < 1 =>&
           & nothing to do"
      stop
    end if

    ! check dimensions
    if ( n /= size(a_H,2) ) stop &
         " ERROR(Reduce_lapack: reduce_decomposed_cmplx): a_H matrix is not a square matrix"
    
    if ( n /= size(a_U,1) ) stop &
         " ERROR(Reduce_lapack: reduce_decomposed_cmplx): wrong size of a_O matrix "

    if ( n /= size(a_U,2) ) stop &
         " ERROR(Reduce_lapack: reduce_decomposed_cmplx): a_O matrix is not a square matrix" 

!   reduce the generalized eigenvalue problem to the standard one
    allocate(b(n,n))
    b(1:n,1:n) = a_U(1:n,1:n)

    allocate(a(n,n))
    a(1:n,1:n) = a_H(1:n,1:n)
    
    call zhegst(itype             &
                ,uplo             &
                ,n                &
                ,a                &
                ,n                &
                ,b                &
                ,n                &
                ,info             &
                )

   if (info .ne. 0 ) then 
          print *, "ERROR(Reduce_lapack::reduce_decomposed_cmplx) : eigenvalue problem reduction ended with info.", info
   end if

   !do i = 1,n
   !  do j =1,n
   !    a_H(i,j) = a(min(i,j), max(i,j))
   !  end do
   !end do
  
    a_H(1:n,1:n) = a(1:n,1:n)

   end subroutine reduce_decomposed_cmplx
!--------------------------------------------
!
!--------------------------------------------
  subroutine reduce_cmplx(a_H, a_O)

    ! Computes all eigenvalues, and optionally, all eigenvectors of a real generalized
    ! symmetric-defined eigenvalue problem of the form
    !
    !    H x = E O x
    ! 
    ! Lapack routine DSPGV is used:
    ! -----------------------------
    !
    ! INPUT / OUTPUT
    !-----------------
    ! a_H     : (in/out) 
    !           IN : matrix to be diagonalized
    !           OUT: right eigenstates (if wanted) ordered as eigenvalue
    !                second matrix index is vector index
    ! a_O     : (in/out) 
    !           IN : right-handside matrix
    !           OUT: 
    ! 
    ! a_eval : (out) 
    !           Eigenvalues ordered with increasing 
    !
    
    implicit none
    complex(dp),intent(inout)    :: a_H(:,:)
    complex(dp),intent(inout)    :: a_O(:,:)
    
!   variables needed for Cholesky decomposition (LAPACK function dpotrf)
    character(len=1),parameter :: uplo = 'u'
    integer :: n
    complex(dp),allocatable :: b(:,:)
    integer :: info

!   additional variables needed for reduction (LAPACK function dsygst)
    integer,parameter     :: itype = 1 
    complex(dp),allocatable           :: a(:,:)

    integer :: i,j

    n = size(a_H,1)

    if ( n < 1 ) then
      write(STDERR,*) " Error (Reduce_lapack: reduce_cmplx): matrix order < 1 =>&
           & nothing to do"
      stop
    end if

    ! check dimensions
    if ( n /= size(a_H,2) ) stop &
         " ERROR(Reduce_lapack: reduce_cmplx): a_H matrix is not a square matrix"
    
    if ( n /= size(a_O,1) ) stop &
         " ERROR(Reduce_lapack: reduce_cmplx): wrong size of a_O matrix "

    if ( n /= size(a_O,2) ) stop &
         " ERROR(Reduce_lapack: reduce_cmplx): a_O matrix is not a square matrix" 

!   perform Cholesky decomposition
    allocate(b(n,n))
    b(1:n,1:n) = a_O(1:n,1:n)
    call zpotrf(uplo      &
               ,n         &
               ,b         &
               ,n         &
               ,info      &
                )
 

    if (info .ne. 0 ) then 
          print *, "ERROR(Reduce_lapack: reduce_cmplx): Cholesky decomposition ended with info.", info
    end if

!   reduce the generalized eigenvalue problem to the standard one
    allocate(a(n,n))
    a(1:n,1:n) = a_H(1:n,1:n)
    
    call zhegst(itype             &
                ,uplo             &
                ,n                &
                ,a                &
                ,n                &
                ,b                &
                ,n                &
                ,info             &
                )

   if (info .ne. 0 ) then 
          print *, "ERROR(Reduce_lapack::reduce_cmplx) : eigenvalue problem reduction ended with info.", info
   end if

   a_H(1:n,1:n) = a(1:n,1:n)

   end subroutine reduce_cmplx

end module reduce_lapack
