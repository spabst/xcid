module gq_integrator
!
  !**PURPOSE
  !  -------
  !  The gq_integrators contains all necessary routines and function to 
  !  perfom gauss quadrature
  !
  !**DESCRIPTION
  !  -----------
  !  The following subroutines (sbr) and functions (fct) are part of 
  !  this module:
  !
  !  -> (interface) gq_caller - a caller interface
  !    -> (sbr) gq_caller_real - for real functions
  !    -> (sbr) gq_caller_cmplx - for complex functions
  !  -> (fct) gauss_quadrature_cmplx - Gauss-Lobatto complex integration
  !  -> (fct) gauss_quadrature - Gauss-Lobatto real integration
  !  -> (fct) coulomb_gq - 2D Coulomb integral 
  !  to check if the integration routine performs correctly:
  !  -> (fct) general_gq_cmplx - Gauss-Lobatto complex integration for a function
  !  -> (fct) general_gq - Gauss-Lobatto real integration for a real function
  !
  !**AUTHOR
  !  ------
  !  written by Arina Sytcheva and Stefan Pabst in June 2013
  !
  !**VERISON
  !  -------
  !  svn info: $Id: gq_integrator.f90 1660 2015-05-11 21:31:57Z spabst $
  !-----------------------------------------------------------------------
  !
  use constants
  use grid_type
  use fem_basis, only : idx_basis_reverse, fem_3refpoints
  use orbital, only : 

  implicit none

  interface
    function t_fct_cplx(r)
      integer, parameter :: dp=kind(1.d0)
      complex(dp),intent(in) :: r
      complex(dp) :: t_fct_cplx
    end function t_fct_cplx

    function t_fct_real(r)
      integer, parameter :: dp=kind(1.d0)
      real(dp),intent(in) :: r
      complex(dp) :: t_fct_real
    end function t_fct_real
  end interface

  interface gq_caller
    module procedure  gq_caller_cplx
  end interface gq_caller

  interface gq_int
    module procedure  gauss_quadrature_cplx
  end interface gq_int


contains


  subroutine gq_caller_cplx(a_matrix, a_grid, ao_pot , ao_deriv_r, ao_deriv_l, ao_rright, ao_rleft, ao_sym)
    implicit none

    complex(dblprec),intent(inout) :: a_matrix(:,:)
    type(t_grid),intent(in) :: a_grid
    real(dblprec),intent(in),optional :: ao_rleft, ao_rright
    logical     ,intent(in), optional :: ao_deriv_r,ao_deriv_l  ! applys to the right basis function
    logical, intent (in), optional :: ao_sym

    procedure(t_fct_real), optional :: ao_pot

    integer :: i, j
    logical :: sym
    

    a_matrix = ZERO

    sym = .false.
    if ( present(ao_sym) )  sym = ao_sym
    
    do i = 1, a_grid%nbasis
      do j = i, min(i+5, a_grid%nbasis)

        a_matrix(i,j) = gauss_quadrature_cplx(      &
                            i,j, a_grid, ao_pot     &
                            , ao_deriv_r,ao_deriv_l &
                            , ao_rright,ao_rleft)

        if ( sym ) then
          a_matrix(j,i) = a_matrix(i,j)
        else
          a_matrix(j,i) = gauss_quadrature_cplx(    &
                            j,i, a_grid, ao_pot     &
                            , ao_deriv_r,ao_deriv_l &
                            , ao_rright,ao_rleft)
        end if  
      end do
    end do

  end subroutine gq_caller_cplx


!--------------------------------------------------------------------
!
!--------------------------------------------------------------------


  function gauss_quadrature_cplx(a_idx1, a_idx2, a_grid, ao_pot , ao_deriv_r,ao_deriv_l , ao_rright,ao_rleft)  result(res)

    use fem_basis, only : fem_basisfct
    implicit none
    
    integer, intent(in) :: a_idx1,a_idx2  ! indicies of FEM function
    type(t_grid),intent(in) :: a_grid
    real(dblprec),intent(in), optional :: ao_rleft, ao_rright
    ! applys to the right basis function
    logical     ,intent(in), optional :: ao_deriv_l , ao_deriv_r 

    procedure(t_fct_real) , optional :: ao_pot

    complex(dblprec)      :: res


    logical :: deriv_r,deriv_l
    real(dblprec) :: rleft, rright
    
    integer :: a_gp1, a_gp2   ! central grid points of FEM function
    integer :: a_type1, a_type2  ! type of FEM function

    integer            :: k
    integer            :: npts
    integer, parameter :: degree_fe = 3
    real(dblprec)      :: xarr1(degree_fe), xarr2(degree_fe), &  ! arrays to initialize the finite elements
                          rsl, rsr 
    ! quadrature points on [-1,1] and integration weights
    real(dblprec),allocatable :: weights(:)
    ! quadrature points mapped to radial ray
    real(dblprec),allocatable :: pts(:)   
    complex(dblprec),allocatable :: wfn1(:), wfn2(:)  ! wave functions

    integer :: nseg,iseg


    res = 0.D0
    
    call idx_basis_reverse(a_idx1, a_gp1, a_type1, a_grid%nbasis) 
    call idx_basis_reverse(a_idx2, a_gp2, a_type2, a_grid%nbasis) 

    deriv_r = .false.
    if ( present(ao_deriv_r) ) then
      deriv_r = ao_deriv_r
    end if
    deriv_l = .false.
    if ( present(ao_deriv_l) ) then
      deriv_l = ao_deriv_l
    end if

    if ( abs(a_gp1-a_gp2) > 1) then
      return
    end if

    rleft = a_grid%rmin
    if ( present(ao_rleft) )   rleft = ao_rleft
    rright = a_grid%rmax
    if ( present(ao_rright) )  rright = ao_rright
    if (rleft .ge. rright) then
      return
    end if

    xarr1(1:3) = fem_3refpoints(a_gp1, a_grid)
    xarr2(1:3) = fem_3refpoints(a_gp2, a_grid)

    ! integration boundaries do not overlap with FEM functions
    if ( rright .le. min(xarr1(1), xarr2(1)) ) then
      return
    else if ( rleft  .ge. max(xarr1(3), xarr2(3)) ) then
      return
    end if

    if ( .not.associated(a_grid%FEM_intgrid) ) then
      stop " ERROR(gauss_quad): FEM_intrgrid is not generated" 
    end if
    
    npts = a_grid%FEM_intgrid%npoints 

    allocate (weights(npts) , pts(npts) , wfn1(npts) , wfn2(npts)  )
    weights = ZERO
    pts = ZERO 
    wfn1 = ZERO
    wfn2 = ZERO

   
    weights = ONE/(a_grid%FEM_intgrid%norm*a_grid%FEM_intgrid%A2u)**2

    ! loop through each segment of left (bra) FEM function
    lp_seg:do iseg=1,2 
      rsl = max(xarr1(iseg), rleft)
      rsr = min(xarr1(iseg+1), rright) 
      ! integration range is empty
      if ( rsl>=rsr ) cycle  
      ! make sure 2. FEM function is non-zero
      if ( rsl>=xarr2(3) .or. rsr<=xarr2(1) )  cycle
      

      pts = (rsr-rsl)*HALF * a_grid%FEM_intgrid%root + (rsr+rsl)*HALF 
      
      forall(k=1:npts) wfn1(k) = fem_basisfct(xarr1, a_type1, pts(k) , deriv_l)
      forall(k=1:npts) wfn2(k) = fem_basisfct(xarr2, a_type2, pts(k) , deriv_r)
  
      if ( present(ao_pot) ) then
        do k = 1, npts
          wfn1(k) = wfn1(k) * ao_pot( pts(k) )
        end do 
      end if

      res = res + (rsr-rsl)*HALF * sum(weights*wfn1*wfn2)
    end do lp_seg

  end function gauss_quadrature_cplx




  !-------------------------------------------------------------
  !  2D Coulomb integral with Gauss-Lobatto
  !-------------------------------------------------------------

  complex(dblprec) function gq_r12(a_idx, a_l, a_grid) result(res)
    !PURPOSE
    !-------
    !  Integrate radial 1/r12 2D integral over r1 and r2 for FEM function
    !
    !   /int dr1 dr2  phi_1(1)*phi_3(1) *  r_<^l/r_>(l+1) *  phi_2(2)*phi_4(2)
    !
    !   phi_i : where i is the index a_idx(i)
    !
    !DESCIRPTION
    !-----------
    !  - swap FEM indicies so that r1<r2 most the time 
    !    exception is of course when r1 and r2 integration region overlap
    !  - devide each radial integral into segments defined by the FEM points
    !
    !
    !IN/OUT
    !------
    !  a_idx : index of the FEM functions
    !  a_l   : Coulomb angular momentum
    !  a_gid : FEM grid
    !
    use fem_basis, only : fem_basisfct,overlap
    implicit none
    integer, parameter :: nseg=2  ! each FEM function expanse over 2 segments
    integer, intent(in) :: a_idx(4), a_l
    type(t_grid), intent(in) :: a_grid
    
    integer :: gp(4),typ(4)
    ! mode: type of overlap between r1 and r2 overlap
    !       = 0 : no overlap between r1 and r2 integrals
    !       = 1 : 1 FEM segmet overlaps
    !       = 2 : 2 FEM segmets overlap 
    !           (all FEM functions are centered around the same FEM point)
    integer :: mode  
    
    real(dblprec) :: xarr(nseg+1,4),rsl,rsr
    integer :: l,npts
    integer :: iseg1,iseg2,k
  
    ! quadrature points on [-1,1] and integration weights
    real(dblprec),allocatable :: weights(:)
    ! quadrature points mapped to radial ray
    real(dblprec),allocatable :: pts(:)   
    complex(dblprec),allocatable :: wfn1(:), wfn2(:)  ! wave functions
   
    integer :: idx(4)
    logical(dblprec) :: olap1d(nseg,2)  ! segment of phi_1/phi_2 has overlap with other FEM function (phi_3/phi_4)
    ! olap2d : segment of phi_1/phi_2 has overlap with the other radial integral
    !          idx1,idx2 : segment of phi_1 and phi_2, respectively
    logical(dblprec) :: olap2d(nseg,nseg)  
    complex(dblprec) :: int1d(nseg,2)   ! store 1D integrals of dr1 and dr2
    complex(dblprec) :: int2d(nseg,nseg)  ! store all possible 2D sub-integrals



    res = ZERO
    if ( a_l<0 )  return

    ! find FEM segment of each FEM function
    idx = a_idx
    do k=1,4
      call idx_basis_reverse(idx(k), gp(k), typ(k), a_grid%nbasis) 
      xarr(:,k) = fem_3refpoints(gp(k), a_grid)
    end do
    
    ! check idx1 and idx3 (idx2 and idx4) are overlapping
    if ( abs(gp(1)-gp(3))>1 )  return
    if ( abs(gp(2)-gp(4))>1 )  return
    
    ! reorder so that the common segments of idx1 and idx3 
    ! are closer to (or at least not furhter away from) the origin
    ! than of idx2 and idx4
    if ( min(gp(1),gp(3)) > min(gp(2),gp(4)) )  then
      ! idx1,idx3 -> idx2,idx4  and idx2,idx4 -> idx3,idx1
      idx  = cshift(idx,shift=-1)
      xarr = cshift(xarr,shift=-1,dim=2)
      typ  = cshift(typ,shift=-1)
      gp   = cshift(gp,shift=-1)

    else if ( max(gp(1),gp(3)) > max(gp(2),gp(4)) )  then
      ! furthest FEM reference point has to be in the r2 integration (idx2,idx4)
      ! this situation should only occur when min() are equal
      
      ! idx1,idx3 -> idx2,idx4  and idx2,idx4 -> idx3,idx1
      idx  = cshift(idx,shift=-1)
      xarr = cshift(xarr,shift=-1,dim=2)
      typ  = cshift(typ,shift=-1)
      gp   = cshift(gp,shift=-1)

    end if


    ! find overlapping segments
    ! r1 integration
    olap1d = .false.
    olap2d = .false.
    select case(gp(1)-gp(3))
    case(1)
      olap1d(1,1) = .true.
    case(0)
      olap1d(:,1) = .true.
    case(-1)
      olap1d(2,1) = .true.
    end select
    
    ! r2 integration
    select case(gp(2)-gp(4))
    case(1)
      olap1d(1,2) = .true.
    case(0)
      olap1d(:,2) = .true.
    case(-1)
      olap1d(2,2) = .true.
    end select
   
    ! r1-r2 integration (also check all FEM function overlap in this segment)
    select case(gp(2)-gp(1))
    case(1)
      olap2d(2,1) = olap1d(2,1) * olap1d(1,2)
    case(0)
      olap2d(1,1) = olap1d(1,1) * olap1d(1,2)
      olap2d(2,2) = olap1d(2,1) * olap1d(2,2)
    case(-1)
      olap2d(1,2) = olap1d(1,1) * olap1d(2,2)
    end select


    ! find overlap mode
    mode = count(olap2d) 

    
    ! FEM intragation arrays 
    npts = a_grid%FEM_intgrid%npoints 
    allocate (weights(npts) , pts(npts)) 
    weights = ONE/(a_grid%FEM_intgrid%norm*a_grid%FEM_intgrid%A2u)**2
    pts = ZERO 
    

    ! perform non-overlapping integrals
    int1d  = ZERO
    int2d  = ZERO
    select case(mode)
    case(0)
      int1d(1,1) = gq_int(idx(1), idx(3), a_grid, rs)
      int1d(1,2) = gq_int(idx(2), idx(4), a_grid, rg)
      res = product(int1d(1,1:2))
      return

    case(1)  ! -------------------

      do iseg1=1,nseg
        rsl = xarr(iseg1,1)
        rsr = xarr(iseg1+1,1)
        int1d(iseg1,1) = gq_int(idx(1), idx(3), a_grid, rs  &
                              , ao_rleft=rsl, ao_rright=rsr)
        rsl = xarr(iseg1,2)
        rsr = xarr(iseg1+1,2)
        int1d(iseg1,2) = gq_int(idx(2), idx(4), a_grid, rg  &
                              , ao_rleft=rsl, ao_rright=rsr)
      end do

    case(2)  ! ----------------

      rsl = xarr(1,1)
      rsr = xarr(2,1)
      int1d(1,1) = gq_int(idx(1), idx(3), a_grid, rs  &
                          , ao_rleft=rsl, ao_rright=rsr)
      rsl = xarr(2,1)
      rsr = xarr(3,1)
      int1d(2,1) = gq_int(idx(1), idx(3), a_grid, rg  &
                          , ao_rleft=rsl, ao_rright=rsr)
      
      rsl = xarr(1,2)
      rsr = xarr(2,2)
      int1d(1,2) = gq_int(idx(2), idx(4), a_grid, rs  &
                          , ao_rleft=rsl, ao_rright=rsr)
      rsl = xarr(2,2)
      rsr = xarr(3,2)
      int1d(2,2) = gq_int(idx(2), idx(4), a_grid, rg  &
                          , ao_rleft=rsl, ao_rright=rsr)
    end select


    ! calculate 2d integrals of non-overlapping segments 
    forall(iseg1=1:nseg,iseg2=1:nseg,.not.olap2d(iseg1,iseg2))
      int2d(iseg1,iseg2) = int1d(iseg1,1) * int1d(iseg2,2)
    end forall
  

    ! calculate 2d integral of overlapping segments
    ! ---------------------------------------------

    ! wfct arrays
    allocate(wfn1(npts) , wfn2(npts))
    wfn1 = ZERO
    wfn2 = ZERO


    ! loop through each segment of left (bra) FEM function
    lp_seg1:do iseg1=1,nseg
      if ( .not.any(olap2d(iseg1,:)) )   cycle   ! no overlap with r2 integration
      
      rsl = xarr(iseg1,1)
      rsr = xarr(iseg1+1,1)
      ! integration range is empty
      if ( rsl>=rsr ) cycle  
     
      pts = (rsr-rsl)*HALF * a_grid%FEM_intgrid%root + (rsr+rsl)*HALF 

      ! product of 1. and 3. function
      forall(k=1:npts) wfn1(k) = fem_basisfct(xarr(:,1), typ(1), pts(k) , .false.)  &
                                *fem_basisfct(xarr(:,3), typ(3), pts(k) , .false.)
  

      lp_seg2:do iseg2=1,nseg
        if ( .not.olap2d(iseg1,iseg2) )   cycle   ! no overlap with r2 integration
      
        ! integral over r2 with r2<=r1
        l = a_l
        do k=1,npts
          wfn2(k) = gauss_quadrature_cplx(idx(2), idx(4), a_grid, rsg , ao_rright=pts(k),ao_rleft=rsl)
        end do
        
        ! integral over r2 with r2>=r1
        l = -a_l-1
        do k=1,npts
          wfn2(k) = wfn2(k) + gauss_quadrature_cplx(idx(2), idx(4), a_grid, rsg , ao_rright=rsr,ao_rleft=pts(k))
        end do

        int2d(iseg1,iseg2) = (rsr-rsl)*HALF * sum(weights*wfn1*wfn2)
        
      end do lp_seg2
    end do lp_seg1

    res = sum(int2d)

  contains

    complex(dblprec) function rsg(a_r) result(res)
      implicit none
      real(dblprec), intent(in) :: a_r

      if ((l>=0.and.pts(k)/=ZERO) .or. (l<0.and.a_r/=ZERO)) then
        res = a_grid%ecs_path(a_r,0)**l          &
            * a_grid%ecs_path(pts(k),0)**(-l-1)
      else
        res = ZERO
      end if
    end function rsg
    
    complex(dblprec) function rs(a_r) result(res)
      implicit none
      real(dblprec), intent(in) :: a_r

      res = a_grid%ecs_path(a_r,0)**a_l
    end function rs
    
    complex(dblprec) function rg(a_r) result(res)
      implicit none
      real(dblprec), intent(in) :: a_r

      if (a_r/=ZERO) then
        res = ONE/a_grid%ecs_path(a_r,0)**(a_l+1)
      else
        res = ZERO
      end if
    end function rg
    
  end function gq_r12



!-----------------------------------------------------
!
!-----------------------------------------------------

end module gq_integrator
