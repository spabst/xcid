module Legendre
  !
  !**PURPOSE
  !  -------
  !  Routines related to Legendre polynomials
  !
  !**DESCRIPTION
  !  -----------
  !  The following functions (fct) and subroutines (sbr) are part of 
  !  this module:
  !   -> (fct) Legendre_P( n, x )     : Legendre polynomial P_n(x)
  !
  !   -> (fct) Legendre_dP( n, x )    : 1st derivative of P, P'_n(x)
  !
  !   -> (fct) normalized_Legendre_P( n, x ) : 
  !                                     sqrt((2n+1)/2)*P_n(x)
  !
  !   -> (fct) associated_Legendre_P( l, m, x ) : 
  !                                     associated Legendre poly. P_l^m(x)
  !
  !   -> (sbr) calculate_Legendre( n, x, p, dp ): 
  !                                     gives P and P'
  !
  !   -> (sbr) generate_LGL_points( x, N ) : 
  !                                     make grid points using P'_N+1(x)=0
  !                                     generate_LGL_points is rewritten with 
  !                                     Fortran 90 from Appendix C in Canuto, 
  !                                     Spectral Methods in Fluid Dynamics 
  !                                     (1988).
  !
  !**AUTHOR
  !  ------
  !  written by Son, Sang-Kil in Dec. 2003
  !  rewritten by Son, Sang-Kil in Feb. 2005 - May 2005
  !
  !
  !**VERSION
  !  -------
  !  svn info: $Id: Legendre.f90 337 2011-06-12 23:15:04Z spabst $
  !----------------------------------------------------------------
  !
  use constants
  implicit none
contains

elemental function Legendre_P(n, x) result (p)
!----------------------------------------------------------------------------
! Function to calculate the Legendre function of x with order n
!----------------------------------------------------------------------------
  integer, intent (in) :: n
  real(dblprec), intent (in) :: x
  real(dblprec) :: p, dp
  call calculate_Legendre(n, x, p, dp)
end function Legendre_P


elemental function normalized_Legendre_P(n, x) result (p)
!----------------------------------------------------------------------------
! Function to calculate the normalized Legendre function of x with order n
!----------------------------------------------------------------------------
  integer, intent (in) :: n
  real(dblprec), intent (in) :: x
  real(dblprec) :: p, dp
  p = Legendre_P(n, x) * sqrt((dble(n)+HALF))
end function normalized_Legendre_P


elemental function associated_Legendre_P( l, m, x ) result (p)
!----------------------------------------------------------------------------
! Function to compute the associated Legendre function of x with order l & m
! ref) Numerical Recipes in Fortran, pp 246-248
!     ( l-m ) P^m_l(x) = x ( 2l-1 ) P^m_(l-1)(x) - ( l+m-1 ) P^m_(l-2)(x)
!     P^m_m(x) = (-1)^m (2m-1)!! (1-x2)^(m/2)
!       ( n!! = 1 * 3 * 5 * ... * m   where m <= n )
!     P^m_(m+1)(x) = x ( 2m+1 ) P^m_m(x)
!----------------------------------------------------------------------------
  real(dblprec) :: p
  integer, intent(in) :: l, m
  real(dblprec), intent(in) :: x
  real(dblprec) :: fact, pll, pmm, pmmp1, somx2
  integer :: i, ll

  ! 0 <= m <= l and -1 <= x <= 1
  ! earlier: stop "Error in associate_Legendre_P" 
  ! stop not allowed for elemental function
  p = ZERO
  if ( m < 0 .or. m > l .or. abs(x) > ONE ) return
  
  pmm = ONE                                     ! compute P_m^m
  if ( m > 0 ) then
    somx2 = sqrt( ( ONE - x ) * ( ONE + x ) )
    fact = ONE
    do i = 1, m
      pmm = pmm * ( -fact * somx2 )
      fact = fact + TWO
    end do
  end if
  if ( l == m ) then
    p = pmm
  else
    pmmp1 = x * dble(2*m+1) * pmm               ! compute P_m+1^m
    if ( l == m + 1 ) then
      p = pmmp1
    else                                        ! compute P_l^m, l > m + 1
      do ll = m + 2, l
        pll = ( x * dble(2*ll-1) * pmmp1 - dble(ll+m-1) * pmm ) / dble(ll-m)
        pmm = pmmp1
        pmmp1 = pll
      end do
      p = pll
    end if
  end if
end function associated_Legendre_P


elemental function Legendre_dP(n, x) result (dp)
!----------------------------------------------------------------------------
! Function to calculate 1st derivative of the Legendre f. of x with order n
!----------------------------------------------------------------------------
  integer, intent (in) :: n
  real(dblprec), intent (in) :: x
  real(dblprec) :: p, dp
  call calculate_Legendre(n, x, p, dp)
end function Legendre_dP


elemental subroutine calculate_Legendre(n, x, p, dp)
!----------------------------------------------------------------------------
! Calculate the value of the Legendre polynomial and its derivative
!   input : n = the order of poly., x = the point to be calculated
!   results :  p = P_n(x),  dp = P'_n(x)
!----------------------------------------------------------------------------
  implicit none
  integer, intent (in) :: n
  real(dblprec), intent (in) :: x
  real(dblprec), intent (out) :: p, dp
  real(dblprec) :: p0, p1, p2, dp0, dp1, dp2
  integer :: i
  !
  ! set initial values
  !
  p0 = ONE;    dp0 = ZERO
  p1 = x;      dp1 = ONE
  select case (n)
    case (0)
      p = p0;  dp = dp0
    case (1)
      p = p1;  dp = dp1
    case default
      do i = 2, n
        !
        ! compute P and P' by recursive relations
        ! - (2n+1) x P_n(x) = (n+1) P_n+1(x) + n P_n-1(x)
        !                                          x P_n-1(x) - P_n-2(x)
        !   --> P_n(x) = 2 x P_n-1(x) - P_n-2(x) - ---------------------
        !                                                    n
        !   To avoid undue accumulation and magnification of round-off error
        !   ref) Arfken and Weber, Mathematical Methods for Physicists, 5th ed.
        ! - P'_n+1(x) + P'_n-1(x) = 2 x P'_n(x) + P_n(x)
        !   --> P'_n(x) = - P'_n-2(x) + 2 x P'_n-1(x) + P_n-1(x)
        !
        ! calculate new values of n from n-1 and n-2
        !   p0 : P_n-2,  dp0 : P'_n-2
        !   p1 : P_n-1,  dp1 : P'_n-1
        !   p2 : P_n,    dp2 : P'_n
        !
        p2 = TWO * x * p1 - p0 - (x * p1 - p0) / dble(i)
        dp2 = - dp0 + TWO * x * dp1 + p1
        !
        ! replace old values with new values
        !
        p0 = p1;  dp0 = dp1;
        p1 = p2;  dp1 = dp2;
      end do
      p = p2;  dp = dp2
  end select
end subroutine calculate_Legendre


subroutine generate_LGL_points( x, N )
!----------------------------------------------------------------------------
! Find the mesh points (collocation points) using the Legendre polynomial
!   x_j (j=1,2,...,N) : the solutions of P'_N+1(x) = 0
! Finally, we can get N points (x(1), x(2), .., x(N)) except -1 and 1.
!----------------------------------------------------------------------------
  real(dblprec), intent(out) :: x(:)
  integer, intent(in) :: N
  real(dblprec) :: xx, del_x
  integer :: N1, NH
  integer, parameter :: KSTOP = 10
  real(dblprec) :: dth, cd, sd, cs, ss, cssave
  real(dblprec) :: p, dp, ddp, q, dq, recsum
  integer :: i, j, k
  N1 = N + 1
  NH = N / 2            ! for example, 11 / 2 = 5
  !
  ! set up initial values
  !   PI = 4.0*atan(1.0)
  !
  dth = PI / dble(2*N1 + 1)
  cd = cos(TWO * dth)
  sd = sin(TWO * dth)
  cs = cos(dth)
  ss = sin(dth)
  !
  ! This procedure is derived from Appendix C (Canuto, 1988).
  ! The original uses q(x) = P_N+1(x) + a P_N(x) + b P_N-1(x) = 0.
  ! However, here I use P'_N(x) = 0 (exactly, P'_N+1(x) = 0).
  ! And you may pay attention to distinguish N and N+1.
  !
  do i = 1, NH
    xx = cs
    do k = 1, KSTOP
      ! get P'_N+1(x)
      call calculate_Legendre(N1, xx, p, dp)
      ! calculate P''_N+1(x)
      ddp = ( 2 * xx * dp - N1*(N1+1) * p ) / (1 - xx**2)
      ! we want to solve q(x) = P'_N+1(x)
      q = dp
      dq = ddp
      !
      ! calculate the reciprocal summation of previous x values
      !      recsum = 0.0
      !      do j = 1, i-1
      !        recsum = recsum + 1.0 / (xx - x(j))
      !      end do
      !
      recsum = sum( ONE / ( xx - x(1:i-1) ) )
      !
      ! This technique is modified Newton-Raphson method for root-polishing.
      ! It needs the first deriv. (dq) and the reciprocal summation (recsum).
      !                                 q(x_k)
      ! x_k+1 = x_k - ------------------------------------------
      !               q'(x_k) - q(x_k) SUM_i=1^j 1/{ x_k - x_i }
      !
      del_x = -q / (dq - recsum * q)
      xx = xx + del_x
      if ( abs( del_x ) < EPS ) exit
    end do
    x(i) = xx
    cssave = cs * cd - ss * sd
    ss = cs * sd + ss * cd
    cs = cssave
  end do
  !
  ! use symmetry for second half of the roots
  !
  do i = 1, NH
    x(N1-i) = x(i)
    x(i) = -x(i)        ! reverse order: it makes the ascending order for r
  end do
  !
  ! if N is odd, zero is also the root of P'_N+1(x)=0
  !
  if ( modulo( N, 2 ) == 1 ) x(NH+1) = ZERO
end subroutine generate_LGL_points

end module Legendre
