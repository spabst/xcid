program TDCIS_program
  !
  ! ----------------------------------------------------------
  ! | PROGRAM TDCIS                                          |
  ! |  performs the time propagation of the CIS coefficient  |
  ! |  and calculates the ion density matrix and expectation |
  ! |  values.                                               |
  ! |  Specific options can be controlled by the input file. |
  ! ----------------------------------------------------------
  !
  ! AUTHOR
  ! -------
  ! (c) and written by Stefan Pabst
  !
  ! VERSION
  ! -------
  ! Version info: $Id: TDCIS_program.f90 2127 2015-11-11 17:02:15Z spabst $
  !
  use constants , dp => dblprec
  use basics, only : timeinfo
  use svn, only    : m_projectrev
  use VarSystem, only : g_program=>m_program,m_svnrev,m_diagCIS
  use diagCIS, only   : diag_CIS_Ham
  implicit none
  integer :: g_tp(8,3)=0

  call date_and_time(values=g_tp(:,1))

  ! TDCIS propagation program is running
  g_program = 2

  ! pass on-the-fly determined global svn version
  ! to module "general"
  m_svnrev = m_projectrev

  ! ---------------
  ! | preparation |
  ! ---------------
  call prepare
  call date_and_time(values=g_tp(:,2))


  
  if ( m_diagCIS ) then
    ! diagonalization
    !----------------
     
    call diag_CIS_Ham 

    !----------------
  else   
    ! propagation
    ! ------------
    print *
    print *, " start time propagation"
    print *, " ----------------------"  
    call propagation
  end if


  ! final time
  ! -----------
  call date_and_time(values=g_tp(:,3))
  call append_time(g_tp)
  call timeinfo(g_tp)
  print *
  print *, "---------------------------------------------------"
  print *, "Time propagation is done and hopes for good results"
  print *, "---------------------------------------------------"
  print *, "Theory guides. Experiment decides. [Unkown]"
  print *


contains


  subroutine propagation
    use varsystem,only :  m_dt,m_tmax,m_dtprint,m_propmode,m_split_dt      &
                         ,m_storewfct,m_backup,m_fwfct,m_fwfctsplit        &
                         ,m_efield,m_npulse,m_split_write                  &
                         ,m_absorb

    use VarWfct, only  : m_wfct,m_wfctprev,m_wfctsplit

    use tdcis,only : IDMcorr_update,observables=>CalcWriteExptVal, calc_idm &
                     ,splitwfct
    use write,only : write_backup
    use propmethods,only : ecm,iecm,rk4,fdiff2,lanarn

    use general, only : printinfo,printvariables
    use inout, only   : opendir
    use basics, only  : numberformat
    use pulse_type
    implicit none

    integer :: nprint,iaux,it
    integer :: ntmax,ntmin,ntprint,ntbackup,ntsplit
    real(dp) :: time,tmin,tmax
    character(LEN=clen) :: outform,file
    type(t_pulse), pointer :: pulse => null()



    ! find time interval [tmin:tmax]
    ! -------------------------------
    !
    !!! NOTE: m_tmax = tmax-tmin 
    !
    ! -> m_tmax=-1 :  tmax is defined by pulse duration
    ! ----------------------------------------
    ! find the relevant time window of the pulses
    tmin = HUGE(ZERO)  ! starting time 
    tmax = -HUGE(ZERO)
    do it=1,m_npulse
      pulse => m_efield(it)
      if ( pulse%rdfile) then
        tmin = min(tmin,minval(pulse%time)+pulse%t0)
        tmax = max(tmax,maxval(pulse%time)+pulse%t0)
      else if (pulse%strength>ZERO) then
        tmin = min(tmin,-2*pulse%duration + pulse%t0)
        tmax = max(tmax,+2*pulse%duration + pulse%t0)
      end if
    end do
    
    ! overwrite tmin
    if ( m_backup%read) then
      tmin = m_wfct%time
    elseif (tmin>tmax) then ! no pulse provided
      tmin=ZERO
    end if
   
    ! overwrite tmax
    if ( m_tmax/=-ONE)  then
      tmax=m_tmax+tmin
    end if

    if ( tmax-tmin <= ZERO )  then
      write(STDERR,"(2(a,ES11.3))") " ERROR: t_final =",tmax," , t_start=",tmin
      stop " ERROR: no propagation needed (t_final=t_start+t_max <= t_start)"
    end if


    ! find time discretization
    ! -------------------------
    ! m_dtprint should be already a multiple of m_dt
    !  -> see subroutine postprocess
    ntprint = int(m_dtprint/m_dt)

    ! shift starting time
    ntmin = floor(tmin/m_dtprint) * ntprint
    ! do time steps last value that is printed 
    ntmax = ceiling(tmax/m_dtprint)  * ntprint

    ! backup steps ( m_backup%dt should be already a multiple of m_dt)
    !  -> see subroutine readSysPara
    ntbackup = int(m_backup%dt/m_dt)

    ! absorption steps with splitting function
    ntsplit = int(m_split_dt/m_dt)


    ! setup format used tp print time information
    ! -------------------------------------------
    nprint=(ntmax-ntmin)/ntprint
    iaux= max(abs(ntmax),abs(ntmin))
    ! determine size needed for display integer/float numbers
    write(outform,'(3a)') "(2("//trim(numberformat("I",one*nprint))//",a),"   &
                        , trim(numberformat("F",m_dt*iaux,ao_small=m_dtprint))&
                        , ")"


    ! ----------------
    ! time propagation
    ! -----------------
    time_prop:do it=ntmin,ntmax
       time = it*m_dt 

       m_wfct%time = time
       m_wfctsplit%time = time

       ! calculate IDM if time is a multiple of ntprint or ntbackup
       ! -> uncorrected IDM gets only build when needed (not every time step)
       if ( mod(it-ntmin,ntprint)==0  .or. mod(it-ntmin,ntbackup)==0 ) then
         m_wfct%idm = calc_idm(m_wfct)  ! calculate uncorrect IDM
       end if

       ! print expectation value
       ! print very first point 
       ! -> needed for correlation function when cross sections are calculated
       !if ( mod(it-ntmin,ntprint)==0 .and. it>ntmin ) then
       if ( mod(it-ntmin,ntprint)==0 ) then
          write(STDOUT,outform)  (it-ntmin)/ntprint," of"  &
             ,nprint," time step => times [a.u.] = ",time
          !flush(STDOUT) 
          
          call observables(m_wfct)          
       end if
       

       ! backup and store wfct
       !  -> in dt_backup steps and the last time step 
       !  -> skip first point at the beginning of the propagation (it>itmin)
       if ( mod(it-ntmin,ntbackup)==0 .or. it==ntmax ) then

         ! store backup (overwrite old file)
         if ( m_backup%write .and. it>ntmin ) then
           call write_backup( m_backup%file_write , m_wfct ) 
         end if

         ! store wfct (each time in a new file)
         if ( m_storewfct .and. mod(it-ntmin,ntbackup)==0 ) then
           write(file,'('//numberformat("I",ONE*(ntmax-ntmin)/ntbackup)//')')   &
               (it-ntmin)/ntbackup
           file = trim(m_fwfct)//"_T"//trim(adjustl(file))
           call write_backup( file , m_wfct )
         end if
       end if


       ! split and store wavefunction
       if ( trim(m_absorb) == "SPL") then 
         ! absorption by splitting function for integer multiples of ntsplit  
         if ( mod(it-ntmin,ntsplit)==0 .and. it>ntmin ) then
           call splitwfct(m_wfct,m_wfctsplit)  
           if (m_split_write) then  
             write(file,'('//numberformat("I",ONE*(ntmax-ntmin)/ntsplit)//')')  &
                 (it-ntmin)/ntsplit
             file = trim(m_fwfctsplit)//"_T"//trim(adjustl(file))
             call write_backup(file, m_wfctsplit)   
           end if
         end if
       end if


       ! last time step: no further action is needed
       if ( it==ntmax ) exit


       ! time step : time -> time + dt
       ! -----------------------------
       select case(m_propmode)
       case(0) ! Euler-Cauchy-Method
          call ecm(time,m_dt,m_wfct%alpha_0,m_wfct%alpha_ai)
       case(1) ! improved Euler-Cauchy-Method
          call iecm(time,m_dt,m_wfct%alpha_0,m_wfct%alpha_ai)
       case(11)! 2nd order finite differencing
          call fdiff2( time,m_dt,m_wfct%alpha_0,m_wfct%alpha_ai  &
                      ,m_wfctprev%alpha_0,m_wfctprev%alpha_ai    &
                     )
       case(2) ! 4th order Runge Kutta
          call rk4(time,m_dt,m_wfct%alpha_0,m_wfct%alpha_ai)
       case(3) ! Lanczos-Arnoldi
          call lanarn(time,m_dt,m_wfct%alpha_0,m_wfct%alpha_ai)
       case default
          print *, "propagation: propagation ID = ",m_propmode
          stop " propagation: propagation method ID unknown"
       end select
      
       ! update IDM (perform time integral)
       ! -> see Eq. 29 in Greenman, PRA 82, 023406 (2011)
       if ( m_absorb /= "SPL") then
         call IDMcorr_update(m_wfct,m_dt)
       end if

    end do time_prop

  end subroutine propagation




  subroutine prepare
    use varsystem,only : m_dt,m_dtprint                         &
         ,m_lmax,m_lsum,m_lprx,m_nocoulomb                      &
         ,m_afield,m_efield,m_fieldtype                         &
         ,m_fenergy,m_backup                                    &
         ,m_absorb,m_cap_strength,m_grid                        &
         ,m_corrfct, m_tsurf                                    &
         ,m_focc,m_forb                                         &
         ,m_refstates_file,m_refstates_use,m_refstates_rootname &
         ,m_refstates_segment

    use varwfct,only : m_lomax,m_actoccorb,m_orbital

    use general,only : printinfo,defaultvalue,init_idoccrev     &
         ,m_idocc=>idocc,m_isoact=>isact,m_isocc=>isocc         &
         ,printvariables,assign_active_orbitals                 &
         ,assign_active_orbitals_file

    use basics, only : getsequence
    use grid, only: initialize_grid
    use read,only : readoccupied,readenergy,readsystempara,readorb
    use pulse, only : read_pulse
    use readmatrices,only : read1p1hmatrices,read2p2hmatrices,init_mweight
    use tdcis,only : init_wavefunction
    implicit none

    character(clen) :: input
    integer  :: i
    
    type(t_string), allocatable :: string(:),string2(:)
    integer, allocatable :: idx(:)

    ! Header
    ! ------
    call printinfo(g_program,ao_header=-1)


    ! input file / flag option
    ! ------------------------
    call getarg(1,input)
    
    
    ! show help and exit program
    if ( trim(input) == "-v" ) then
      write(STDOUT,'(a)') " usage: tdcis.x [input file / -v]" 
      stop
    end if


    ! read global variables
    !-----------------------
    call defaultvalue 
    call readsystempara(input)

    if ( trim(m_absorb) == "ECS" ) then
      m_cap_strength = ONE
    end if
  

    ! print important variables 
    !  -> mode=2 : output is designed for time propagation
    call printinfo(g_program,STDOUT,ao_header=0)


    if ( m_dt      < zero) stop " ERROR: dt<0"
    if ( m_dtprint < zero) stop " ERROR: dtprint<0"

    ! initialize grid
    ! -> needed for t-surf
    if ( m_tsurf ) then
      call initialize_grid(m_grid)
    end if

    ! read orbital informations
    ! --------------------------
    ! define occupied orbitals and prepare related fields
    call readoccupied(m_focc)
    if (m_lomax>m_lmax) stop " WARNING: max. occupied l > lmax"
    

    ! read in complex orbital energy
    ! ------------------------------
    ! -> need to be after initialize_grid to reset m_nmax,m_nmaxl
    write(STDOUT,'(2a)') "reading energy file... ",trim(m_fenergy)
    call readenergy(m_fenergy)


    ! find all active occupied orbitals
    ! ----------------------------------
    if ( m_backup%read .and. m_backup%actorbfile ) then
      ! active occupied orbital information in backup file is used
      call assign_active_orbitals_file(m_backup%file_read)
    else
      call assign_active_orbitals(m_actoccorb)
    end if

    ! initialize idoccactrev
    ! ------------------------------
    call init_idoccrev

    ! read orbitals
    ! -> needed for t-surf
    if ( m_tsurf ) then
      write(STDOUT,'(2a)') "reading orbital file... ",trim(m_forb)
      call readorb(m_forb,m_orbital,m_grid,m_lmax)
    end if


    ! reference states
    ! ----------------
    ! generate all file names and store them in m_refstate_file
    if ( m_refstates_use ) then
      call getsequence(m_refstates_segment,idx)

      if ( allocated(idx) ) then  ! sequence is provided on input
        allocate(m_refstates_file(size(idx)))
 
        do i=1,size(idx)
          write(input,*) idx(i)
          m_refstates_file(i)%txt=trim(m_refstates_rootname)//trim(adjustl(input))
        end do

      else  ! no sequence provided -> use jsut rootname
        allocate(m_refstates_file(1))
        m_refstates_file(1)%txt=trim(m_refstates_rootname)
      end if

    end if


    ! pulse shape
    ! -----------
    call read_pulse


    ! print important running parameter
    ! ------------------------------
    call printvariables


    ! read matrix elements
    ! ----------------------
    ! generate Clebsch-Gordon ratios
    ! s.t. only m=0 1p1h entries need to be stored
    call init_mweight
   
    ! read matrix elements: z,a,w,vc
    ! ------------------------------
    print *
    print *, "reading 1p1h matrices"
    call read1p1hmatrices


    ! read matrix elements: coulomb
    ! ------------------------------
    if (.not.m_nocoulomb) then
       print *
       print *, "reading 2p2h matrices"
       call read2p2hmatrices
    end if

    ! initialize wavefunction arrays
    ! ------------------------------
    ! -> after matrix elements are read because
    !    dipsymm is needed if correlation function should be calculated
    call init_wavefunction(m_backup,m_corrfct)


    ! write header information into all output files
    ! ------------------------------
    call writeheaders

    write(STDOUT,*)
  end subroutine prepare
  
 
  ! ----------------------------------------------
  ! ----------------------------------------------


  subroutine writeheaders
    !
    ! write headers into all output files
    !
    use VarSystem, only : m_lmax , m_SOcoupl               &
         ,m_storeIDMcapL , m_storeEV , m_ev                &
         ,m_fev , m_frho1 , m_frho2 , m_fcorr              &
         ,m_fIDev , m_fIDrho1 , m_fIDrho2 , m_fIDrho2l     &
         ,m_fIDcorr , m_corrfct                            &
         ,m_backup

    use VarWfct,only    : m_ioccact,m_idoar=>m_idoccactrev

    use inout,only : opendir
    use general,only : printinfo,printvariables
    implicit none

    integer :: l,i,fID
    character(clen) :: txt
    character(10) :: append


    ! local variables
    ! ----------------
    l=0
    i=0

    ! append only when a previous run is continued
    append = ""
    if ( m_backup%read .and. m_backup%append ) append="append"

    ! ------------------
    ! open output files
    ! ------------------

    ! expectation values
    ! ------------------
    fID = m_fIDev
    call opendir(fID,m_fev,position=append)  ! expectation values 
    call printinfo(2,fID,'#')
    call printvariables(1,fID,'#')
    write(fID,'(a,3(/,a))',advance="no")  "#"                                &
     ,"# This file stores the laser field and optionally expectation values" &
     ,"# All values are given in atomic units."                              &
     ,"# COLUMNS: time | "
    
    if ( m_ev(1) ) write(fID,'(a)',advance="no") " dipole moment | "
    if ( m_ev(2) ) write(fID,'(a)',advance="no") " ionic dipole moment | "
    if ( m_ev(3) ) write(fID,'(a)',advance="no") " kinetic momentum | "
    if ( m_ev(4) ) write(fID,'(a)',advance="no") " dipole acceleration | "
 
    write(fID,'(a)',advance="yes") " electric field | vector potential"


    ! ion densisty matrices
    ! ----------------------
    fID = m_fIDrho1
    call opendir(fID,m_frho1,position=append)  ! save ion density matrix
    call printinfo(2,fID,'#')
    call printvariables(1,fID,'#')
    write(fID,'(a,3(/,a))')  "#"                                             &
     ,"# This file stores the CAP-corrected ion density matrix elements."    &
     ,"# Time is given in atomic units."                                     &
     ,"# COLUMNS: time  |  rho_0  |  (rho_ij, i<=j, m_i==m_j)"


    fID = m_fIDrho2
    call opendir(fID,m_frho2,position=append)  ! save ion density matrix
    call printinfo(2,fID,'#')
    call printvariables(1,fID,'#')
    write(fID,'(a,3(/,a))')  "#"                                            &
         ,"# This file stores the CAP correction of the &
             &ion density matrix elements."                                 &
         ,"# Time is given in atomic units."                                &
         ,"# COLUMNS: time  |  rho_0  |  (rho_ij, i<=j, m_i==m_j)"


    if (m_storeIDMcapL) then
      do l=0,m_lmax
        fID = m_fIDrho2l + l
        write(txt,'(i3.3)') l
        write(txt,'(2a)') "_L",trim(adjustl(txt))
        call opendir(fID,file=trim(m_frho2)//trim(txt),position=append)
        call printinfo(2,fID,'#')
        call printvariables(1,fID,'#')
        write(fID,'(a,3(/,a))')  "#"                                         &
          ,"# This file stores the CAP-correction of the ion density matrix."&
          ,"# Time is given in atomic units."                                &
          ,"# COLUMNS: time  |  rho_0  | (rho_ij, i<=j, m_i==m_j)"
      end do
    end if

   
    ! correlation function
    ! --------------------
    if ( m_corrfct ) then 
      fID = m_fIDcorr
      call opendir(fID,m_fcorr,position=append)  ! save ion density matrix
      call printinfo(2,fID,'#')
      call printvariables(1,fID,'#')
      write(fID,'(a,3(/,a))')  "#"                              &
        ,"# This file stores the correlation function corr(t) = &
            &<HF| z G(t,0) z |HF>."                             &
        ,"# Time is given in atomic units."                     &
        ,"# COLUMNS: time  |  corr(t)"
    end if

  end subroutine writeheaders


  ! ----------------------------------------------
  ! ----------------------------------------------


  subroutine append_time(a_tp)
    use varsystem, only : m_fIDev,m_fIDrho1,m_fIDrho2,m_fIDrho2l,m_fIDwfct   &
                          ,m_fIDcorr

    implicit none
    integer, intent(in) :: a_tp(:,:)
    logical :: opend ! opened
    integer :: i,ID

    ID = m_fIDev
    inquire(ID,opened=opend)
    if ( opend ) call timeinfo(a_tp,ID,"#")

    ID = m_fIDrho1
    inquire(ID,opened=opend)
    if ( opend ) call timeinfo(a_tp,ID,"#")

    ID = m_fIDrho2
    inquire(ID,opened=opend)
    if ( opend ) call timeinfo(a_tp,ID,"#")

    ID = m_fIDwfct
    inquire(ID,opened=opend)
    if ( opend ) call timeinfo(a_tp,ID,"#")
    
    ID = m_fIDcorr
    inquire(ID,opened=opend)
    if ( opend ) call timeinfo(a_tp,ID,"#")

    ! l-resolved CAP
    opend=.true.
    i=0
    do while ( opend )
      ID = m_fIDrho2l+i
      inquire(ID,opened=opend)
      if ( opend ) call timeinfo(a_tp,ID,"#")

      i = i +1
    end do
    
  end subroutine append_time

end program TDCIS_program
