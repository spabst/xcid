module orbital
  !
  !**PURPOSE
  !  -------
  !
  !**DESCRIPTION
  !  -----------
  !  Contains functions and rountine that operate on the orbitals.
  !  The following subroutines (sbr) and functions (fct) are part of 
  !  this module:
  !
  !  -> (sbr) init_orbital  : Initialize the object m_orbital
  !  -> (sbr) reset_orbital : reset all values and deallocate/nullify all arrays/pointers
  !  -> (fct) get_orbital_radial_all:
  !                           calculate the radial orbital wfct at all points
  !  -> (fct) get_orbital_radial_gp:
  !                           calculate the radial orbital wfct at one point
  ! 
  ! 
  !**AUTHOR
  !  ------
  !  (c) and written by Stefan Pabst
  !
  !**VERSION
  !  -------
  !  svn info: $Id: orbital.f90 1658 2015-05-08 21:46:13Z spabst $
  !-----------------------------------------------------------------------
  !
  use constants, dp=>dblprec
  use varwfct, only : t_orbital
  implicit none
  
  ! local variables used to evaluate orbitals on grid points
  real(dp), allocatable, private :: m_dvrbasis(:,:,:)
  logical, allocatable, private :: m_eval(:,:) ! <- check of grid point is already evalutated


  interface get_orbital_radial
    module procedure  get_orbital_radial_all,get_orbital_radial_gp
  end interface get_orbital_radial

contains


  subroutine init_orbital(a_orb,a_mode,ao_n,ao_l)
    ! initialize 1-particle orbital array
    ! 
    ! INPUT
    !------
    ! orb      : orbital object
    !
    ! mode = 0 : initialize all except spatial array "wfct" [default]
    !            Note : This saves unecessary memory space 
    !                   (which can be large for high lmax)
    !      = 1 : initialize wfct (or everything if nothing is allocated yet)
    ! 
    ! n,l      : INTEGER - in - optional
    !            only allocate wfct for the (n,l) orbital
    !
    use varsystem,only : m_grid,m_lmax
    use grid_type
    implicit none
    
    type(t_orbital), allocatable, intent(inout) :: a_orb(:,:)
    integer,intent(in) :: a_mode
    integer,intent(in),optional :: ao_n,ao_l

    logical :: preallo,allo
    integer :: n,l

    ! check if m_orbital is already allocated in mode=1
    ! if m_orbital is preallocated only allocate wfct
    preallo = allocated(a_orb)

    if (present(ao_n) .neqv. present(ao_l))  &
        stop " error (init_orbital): (n,l) pair must be completely passed"

    if (present(ao_n) .and. a_mode == 0)   &
        stop " error (init_orbital): (n,l) pair is given in wrong mode"

    ! check input
    if (a_mode/=0 .and. a_mode/=1) &
       stop " error (init_orbital): invalid mode!!"

    ! deallocate if allocated
    if (a_mode==0) then
      if (allocated(a_orb))  deallocate(a_orb)
      preallo = .false.
    end if

    ! --- allocating ---
    if (.not.preallo) allocate(a_orb(0:m_grid%nbasis-1,0:m_lmax))

    do n=0,m_grid%nbasis-1
       do l=0,m_lmax

         ! set values to zero if object is new
         if (.not.preallo) then
           a_orb(n,l)%n = n
           a_orb(n,l)%l = l
           a_orb(n,l)%occ = 0
           a_orb(n,l)%energy = ZERO
           a_orb(n,l)%energy_so = ZERO
           a_orb(n,l)%grid => m_grid
           a_orb(n,l)%nbasis = 0
         end if
          

         ! initialize wave function array
         allo_wfct:if (a_mode==1) then
           allo = .true.

           ! find (n,l) orbital if specified
           if (present(ao_n)) then
             if (ao_n/=n .or. ao_l/=l) allo=.false.
           end if

           if ( allo ) then
             if (allocated(a_orb(n,l)%wfct))  deallocate(a_orb(n,l)%wfct)         
             allocate(a_orb(n,l)%wfct(m_grid%nbasis))
             a_orb(n,l)%wfct = ZERO
             a_orb(n,l)%nbasis = m_grid%nbasis
           end if
         end if allo_wfct

       end do
    end do
  end subroutine init_orbital


  elemental subroutine reset_orbital(a_orb)
    use varwfct, only  : t_orbital
    implicit none
    type(t_orbital),intent(inout) :: a_orb

    a_orb%n = -1
    a_orb%l = -1
    a_orb%occ = 0
    a_orb%energy = ZERO
    a_orb%energy_so = ZERO
    a_orb%nbasis = 0
    if ( associated(a_orb%grid) )   a_orb%grid => null()
    if ( allocated(a_orb%wfct)  )   deallocate(a_orb%wfct)
  end subroutine reset_orbital




  function get_orbital_radial_all(a_orbital) result(res)
    ! radial orbital wfct u(r) at all grid point
    implicit none
    type(t_orbital),intent(in) :: a_orbital
    complex(dp), allocatable :: res(:)

    integer :: n


    n=size(a_orbital%wfct)
    if ( n<1 ) return   ! no wfct objt exist

    allocate(res(n))
    do n=1,size(res)
      res(n) = get_orbital_radial_gp(a_orbital, n)
    end do
  end function get_orbital_radial_all



  complex(dp) function get_orbital_radial_gp(a_orbital, a_gridpoint, ao_deriv) result(res)
    !
    ! radial orbital wfct u(r) at a grid point a_gridpoint 
    ! of the initial grid defined by the input file
    !
    !  ao_deriv : get 1. derivative  (otherwise function itself)
    !
    use grid_type
    use FEM_basis, only: idx_basis
    use varwfct, only : t_orbital
    use varmatrix,only : m_femdvr_trafo
    implicit none

    type(t_orbital),intent(in) :: a_orbital
    integer,intent(in)     :: a_gridpoint
    logical,intent(in), optional :: ao_deriv

    integer :: npoints, gp, nbasis
    ! DVR specific 
    integer :: i, ideriv

    real(dp), allocatable :: bf(:)
    type(t_grid), pointer :: grid => null()
    logical :: deriv

    gp = a_gridpoint
    grid => a_orbital%grid
    npoints = grid%npoints
    nbasis = grid%nbasis
    
    deriv=.false.
    if ( present(ao_deriv) )  deriv = ao_deriv



    select case(a_orbital%grid%type)
      case("PSG")
        if ( deriv )  return   ! derivative 
        res=a_orbital%wfct(gp)*a_orbital%grid%A2u(gp)

      case("FEM")
        if ( gp==1 .or. gp==npoints ) then
          res = CZERO
        else
          if (.not.deriv) then
            ! FEM function at FEM grid point is 1
            i = idx_basis(gp,1,npoints)
            res = a_orbital%wfct(i)
          else
            i = idx_basis(gp,2,npoints)
            res = a_orbital%wfct(i) * get_fem(grid%r(gp),i,.true.,grid)
          end if
        end if

      case("DVR")
        
        if ( .not.allocated(m_dvrbasis) ) then
          ! generate DVR basis
          allocate(m_dvrbasis(nbasis,npoints,2),m_eval(npoints,2))
          m_dvrbasis = ZERO
          m_eval = .false.
        end if

        ideriv = 1
        if ( deriv )  ideriv = 2

        if ( .not.m_eval(gp,ideriv) ) then
          allocate(bf(nbasis))
          bf = ZERO
          
          ! get value of all FEM basisfcts at the grid point
          do i = 1, nbasis
            bf(i) = get_fem(grid%r(gp), i, deriv, grid)
            !! old way
            !! find FEM index
            !call idx_basis_reverse(i, j, fndx, nbasis)
            !xarr = basis_function_array(j,a_orbital%grid%DVR_grid)
            !
            !! get FEM basisfct at the grid point
            !bf(i) = fem_basisfct(xarr, fndx, a_orbital%grid%r(gp), .false.)
          end do 

          ! get DVR basis at gridpoint
          do i = 1, nbasis
            m_dvrbasis(i,gp,ideriv) = sum(m_femdvr_trafo(:,i)*bf(:))
          end do
          m_eval(gp,ideriv) = .true.
        end if
        
        ! evaluate orbital in the DVR basis
        res = sum(a_orbital%wfct(:)*m_dvrbasis(:,gp,ideriv))
    end select

  end function get_orbital_radial_gp



  real(dp) elemental function get_fem(a_r,a_i,ao_deriv,a_grid) result(res)
    use grid_type
    use FEM_basis, only: fem_basisfct, idx_basis_reverse, fem_3refpoints
    implicit none
    real(dp), intent(in) :: a_r
    integer, intent(in) :: a_i
    logical,intent(in), optional :: ao_deriv
    type(t_grid),intent(in) :: a_grid

    integer  :: j,fndx
    real(dp) :: xarr(3)
    logical  :: deriv


    res = ZERO

    deriv=.false.
    if ( present(ao_deriv) )  deriv = ao_deriv
  

    ! find FEM index
    call idx_basis_reverse(a_i, j, fndx, a_grid%nbasis)

    ! get 3 grid points defining the FEM function
    select case(a_grid%type)
    case('DVR')
      xarr = fem_3refpoints(j,a_grid%DVR_grid)
    case('FEM')
      xarr = fem_3refpoints(j,a_grid)
    case default
      return
    end select
   
    ! get FEM basisfct at the grid point
    res = fem_basisfct(xarr, fndx, a_r, deriv)
  end function get_fem



  function get_orbital_overlap(a_orb1,a_orb2,ao_mode) result(res)
    ! DESCRIPTION
    ! -----------
    ! calculate the overlap between two one-partical orbitals
    !  -> standard inner porduct to be used is the symmetric one
    !     since Hamiltonian is complex symmetric

    ! INPUT
    ! -----
    ! a_orb1,a_orb2 : TYPE(t_orbitals)
    !                 one-partical orbitals
    ! ao_mode       : CHARACTER - OPTIONAL
    !                 = 'herm' : hermitian inner product
    !                 = 'symm' : symmetric inner product [default]
    use varmatrix, only :  olap=>m_fem_ov
    implicit none
    type(t_orbital),intent(in) :: a_orb1,a_orb2
    character(*), intent(in), optional :: ao_mode
    complex(dp) :: res

    character(clen) :: mode
    complex(dp),allocatable :: orb(:)
    real(dp) :: norm
    integer :: n

    res = ZERO
    
    mode = 'symm'
    if ( present(ao_mode) )  mode = trim(ao_mode)
    
    ! integration norm/weight: only relevant for PSG which should be N(N+1)/2
    norm = a_orb1%grid%norm  

    if ( size(a_orb1%wfct) /= size(a_orb2%wfct) )  stop " ERROR(get_orbital_overlap): different dimensions of wfct"
    if ( size(a_orb1%wfct)==0 )  stop " ERROR(get_orbital_overlap): wfct has 0 size"
    n = size(a_orb1%wfct)

    allocate(orb(n))
    orb = ZERO

    ! prepare bra state
    select case(mode)
    case('symm')
      orb = a_orb1%wfct
    case('herm')
      orb = conjg(a_orb1%wfct)
    case default
      stop " ERROR(get_orbital_overlap): invalid mode"
    end select

    ! perform sum
    select case(a_orb1%grid%type)
    case('PSG','DVR')
      res = sum(orb * a_orb2%wfct) / norm**2
    case('FEM')
      res = sum( orb(1:n) * matmul(a_orb2%wfct(1:n),olap(1:n,1:n)) )
    end select
  end function get_orbital_overlap



  function get_orbital_norm(a_orb) result(res)
    ! calculate norm of orbital (according to symmetric inner product)
    implicit none
    type(t_orbital),intent(in) :: a_orb
    complex(dp) :: res
    
    res = get_orbital_overlap(a_orb,a_orb)
  end function get_orbital_norm

end module orbital
