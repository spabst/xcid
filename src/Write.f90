module Write
  !
  !**PURPOSE
  !  Subroutines for writting information (mainly wfct) into files.
  !
  !**DESCRIPTION
  !  -----------
  !  The following subroutines (sbr) and functions (fct) are part of
  !  this module:
  !
  !  -> (sbr) store_IDMs:    write corrected IDM and IDM correction in the
  !                          corresponding file
  !
  !  -> (sbr) write_IDM :    write all IDM entries in one line
  !
  !  -> (sbr) write_backup :
  !                          write a snapshot of the current time in a file
  !                          store IDM, time, and wavefunction
  !
  !  -> (sbr) write_wavefunction :
  !                          store alpha coefficients for each time a block
  !                          in corresponding file
  !
  !**AUTHOR
  !  ------
  !  (c) and written by Stefan Pabst
  !
  !**VERSION
  !  -------
  !  svn info: $Id: Write.f90 1316 2014-10-05 22:22:01Z spabst $
  !-------------------------------------------------------------
  !
  use Constants
  implicit none

contains

  subroutine store_IDMs ( a_wfct , a_t )
    ! PURPOSE
    ! -------
    ! write reduced density matrices into files
    !
    use varsystem,only : m_lmax                           &
                        ,m_fIDrho1,m_fIDrho2,m_fIDrho2l

    use varwfct, only  : t_wfct
    implicit none

    type(t_wfct), intent(in) :: a_wfct
    real(dblprec), intent(in) :: a_t
    integer :: l,t


    ! print corrected IDM
    ! -> see Eq. 30 in Greenman, PRA 82, 023406 (2011)
    write(m_fIDrho1,'(2ES18.8E3)',advance="no") a_t,abs(a_wfct%alpha_0)**2
    call write_IDM( m_fIDrho1 , a_wfct%idm+a_wfct%idmcorr )

    ! print IDM correction
    !  -> see Eq. 29 in Greenman, PRA 82, 023406 (2011)
    write(m_fIDrho2,'(2ES18.8E3)',advance="no") a_t,abs(a_wfct%alpha_0)**2
    call write_IDM( m_fIDrho2 , a_wfct%idmcorr )


    ! start new line in IDM absorption detail ffiles
    ! write absorption detail to file (L resovled)
    if (a_wfct%store_IDMcorrL) then
       do l=0,m_lmax
          write(m_fIDrho2l+l,'(2ES18.8E3)',advance="no") a_t,abs(a_wfct%alpha_0)**2
          call write_IDM( m_fIDrho2l+l ,a_wfct%idmcorrL(:,:,l) )
       end do
    end if

  end subroutine store_IDMs


  subroutine write_IDM ( a_fid , a_idm , ao_mode , ao_str )
    !
    !**PURPOSE
    !  -------
    !   Print ion density matrix in one line
    !
    !**INPUT / OUTPUT
    !  --------------
    !  a_fid   : INTEGER - IN
    !            output unit/ID
    !
    !  a_idm   : COMPLEX_dp(:,:) - IN
    !            Ion Density Matrix rho_ij
    !
    !  ao_mode : INTEGER - IN - OPTIONAL
    !            = 1 : j>=i , sigma_j = sigma_i
    !                  [default]
    !            = 2 : j,i  , sigma_j = sigma_i
    !            = 3 : loop through all
    !
    !  ao_str  : CHARACTER(*) - IN - OPTIONAL
    !            characters/symbols put in front of each line
    !
    use VarWfct, only : m_ioccact,m_occact=>m_idoccactrev
    implicit none

    integer, intent(in) :: a_fid
    complex(dblprec), intent(in) :: a_idm(:,:)

    integer, intent(in), optional      :: ao_mode
    character(*), intent(in), optional :: ao_str

    integer :: mode
    character(clen) :: str
    integer :: i,j



    if ( size(a_idm,1)<m_ioccact .or. size(a_idm,2)<m_ioccact )  then
      !write(STDERR,*) " ERROR(write_IDM): size of density matrix is too small -> IDM is not stored"
      return
    end if


    mode = 1
    if ( present(ao_mode) ) mode = ao_mode
    str = ""
    if ( present(ao_str) ) str = adjustl(ao_str)


    ! beginning of line
    write( a_fID , '(a,3X)' , advance="no")  trim(str)

    do i=1,m_ioccact
      do j=1,m_ioccact

        if ( mode == 1 .and. j<i ) cycle
        if ( mode == 1 .or. mode == 2 ) then
          if ( m_occact(i)%mj /= m_occact(j)%mj ) cycle
        end if

        write( a_fID , '(2X,2ES17.8E3)' , advance="no")  a_idm(i,j)
      end do
    end do
    write( a_fID , *)

  end subroutine write_IDM



  subroutine write_backup( a_file , a_this , ao_energy )
    !
    !**PURPOSE
    !  write backup file
    !
    !**INPUT/OUTPUT
    !
    !  a_file : file name
    !  a_this : wfct-object containing all wavefunction informations
    !
    use varwfct, only   : t_wfct
    use general, only   : printinfo,printvariables
    use inout,   only   : opendir,find_openpipe
    implicit none

    character(*), intent(in) :: a_file
    type(t_wfct),intent(in) :: a_this
    complex(dblprec), intent(in), optional :: ao_energy

    integer :: fid
    logical :: ex

    fid = find_openpipe()

    call opendir(fID,a_file)
    call printinfo(2,fID,"#")
    call printvariables(1,fID,'#','#ORB')

    write(fID,'("#",/a)') "# IDM CAP-correction"
    call write_IDM( fid , a_this%idmcorr , 3 , "##IDM")

    if ( present(ao_energy) )  then
      write(fID,'("#",/,"##ENERGY ",2ES18.10)') ao_energy
    end if


    call write_wavefunction( a_this%time , a_this%alpha_0 , a_this%alpha_ai , fID)
    close(fID)
  end subroutine write_backup



  subroutine write_wavefunction( a_time , a_alpha0 , a_alpha_ai , ao_fID )
    !
    ! write all wavefunction coefficients in the pipe/unit (ao_fID).
    ! if ao_fID not given then m_fIDwfct is used.
    ! The alpha coeff. for a given time a_time is stored in a block.
    ! The head of the block has the time information.
    ! The information is stored in the format:
    ! n_a , l_a , 2*ms_a , ID_i , alpha^a_i.
    ! ms_a=0 when SOcoupl is not activated.
    !
    ! For alpha_0, n_a=l_a=ID_i=-1.
    !
    use varsystem,only : m_fIDwfct,m_lmax,m_nmax,m_SOcoupl
    use varwfct,only   : m_1p1hr=>m_id1p1hclassrev , m_i1p1hclass
    implicit none

    real(dblprec),intent(in) :: a_time
    complex(dblprec),intent(in) :: a_alpha0,a_alpha_ai(0:,0:,1:)
    integer,intent(in),optional :: ao_fID

    integer :: n,l,i,fID


    fID = m_fIDwfct
    if ( present(ao_fID) ) fID = ao_fID

    ! print alpha_0 coefficient
    write(fID,'("#",/,"##T time [a.u.] = ",ES19.10E3)') a_time
    
    write(fID,'(2("#",/),a)') "# COLUMNS: n_a , l_a , 2*ms_a &
                              &, 1p-1h ID , CI-coefficient"
    write(fID,'(2(a,/))')                                         &
        "# NOTE: ms_a=0, when spin-orbit interaction is not activated."    &
       ,"# NOTE: n_a = l_a = occ.ID = -1, when it is referred to HF ground &
                &state configuration"
    
    write(fID,'(i5,3i4,2ES24.15E3)') -1,-1,-1,-1,a_alpha0


    do i=1,m_i1p1hclass
      if ( i > ubound(a_alpha_ai,3) ) stop " ERROR(write_wfct): i index gets too large"
      do l=0,m_lmax
        if ( l > ubound(a_alpha_ai,2) ) stop " ERROR(write_wfct): l index gets too large"
        do n=0,m_nmax
          if ( n > ubound(a_alpha_ai,1) ) stop " ERROR(write_wfct): n index gets too large"

          if ( a_alpha_ai(n,l,i) /= ZERO ) then
            write(fID,'(i5,3i4,2ES24.15E3)')                  &
                      n,l,m_1p1hr(i)%ms                       &
                     ,m_1p1hr(i)%idhole, a_alpha_ai(n,l,i)
          end if

        end do
      end do
    end do

  end subroutine write_wavefunction

end module Write
