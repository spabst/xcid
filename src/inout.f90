module inout
  !
  !**PURPOSE
  !  --------
  !  Collection of basics read/write routines
  !
  !** DESCRIPTION
  !  -----------
  !  The following subroutines (sbr) and functions (fct) are part of 
  !  this module:
  !
  !  -> (fct) find_openpipe: find an open pipe
  !
  !  -> (sbr) inquireopen  : check file existence and open it
  !
  !  -> (sbr) opendir      : open file and create directory if necessary
  !
  !  -> (sbr) find_allfiles: find list of files matching a defined pattern
  !
  !  -> (fct) flength        : count lines of given file may ignore empty
  !                            and comment lines
  !
  !  -> (sbr) readfile     : read function from file
  !
  !  -> (sbr) flaginput    : create input file from flag options
  !
  !
  !**AUTHOR
  !  ------
  !  (c) and written by Stefan Pabst
  !
  !**VERISON
  !  -------
  !  svn info: $Id: inout.f90 1662 2015-05-11 21:57:00Z spabst $
  ! ---------------------------------------------------------------------
  !
  use constants, dp=>dblprec
  implicit none
  
  interface readfile
    module procedure  readfile_fct,readfile_fcts
  end interface readfile

contains

  function find_openpipe( ao_min , ao_max ) Result(fid)
    !
    !**PURPOSE
    !  Find an open pipe.
    !**DESCRIPTION
    !  ao_min and ao_max limit the range if provided
    !   -> standard range is from 20 to 9999
    !
    implicit none
    integer, intent(in), optional :: ao_min,ao_max
    integer :: fid

    integer :: id,imin,imax
    logical :: info

    fid = -1

    imin=20
    if ( present(ao_min) ) imin=abs(ao_min)
    imax=9999
    if ( present(ao_max) ) imax=abs(ao_max)
    info = .false.


    do id=imin,imax
      inquire(id,opened=info)

      ! non-opend pipe found
      if ( .not.info ) then
        fid = id
        return
      end if
    end do

  end function find_openpipe


  subroutine inquireopen(a_stat,a_fid,a_fname,ao_action)
    ! check if file exists and if it does open it
    !
    ! PARAMETER
    ! ---------
    ! stat  - (out)
    !         file existence 
    ! fid   - (in)
    !         file id the file should be linked to
    ! fname - (in) 
    !         file name
    ! action- (in) - optional 
    !         action argument for open call.
    !         readwrite is used as default.
    !
    implicit none
    logical,intent(inout) :: a_stat
    integer,intent(in) :: a_fid
    character(*),intent(in),optional :: ao_action
    character(*),intent(in) :: a_fname

    character(20) :: act
    
    act = "readwrite"
    if (present(ao_action) ) then
      if ( len_trim(ao_action) /= 0 ) act = ao_action
    end if
    
    inquire(file=a_fname , exist=a_stat)
    if (a_stat) then
      open(a_fid,file=a_fname,action=act,status="old")
    end if
  end subroutine inquireopen

  
  subroutine opendir(id,file,status,position,form)
    use ifport
    !
    ! create directory in which the file is if it doesn't exist
    ! -> need it only for new files -> want to write something 
    !    action=read option would not make sense
    !
    ! PARAMETER
    ! ---------
    ! id   - (in)
    !        unit id for open,read,write,close
    ! file - (in) 
    !        file name
    ! status   - CHARACTER - IN - OPTIONAL
    !            status option in open command
    !            default : "unknown" (also if status is empty)
    ! position - CHARACTER - IN - OPTIONAL
    !            position option in open command
    !            default : "asis" (also if position is empty)
    ! 
    !
    implicit none
    integer,intent(in) :: id
    character(*),intent(in) :: file
    character(*),intent(in),optional :: status,position,form

    character(300) :: dir,txt
    character(20) :: i_status,i_position,i_form
    integer :: pos, aux
    logical :: res


    ! check if directories are involved 
    !pos = scan (file, "/" , .true. )
    pos = scan (file, "/" , .false. )

    !if ( pos /= 0 ) then
    txt = file
    aux = 0
    posdo: do 
      if (pos /= 0 ) then
        aux = aux + pos 
        dir = file(1:aux)

        ! check existence

        !txt = " mkdir -p "//trim(dir)
        !call system (txt)  <- cause somehow in a few cases problems

        res = makedirqq(trim(dir))
        if ( .not.res ) then  ! not successfully created
          if  (  GETLASTERRORQQ() == ERR$NOENT ) stop "ERROR(inout): path does not exist"
          if  (  GETLASTERRORQQ() == ERR$ACCES ) stop "ERROR(inout): permission to denied"
        end if

        txt = file(aux+1:len(trim(file)))
        pos = scan ( txt, "/", .false.)
      else
        exit posdo
      end if
    end do posdo
    !end if
    
    ! set default values (FORTRAN default values)
    i_status = "unknown"
    if ( present(status)   ) then 
      if ( len_trim(status) /= 0 )   i_status   = adjustl(status)
    end if
    
    i_position = "asis"
    if ( present(position) ) then
      if ( len_trim(position) /= 0 ) i_position = adjustl(position)
    end if
    
    i_form = "formatted"
    if ( present(form) ) then
      if ( len_trim(form) /= 0 ) i_form = adjustl(form)
    end if

    ! no directory
    open( id , file=file , status=i_status , position=i_position , form=i_form)
  end subroutine opendir



  subroutine find_allfiles( a_froot , a_file , a_mode )
   !
   !**PURPOSE
   !  Find list of files containing in their name the
   !  character sequence defined in a_front.
   !**DESCRIPTION
   !  Utilize the shell program "ls" to find all files.
   !  File list is written in the file a_file.
   !  a_mode defines wether character string should appear
   !  at the beginning (="b") or the end (="e") of the file name
   !
    implicit none
    character(*), intent(in) :: a_froot,a_file
    character, intent(in) :: a_mode


    select case (a_mode)
    case("e")
      call system( "ls  *"//trim(a_froot)//" > "//trim(a_file) )
    case("b")
      call system( "ls  "//trim(a_froot)//"* > "//trim(a_file) )
    case default
      stop " ERROR(find_allfiles): mode is invalid (only 'e' and 'b' is allowed)"
    end select

  end subroutine find_allfiles



  integer function flength(a_file,ao_mode,ao_com) Result(num)
    !
    ! find number of lines in the file "a_file" but skip comment lines
    ! starting with the string ao_com
    !   ao_mode = "E" : skip empty lines
    !           = "N" : skip no lines  [default]
    !
    implicit none
    character(*),intent(in) :: a_file

    character,intent(in),optional :: ao_mode
    character(*),intent(in), optional :: ao_com

    character :: mode
    character(5000) :: txt

    integer :: fid
    integer :: iread,sz_com
    logical :: da,offen



    mode = "N"
    if ( present(ao_mode) ) mode = ao_mode

    txt=mode
    select case(trim(txt))
    case("E","N")
    case("e")
      mode="E"
    case("n")
      mode="N"
    case default
      stop " ERROR(flength): invalid mode"
    end select
    txt=""


    ! size of line header that defines whether a line is a comment line
    sz_com = 0
    if ( present(ao_com) ) sz_com = len_trim(adjustl(ao_com))


    ! check file exist
    num=0
    inquire(file=a_file,exist=da,opened=offen)
    if (.not.da) then
       write(STDERR,'(3a)') "flength: file (",trim(a_file),") doesn't exist"
       return
    end if


    ! open file
    ! ----------
    !  -> if file is already open then start in line 1
    !     and don't close file after length is found
    if ( offen ) then
      inquire(file=a_file,number=fid)
      rewind(fid)
    else
      ! find open/free pipe
      fid = find_openpipe()
      open(fid,file=a_file,action="read")
    end if


    ! open file
    ! ----------
    iread=0
    ! loop through file
    do while(iread==0)
       txt=""
       read(fid,"(a)",iostat=iread) txt
       if (iread/=0) exit

       txt=trim(adjustl(txt))

       ! skip comment lines
       if ( present(ao_com) .and. sz_com<=len_trim(txt) ) then
         if ( trim(txt(1:sz_com)).eq.trim(ao_com) ) cycle
       end if

       ! skip empty lines
       if ( mode=="E" .and. len_trim(txt)==0  ) cycle

       num=num+1
    end do

    ! check reading status and close file
    if (iread>0) then
       print *, "flength: error during reading ",trim(a_file)," occurred"
       stop
    end if

    ! close or rewind file
    if ( offen ) then
      rewind(fid)
    else
      close(fid)
    end if

  end function flength


  ! -------------------------------------------------------------------
  ! -------------------------------------------------------------------


  subroutine readfile_fct( a_fid , a_x , ao_y , ao_com , ao_col )
    !
    !**PURPOSE
    !  -------
    !  Interface to read one function from file with the
    !  subroutine "readfile_fcts", which reads several functions
    !  from a file. Since a_y has rank 1 and "readfile_fcts"
    !  requires a rank 2 array this routine acts as an interface.
    !
    implicit none

    integer, intent(in) :: a_fid
    real(dp), intent(inout) :: a_x(:)
    real(dp), intent(inout),optional :: ao_y(:)
    character(*), intent(in), optional :: ao_com
    integer, intent(in), optional :: ao_col

    integer :: n,col
    real(dp), allocatable :: y(:,:)


    if ( present(ao_y) )  then
      n=size(ao_y)
      col = 2
    else
      n = size(a_x)
      col = 1
    end if
    if ( present(ao_col) )  col=ao_col

    allocate( y(n,1) )
    y = ZERO
    
    
    if ( .not.present(ao_y) .and. col/=1 ) then
      stop " ERROR(readfile_fct): only x should be read but it is not the 1. column"
    end if

    ! read file
    call readfile_fcts( a_fid, a_x , y , ao_com , col )

    if ( present(ao_y) ) then
      ao_y = y(:,1)
    end if

  end subroutine readfile_fct


  subroutine readfile_fcts( a_fid , a_x , a_y , ao_com , ao_col1 , ao_col2 )
    !
    !**PURPOSE
    !  -------
    !  Read several 1D functions from file 
    !
    !**DESCRIPTION
    !  -----------
    !  Comments lines starting with the characters defined in ao_com
    !  are ignored. With ao_col1/2 it is possible to read simultaneously
    !  several columns.
    !    -> 1. column    :  a_x 
    !    -> n.-m. column : a_y(:,1:m-n)
    !
    !  If the file is already open then continue reading the file.
    !  Reading will be stopped when a comment or blank line is encountered
    !  after the data part started. 
    !
    !**INPUT/OUTPUT
    !  ------------
    !
    !    a_fid  : INTEGER - IN
    !             file unit/pipe ID  ( file must be already opened)
    !
    !    a_x    : REAL(nx) - INOUT
    !             arguments of functions
    !             nx = size of array
    !
    !    a_y    : REAL(ny1,ny2) - INOUT
    !             functions
    !              ny1 = size of functions ( should be the same as nx
    !                                        but is not impossed )
    !              ny2 = number of functions ( > ao_col2-ao_col1 )
    !
    !   ao_com  : CHARACTER(*) - IN - OPTIONAL
    !             character string that defines a comment line
    !             [ default : "#" ]
    !
    !  ao_col1/2: INTEGER - IN - OPTIONAL
    !             columns of 1. and last functions
    !              -> if ao_col=1 then only a_x is read
    !              -> if ao_col2 is not provided it is set to ao_col1
    !               ao_col1 : this column stored in a_y(:,1)
    !               ao_col2 : this column stored in a_y(:,1+ao_col2-ao_col1) 
    !
    implicit none
  
    integer, intent(in) :: a_fid
    real(dp), intent(inout) :: a_x(:),a_y(:,:)

    character(*), intent(in), optional :: ao_com
    integer, intent(in), optional :: ao_col1,ao_col2

    character(clen) :: com
    character(500*clen) :: line   ! should be 1E5 characters long
    
    real(dp), allocatable :: aux(:)

    integer :: col(2),ncol,ncom,nx,ny(2)
    integer :: i,j,info,recinfo

    logical :: opnd


    inquire(a_fid,opened=opnd)
    if ( .not.opnd ) then
      write(STDERR,*) " ERROR(readfile): file unit/pipe",a_fid," is not opened"
      stop
    end if
  
    ! check optional input parameter and use default value when
    ! arguments are not provided through the input
    com = "#"
    if ( present(ao_com) ) com = adjustl(trim(ao_com))
    ncom = len_trim(com)

    col = 1
    if ( present(ao_col2) .and. .not.present(ao_col1) ) then
      stop " ERROR(readfile): last but not first column is defined"
    end if
    if ( present(ao_col1) ) col    = ao_col1-1
    if ( present(ao_col2) ) col(2) = ao_col2-1

    if ( col(1)<0 )       stop " ERROR(readfile): column number is negative"
    if ( col(2)<col(1) )  stop " ERROR(readfile): first column no. > last column no."


    ! check sizes
    nx = size(a_x)
    ny = shape(a_y)
    if ( nx>ny(1) ) stop " ERROR(readfile): array for x is smaller than for y"

    ncol = col(2)-col(1) + 1
    if ( ny(2) < ncol )   &
      stop "ERROR(readfile): more data columns recorded than &
                            &the data object can store"

    ! aux is not needed if only first column is read
    if ( col(2)>0 ) then
      allocate(aux(1:col(2)))
      aux = ZERO
    end if
    

    ! reading file
    ! ------------
    ! first, skip all comment and empty lines
    info = 0
    read_header: do while (info == 0 )

      read(a_fid,'(a)',iostat=info) line
      if ( info /= 0 ) exit
      line = adjustl(line)

      ! get out of loop when line is not a comment or empty line
      if ( line(1:ncom) /= trim(com) .and. len_trim(line)>0 ) exit
    end do read_header
    backspace(a_fid)

    if ( info < 0 ) then
      return  ! normal loop exit (end of file)
    elseif ( info > 0 ) then
      stop " ERROR(readfile): error in reading of header"
    end if


    ! read data
    !  -> comment or empty line stops the reading
    !  -> stop reading when end of data object is reached
    i=0
    info = 0
    read_data: do while ( info == 0 .and. i<min(nx,ny(1)) )
      i = i+1
      
      ! read line and check that it isn't a comment or empty line
      read(a_fid,'(a)',iostat=info) line
      if ( info /= 0 ) exit
      line=adjustl(line)
      if ( line(1:ncom) == trim(com) .or. len_trim(line)==0 ) exit
      

      ! read x and first "col2" columns
      if ( col(2)>0 ) then
        read(line,*,iostat=recinfo) a_x(i) , (aux(j), j=1,col(2))
      else
        read(line,*,iostat=recinfo) a_x(i) 
      end if

      
      ! line is too short
      if ( recinfo < 0 ) then
        write(STDERR,*) " ERROR(readfile): number of columns in a line is &
                                          &too low"

        write(STDERR,*) " file ID   = ",a_fid
        write(STDERR,*) " last line = ",trim(line)
        stop
      end if 

      if ( len(line)-len_trim(line) < 10 ) then
         write(STDERR,*) "WARNING(readorb): file line reaches max. length of &
                                  & internal string length (=10^5 characters)"
      end if


      ! store just "col1:col2" columns
      if ( col(2)>0 ) then
        a_y( i , 1:ncol ) = aux( col(1):col(2) ) 
      end if
    end do read_data
    

    ! check for errors
    if ( info > 0 ) then
      stop " ERROR(readfile): error in reading data"
    end if

    if ( i > min(nx,ny(1)) ) then
      write(STDERR,*) " WARNING(readfile): data record is longer &
                                          &than internal object"
    end if

  end subroutine readfile_fcts



  subroutine flaginput(a_nml,a_fid,a_stat)
    !
    ! PURPOSE
    ! --------
    ! Turn flag options of the command line into a formatted input file.
    !
    ! INPUT
    ! -----
    !  a_nml  : CHARACTER - in 
    !           name of the namelist
    !  a_fid  : INTEGER - in
    !           unit ID of temporary file 
    !           (status=scratch <- no file name needed)
    !  a_stat : LOGICAL - out
    !           false = unsuccessful exit
    !           true  =   successful exit
    !
    ! DESCRIPTION
    ! -----------
    ! Generating a formatted input file where the parameters are taken from 
    ! the flags provided at the start of the program.
    ! Flags starting with "--" indicate a new variable and all flags following
    ! till the next flag starts with "--" is written in one line in the input 
    ! file. All flags before the first flag starting with "--" are ignored.
    ! The flag "--end" terminated the read.
    !
    use IFPORT
    implicit none

    character(*),intent(in) :: a_nml
    logical,intent(out) :: a_stat
    integer,intent(in) :: a_fid
    character(1000) :: flag,ele
    character(99999) :: allflags
    logical :: da,quotes,set_eqlsgn
    integer :: i,nflags
    integer :: ipos

    ! number of flags
    nflags = iargc()

    ! a_stat = 0 : unsuccessful exit
    a_stat = .false.

    ! combine all flags in one string 
    ! find first string starting with "--"
    allflags=""
    ipos = 0
    do i=1,nflags
      call getarg(i,flag)
      allflags = trim(allflags) //" "// trim(adjustl(flag))

      if (ipos==0 .and. flag(1:2)=="--") then
        ipos = i
      end if
    end do

    ! do nothing if no right-formatted flag is found
    if (ipos==0) return


    inquire(a_fid,opened=da)
    if (da) then
      write(5,*) " ERROR(flaginput): Pipe unit",a_fid," is already used"
      stop
    end if

    ! create temporary name
    open(a_fid,status="scratch")
    !! -> don't use status="scratch" since it write to /tmp which might be full
    !! -> write always in current directory -> when this is full then program can't run
    !write(flag,*) getpid()
    !write(flag,'(a)')  ".input."//trim(adjustl(flag))//".tmp"
    !open(a_fid,file=flag)
    
    write(a_fid,'(3a)',advance="no")     "&",trim(a_nml)   &
        ," ! this is an automatically generated input file"  ! header
  
    ! find first "--" occurrence and truncate the rest
    ipos = index(allflags,"--")
    allflags = adjustl(allflags(ipos:))

    ! loop through all elements in allflags and separate them
    ! in each flag argument which is then handled one after each other
    lp_flags:do while (len_trim(allflags) /= 0)
      
      ! find postion where next flag statement starts (and current one ends)
      ipos = index(allflags," --")
      
      if ( ipos==0 ) then
        ! last flag statement
        flag = allflags
        allflags = " "
      else
        flag = allflags(1:ipos-1)
        ! remove current flag statement from list
        allflags = adjustl(allflags(ipos+1:))
      end if

      
      ! process  current flag statement
      ! -------------------------------
      ! find whether "=" needs to be set
      ipos = scan(flag,"=")
      set_eqlsgn = .false.
      if ( ipos == 0 )  set_eqlsgn = .true.
      
      ! find if quotes need to be set (only when "/" appears 
      ! and no " or ' quotes are already set)
      ipos = scan(flag,"/")
      quotes = .false.
      if ( ipos /= 0 ) then
        ipos = scan(flag,"'") + scan(flag,'"')
        if ( ipos == 0 )  quotes = .true.
      end if

      ! write flag statement
      if ( .not.quotes .and. .not.set_eqlsgn ) then
        write(a_fid,'(/,a)',advance="no") trim(flag(3:))
      elseif ( .not.quotes .and. set_eqlsgn ) then
        ipos = scan(flag," ")  ! find 1. blank space (should be after parameter name) 
        write(a_fid,'(/,a,"= ",a)',advance="no") flag(3:ipos),trim(flag(ipos+1:))
      elseif ( quotes .and. .not.set_eqlsgn ) then
        ipos = scan(flag,"=")  ! find equal sign -> put quotes directly afterwards 
        write(a_fid,'(/,4a)',advance="no") flag(3:ipos)," '",trim(flag(ipos+1:)),"'"
      elseif ( quotes .and. set_eqlsgn ) then
        ipos = scan(flag," ")  ! find 1. blank space (should be after parameter name) 
        write(a_fid,'(/,4a)',advance="no") flag(3:ipos),"= '",trim(flag(ipos+1:)),"'"
      end if


      ! no more flag statements are following
      ! -------------------------------------
      if (trim(flag) == "--end") exit
    end do lp_flags
    

    ! clsoe namelist and go back to the beginning of the namelist
    write(a_fid,'(/,"/")')
    rewind(a_fid) 
    ! a_stat = 1 : successful exit
    a_stat = .true.
    
    return
  end subroutine flaginput


end module inout
