
module FEMDVR_Hamil

  !
  !**PURPOSE
  !  -------
  !  The MODULE Hamiltonian contains all necessary routines and function to 
  !  build the FEM-DVR Hamiltonian
  !
  !**DESCRIPTION
  !  -----------
  !  The following subroutines (sbr) and functions (fct) are part of 
  !  this module:
  !  
  !  -> (sbr) init_fem_matrices : initiates FEM matrices
  !  -> (sbr) buildUFEM         : build local potential + angular potential
  !  -> (fct) local_pot         : an auxilary function to use with buildUFEM,
  !                               represents the local potential = nuclear potential +
  !                               angular potential + CAP
  !  -> (sbr) buildCoulomb      : computes the matrix of Coulomb e-e interaction
  !  -> (sbr) buildVFEM         : mean-field potential  
  !  -> (sbr) buildSymT         : build radial kinetic operator in symmetric form
  !  -> (sbr) buildOv           : build overlap
  !                               the FE basis functions are not orthogonal
  !  -> (fct) local_unity       : an auxilary function to use with buildOv or
  !                               in any intergartion where basis functions are integrated with 1
  !  -> (fct) local_unity_cmplx : an auxilary function to use with buildSymT or
  !                               in any intergartion where basis functions are integrated with complex 1
  !  -> (fct) expfun, 
  !  -> (fct) expfun_real
  !  -> (fct) check_gq          : a set of functions to recheck Gauss quadrature at any point when it is needed
  !                               can be uncommented in init_fem_matrices
  ! 
  !  -> m_pot                   : module variable of type t_local_potantial
  !                               needed for the calculation of U m.e.
  !  -> (sbr) init_m_pot        : initializes the m_pot
  !
  !**AUTHOR
  !  ------
  !  written by Arina Sytcheva in Juli 2013
  !
  !**VERISON
  !  -------
  !  svn info: $Id: FEM-DVR_Hamil.f90 1711 2015-05-22 18:44:19Z spabst $
  !-----------------------------------------------------------------------
  !
  use constants, dp=>dblprec
  use varwfct, only : t_orbital
  use grid_type
  use grid, only : build_femdvr_trafo
  use gq_integrator,  only : gq_caller

  implicit none

  contains

  subroutine init_femdvr_matrices(a_mode)
    !
    !  initialize fem matrices
    !  a_mode   :  program ID
    !              = 0,1,2,3 : HF, Matrix, TDCIS, Density
    !
    use varsystem,only : m_nuccharge, m_cap_strength, m_Rabsorb     &
                        ,m_lmax, m_lsum, m_nelec, m_nocoulomb       &
                        ,m_grid, m_fem_nintpts, m_nmax, m_nmaxl     &
                        ,m_npoints, m_cs_angle, m_cs_smooth

    use varmatrix, only : m_fem_ov, m_fem_ke, m_fem_u               &
                         ,m_femdvr_trafo, m_fem_gradient            &
                         , m_fem_ev_gradient

    use analytic_integrator, only : int_caller    
    use FEM_Hamiltonian, only : build_fem_matrix

    use f95_precision
    use blas95
    implicit none

    integer,intent(in) :: a_mode

    integer,parameter :: fe_degree = 3
    integer :: l,nbasis
    logical :: err
    complex(dp),allocatable :: diag(:), ecs_f(:), r(:)


    integer :: i, idx(2), j

    real(dp),allocatable :: fem_ke(:,:), eigval(:), aux(:,:), fem_grad(:,:) &
                           ,fem_ovup(:,:)
    real(dp) :: cs(3)

                           
    err = .false.

    if( .not. allocated(m_grid%r) ) then
      stop " ERROR(init_fem_matrices): grid is not initialized"
    end if

    nbasis = m_grid%nbasis

    ! build kinetic energy matrix and local potential for Hamiltonian (only used in HF)
    if_HF: if ( a_mode==0 ) then
      ! initialize matrices needed for HF

      allocate(m_fem_ke(nbasis,nbasis))  ! DVR representation
      m_fem_ke = ZERO
      allocate(m_fem_u(nbasis,nbasis,0:m_lmax))
      m_fem_u  = ZERO

      allocate( fem_ke(nbasis,nbasis) )  ! FEM representation (only temporarily needed)
      fem_ke = ZERO

      ! analytic
      call int_caller(fem_ke, m_grid%DVR_grid, m_grid%rmin, m_grid%rmax, .true., 0)
      ! numeric
      !call build_fem_matrix(fem_ke, m_grid%DVR_grid, ao_deriv_l=.true., ao_deriv_r=.true.) 
      fem_ke = fem_ke/2.D0

      if ( .not. allocated(m_femdvr_trafo) ) then
        allocate( m_femdvr_trafo(nbasis,nbasis), eigval(nbasis) )
        m_femdvr_trafo = ZERO
        eigval = ZERO
        call build_femdvr_trafo(m_femdvr_trafo,eigval,m_grid%DVR_grid)
        deallocate(eigval)
      end if

      ! express kinetic energy matrix in the DVR basis
      allocate( aux(nbasis,nbasis) )
      aux = ZERO
      
      !!aux = matmul(fem_ke,m_femdvr_trafo)
      !forall (i=1:nbasis, j=1:nbasis)
      !  aux(i,j) = sum(fem_ke(i,:)*m_femdvr_trafo(:,j))
      !end forall
      call gemm(fem_ke,m_femdvr_trafo,aux)  ! BLAS95

      !forall (i=1:nbasis, j=1:nbasis)
      !  m_fem_ke(i,j) = sum(m_femdvr_trafo(:,i)*aux(:,j))
      !end forall
      call gemm(m_femdvr_trafo,aux,fem_ke,transa='t')
      m_fem_ke = fem_ke

      deallocate(fem_ke, aux)
  
      ! ECS implementation
      allocate(r(nbasis), ecs_f(nbasis))
      allocate(diag(nbasis))
      if ( m_grid%absorbing == "ECS" ) then
        ecs_f = m_grid%cs%F(:,1)
        r = m_grid%cs%F(:,0)
        forall (i=1:nbasis)
          diag(i) = ONE/ecs_f(i)
        end forall
        ! transform k.e.
        forall (i=1:nbasis,j=1:nbasis)
          m_fem_ke(i,j) = diag(i) * m_fem_ke(i,j) * diag(j)
        end forall
      else
        r = m_grid%r
        diag = ZERO
      end if
     

      
      ! -----------------
      ! --- potential ---
      ! -----------------
      diag = buildUDVR(m_nuccharge,m_cap_strength,m_grid)
      
      loop_ang_mom: do l=0,m_lmax
        do i = 1, nbasis
          m_fem_u(i,i,l) = diag(i) + HALF * dble(l * (l + 1)) / r(i) / r(i)
        end do
      end do loop_ang_mom
      deallocate(diag,r,ecs_f)

      err = .false.
      write(STDOUT,*) "init_femdvr_matrices: FEMDVR matrices for HF.x have been computed"

    end if if_HF
!-----------------------------------------------------------------------------


    if_matrix: if ( a_mode == 1 ) then
      !! initialize fem-dvr matrices needed for the matrix.x

      if ( .not. allocated(m_femdvr_trafo) ) then
        allocate( m_femdvr_trafo(nbasis,nbasis),eigval(nbasis) )
        m_femdvr_trafo = ZERO
        eigval = ZERO
        call  build_femdvr_trafo(m_femdvr_trafo,eigval,m_grid%DVR_grid)
        deallocate(eigval)
      end if

      ! compute gradient
      allocate(m_fem_gradient(nbasis,nbasis))
      allocate(m_fem_ev_gradient(nbasis,nbasis))
      allocate(fem_grad(nbasis,nbasis))
      allocate(aux(nbasis,nbasis))
      m_fem_gradient = ZERO
      m_fem_ev_gradient = ZERO
      fem_grad = ZERO
      aux= ZERO
      call int_caller(fem_grad, m_grid%DVR_grid, m_grid%rmin, m_grid%rmax, .false., 0, ao_do_deriv=.true., ao_sym=.false.)


      ! rotate gradient
      !aux = matmul(fem_grad,m_femdvr_trafo)
      !m_fem_gradient = matmul(transpose(m_femdvr_trafo),aux)
      call gemm(fem_grad,m_femdvr_trafo,aux)
      call gemm(m_femdvr_trafo,aux,fem_grad,transa='t')
      m_fem_gradient = fem_grad
      deallocate(aux,fem_grad)

      ! ECS implementation
      if ( m_grid%absorbing == "ECS" ) then
        allocate(r(nbasis), ecs_f(nbasis))
        allocate(diag(nbasis))
        ecs_f = m_grid%cs%F(:,1)
        r = m_grid%cs%F(:,0)
        forall (i=1:nbasis)
          diag(i) = ONE/sqrt(ecs_f(i))
        end forall
        ! transform gradient
        forall (i=1:nbasis,j=1:nbasis)
          m_fem_gradient(i,j)    = diag(i)*m_fem_gradient(i,j)*diag(j)
          m_fem_ev_gradient(i,j) = conjg(diag(i))*m_fem_gradient(i,j)*diag(j)
        end forall
        deallocate(r,ecs_f,diag)
      end if

      err = .false.
      write(STDOUT,*) "init_femdvr_matrices: FEMDVR matrices for Matrix.x have been computed"

    end if if_matrix
!--------------------------------------------------------

    if_tdcis_dens: if ( a_mode == 2 .or. a_mode == 3 ) then
     ! initialize femdvr matrices needed for tdcis or density
     ! only femdvr_trafo is needed
     if ( .not. allocated(m_femdvr_trafo) ) then
        allocate( m_femdvr_trafo(nbasis,nbasis),eigval(nbasis) )
        m_femdvr_trafo = ZERO
        eigval = ZERO
        call  build_femdvr_trafo(m_femdvr_trafo,eigval,m_grid%DVR_grid)
        deallocate(eigval)
      end if

    end if if_tdcis_dens

  end subroutine init_femdvr_matrices



!-------------------------------------
!  local potential
!-------------------------------------

  function buildUDVR(a_z,a_cap,a_grid) Result(mat)
    ! generate local potential part of the Hamiltonian,
    ! which consists of the nuclear potential and the complex absorbing 
    ! potential
    !
    !   U = -Z/r                          , r<Rabsorb
    !     = -Z/r - i cap * (r-Rabsorb)**2 , r>Rabsorb
    !
    ! As defined in line 2 in Eq. 47 in Greenman, PRA 82, 023406 (2011)
    !
    ! INPUT
    ! -----
    !  z  - nuclear charge
    ! cap - CAP strength
    ! grid- grid object
    !
    use varsystem,only : m_Rabsorb,m_mp => m_modelpot
    use grid_type
    use splineinterpol, only : spline => eval_spline_object
    implicit none
    
    complex(dp),allocatable :: mat(:)

    real(dp),intent(in) :: a_z,a_cap
    type(t_grid),intent(in) :: a_grid

    integer :: i,nbasis
    complex(dp), allocatable :: r(:,:)  ! local assignment of r


    nbasis = a_grid%nbasis

    ! set up matrix
    allocate(mat(nbasis))
    mat = ZERO

    ! CAP or ECS
    ! ----------
    allocate(r(nbasis,0:3))
    r = ZERO
    if ( a_grid%absorbing == "ECS" ) then
      r = a_grid%cs%F
    else
      r(:,0) = a_grid%r
      r(:,1) = ONE
      r(:,2:3) = ZERO
    end if


    if ( m_mp%active ) then
      ! model potential
      ! ---------------
      !  -> multiply r/F(r) on model potential (/= 1 only for complex scaling)
      !  -> assuming the complex scaling starts where model potential
      !     has the behavior ~ 1/r [substitude with 1/F(r) for complex scaling]
      !
      do i=1,nbasis
        mat(i) = spline(a_grid%r(i),m_mp) * (a_grid%r(i)/r(i,0))
        
        !TMP  mat(i) = 7.50D0 * r(i,0) * r(i,0) * exp(-r(i,0))  ! test potential
        !write(30,*) a_grid%r(i),spline(a_grid%r(i),m_mp)
        !write(30,"(4ES20.7)") a_grid%r(i), mat(i)
      end do
    else 
      ! nuclear potential
      ! -----------------
      mat = -a_z/r(:,0)
    end if


    ! Complex Absorbing region
    ! -------------------------
    if ( a_grid%absorbing == "CAP" ) then   
      ! CAP
      ! ----
      do i=1,nbasis
        if ( abs(r(i,0)) < m_Rabsorb ) cycle
          mat(i) = mat(i) - IMG*a_cap * (r(i,0)-m_Rabsorb)**2
      end do
    else if ( a_grid%absorbing == "ECS" ) then   
      ! complex scaling
      ! ---------------    
      mat = mat - (TWO*r(:,3)*r(:,1) - THREE*r(:,2)**2) / (8.D0*r(:,1)**4)
    end if
  end function buildUDVR

end module FEMDVR_Hamil
