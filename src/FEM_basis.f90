module FEM_basis

  !**PURPOSE
  !  -------
  !  The MODULE FEM_basis contains all necessary routines and function to 
  !  call and manipulate FEM basis functions.
  !
  !**DESCRIPTION
  !  -----------
  !  The following subroutines (sbr) and functions (fct) are part of 
  !  this module:
  !
  !  -> (sbr) idx_basis_reverse  : transforms from the FEM function through index 
  !           to the pair gridpoint-type identifing the FE function 
  !  -> (fct) idx_basis : transforms from the pair gridpoint-type identifing the FE function 
  !           to the FEM function through index 
  !  -> (fct) basisfct_real: returns the value of the FE basis function at the argument x
  !                  Notes:  This calculates the 3 finite element basis functions
  !                  that span the two sectors (xm,x0) and (x0,xf), where 
  !                  normally xm < x0 < xf.  The position x is where these
  !                  functions are calculated, and their first derivatives
  !                  du0,du1,du2.  These have the following properties:
  !                  u0(x0)=1, u0'(x0)=0=u0''(x0);
  !                  u1(x0)=0=u''(x0), u1'(x0)=1;
  !                  u2(x0)=u2'(x0)=0, u2''(x0)=1;
  !                  Moreover, all of these functions have vanishing value,
  !                  and vanishing first and second derivatives at the boundaries
  !                  xm and xp.

  !  -> (fct) basiscoef_real: returns coefficients of the underlying polynomial
  !
  !  -> (fct) fem_3refpoints - returns a FE for the basis function
  !
  !
  !**AUTHORS
  !  ------
  !  written by Arina Sytcheva and Stefan Pabst in June 2013
  !          based on the FEM_basis.f (sbr) Basis(xm,x0,xp,x,u0,u1,u2,du0,du1,du2)
  !          A.S. recived from Robin Santra in February 2013
  !
  !**VERISON
  !  -------
  !  svn info: $Id: FEM_basis.f90 1638 2015-05-03 22:39:50Z spabst $
  !-----------------------------------------------------------------------
  !


  use constants, dp=>dblprec
  implicit none

  interface fem_basisfct
    module procedure basisfct_real 
  end interface

contains

  pure subroutine idx_basis_reverse(a_i, a_ifun, a_itype, a_nbasis)
    implicit none
    ! a_i - the index of the basis function
    ! a_nbasis - number of basis functions
    ! returns :
    ! a_ifun  - the grid point index 
    ! a_itype - the type of the function
    integer, intent(in) :: a_i
    integer, intent(out) :: a_ifun
    integer, intent(out) :: a_itype
    integer, intent(in) :: a_nbasis   !  volume of the FEM basis

    ! 1. FEM grid point   : only types 2 and 3 exist (f(r=0)=0)
    ! last FEM grid point : only types 2 and 3 exist (f(rmax)=0)
    ! => nbasis = 3*FEM_grid_points - 2

    a_itype= 0    
    a_ifun = a_i/3 + 1 
    if ( 3*a_ifun-2 > a_nbasis )  return

    if (a_i .le. a_nbasis - 2 ) then
     ! all except last FEM grid point
     a_itype = mod(a_i,3) + 1
    else ! last FEM grid point
     a_itype = mod(a_i,3) + 2
    end if
  end subroutine idx_basis_reverse


  elemental integer function idx_basis(a_ipoint, a_itype, a_npoints) Result(idx)
    implicit none
    ! a_ifun  - the grid point index 
    ! a_itype - the type of the function
    ! a_npoints - number of grid points
    ! returns a_i - the index of the basis function
    integer, intent(in) :: a_ipoint
    integer, intent(in) :: a_itype
    integer, intent(in) :: a_npoints  ! number of grid points
    
    idx = 0
    if ( a_itype==1 .and. a_ipoint==1 ) return
    if ( a_itype==1 .and. a_ipoint==a_npoints ) return

    if (a_ipoint < a_npoints) then
      idx = (a_ipoint-1)*3 + a_itype - 1
    else
      idx = (a_ipoint-1)*3 + a_itype - 2
    end if

  end function idx_basis



!----------------------------------------------------------------------------------------
!
!----------------------------------------------------------------------------------------

  elemental logical function overlap(a_idx1,a_idx2) result(res)
    ! check whether FEM functions with index idx1 and idx2 are overlapping
    integer, intent(in) :: a_idx1,a_idx2

    res = .false.
    if (abs(a_idx1/3-a_idx2/3) < 2)  res=.true.
  end function overlap
 

  pure logical function overlap4(a_idx) result(res)
    ! check wether FEM function with index idx1,...,idx4 are overlapping
    integer, intent(in) :: a_idx(4)
    integer :: idxs,idxl

    res = .false.
    idxs=minval(a_idx)
    idxl=maxval(a_idx)
    ! compare FEM function with largest wand smallest index (make use of that index is monotonic)
    if (abs(idxs/3-idxl/3) < 2)  res=.true.
  end function overlap4


!----------------------------------------------------------------------------------------
!
!----------------------------------------------------------------------------------------

  pure function basisfct_real(  &
         a_xarr,      &       ! interval
         a_type,      &       ! 1st, 2nd, or 3rd function to be calculated
         a_x,         &       ! argument
         a_do_deriv  &        ! flag to switch between 1. and 0. derivative 
         ) result(res)
    implicit none
      
    integer, parameter :: dp = selected_real_kind(15)
    integer, parameter   :: degree_fe = 3
    real(dp), intent(in) :: a_xarr(degree_fe)
    integer, intent(in)  :: a_type
    real(dp), intent(in) :: a_x
    logical,  intent(in) :: a_do_deriv

    real(dp) :: res

!          type 1: u0(x0)=1, u0'(x0)=0=u0''(x0);  i.e. nonzero value 
!          type 2: u1(x0)=0=u''(x0), u1'(x0)=1;   i.e. nonzero first derivative
!          type 3: u2(x0)=u2'(x0)=0, u2''(x0)=1;  i.e. nonzero second derivative
    
    real(dp) :: xp, & !right end of the interval
                x0, & !center point of the interval
                xm, & ! left point of the interval
                g,  & ! right difference
                h,  & ! left difference
                r      ! running argument
    
    real(dp) :: r2,r3,r4,r5


    res = ZERO
   
    xm = a_xarr(1)
    x0 = a_xarr(2)
    xp = a_xarr(3)
      
    g=xp-x0
    h=x0-xm
    r=a_x-x0

    if(r>=-h .AND. r<=g) THEN
      if (r==0.d0 ) then
        g=1.d0
      elseif ( r<0.D0 ) then
        g=-h
      end if

      r=r/g
      r2=r*r
      r3=r2*r
      r4=r3*r
      r5=r4*r
      if ( a_do_deriv .eqv. .false. ) then
        select case ( a_type )
        case (1)
          res = 1.d0-10.d0*r3+15.d0*r4-6.d0*r5  ! u0
        case (2)        
          res = g*(r-6.d0*r3+8.d0*r4-3.d0*r5)   ! u1
        case (3)
          res = 0.5d0*g*g*(r2-3.d0*r3+3.d0*r4-r5)  ! u2
        end select
      else
        select case ( a_type )
        case (1) 
          res = (-30.d0*r2+60.d0*r3-30.d0*r4)/g  ! du0
        case (2)
          res = 1.d0-18.d0*r2+32.d0*r3-15.d0*r4  ! du1
        case (3)
          res = (2.d0*r-9.d0*r2+12.d0*r3-5.d0*r4)*g/2.d0 ! du2
        end select
      end if

    end if 

  end function basisfct_real
!----------------------------------------------------------------------------------------
!
!----------------------------------------------------------------------------------------

!------------------------------------------------------
! coefficients for basis functions
!------------------------------------------------------

  function basiscoef_real(a_xarr, &               ! interval
                          a_type, &               ! 1st, 2nd, or 3rd function to be calculated
                             a_x, &               ! argument
                      a_do_deriv, &               ! flag to switch between derivative and function value
                         ao_mode)                 !  without ao_mode or ao_mode = .true.: coefficients in the expansion sum c_i r^i
                                                  !  ao_mode=.false.: coefficients in the expansion sum d_i ((r-x0)/(x3-x2))^i for the right polynomial
                                                  !                                             sum d_i ((r-x0)/(x1-x2))^i for the left  polynomial
    use basics, only : binominal

    implicit none
    
    integer, parameter   :: degree_fe = 3
    real(dp), intent(in) :: a_xarr(degree_fe)
    integer, intent(in)  :: a_type
    real(dp), intent(in) :: a_x
    logical,  intent(in) :: a_do_deriv
    logical,  intent(in), optional :: ao_mode
    real(dp) :: basiscoef_real(0:5)

    ! type 1: u0(x0)=1, u0'(x0)=0=u0''(x0);  i.e. nonzero value 
    ! type 2: u1(x0)=0=u''(x0), u1'(x0)=1;   i.e. nonzero first derivative
    ! type 3: u2(x0)=u2'(x0)=0, u2''(x0)=1;  i.e. nonzero second derivative
    
    real(dp) :: xp, & !right end of the interval
                x0, & !center point of the interval
                xm, & ! left point of the interval
                g,  & ! right difference
                h,  & ! left difference
                hneg, & ! negativ h
                r,  &  ! running argument
                aux(0:5,0:5) !, &
                !coef_real(0:5)
    integer,save ::  minus1(0:5)
    integer  :: i,k
    logical  :: mode

    mode = .true.
    if ( present( ao_mode)) mode = ao_mode

    xm = a_xarr(1)
    x0 = a_xarr(2)
    xp = a_xarr(3)
    
    basiscoef_real(0:5) = 0.d0
    !coef_real(0:5) = 0.d0

    g=xp-x0
    h=x0-xm
    hneg=-h
    r=a_x-x0
    if(r.GE.0.d0 .AND. r.LE.g) THEN
      if ( a_do_deriv .eqv. .false. ) then
        select case ( a_type )
        case (1)
          basiscoef_real(0)=1.d0
          basiscoef_real(3)=-10.d0
          basiscoef_real(4)=15.d0
          basiscoef_real(5)=-6.d0
        case (2)        
          basiscoef_real(1)=g
          basiscoef_real(3)=-6.d0*g
          basiscoef_real(4)=8.d0*g
          basiscoef_real(5)=-3.d0*g
        case (3)
          basiscoef_real(2)=0.5d0*g*g
          basiscoef_real(3)=-1.5*g*g
          basiscoef_real(4)=1.5*g*g 
          basiscoef_real(5)=-0.5D0*g*g
        end select
      else
        select case ( a_type )
        case (1) 
          basiscoef_real(2)=-30.d0/g
          basiscoef_real(3)=60.d0/g
          basiscoef_real(4)=-30.d0/g
        case (2)
          basiscoef_real(0)=1.d0
          basiscoef_real(2)=-18.d0
          basiscoef_real(3)=32.d0
          basiscoef_real(4)=-15.d0
        case (3)
          basiscoef_real(1)=1.d0*g
          basiscoef_real(2)=-4.5d0*g
          basiscoef_real(3)=6.d0*g
          basiscoef_real(4)=-2.50*g
        end select
      end if

    else if (r.LT.0.d0 .AND. r.GE. hneg) then

      g=hneg
      if ( a_do_deriv .eqv. .false. ) then
        select case ( a_type )
        case (1)
          basiscoef_real(0)=1.d0
          basiscoef_real(3)=-10.d0
          basiscoef_real(4)=15.d0
          basiscoef_real(5)=-6.d0
        case (2)
          basiscoef_real(1)=g
          basiscoef_real(3)=-6.d0*g
          basiscoef_real(4)=8.d0*g
          basiscoef_real(5)=-3.d0*g
        case (3)
          basiscoef_real(2)=0.5d0*g*g
          basiscoef_real(3)=-1.5d0*g*g
          basiscoef_real(4)=1.5d0*g*g
          basiscoef_real(5)=-0.5d0*g*g
        end select
      else
        select case ( a_type )
        case (1)
          basiscoef_real(2)=-30.d0/g
          basiscoef_real(3)=60.d0/g
          basiscoef_real(4)=-30.d0/g
          basiscoef_real(5)=0.d0
        case (2)
          basiscoef_real(0)=1.d0
          basiscoef_real(2)=-18.d0
          basiscoef_real(3)=32.d0
          basiscoef_real(4)=-15.d0
        case (3)
          basiscoef_real(1)=g
          basiscoef_real(2)=-4.5d0*g
          basiscoef_real(3)=6.d0*g
          basiscoef_real(4)=-2.5d0*g
        end select
      end if
        !return
    end if 

    if ( mode ) then
      do i = 0, 5
        minus1(i) = mod(i,2)*2-1
      end do
   
      aux  = ZERO
      do i= 0,5
        do k= 0,i
          aux(i,k) = binominal(i,i-k)*basiscoef_real(i)*minus1(i-k)*x0**(i-k)/g**i
!          aux(i,k) = binominal(i,i-k)*basiscoef_real(i)*minus1(i-k)*(x0/g)**i/x0**k
        end do
      end do
   
      forall(k=0:5)
        basiscoef_real(k) = sum(aux(:,k))  
      end forall
    end if

  end function basiscoef_real
  
  
  ! ------------------------------------------------


  pure function fem_3refpoints(a_i, a_grid) result(res)
    use grid_type
    implicit none

    integer, intent(in) :: a_i !! grid index
    type(t_grid),intent(in) :: a_grid
    real(dp) ::  res(1:3)

    integer :: npoints


    res = ZERO
    npoints = a_grid%npoints

    if ( a_i .gt. npoints )  return
    

    if ( a_i .eq. 1) then
      res(1) = a_grid%r(1)
      res(2:3) = a_grid%r(1:2)
    else if ( a_i .eq. a_grid%npoints ) then
      res(1:2) = a_grid%r(npoints-1:npoints)
      res(3) = a_grid%r( npoints )
    else
      res(1:3) = a_grid%r(a_i-1:a_i+1)
    end if
  end function fem_3refpoints

end module FEM_basis
