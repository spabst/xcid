module PHMatrices
  !
  !**PURPOSE
  !  -------
  !  Contains subroutines for 1-particle-1-hole matrix generation.
  !  Please call externally only the main subroutine "phmatrix".
  !  
  !
  !**DESCRIPTION
  !  -----------
  !  The following subroutines (sbr) and functions (fct) are part of 
  !  this module:
  !   -> phmatrix    : main routine to be called to generate
  !                    all matrix elements of all 1p1h matrices
  !                    see Appendix A in Greenman, PRA 82, 023406 (2011)
  !
  !   -> radial1p1h  : Calculate the radial part of the matrix elements
  !   -> writeout1p1h: Calculate the angular part of the matrix elements
  !
  !**AUTHOR
  !  ------
  !  (c) and written by Stefan Pabst
  !
  !**VERSION
  !  -------
  !  svn info: $Id: PHMatrices.f90 1748 2015-06-03 02:22:53Z spabst $
  !-----------------------------------------------------------------------
  !
  use varsystem, only : m_grid
  use constants
  implicit none


contains

  subroutine phmatrix(a_fid,a_mode,a_orb1,a_orb2)
    ! 
    ! DESCRIPTION
    ! -----------
    ! For each 1p-1h operator we can exploit the Wigner-Eckart theorem.
    ! Due to efficiency not the reduced matrix elements will be stored but
    ! rather the m_1=m_2=m_3=0 element. 
    ! If other m-dependencies are needed the clebsch-gordan
    ! <l1,0,l2,0|l3,0> must be replaced by <l1,m1,l2,m2|l3,m3> before they are
    ! used further.
    !
    ! Input:
    ! ------
    ! a_fid (required) 
    ! - is the output ID of the file
    !
    ! a_mode (required)
    !  = 101 : overlap matrix  (lp=lq)
    !          -> Eq. A3 in Greenman, PRA 82, 023406 (2011)
    !  = 102 : overlap with splitting function [1/(1+e^(-(r-r_c)/delta)]  (lp=lq)
    !          -> as Eq. (5) in Tong et al., PRA 74, 031405(R),(2006)
    !  = 110 : dipole matrix [symmetric]  (lp-lq=+-1)
    !          -> Eq. A4 in Greenman, PRA 82, 023406 (2011)
    !  = 111 : dipole matrix [hermitian]  (lp-lq=+-1)
    !          -> as Eq. A5 in Greenman, PRA 82, 023406 (2011)
    !             just for the dipole moment
    !  = 112 : dipole acceleration matrix (lp-lq=+-1)
    !          -> Eq. A5 in Greenman, PRA 82, 023406 (2011)
    !  = 120 : momentum operator [antisymmetric]  (lp-lq=+-1)
    !  = 121 : momentum operator [hermitian]  (lp-lq=+-1)
    !
    ! a_orbx (required)
    ! - defines which type of orbitals are used for position x
    !  = -1: active virtual + all occupied orbitals
    !  = 0 : all active orbitals
    !  = 1 : active virtual orbitals
    !  = 2 : all occupied orbitals
    !  = 3 : active occupied orbitals
    
    ! loop through everything
    ! depending on mode generate f(r) for radial integration
    ! ov, w : lp=lq
    ! dip + dipacc : lq=lp+1 (lp-1 can be omitted due to symmetry)
    !
    use VarSystem,only : m_nuccharge,m_Rabsorb,m_split_r   &
                        ,m_split_smooth, m_lmax,m_useSymm
    use VarWfct,only   : m_lomax
    use varmatrix,only : m_fem_ov, m_fem_dipole                  &
                        ,m_fem_gradient                          &
                        ,m_fem_ev_dipole,m_fem_ev_gradient       &
                        ,m_fem_ev_dipacc
    use general,only   : findnrange
    use FEM_Hamiltonian, only :  build_fem_matrix
    implicit none

    integer,intent(in) :: a_fid,a_mode,a_orb1,a_orb2
    
    integer :: i,dl,l1,l2
    integer,dimension(2) :: l,lma,nmax,nmin,orb,in
    logical :: symm,conjugate
  
    complex(dblprec),allocatable :: rmat(:,:),rpot(:,:)

    integer :: nbasis

    ! gradient
    integer :: isgn
    complex(dblprec),allocatable :: grad(:,:),rlocal(:,:)  


    nbasis = m_grid%nbasis

    conjugate=.false.
    select case ( m_grid%type )
      case ( "PSG", "DVR" ) 
        allocate(rpot(nbasis,1))
      case ( "FEM" )
        allocate(rpot(nbasis,nbasis))
      case default
        allocate(rpot(nbasis,1))
    end select
    rpot = ZERO


    ! - set if symmetric or hermitian inner product should be
    !   used to calculate matrix elements
    ! - set change in angular momenta (dl)
    select case(a_mode)
    case(101) ! overlap
      select case ( m_grid%type )
      case ( "PSG" , "DVR" )
        rpot = ONE
      case ( "FEM" )
        rpot = m_fem_ov
      end select
      dl=0
      conjugate=.true.


    case(102) ! splitting function
      select case ( m_grid%type )
      case ( "PSG" , "DVR" )
        do i=1,nbasis
          rpot(i,1) = splittingfct(m_grid%r(i)*CONE,m_split_r,m_split_smooth)
        end do
      case ( "FEM" )
        call build_fem_matrix(rpot,m_grid,caller_splitting)
      end select
      dl=0
      conjugate=.false.


    case(103)  ! gradient (needed for tsurf)
      if ( allocated(rpot) )  deallocate(rpot)
      allocate( rpot(nbasis,nbasis))
      select case ( m_grid%type )
      case ( "PSG" )
        call init_gradient(rpot)
      case ( "DVR", "FEM")
        return  ! gradient at r=tsurf_r is done analytically 
      end select
      dl=0
      conjugate=.false.


    case(110,111) ! dipole (symmetric/hermitian) - for time propagation
      dl=1

      if (a_mode == 110) then
        conjugate=.false.
        select case ( m_grid%type )
        case ( "PSG", "DVR" )
          call radialfct(1,rpot(:,1))
        case ( "FEM" )
          rpot = m_fem_dipole
        end select
      elseif (a_mode == 111) then
        conjugate=.true.
        select case ( m_grid%type )
        case ( "PSG", "DVR" )
          call radialfct(1,rpot(:,1),ao_ev=.true.)
        case ( "FEM" )
          rpot = m_fem_ev_dipole
        end select
      end if


    case(112) ! dipole acceleration
      conjugate=.true.
      select case ( m_grid%type )
      case ( "PSG", "DVR" )
        call radialfct(-2,rpot(:,1),ao_ev=.true.)
      case ( "FEM" )
        rpot = m_fem_ev_dipacc 
      end select

      rpot=rpot*m_nuccharge
      dl=1


    case(120,121) ! momentum (antisymmetric/hermitian)
      
      deallocate(rpot)
      allocate(rpot(nbasis,nbasis))
      rpot = ZERO

      allocate( grad(nbasis,nbasis), rlocal(nbasis,1) )
      grad = ZERO
      rlocal = ZERO
      
      dl=1
      if ( a_mode == 120 ) then
        conjugate=.false.
        select case ( m_grid%type )
        case ( "PSG" )
          call init_gradient(grad)
          call radialfct(-1,rlocal(:,1))
        case ("DVR")
          grad = -m_fem_gradient   ! stored transposed form
          call radialfct(-1,rlocal(:,1))
        case ("FEM")
          grad = -m_fem_gradient   ! stored transposed form
          deallocate(rlocal)
          allocate(rlocal(nbasis,nbasis))
          rlocal = ZERO
          call build_fem_matrix(rlocal,m_grid,caller_ir)
        end select
      else if (a_mode == 121) then
        conjugate=.true.
        select case ( m_grid%type )
        case ( "PSG" )
          call init_gradient(grad,ao_ev=.true.)
          call radialfct(-1,rlocal(:,1),ao_ev=.true.)
        case ("DVR")
          grad = -m_fem_ev_gradient  ! stored transposed form
          call radialfct(-1,rlocal(:,1),ao_ev=.true.)
        case ("FEM")
          grad = -m_fem_ev_gradient  ! stored transposed form
          deallocate(rlocal)
          allocate(rlocal(nbasis,nbasis))
          rlocal = ZERO
          call build_fem_matrix(rlocal,m_grid,caller_ir_ev)
        end select
      end if


    case default
       stop "phmatrix: mode input is unknown"
    end select
  

    orb(1)=a_orb1
    orb(2)=a_orb2
   
    ! check input correctness
    ! set maximum anglular momenta
    do i=1,2
       select case (orb(i))
       case (-1,0,1)
          lma(i)=m_lmax
       case(2,3)
          lma(i)=m_lomax
       case default
          stop "phmatrix: orb specifier is invalid"
       end select       
    end do
    
    symm=.false.
    if (orb(1)==orb(2) .and. m_useSymm) symm=.true.
    ! loop through all possible l combinations
    do l1=0,lma(1)
       do l2=0,lma(2)
          if (symm .and. l2>l1) cycle 
          if (abs(l2-l1)/=dl) cycle

          ! progress info when l is large
          if (mod(l1,10)==0 .and. l1>0) write(STDOUT,'(3X,a,i4)') "l = ",l1
          
          l(1)=l1
          l(2)=l2
          

          ! find n range
          ! ------------
          call findnrange(2,orb,l,nmin,nmax)
          if (symm.and.l(2)==l(1).and.nmax(2)>nmax(1)) nmax(2)=nmax(1)
          in=nmax-nmin+1
          if (minval(in)<1) cycle
         
          ! assemble l-dependent rpot 
          !  -> needed for gradient and CAP of ECS
          !  -> don't assmebly earlier because of l dependence
          ! ---------------------------------------------------
          ! resize rpot
          if ( a_mode==120.or.a_mode==121 ) then
         
            if ( l2 > l1 ) isgn = +1
            if ( l1 > l2 ) isgn = -1
            
            rpot = -IMG * grad
            ! add l dependent local potential
            select case(m_grid%type)
            case("PSG","DVR")
              do i=1,size(rpot,1)
                rpot(i,i) = rpot(i,i) - IMG * isgn * max(l1,l2)*rlocal(i,1)
              end do
            case("FEM")
              rpot = rpot - IMG * isgn * max(l1,l2) * rlocal
            end select
          end if
          

          ! start calculating entries
          ! -------------------------
          allocate(rmat(in(1),in(2)))
          rmat=ZERO
          
          call radial1p1h(rmat,rpot,l,nmin,nmax,symm,conjugate)
          call writeout1p1h(a_fid,rmat,l,dl,nmin,nmax,orb,symm)

          deallocate(rmat)
       end do
    end do


  contains


    function caller_ir(a_r) result(res)
      implicit none
      real(dblprec), intent(in) :: a_r
      complex(dblprec) :: res

      res = m_grid%ecs_path(a_r,0)
      if ( res /= ZERO )  then
        res = ONE / res
      else
        res = ZERO
      end if
    end function caller_ir
    
    function caller_ir_ev(a_r) result(res)
      implicit none
      real(dblprec), intent(in) :: a_r
      complex(dblprec) :: res

      res = a_r * abs( m_grid%ecs_path(a_r,1) )
      if ( res/=ZERO )  then
        res = ONE / res
      else
        res = ZERO
      end if
    end function caller_ir_ev
    

    function caller_splitting(a_r) result(res)
      implicit none
      complex(dblprec), intent(in) :: a_r
      complex(dblprec) :: res

      res = splittingfct(a_r,m_split_r,m_split_smooth)
    end function caller_splitting
    

    subroutine radialfct(a_pot,a_fct,ao_r0,ao_ev)
      ! ao_ev : operator is for expectation value
      !         -> matters only for ECS
      use varwfct, only : m_orbital
      use grid_type
      implicit none

      integer,intent(in) :: a_pot
      complex(dblprec),intent(out) :: a_fct(:)
      real(dblprec),intent(in),optional :: ao_r0
      logical, intent(in), optional :: ao_ev
      
      integer :: i
      type(t_grid), pointer :: grid => null()
      complex(dblprec), allocatable :: rgrid(:),aux(:)
      
      logical :: ev


      if ( associated(m_orbital(0,0)%grid) ) then
        grid => m_orbital(0,0)%grid
      else
        stop " ERROR(radialfct): orbitals have no associated grid"
      end if
      

      if ( size(a_fct) /= grid%nbasis )   &
          stop " ERROR(radialfct) size inconsistency"


      ev = .false.
      if ( present(ao_ev) )  ev = ao_ev


      ! Initialize mapping in case complex scaling is used
      ! ------------------------------------------------------
      allocate(rgrid(grid%nbasis))
      allocate(aux(grid%nbasis))
      rgrid = ZERO
      aux = ONE

      if ( grid%absorbing == "CAP" .or. grid%absorbing == "SPL" .or. ev) then
        rgrid = grid%r
      else if ( grid%absorbing == "ECS" ) then
        rgrid = grid%cs%F(:,0)
      end if


      ! calculate potential
      if ( present(ao_r0) ) then
         forall(i=1:grid%nbasis , grid%r(i)>ao_r0)
            a_fct(i) = (rgrid(i)-ao_r0)**a_pot
         end forall
      else
         a_fct = rgrid**a_pot
      end if

      ! additional factor when calculating the expectation value when ECS is active
      if (ev .and. grid%absorbing=="ECS") then  
        a_fct = a_fct/abs(grid%cs%F(:,1))
      end if
    end subroutine radialfct



    elemental function splittingfct(a_r,a_r0,a_del)  result(res)
      ! calculate splitting function
      use varwfct, only : m_orbital
      use grid_type
      implicit none

      complex(dblprec) :: res
      real(dblprec),intent(in)  :: a_r0,a_del
      complex(dblprec), intent(in) :: a_r

      integer :: i

      real(dp) :: x

      ! calculate function
      x = (a_r-a_r0) / a_del
      if ( x>=ZERO ) then
        res = ONE/( ONE + exp(-x) )
      else
        res = exp(x)/(ONE+exp(x))
      end if
    end function splittingfct

  end subroutine phmatrix


  
  subroutine init_gradient(a_grad,ao_grid,ao_ev)
    ! ao_ev :  logical swtich for expectation values with ECS
    use varsystem, only : m_grid
    use grid_type
    implicit none
    
    complex(dblprec),intent(out) :: a_grad(:,:)
    type(t_grid), optional, target :: ao_grid
    logical, intent(in), optional :: ao_ev

    type(t_grid), pointer :: grid => null()
    complex(dblprec), allocatable :: rp(:),ecs_f(:)
    real(dblprec) :: fac
    integer :: i,j
    
    logical :: ev


    ev = .false.
    if ( present(ao_ev) )  ev = ao_ev


    if ( present(ao_grid) ) then
      grid => ao_grid
    else
      grid => m_grid
    end if


    ! CAP or ECS
    ! ----------
    allocate( ecs_f(grid%nbasis) )
    ecs_f = ONE   ! no mapping 
    if ( grid%absorbing == "ECS" ) then
      ecs_f = grid%cs%F(:,1)
    end if

    ! rp(x) = 1 / sqrt( r'(x) * f(x) )
    !  -> r'(x)     : derivative of the mapping between the real-r path and 
    !                 the [-1:1] region for Gauss-Lobatto
    !  -> ecs_f(x)  : derivative of the mapping between the complex-r path and
    !                 the real r-path ( only needed when complex scaling is used)
    allocate( rp(grid%nbasis) )
    rp = ZERO
    fac = grid%rmax/TWO * grid%zeta   
    forall(i=1:grid%nbasis)   
      rp(i) = sqrt( (ONE-grid%root(i)+grid%zeta)**2    &
                    / (fac*(TWO+grid%zeta))            &
                    / ecs_f(i)                         &
                  )
    end forall

    a_grad = ZERO

    ! store in a transposed way s.t. matrix-vector multi can be done
    ! via the first index of a_grad
    if ( grid%absorbing == "ECS" .and. ev ) then
      ! conjugate ECS derivative factor coming from the bra orbital
      forall(i=1:grid%nbasis,j=1:grid%nbasis, i/=j)
        a_grad(i,j) = rp(i)*conjg(rp(j))/( grid%root(j)-grid%root(i) )
      end forall
    else
      forall(i=1:grid%nbasis,j=1:grid%nbasis, i/=j)
        a_grad(i,j) = rp(i)*rp(j)/( grid%root(j)-grid%root(i) )
      end forall
    end if
    
  end subroutine init_gradient



  subroutine writeout1p1h(a_fid,a_rmat,a_l,a_dl,a_nmi,a_nma,a_orb,a_symm)
    use varsystem,only : m_useMsymm,m_llimit1p,m_matrix_format
    use varwfct,only   : m_orbital
    use general,only   : isocc,isact
    use angmom, only   : cleb
    implicit none

    integer,intent(in) :: a_fid,a_dl
    complex(dblprec),intent(in) :: a_rmat(:,:)
    integer,intent(in),dimension(2) :: a_l,a_nmi,a_nma,a_orb
    logical,intent(in) :: a_symm

    integer :: ii,im
    integer :: n1,n2,nid(2),n(2)
    complex(dblprec) :: matele
    real(dblprec) :: angular
    logical :: act


    ! angular part when all angular projections are 0 (m=0) 
    angular = (TWO*a_l(2)+1)/(TWO*a_l(1)+1)
    angular = sqrt(angular) * cleb(2*a_l(2),0,2*a_dl,0,2*a_l(1),0)**2

    n_1:do n1=a_nmi(1),a_nma(1)
      nid(1)=n1-a_nmi(1)+1
      n(1)=n1

      n_2:do n2=a_nmi(2),a_nma(2)
        nid(2)=n2-a_nmi(2)+1
        n(2)=n2
        if (a_symm.and.a_l(1)==a_l(2).and.n2>n1) cycle


        ! check if orbitals are occupied and active when orb=0,3
        if ((a_orb(1)==0 .or. a_orb(1)==3)) then
          act = .false.
            
          do ii=1,2
            if (isocc(n(ii),a_l(ii))) cycle
            do im=0,a_l(ii)
              if ( isact(n(ii),a_l(ii),im) ) act=.true.
            end do
          end do

          if (.not.act) cycle
        end if


        matele = a_rmat(nid(1),nid(2)) * angular

        if (abs(matele)>m_llimit1p) then
          if ( trim(m_matrix_format)=="bin") then
            write(a_fid) (n(ii),a_l(ii),ii=1,2),matele
          else
            write(a_fid,'(4i6,2ES16.8)') (n(ii),a_l(ii),ii=1,2),matele
          end if
        end if

      end do n_2
    end do n_1
    return          
  end subroutine writeout1p1h




  subroutine radial1p1h(a_fct,a_rpot,a_l,a_nmin,a_nmax,a_symm,a_conj)
    use varwfct,only : m_orbital
    implicit none

    integer, intent(in) :: a_l(2),a_nmin(2),a_nmax(2)
    complex(dblprec), intent(in)  :: a_rpot(:,:)
    complex(dblprec), intent(out) :: a_fct(:,:)

    logical :: a_symm,a_conj
    integer :: i,ns,n1,n2,nid(2)
    complex(dblprec), allocatable :: orb(:),orb1(:)
    
    
    a_fct = ZERO
    ns = size(m_orbital(0,0)%wfct)
    allocate( orb(ns) )
    orb = ZERO
    allocate( orb1(size(m_orbital(0,0)%wfct)) )
    orb1 = ZERO


    do n2=a_nmin(2),a_nmax(2)
      nid(2)=n2-a_nmin(2)+1
      
      if ( size(a_rpot,2) == 1 ) then
        orb = a_rpot(:,1)*m_orbital(n2,a_l(2))%wfct
      elseif ( size(a_rpot,2) == ns ) then
        forall(i=1:ns)
          orb(i) = sum(a_rpot(:,i)*m_orbital(n2,a_l(2))%wfct)
        end forall
      else
        orb = ZERO
      end if

      do n1=a_nmin(1),a_nmax(1)
        nid(1)=n1-a_nmin(1)+1
        if (a_symm .and. a_l(1)==a_l(2) .and. n2>n1) cycle
        
        if (a_conj) then
          a_fct(nid(1),nid(2)) = dot_product( m_orbital(n1,a_l(1))%wfct , orb )
        else
          a_fct(nid(1),nid(2)) = sum( m_orbital(n1,a_l(1))%wfct * orb )
        end if

      end do
    end do
    
    ! integration norm (Gauss quadrature)
    a_fct = a_fct /m_orbital(0,0)%grid%norm**2
  end subroutine radial1p1h

end module PHMatrices
