module FEM_Hamiltonian

  !
  !**PURPOSE
  !  -------
  !  The MODULE Hamiltonian contains all necessary routines and function to 
  !  build the Hamiltonian
  !
  !**DESCRIPTION
  !  -----------
  !  The following subroutines (sbr) and functions (fct) are part of 
  !  this module:
  !  
  !  -> (sbr) init_fem_matrices : initiates FEM matrices
  !  -> (sbr) buildUFEM         : build local potential + angular potential
  !  -> (fct) local_pot         : an auxilary function to use with buildUFEM,
  !                               represents the local potential = nuclear potential +
  !                               angular potential + CAP
  !  -> (sbr) buildCoulomb      : computes the matrix of Coulomb e-e interaction
  !  -> (sbr) buildVFEM         : mean-field potential  
  !  -> (sbr) buildSymT         : build radial kinetic operator in symmetric form
  !  -> (sbr) buildOv           : build overlap
  !                               the FE basis functions are not orthogonal
  !  -> (fct) local_unity       : an auxilary function to use with buildOv or
  !                               in any intergartion where basis functions are integrated with 1
  !  -> (fct) local_unity_cmplx : an auxilary function to use with buildSymT or
  !                               in any intergartion where basis functions are integrated with complex 1
  ! 
  !  -> m_pot                   : module variable of type t_local_potantial
  !                               needed for the calculation of U m.e.
  !  -> (sbr) init_m_pot        : initializes the m_pot
  !
  !**AUTHOR
  !  ------
  !  written by Arina Sytcheva in April 2013
  !
  !**VERISON
  !  -------
  !  svn info: $Id: FEM_Hamiltonian.f90 1660 2015-05-11 21:31:57Z spabst $
  !-----------------------------------------------------------------------
  !
  use constants, dp=>dblprec
  use varwfct, only : t_orbital
  use grid_type
  use gq_integrator,  only : gq_caller,t_fct_cplx,t_fct_real

  implicit none
  
  interface build_fem_matrix
    module procedure build_fem_matrix_real, build_fem_matrix_cmplx
  end interface

  type :: t_local_potential
    integer :: angmom = 0
    integer :: charge = 0
    real(dp) :: eta = ZERO
    real(dp) :: rabsorb = ZERO
    logical  :: initialized = .false.
  end type t_local_potential

  type(t_local_potential),private :: m_pot


  ! FEM r12 Coulomb arrays
  complex(dp),allocatable, private :: m_coul1d_s(:,:,:),m_coul1d_l(:,:,:),m_coul2d(:,:,:,:,:)

contains


  subroutine init_m_pot(a_l,a_z,a_cap,a_rabs)
    
    integer,intent(in)      :: a_l
    real(dp),intent(in)     :: a_z
    real(dp),intent(in)     :: a_cap
    real(dp),intent(in)     :: a_rabs

    m_pot = t_local_potential(a_l,a_z,a_cap,a_rabs, .true.)

  end subroutine init_m_pot



  subroutine init_fem_matrices(ao_mode)
    !
    !  initialize fem matrices
    !  ao_mode   : 0 = initialize matrices needed for HF (default)
    !              1 = initialize matrices for Matrix
    !              2 = initialize matrices for TDCIS 
    !
    use varsystem,only : m_nuccharge,m_cap_strength,m_Rabsorb     &
                        ,m_lmax,m_lsum,m_nelec,m_nocoulomb        &
                        ,m_grid, m_fem_nintpts
    use varmatrix, only : m_fem_ov ,  m_fem_ke , m_fem_u          &
                         , m_fem_dipole                           &
                         ,m_fem_gradient                          &
                         ,m_fem_ev_gradient, m_fem_ev_dipole      &
                         ,m_fem_ev_dipacc                         &
                         ,m_femdvr_trafo


    use reduce_lapack, only : cholesky_decomp
    !use analytic_integrator, only : int_caller
    use grid, only : build_femdvr_trafo

    implicit none

    integer,intent(in),optional :: ao_mode
    integer,parameter :: fe_degree = 3
    integer :: l,nbasis

    integer :: mode
    complex(dp), allocatable :: mat(:,:)
    real(dp), allocatable :: auxr(:)
    

    if( .not. allocated(m_grid%r) ) then
      stop " ERROR(init_fem_matrices): grid is not initialized"
    end if

    nbasis = m_grid%nbasis

    mode = 0
    if ( present(ao_mode) )  mode = ao_mode


    modeselect:select case(mode) 
    case(0)

      allocate( m_fem_ke(nbasis,nbasis) )
      m_fem_ke = ZERO
      allocate( m_fem_ov(nbasis,nbasis) )
      m_fem_ov   = ZERO
      allocate(m_fem_u(nbasis,nbasis,0:m_lmax))
      m_fem_u  = ZERO


      ! build overlap
      !call int_caller(m_fem_ov, m_grid, m_grid%rmin, m_grid%rmax, .false., 0)  ! analytic integration
      call build_fem_matrix(m_fem_ov, m_grid)  ! numeric integration
      !call cholesky_decomp(m_fem_ov,m_fem_ovup)  ! not needed anymore


      !fem_ke = ZERO
      !call int_caller(fem_ke, m_grid, m_grid%rmin, m_grid%rmax, .true., 0)  ! analytic integration
      ! numeric integration
      call build_fem_matrix(m_fem_ke, m_grid, kinetic_mass, ao_deriv_l=.true., ao_deriv_r=.true.)  
      call build_fem_matrix(m_fem_u(:,:,0), m_grid, kinetic_potential)  
      m_fem_ke = m_fem_ke/TWO  + m_fem_u(:,:,0)
      
      ! local potentials
      ! -----------------
      m_fem_u(:,:,0) = ZERO 
      loop_ang_mom: do l=0,m_lmax
        call init_m_pot(l,m_nuccharge,m_cap_strength,m_Rabsorb)
        call build_fem_matrix(m_fem_u(:,:,l), m_grid, local_pot)
        m_pot%initialized = .false.
      end do loop_ang_mom

      ! coulomb 
      if ( m_nelec>1 .and. .not.m_nocoulomb ) then
        call init_fem_coulomb(m_grid,m_lsum)
      end if

      ! find DVR eigenstates = FEM-DVR trafo
      allocate( m_femdvr_trafo(nbasis,nbasis) )
      m_femdvr_trafo = ZERO
      allocate(auxr(nbasis))
      call build_femdvr_trafo(m_femdvr_trafo,auxr,m_grid)
      deallocate(auxr)

      write(STDOUT,*) "init_fem_matrices: FEM matrices for HF have been computed"


    case(1,2)

      allocate(mat(nbasis,nbasis))  !temporary array
      mat = ZERO

      allocate(m_fem_ov(nbasis,nbasis))
      m_fem_ov   = ZERO
      allocate(m_fem_dipole(nbasis,nbasis))
      m_fem_dipole  = ZERO
      allocate(m_fem_gradient(nbasis,nbasis))
      m_fem_gradient = ZERO
      
      
      allocate(m_fem_ev_dipole(nbasis,nbasis))
      m_fem_ev_dipole  = ZERO
      allocate(m_fem_ev_gradient(nbasis,nbasis))
      m_fem_ev_gradient = ZERO
      allocate(m_fem_ev_dipacc(nbasis,nbasis))
      m_fem_ev_dipacc   = ZERO

      !! overlap 
      !!----------------------------------------------------------------------------
      !call int_caller(m_fem_ov, m_grid, m_grid%rmin, m_grid%rmax, .false., 0)
      call build_fem_matrix(m_fem_ov, m_grid)  ! numeric integration

      !! dipole moment
      !!----------------------------------------------------------------------------
      call build_fem_matrix(m_fem_dipole, m_grid, local_r)  ! numeric integration

      !! gradient
      !!---------------------------------------------------------------------------
      !!call gq_caller(m_fem_gradient, m_grid, ao_deriv_r=.true., ao_sym=.false.)
      call build_fem_matrix(m_fem_gradient, m_grid, ecs_inverse_map, ao_deriv_r=.true.)  
      mat = m_fem_gradient
      call build_fem_matrix(mat, m_grid, ecs_gradient_pot)  
      m_fem_gradient = m_fem_gradient + mat
      
      
      !! expectation values
      !!--------------------------
      
      call build_fem_matrix(m_fem_ev_dipole,   m_grid, ev_dipole)  
      call build_fem_matrix(m_fem_ev_dipacc,   m_grid, ev_dipacc)  
      call build_fem_matrix(m_fem_ev_gradient, m_grid, ev_gradient, ao_deriv_r=.true.)  
     
      mat = ZERO
      call build_fem_matrix(mat, m_grid, ev_gradient_pot)  
      m_fem_ev_gradient = m_fem_ev_gradient + mat

      !! Coulomb interaction
      !!----------------------------------------------------------------------------
      if ( m_nelec>1 .and. .not.m_nocoulomb .and. mode==1) then
        call init_fem_coulomb(m_grid,m_lsum)
      end if

      write(STDOUT,*) "init_fem_matrices: FEM matrices for TDCIS have been computed"
        
    end select modeselect

  end subroutine init_fem_matrices

!----------------------------------------------------------
!
!----------------------------------------------------------

  subroutine init_fem_coulomb(a_grid,a_l)
    ! generate coulomb matrix elements
    ! coul(mu, nu, rho, sigma, l),  see coulomb_gq(...) in gauss_q..._integrators.f90
    ! third index runs 1...5, because rho runs mu,...min(fe_basis, mu+5)
    ! fourth index runs 1...5, because sigma runs nu,...min(fe_basis, nu+5)
    
    implicit none
    integer, intent(in) :: a_l
    type(t_grid),intent(in) :: a_grid

    integer :: lc  ! needs to defined here s.t. function "pot" can access it
    
    integer :: nbasis , l
    complex(dp),allocatable :: aux(:,:,:,:,:)


    nbasis = a_grid%nbasis

    ! get 1D integrals for non-overlapping r1-r2 integrals
    if ( allocated(m_coul1d_s) )  deallocate(m_coul1d_s)
    if ( allocated(m_coul1d_l) )  deallocate(m_coul1d_l)
    if ( allocated(m_coul2d) )    deallocate(m_coul2d)
    allocate(m_coul1d_s(nbasis,6,0:a_l),m_coul1d_l(nbasis,6,0:a_l))
    allocate(m_coul2d(nbasis,6,6,6,0:a_l))
    m_coul1d_s = ZERO
    m_coul1d_l = ZERO
    m_coul2d   = ZERO
   
    
    do l=0,a_l
      ! this integration region is closer to 0
      call calc_int1d_nonolap(m_coul1d_s,'smaller')  
      ! this integration region is further away form 0
      call calc_int1d_nonolap(m_coul1d_l,'larger')  
      ! all 4 FEM functions overlap
      call calc_int2d_olap
    end do


  contains


    subroutine calc_int1d_nonolap(a_this,a_mode)
      ! 
      ! calculate 1D integral for the case that r1 and r2 integration don't overlap
      !  -> only need to distinguish between r1<r2 or r1>r2 which is defined by a_mode
      !  -> this integration is over r1
      !
      !  a_mode = 'smaller' : r1<r2
      !         = 'larger'  : r1>r2
      !
      use gq_integrator, only : gq_1d=>gauss_quadrature_cplx
      use FEM_basis, only : overlap
      implicit none
      complex(dp), intent(inout) :: a_this(:,:,0:)
      character(*), intent(in) :: a_mode

      integer :: n1,n3,in3


      select case(a_mode)
      case('smaller')  
        lc = l
      case('larger')   
        lc = -l-1
      case default
        stop " ERROR(calc_int1d_nonolap): wrong mode"
      end select

      do n1=1,nbasis
        do n3=n1,nbasis
          in3 = n3-n1+1
          if (.not.overlap(n1,n3)) cycle
                  
          a_this(n1,in3,l) = gq_1d(n1,n3,a_grid,ao_pot=pot)
        end do
      end do

    end subroutine calc_int1d_nonolap


    subroutine calc_int2d_olap
      use gq_integrator, only : gq_r12
      use FEM_basis, only : overlap,overlap4
      implicit none

      integer :: n1,n2,n3,n4,in2,in3,in4


      lp_n1:do n1 = 1 , nbasis     ! n1<n2 
      do n3 = n1 , min(n1+5,nbasis)
        if (.not.overlap(n1,n3)) cycle
        in3 = n3-n1+1
      
        lp_n2:do n2 = n1 , min(n1+5,nbasis)
        do n4 = n2 , min(n1+5,nbasis)
          if (.not.overlap(n2,n4)) cycle
          in2 = n2-n1+1
          in4 = n4-n1+1

          if (overlap4([n1,n2,n3,n4])) then 
            ! all 4 FEM function are overlapping 
            m_coul2d(n1,in2,in3,in4,l) = gq_r12([n1,n2,n3,n4],l,a_grid)
            m_coul2d(n1,in4,in3,in2,l) = m_coul2d(n1,in2,in3,in4,l)
          end if

        end do
        end do lp_n2

      end do
      end do lp_n1
      
      
      !lp_n2:do n2 = 1 , nbasis
      !do n4 = n2 , min(n2+5,nbasis)
      !  in4 = n4-n2+1
      !  if (.not.overlap(n2,n4)) cycle

      !  lp_n1:do n1 = 1 , n2     ! n1<n2 
      !  do n3 = n1 , min(n1+5,nbasis)
      !    in3 = n3-n1+1
      !    if (.not.overlap(n1,n3)) cycle

      !    if (.not.overlap4([n1,n2,n3,n4])) then 
      !      ! non-overlapping (n1,n3)-(n2,n4) pairs
      !      m_coul(n1,n2,in3,in4,m_l) = int1d_s(n1,in3) * int1d_l(n2,in4)
      !      m_coul(n2,n1,in4,in3,m_l) = m_coul(n1,n2,in3,in4,m_l)
      !    else
      !      ! overlapping (n1,n3)-(n2,n4) pairs
      !      m_coul(n1,n2,in3,in4,m_l) = gq_r12([n1,n2,n3,n4],m_l,a_grid)
      !      m_coul(n2,n1,in4,in3,m_l) = m_coul(n1,n2,in3,in4,m_l)
      !    end if

      !  end do
      !  end do lp_n1

      !end do
      !end do lp_n2
    end subroutine calc_int2d_olap


    complex(dp) function pot(a_r) result(res)
      real(dp), intent(in) :: a_r

      if ( lc>=0 .or. a_r/=ZERO ) then
        res = a_r**lc
      else
        res = ZERO
      end if
    end function pot

  end subroutine init_fem_coulomb


  complex(dp) elemental function coulomb_r12(a_n1,a_n2,a_n3,a_n4,a_l) result (res)
    ! calculate 2D radial integral involving the 4 FEM basis functions
    ! (n1,n3) and (n2,4) share the same radial integral
    use FEM_basis, only : overlap,overlap4
    implicit none
    integer, intent(in) :: a_n1,a_n2,a_n3,a_n4,a_l

    integer :: n1,n2,in2,in3,in4

    res = ZERO
    ! test field are allocated
    if ( .not.(allocated(m_coul1d_l) .and. allocated(m_coul1d_s).and.allocated(m_coul2d)) ) return

    if ( a_l<0 .or. a_l>ubound(m_coul2d,5) )  return
    ! for each pair needs to have an overlaping region 
    if ( .not.(overlap(a_n1,a_n3).and.overlap(a_n2,a_n4)) ) return

    ! find which pair is smaller
    if ( min(a_n1,a_n3)<=min(a_n2,a_n4) ) then
      n1  = min(a_n1,a_n3) 
      in3 = abs(a_n1-a_n3)+1
      n2  = min(a_n2,a_n4) 
      in4 = abs(a_n2-a_n4)+1
    else
      n1  = min(a_n2,a_n4) 
      in3 = abs(a_n2-a_n4)+1
      n2  = min(a_n1,a_n3) 
      in4 = abs(a_n1-a_n3)+1
    end if

    ! check if all 4 FEM overlap
    if (.not.overlap4([a_n1,a_n2,a_n3,a_n4])) then 
      ! non-overlapping (n1,n3)-(n2,n4) pairs
      res = m_coul1d_s(n1,in3,a_l) * m_coul1d_l(n2,in4,a_l)

    else
      ! overlapping (n1,n3)-(n2,n4) pairs

      ! reference everything to the smallest FEM index n1
      in2 = abs(n2-n1)+1
      in4 = in4 + in2 - 1 

      res = m_coul2d(n1,in2,in3,in4,a_l)
    end if

  end function coulomb_r12




  subroutine buildVFEM(a_V,a_l,a_orb,a_grid)
     ! generate mean-field potential defined by
     ! occupied orbitals given in orbital as defined in 
     !
     ! INPUT
     ! -----
     !   a_V  - direct and exhange part of the mean-field potential
     !   a_l  - angular momentum of sub Hamilton matrix
     !   a_orb - set of occupied orbitals
     !   a_grid - underlying grid
     !   a_number_subdivisions - parameter of the Gauss-Lobatto quadrature
     !
     use varsystem, only : m_rabsorb, m_nuccharge, m_cap_strength 
     use varwfct, only : m_ioccnl, m_occnl, m_orbital
     use grid_type
     implicit none

     complex(dp),intent(out) :: a_V(:,:)
     integer,intent(in) :: a_l
     type(t_orbital),intent(in) :: a_orb(:)
     type(t_grid),intent(in) :: a_grid

     complex(dp),allocatable :: mfdi(:,:),mfex(:,:)
     integer :: npoints, nbasis, nocc
! 
! 
!     ! rgrid : local assignment to real r-path (no complex scaling) or to the complex
!     !         r path (complex scaling)
!     complex(dp),allocatable :: rgrid(:)
!    
! 
     a_V = ZERO
     npoints = a_grid%npoints
     nbasis = a_grid%nbasis
     nocc = size(a_orb)


     if ( all(a_grid%r(:) .eq. ZERO) ) then
       print *, "buildVFEM: a_grid%r is empty"
       stop
     end if

     if (m_ioccnl /= nocc) then
       print *, "buildVFEM: m_ioccnl is larger than the number of occ orbitals supplied"
       stop
     end if
        
    if (nbasis > size(a_V,1) .or. size(a_V,1)/=size(a_V,2) ) then
      print *, "buildVFEM: inconsistent matrix size"
      stop
    end if

    allocate(mfdi(nbasis,nbasis))
    mfdi = ZERO
       
    allocate(mfex(nbasis,nbasis))
    mfex = ZERO


    call MFdirect(mfdi)
    call MFexchange(mfex)
    a_V = mfdi - mfex

 
  contains

    subroutine MFdirect(a_mfdi)
      !
      ! generate direct part of mean-field potential

      use varsystem, only : m_rabsorb, m_nuccharge &
                           ,m_cap_strength
      use FEM_basis, only: overlap
      implicit none

      complex(dp),intent(out) :: a_mfdi(1:,1:)
      integer :: fem1,fem2,fem3,fem4,ifem3  ! particle basis
      integer :: ii,lo  ! occupied orbital
      real(dp) :: occ

      integer :: ni
      complex(dp),allocatable :: auxorb(:,:),auxmat(:,:)


      mfdi = ZERO

      allocate(auxorb(nbasis,6))
      auxorb = ZERO
      allocate(auxmat(nbasis,6))
      auxmat = ZERO

      do ii = 1 , nocc
        lo = m_occnl(ii,2)
        occ = a_orb(ii)%occ
        if (occ<1) cycle
        
        auxorb(:,1) = auxorb(:,1) + occ * a_orb(ii)%wfct(:) * a_orb(ii)%wfct(:)
        do ifem3=2,6
          fem1 = nbasis - ifem3 + 1
          auxorb(1:fem1,ifem3) = auxorb(1:fem1,ifem3)    &
                          + 2*occ * a_orb(ii)%wfct(ifem3:nbasis) * a_orb(ii)%wfct(1:fem1)   
        end do
      end do

      do fem2 = 1 , nbasis
        do fem4 = fem2 , min(fem2+5, nbasis)   ! use (n2,n4)-symmetry
          if (.not.overlap(fem2,fem4)) cycle   ! not needed (and seem to give no speed up)

          do fem1=1,nbasis
            do fem3=fem1,min(fem1+5, nbasis) 
              ifem3 = fem3-fem1+1
              auxmat(fem1,ifem3) = coulomb_r12(fem1,fem2,fem3,fem4,0)
            end do
          end do

          a_mfdi(fem2,fem4) = sum( auxorb * auxmat )
          a_mfdi(fem4,fem2) = a_mfdi(fem2,fem4)
        end do
      end do

   end subroutine MFdirect



   subroutine MFexchange(a_mfex)
      ! generate exchange part of mean-field potential

      use varwfct,only : m_occnl,m_lomax
      use varsystem, only : m_lsum
      use angmom,only : cleb
      use FEM_basis, only: overlap

      implicit none
      
      complex(dp),intent(out) :: a_mfex(:,:)
      real(dp) :: clebsch(0:2*m_lomax,m_ioccnl)   ! array containing Clebsch-Gordan coefs squared
       
 
      integer :: i, fem1,fem2,fem3,fem4
      real(dp) :: aux, aux1
      integer :: ilc,lc(0:2*m_lomax),nilc, lo
      

      ! find range of Lc=|a_l-lo|...a_l+lo  that fits for all occupied orbitals
      lc = 0
      nilc = a_l+m_lomax - max(0,a_l-m_lomax)

      ! set up clebsch-gordon coefficient
      clebsch = ZERO
      do i=1,nocc !m_ioccnl
        lo = m_occnl(i,2)
        do ilc=0,nilc
          lc(ilc) = max(0,a_l-m_lomax) + ilc
          clebsch(ilc,i) = cleb(2*a_l,0, 2*lc(ilc),0, 2*lo,0)**2
        end do
      end do

      ! radial contribution
      lp_occorb: do i = 1, nocc  ! m_ioccnl
      lp_lc: do ilc = 0,nilc
        if ( clebsch(ilc,i) == ZERO )  cycle
        if ( lc(ilc)>ubound(m_coul2d,dim=5) )  cycle   ! no exchange beyond m_lsum

        lp_1: do fem1 = 1 , nbasis
        lp_2: do fem2 = fem1 , nbasis 

          aux1 = 0.0d0
          do fem3 = max(1,fem1-5), min(fem1+5,nbasis)
            do fem4 = max(1,fem2-5), min(fem2+5,nbasis)
              
              aux = coulomb_r12(fem1,fem2,fem3,fem4,lc(ilc))
              aux1  = aux1 + a_orb(i)%wfct(fem3)* aux * a_orb(i)%wfct(fem4)
            end do
          end do

          a_mfex(fem1,fem2) = a_mfex(fem1,fem2) + aux1 * clebsch(ilc,i) 
          a_mfex(fem2,fem1) = a_mfex(fem1,fem2)
        end do lp_2
        end do lp_1

      end do lp_lc
      end do lp_occorb

     end subroutine MFexchange

  end subroutine buildVFEM

!--------------------------------------------------------
!
!--------------------------------------------------------

  subroutine build_fem_matrix_cmplx(a_M,a_grid,ao_operator,ao_deriv_r,ao_deriv_l,ao_sym)
    !
    ! generates the an FEM matrix
    ! -----
    ! a_M - matrix
    ! a_grid  - underlying grid
    !
    implicit none

    complex(dp),intent(inout)  :: a_M(:,:)
    type(t_grid),intent(in) :: a_grid
    logical,intent(in),optional   :: ao_deriv_l,ao_deriv_r
    logical,intent(in),optional   :: ao_sym
   
    procedure(t_fct_real), optional :: ao_operator

    if ( size(a_M (1,:)) /= a_grid%nbasis .or. size(a_M (:,1)) /= a_grid%nbasis ) &
         stop "build_fem_matrix_cmplx: inconsistent matrix size" 

    call gq_caller(a_M, a_grid, ao_operator, ao_deriv_r, ao_deriv_l, ao_sym=ao_sym)
  end subroutine build_fem_matrix_cmplx



  subroutine build_fem_matrix_real(a_M,a_grid,ao_operator,ao_deriv_r,ao_deriv_l,ao_sym)
    !
    ! generates the an FEM matrix
    ! -----
    ! a_M - matrix
    ! a_grid  - underlying grid
    !
    implicit none

    real(dp),intent(inout)  :: a_M(:,:)
    type(t_grid),intent(in) :: a_grid
    logical,intent(in),optional   :: ao_deriv_l,ao_deriv_r
    logical,intent(in),optional   :: ao_sym
    
    complex(dp), allocatable :: m(:,:)
    
    procedure(t_fct_real), optional :: ao_operator


    if ( size(a_M (1,:)) /= a_grid%nbasis .or. size(a_M (:,1)) /= a_grid%nbasis ) &
         stop "build_fem_matrix_cmplx: inconsistent matrix size" 

    allocate(M(a_grid%nbasis,a_grid%nbasis))
    M = ZERO

    ! use complex version
    call gq_caller(M, a_grid, ao_operator, ao_deriv_r, ao_deriv_l, ao_sym=ao_sym)
    a_M = dble(M)
  end subroutine build_fem_matrix_real



!-----------------------------------------------------------------------
!  Potentials needed for TDCIS
!-----------------------------------------------------------------------

  complex(dp) function local_pot(a_x)
    use varsystem,only :  m_Rabsorb           &
                        , m_mp => m_modelpot  &
                        , m_absorb            &
                        , m_grid
    use splineinterpol, only : spline => eval_spline_object
    implicit none

    real(dp), intent(in) :: a_x
    complex(dp) :: x

    real(dp) :: eta, rabs, ch !, mom
    integer :: mom, i
    logical, save :: file_out = .false.


    local_pot = ZERO

    if ( .not. m_pot%initialized ) then
      !print *,"local_pot: module variable m_pot is not initialized"
      !stop
      return
    end if

    
    ch = m_pot%charge
    mom = m_pot%angmom

    if ( m_absorb=='ECS' ) then
      x = m_grid%ecs_path(a_x,0) 
    else
      x = a_x
    end if


    if ( m_mp%active ) then
      ! model potential
      ! ---------------
      !  -> multiply r/F(r) on model potential (/= 1 only for complex scaling)
      !  -> assuming the complex scaling starts where model potential
      !     has the behavior ~ 1/r [substitude with 1/F(r) for complex scaling]
      !
      if ( x/=ZERO )  local_pot = spline(a_x,m_mp)  *  a_x/x
      
      !model potential
      !local_pot = 7.50D0 * x * x * exp(-x)
    else 
      ! nuclear potential
      ! -----------------
      if ( x/=ZERO )  local_pot = - ch / x 
    end if

    if ( x/=ZERO ) local_pot = local_pot + HALF * dble(mom * (mom + 1)) / x / x


    if ( m_absorb=='CAP' .and. m_pot%eta /= ZERO ) then
      rabs = m_pot%rabsorb
      eta = m_pot%eta
      if ( a_x > rabs) then
        local_pot = local_pot - IMG* eta * (x - rabs) * (x - rabs)
      end if
    end if

  end function local_pot

!---------------------------------------------------------------------
  
  elemental complex(dp) function local_r(x) result(res)
    use varsystem, only : m_grid
    implicit none 
    real(dp), intent(in) :: x

    res = m_grid%ecs_path(x,0)
  end function local_r
  
!---------------------------------------------------------------------
  
  elemental complex(dp) function local_dipacc(x) result(res)
    use varsystem, only : m_grid
    implicit none 
    real(dp), intent(in) :: x

    if (x/=ZERO) then
      res = ONE/m_grid%ecs_path(x,0)**2
    else
      res = ZERO
    end if
  end function local_dipacc
  
!---------------------------------------------------------------------

  elemental complex(dp) function local_r2(x) result(res)
    use varsystem, only : m_grid
    implicit none 
    real(dp), intent(in) :: x
    
    res = m_grid%ecs_path(x,0)**2
  end function local_r2
  
!---------------------------------------------------------------------

  elemental complex(dp) function local_unity(x) result(res)
    implicit none 
    real(dp), intent(in) :: x
    res = ONE
  end function local_unity

!---------------------------------------------------------------------

  complex(dp) function kinetic_mass(x)  result(res)
    !
    !  With ECS path way F(r), kinetic operator transforms from -1/2*d^2_r -> -1/2*d_r*1/f^2*d_r + (2 f*f'' - 5 f'^2)/(8f^4)
    !  and has a new "kinetic" and "potential"-like term.
    !  -> f(r) = d_r F(r) = F'(r)
    !
    !  This function calculates the "mass" term 1/f^2 for the kintetic-like term.
    !
    use varsystem, only : m_grid
    implicit none
    real(dp), intent(in) :: x

    
    res = m_grid%ecs_path(x,1)**2
    if ( res/=ZERO )  then
      res = ONE/res
    else  ! should not happen
      res = ZERO
    end if

  end function kinetic_mass

!---------------------------------------------------------------------
  
  elemental complex(dp) function kinetic_potential(x)  result(res)
    !
    !  With ECS path way F(r), kinetic operator transforms from -1/2*d^2_r -> -1/2*d_r*1/f^2*d_r + (2 f*f'' - 5 f'^2)/(8f^4)
    !  and has a new "kinetic" and "potential"-like term.
    !  -> f(r) = d_r F(r) = F'(r)
    !
    !  This function calculates the potential-like term.
    !
    use varsystem, only : m_grid
    implicit none
    real(dp), intent(in) :: x
    complex(dp) :: f,f1,f2

   
    res = ZERO
    if ( m_grid%absorbing == "ECS" ) then
      f  = m_grid%ecs_path(x,1)
      f1 = m_grid%ecs_path(x,2)
      f2 = m_grid%ecs_path(x,3)
      if ( f/=ZERO)   res = (TWO*f*f2 - 5.D0*f1**2) / (8.d0 * f**4)
    end if
  end function kinetic_potential



  complex(dp) function ecs_inverse_map(a_x) result(res)
    use varsystem, only : m_grid
    implicit none
    real(dp), intent(in) :: a_x

    res = m_grid%ecs_path(a_x,1)
    if ( res/=ZERO )  then
      res = ONE/res
    else  ! should not happen
      res = ZERO
    end if
  end function ecs_inverse_map


  complex(dp) function ecs_gradient_pot(a_x)  result(res)
    use varsystem, only : m_grid
    implicit none
    real(dp), intent(in) :: a_x

    res = m_grid%ecs_path(a_x,1)**2
    if ( res/=ZERO )  then
      res = -HALF * m_grid%ecs_path(a_x,2)/res
    else  ! should not happen
      res = ZERO
    end if
  end function ecs_gradient_pot



  complex(dp) function ev_gradient(a_x)  result(res)
    use varsystem, only : m_grid
    implicit none
    real(dp), intent(in) :: a_x

    res = abs(m_grid%ecs_path(a_x,1))
    if ( res/=ZERO )  then
      res = ONE/res
    else  ! should not happen
      res = ZERO
    end if
  end function ev_gradient


  complex(dp) function ev_gradient_pot(a_x)  result(res)
    use varsystem, only : m_grid
    implicit none
    real(dp), intent(in) :: a_x

    res = m_grid%ecs_path(a_x,1)
    res = res * abs(res)
    if ( res/=ZERO )  then
      res = -HALF * m_grid%ecs_path(a_x,2) /res
    else  ! should not happen
      res = ZERO
    end if
  end function ev_gradient_pot


  complex(dp) function ev_dipole(a_x)  result(res)
    ! r/ |f(r)| where f(r) = F'(r)
    use varsystem, only : m_grid
    implicit none
    real(dp), intent(in) :: a_x

    res = abs(m_grid%ecs_path(a_x,1))
    if ( res/=ZERO )  then
      res = a_x/res
    else  ! should not happen
      res = ZERO
    end if
  end function ev_dipole
  
  
  complex(dp) function ev_dipacc(a_x)  result(res)
    ! r/ |f(r)| where f(r) = F'(r)
    use varsystem, only : m_grid
    implicit none
    real(dp), intent(in) :: a_x

    res = abs(m_grid%ecs_path(a_x,1))
    if ( res/=ZERO .and. a_x/=ZERO )  then
      res = ONE/res/a_x**2
    else  ! should not happen
      res = ZERO
    end if
  end function ev_dipacc


end module FEM_Hamiltonian


