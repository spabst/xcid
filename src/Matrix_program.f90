program Matrix_program
  !
  !**PURPOSE
  !  -------
  !  Calculate all 1-particle (1p) and all 2-particle (2p)
  !  matrix elements with respect to the symmetric inner product 
  !  ( Eq. 9 in Greenman, PRA 82, 023406 (2011)) needed
  !  for the time propagation of the CI coefficients alpha.
  !   -> see Eqs. 5 in Greenman, PRA 82, 023406 (2011)
  !  It includes overlap, dipole moment, and Coulomb interaction.
  !
  !  Furthermore, calculate matrix elements needed for generating
  !  expectation values. Here the hermitian inner product ( Eq. 11 in 
  !  Greenman, PRA 82, 023406 (2011)) is used.
  !  This includes the dipole moment and the dipole acceleration.
  !
  !
  !**DESCRIPTION
  !  -----------
  !  Coulomb matrix element program within the CIS model
  !   - calculate direct and exchange terms
  !   - perform approximation when difference in ang. mom is larger than
  !       a threshold lprx:
  !       -> v_aijb=0  
  !
  !**VARIABLES
  !  ---------
  !  g_nswitch1p1p, g_nswitch2p2h : 
  !                 number of 1p1h/2p2h matrices that can be calculated
  !
  !  g_switch1p1h : Markers activating matrix element calculations
  !  g_switch2p2h   Mode defines which markers are set                
  !                 g_switch1p1h markes only 1p1h matrix calculations 
  !                 g_switch2p2h markes only 2p2h matrix calculations 
  !                 
  !                 switch1p1h: two indices ele,idx: 
  !                   idx   : idx of the switch (1...g_nswitch1p1h)
  !                     = 1 : mode=101 overlap matrix
  !                     = 2 : mode=102 splitting function
  !                     = 3 : mode=110 Dipole (symmetric)
  !                     = 4 : mode=111 Dipole (hermitian)
  !                     = 5 : mode=112 Dipole acceleration
  !                     = 6 : mode=120 kinetic momentum (antisymmetric)
  !                     = 7 : mode=121 kinetic momentum (hermitian)
  !                     = 8 : mode=103 radial gradient (for tsurf)
  !         
  !                   ele=1 : information about switch status (0/1)
  !                   ele=2 : mode connecting to the matix type  
  !
  !                 switch2p2h: one index idx: 
  !                   idx   : idx of the switch (1...g_nswitch2p2h)
  !                     = 1 : Coulomb matrix (v_ijkl)
  !                     = 2 : Coulomb matrix (v_ajib)
  !                     = 3 : Coulomb matrix (v_ajbi)
  !
  !**AUTHOR
  !  -------
  !  (c) and written by Stefan Pabst, 2011
  !
  !**VERISON
  !  -------
  !  svn info: $Id: Matrix_program.f90 1718 2015-05-25 14:36:59Z spabst $
  !---------------------------------------------------------------------
  !
  use constants
  use basics, only : timeinfo
  use svn, only    : m_projectrev
  use VarSystem, only : g_program=>m_program,m_svnrev  
  implicit none
  integer, parameter :: g_nswitch1p1h=8
  integer, parameter :: g_nswitch2p2h=3

  integer :: g_switch1p1h(2,g_nswitch1p1h)
  logical :: g_switch2p2h(g_nswitch2p2h)

  integer :: g_tp(8,3)=0
  
  call date_and_time(values=g_tp(:,1))

  ! Matrix program is running
  g_program = 1

  ! pass on-the-fly determined global svn version
  ! to module "general"
  m_svnrev = m_projectrev

  ! ---------------
  ! | preparation |
  ! ---------------
  call prepare
  call date_and_time(values=g_tp(:,2))



  ! ----------------------------------
  ! | 1-particle matrix calculations |
  ! ----------------------------------
  call one_ph_matrices(g_switch1p1h)

  ! -----------------------------
  ! | load Coulomb calculations |
  ! ----------------------------- 
  call two_ph_matrices(g_switch2p2h)

  ! -----------
  ! | the end |
  ! -----------
  call date_and_time(values=g_tp(:,3))
  call timeinfo(g_tp)
  write(STDOUT,*) 
  write(STDOUT,*)  "----------------------------------------------"
  write(STDOUT,*)  "Matrix elements are created...have fun with it"
  write(STDOUT,*)  "------------------------------------------------"
  write(STDOUT,*)  "'Denn es ist ausgezeichneter Menschen unwuerdig,"
  write(STDOUT,*)  " gleich Sklaven Stunden zu verlieren mit Berech-"
  write(STDOUT,*)  " nungen.' [Gottfried Wilhelm von Leibniz]"
  write(STDOUT,*) 


!##########################################
!##########################################
contains
!##########################################
!##########################################

! --- input routines ---


  subroutine prepare
    use varsystem,only : m_lmax,m_lprx,m_focc,m_forb,m_fenergy,m_SOcoupl   &
                        ,m_grid,m_grid_type,m_grid_files,m_fem_nintpts

    use varwfct,only : m_lomax,m_actoccorb,m_orbital

    use read,only :    readorb,readoccupied,readenergy,readsystempara

    use general,only : defaultvalue,printinfo,printvariables,init_idoccrev  &
                      ,assign_active_orbitals
    use grid, only : initialize_grid
    use Legendre, only : Legendre_P
    use FEM_Hamiltonian,only : init_FEM_matrices
    use femdvr_hamil,only : init_femdvr_matrices
    implicit none

    integer :: ijunk
    character(LEN=clen) :: input,txt
    integer :: mode
    logical :: ex


    ! Header
    ! ------
    call printinfo(g_program,ao_header=-1)

    call getarg(1,input)  
    ! show help and exit program
    if ( trim(input) == "-v" ) then
      call printhelp
      stop
    end if

    ! check existence of the input file
    ex = .FALSE.
    inquire(file=input,exist=ex)

    ! info output
    if (.not.ex) then
      ! input file doesn't exist => assume is mode info
      txt=input
    else
      ! 2. argument defines which calculation will be performed
      call getarg(2,txt)
    end if
    

    ! initialize switches
    g_switch1p1h = 0
    g_switch1p1h(2,1) = 101
    g_switch1p1h(2,2) = 102
    g_switch1p1h(2,3) = 110
    g_switch1p1h(2,4) = 111
    g_switch1p1h(2,5) = 112
    g_switch1p1h(2,6) = 120
    g_switch1p1h(2,7) = 121
    g_switch1p1h(2,8) = 103
    
    g_switch2p2h = .false.


    ! set switches
    if ( len_trim(txt)/=0 ) then
      read(txt,*)  mode
    else
      mode = -1
    end if

    select case (mode)
    case(0)  ! all matrices
       g_switch1p1h(1,:) = 1
       g_switch2p2h = .true.
       
    !--------------------------
    case(100) ! 1p1h matrices
       g_switch1p1h(1,:) = 1
    case(101:199) 
       forall(ijunk=1:g_nswitch1p1h, g_switch1p1h(2,ijunk)==mode)  &
          g_switch1p1h(1,ijunk) = 1

    !--------------------------
    case(200) ! 2p2h matrices
       g_switch2p2h = .true.
    case(201) ! Coulomb (v_ijkl)
       g_switch2p2h(1) = .true.
    case(202) ! Coulomb (v_ijkl)
       g_switch2p2h(2:3) = .true.
    case(203) ! Coulomb (v_ajib)
       g_switch2p2h(2) = .true.
    case(204) ! Coulomb (v_ajbi)
       g_switch2p2h(3) = .true.

    case default
      call printhelp
      if (ex) then
        write(STDERR,'(/,x,a)') " ERROR: invalid/no mod provided"
      else
        write(STDERR,'(a)') " ERROR: input file and/or mode is invalid"
      end if
      stop
    end select

  
    ! read/store/show system parameters
    ! ---------------------------------
    ! default system parameters
    call defaultvalue    
    
    ! check input file
    call readsystempara(input)

    ! print parameters (mode=1 : designed for matrix calculation)
    call printinfo(g_program,ao_header=0)


    ! initialize grid (m_grid)
    ! -------------------------------------------------
    !  -> needed to find number of basis functions
 
    call initialize_grid(m_grid)

    ! ------------------
    
    ! define occupied and active orbitals
    write(STDOUT,*)  "reading occupation file... ",trim(m_focc)
    call readoccupied(m_focc)

    ! read in complex orbital energy
    write(STDOUT,*)  "reading energy file... ",trim(m_fenergy)
    call readenergy(m_fenergy)

    call assign_active_orbitals(m_actoccorb)


    write(STDOUT,*)  "reading orbital file... ",trim(m_forb)
    if (mode==201) then
      ! need only orbitals up to l=lomax
      call readorb(m_forb,m_orbital,m_grid,m_lomax)
    else
      ! need all orbitals up to l=lmax
      !  -> identical to call readorb(forb,forbc)
      call readorb(m_forb,m_orbital,m_grid,m_lmax)
    end if


    ! initialize occupied orbital objects
    call init_idoccrev
    write(STDOUT,*)

    ! ----------------------
    ! don't initialize FEM matrix before readoccupied (where m_lsum is set)
    ! initialize FEM matrices
    if ( m_grid%type=="FEM" )  call init_fem_matrices(1)
    ! initialize FEM-DVR matrices
    if ( m_grid%type=="DVR" )  call init_femdvr_matrices(1)
   
    ! ----------------------
   
    ! print important system variables
    call printvariables
  end subroutine prepare


  subroutine two_ph_matrices(a_switch)
    ! g_switches
    !     = 6 : Coulomb matrix (v_ijkl)
    !     = 7 : Coulomb matrix (v_ajib)
    !     = 8 : Coulomb matrix (v_ajbi)
    !
    ! use coulomb(datei,wmode,orb1,orb2,orb3,orb4,msymm1,msymm2,msymm3)
    !   (orb1,orb2|r_12|orb3,orb4)  - symmetric scalar product
    ! orbx (required)
    !  - defines which type of orbitals are used for position x
    !     = -1: active virtual + all occupied orbitals
    !     = 0 : all active orbitals
    !     = 1 : active virtual orbitals
    !     = 2 : all occupied orbitals
    !     = 3 : active occupied orbitals
    ! msymmx (optional)  <- details see description in module
    !
    use varsystem,only : m_fcouocc,m_fcoudir,m_fcouexc,m_SOcoupl  &
                        ,m_matrix_format                          &
                        ,m_nelec
    use coulombmatrix,only : coulomb
    use general,only    : printinfo,printvariables
    use inout, only     : opendir
    implicit none
    integer :: datei
    logical, intent(in) :: a_switch(:)


    if ( m_nelec==1 .and. any(a_switch) ) then
      write(STDOUT,*) "matrix.x: no 2-body Coulomb matrix needs to be calculated"
      write(STDOUT,*) "          for hydrogren-like system (nelec=1)"
      return
    end if

    datei=200
    if (a_switch(1)) then
      write(STDOUT,*) "Calculate occupied Coulomb matrix... "  &
                      //trim(m_fcouocc)

      if ( trim(m_matrix_format)=="bin") then
        call opendir(datei,file=trim(m_fcouocc)//".info")
        call printinfo(1,datei,'#')
        call printvariables(1,datei,'#')
        close(datei)
        call opendir(datei,file=trim(m_fcouocc),form="unformatted")
      else
        call opendir(datei,file=trim(m_fcouocc))
        call printinfo(1,datei,'#')
        call printvariables(1,datei,'#')
      end if
      
      call coulomb(datei,2,2,2,2)
      close(datei) 
      write(STDOUT,*) 
    end if
    
    ! ------------------
    
    if (a_switch(2)) then    ! direct term
      write(STDOUT,*) "Calculate direct Coulomb matrix... " &
           //trim(m_fcoudir)

      if ( trim(m_matrix_format)=="bin") then
        call opendir(datei,file=trim(m_fcoudir)//".info")
        call printinfo(1,datei,'#')
        call printvariables(1,datei,'#')
        close(datei) 
        call opendir(datei,file=trim(m_fcoudir),form="unformatted")
      else
        call opendir(datei,file=trim(m_fcoudir))
        call printinfo(1,datei,'#')
        call printvariables(1,datei,'#')
      end if

      call coulomb(datei,1,3,3,1,13,24) 
      close(datei) 
      write(STDOUT,*) 
    end if
    
    ! ------------------
    
    if (a_switch(3)) then    ! exchange term
      write(STDOUT,*) "Calculate exchange Coulomb matrix... " &
           //trim(m_fcouexc)

      if ( trim(m_matrix_format)=="bin") then
        call opendir(datei,file=trim(m_fcouexc)//".info")
        call printinfo(1,datei,'#')
        call printvariables(1,datei,'#')
        close(datei) 
        call opendir(datei,file=trim(m_fcouexc),form="unformatted")
      else
        call opendir(datei,file=trim(m_fcouexc))
        call printinfo(1,datei,'#')
        call printvariables(1,datei,'#')
      end if
      
      if (m_SOcoupl) then
        ! exchange part does not full constrain 
        ! m_1=m_4 and/or m_2=m_4
        call coulomb(datei,1,3,1,3) 
      else
        call coulomb(datei,1,3,1,3,14,23) 
      end if       
      close(datei) 

      write(STDOUT,*) 
    end if
        
  end subroutine two_ph_matrices


  subroutine one_ph_matrices(a_switch)
    ! calculate 1p1h matrices by calling 
    ! phmatrix(a_fid,a_mode,a_orb1,a_orb2)
    ! a_fid (required) 
    ! - is the stream ID of the file
    !
    ! a_mode (required)
    !  = 101 : overlap matrix  (lp=lq)
    !  = 102 : splitting function
    !  = 103 : radial gradient 
    !  = 110 : dipole matrix [symmetric]  (lp-lq=+-1)
    !  = 111 : dipole matrix [hermitian]  (lp-lq=+-1)
    !  = 112 : dipole acceleration matrix (lp-lq=+-1)
    !  = 120 : kinetic momentum [symmetric]  (lp-lq=+-1)
    !  = 121 : kinetic momentum [hermitian]  (lp-lq=+-1)
    !
    ! a_orbx (required)
    ! - defines which type of orbitals are used for position x
    !  = -1: all orbitals
    !  = 0 : all active orbitals
    !  = 1 : virtual orbitals
    !  = 2 : all occupied orbitals
    !  = 3 : only active occupied orbitals
    !
    use varsystem,only  : m_folap,m_fsplit,m_fdipsymm,m_fdipherm,m_fdipacc  &
                         ,m_fmomsymm,m_fmomherm,m_ftsurfmat                 &
                         ,m_matrix_format
    use phmatrices,only : phmatrix
    use general,only    : printinfo,printvariables
    use inout, only     : opendir,find_openpipe
    implicit none
    
    integer :: i,datei
    integer, intent(in) :: a_switch(:,:)
    character(clen) :: ffile



    datei=find_openpipe()

    do i=1,size(a_switch,2)
      ! only calculate matrices that selected
      if ( a_switch(1,i)/=1 ) cycle  

      ! mode specific section
      select case(a_switch(2,i))
      case(101)
        ffile = trim(m_folap)
        write(STDOUT,*) "Calculate overlap... ",trim(ffile)
      case(102)
        ffile = trim(m_fsplit)
        write(STDOUT,*) "Calculate splitting function... ",trim(ffile)
      case(103)
        ffile = trim(m_ftsurfmat)
        write(STDOUT,*) "Calculate gradiante (for tsurf)... ",trim(ffile)
      case(110)
        ffile = trim(m_fdipsymm)
        write(STDOUT,*) "Calculate dipole (symmetric)... ",trim(ffile)
      case(111)
        ffile = trim(m_fdipherm)
        write(STDOUT,*) "Calculate dipole (hermitian)... ",trim(ffile)
      case(112)
        ffile = trim(m_fdipacc)
        write(STDOUT,*) "Calculate dipole acceleration... ",trim(ffile)
      case(120)
        ffile = trim(m_fmomsymm) 
        write(STDOUT,*) "Calculate momentum operator (antisymm.)... ",trim(ffile)
      case(121)
        ffile =  trim(m_fmomherm)
        write(STDOUT,*) "Calculate momentum operator (hermitian)... ",trim(ffile)
      case default
        cycle
      end select

      ! check format
      if ( trim(m_matrix_format)=="bin") then
        call opendir(datei,file=trim(ffile)//".info")
        call printinfo(1,datei,'#')
        call printvariables(1,datei,'#')
        close(datei)
        call opendir(datei,file=trim(ffile),form="unformatted")
      else
        call opendir(datei,file=trim(ffile))
        call printinfo(1,datei,'#')
        call printvariables(1,datei,'#')
      end if

      ! calculate matrix
      ! ----------------
      call phmatrix(datei,a_switch(2,i),-1,-1)
      close(datei)
      write(STDOUT,*) 
    end do

  end subroutine one_ph_matrices


  subroutine printhelp
    implicit none

     write(STDOUT,*) 
     write(STDOUT,*)  'usage: matrix.x [input file / -v ] [mode]'
     write(STDOUT,*)  '----------------------------------------------'
     write(STDOUT,*)  ' mode =    0 - all one and two particle operators'
     write(STDOUT,*)  '      =  100 - all one particle operators'
     write(STDOUT,*)  '      =  200 - all two particle operators'
     write(STDOUT,*) 
    ! one particle operators
     write(STDOUT,*)  '      =  101 - overlap matrix'
     write(STDOUT,*)  '      =  102 - splitting function'
     write(STDOUT,*)  '      =  103 - radial gradient (for tsurf)'
     write(STDOUT,*)  '      =  110 - dipole matrix (symmetric)'
     write(STDOUT,*)  '      =  111 - dipole matrix (hermitian)'
     write(STDOUT,*)  '      =  112 - dipole acceleration matrix'
     write(STDOUT,*)  '      =  120 - momentum operator (antisymmetric)'
     write(STDOUT,*)  '      =  121 - momentum operator (hermitian)'
     write(STDOUT,*) 
    ! two particle operators
     write(STDOUT,*)  '      =  201 - Coulomb matrix (only occupied orbitals)'
     write(STDOUT,*)  '      =  202 - Coulomb matrices (v_ajib and v_vajbi)'
     write(STDOUT,*)  '      =  203   - direct Coulomb matrix (v_ajib)'
     write(STDOUT,*)  '      =  204   - exchange Coulomb matrix (v_ajbi)'
     write(STDOUT,*) 
  end subroutine printhelp

end program Matrix_program
