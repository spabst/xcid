module EigSlv
  ! --------------------------------------------------------------------------
  ! PURPOSE
  ! -------
  ! List of subroutines that provides an easy interface to the 
  ! underlying LAPACK routines, which calculate eigenvalue/eigenstates.
  ! 
  ! LAPACK URL: http://www.netlib.org/lapack/
  !
  ! DESCRIPTION
  ! -----------
  ! The following subroutines (sbr) and functions (fct) are part of 
  ! this module:
  !
  !
  !  -> (int) eigensolver     : interface to all these routines
  !  -> (sbr) get_DCGenSelect_mat:
  !                             general complex 
  !                             (calls get_DCGenSelect_sbr)
  !  -> (sbr) get_DCGenSelect_sbr:
  !                             general complex (non-generalized eigenvalue problem)
  !                             (uses ARPACK routine znaup and zneup)
  !  -> (sbr) get_DRSymSelect : real symmetric matrix with the ability
  !                             to select range of eigenvalues/vectors.
  !                             (use lapack routine: dsyevx)
  !
  ! LAPACK - INFO
  ! -------------
  ! NOT ALL OF THEM ARE IMPLEMENTED
  ! for real symmetric matrix
  ! - DSYEV  : all eigenvalues and eigenvectors
  ! - DSYEVD : all eigenvalues and eigenvectors by divide-and-conquer
  ! - DSYEVX : selected eigenvalues and eigenvectors
  ! - DSYEVR : selected eigenvalues and eigenvectors by divide-and-conquer
  ! - DSYGV  : all eigenvalues/eigenvectors of generalized symmetric-definite
  !
  ! for real nonsymmetric matrix
  ! - DGEEV  : all eigenvalues and left/right eigenvectors
  ! - DGEEVX : all eigenvalues and left/right eigenvectors with balancing
  ! - DGEGV / DGGEV : all eigenvalues/eigenvectors of generalized non-symmetric
  !
  ! for complex Hermitian matrix
  ! - ZHEEV  : all eigenvalues and eigenvectors
  ! - ZHEEVD : all eigenvalues and eigenvectors by divide-and-conquer
  ! - ZHEEVX : selected eigenvalues and eigenvectors
  ! - ZHEEVR : selected eigenvalues and eigenvectors by divide-and-conquer
  !
  ! for complex non-Hermitian matrix
  ! - ZGEEV  : all eigenvalues and left/right eigenvectors
  ! - ZGEEVX : all eigenvalues and left/right eigenvectors with balancing
  !
  ! AUTHOR INFORMATION
  ! ------------------
  ! (c) and written by Stefan Pabst in May 2011
  !
  ! Version info: $Id: EigSlv.f90 1956 2015-08-18 18:26:04Z spabst $
  ! ----------------------------------------------------------------------
  !
  implicit none
  integer,parameter :: dp = kind(1.D0) , STDERR = 0 , STDOUT = 6
  real(dp),parameter :: ONE=1.D0,ZERO=0.D0
  

  interface eigensolver
     module procedure get_DCGenAll, get_DRSymSelect, get_DCGenSelect_mat, get_DCGenSelect_sbr &
                      , get_DCGenAll_Gen, get_DRSymAll_Gen, get_DCGenAll_GenDiag
  end interface


contains


  ! DCGenAll = double complex,general matrix
  !                  ,all values,eigenvalue,eigenvector
  !            uses the LAPACK library
  subroutine get_DCGenAll(a_M,ao_val,ao_mode,ao_lvec,ao_order)
    ! Calculate right eigenstates of the complex matrix a_M
    !
    ! Lapack routine ZGEEV is used:
    ! -----------------------------
    !   Computes the eigenvalues and left and right eigenvectors of
    !   a general matrix.
    !
    ! INPUT / OUTPUT
    !-----------------
    ! ao_order: (in) - optional
    !           leading order of the matrix 
    !           => diagonalize ao_order x ao_order junk of a_M

    ! a_M     : (in/out) 
    !           IN : matrix to be diagonalized
    !           OUT: right eigenstates (if wanted) ordered as eigenvalue
    !                second matrix index is vector index
    !
    ! ao_lvec : (out) 
    !           left eigenstates (if wanted) ordered as eigenvalue
    !           second matrix index is vector index
    !
    ! ao_eval : (out) - optional
    !           Eigenvalues ordered with increasing Re(eigenvalue)
    !
    ! ao_mode : (in) - optional
    !           = 0 : eigenvalue & Right & Left eigenvector 
    !           = 1 : eigenvalue & Right eigenvector 
    !                 [default]
    !           = 2 : eigenvalue 
    !
    implicit none
    integer,intent(in),optional :: ao_order,ao_mode
    complex(dp),intent(inout) :: a_M(:,:)
    complex(dp),optional :: ao_val(:),ao_lvec(:,:)

    complex(dp),allocatable :: matrix(:,:),vl(:,:),vr(:,:),work(:)  &
         ,val(:)
    integer,allocatable :: idx(:)
    real(dp),allocatable:: rwork(:)

    integer :: i,ndim,mode,info,nwork,nvl,nvr
    character :: jobvr,jobvl

    mode = 1  ! default
    if (present(ao_mode)) mode = ao_mode
    
    ! get dimension
    ndim = size(a_M,1)
    if (present(ao_order)) ndim = ao_order
    if ( ndim < 1 ) then
      write(STDERR,*) " warning (get_DCGenAll): matrix order < 1 =>&
           & nothing to do"
      return
    end if

    ! check dimensions
    if ( ndim /= size(a_M,2) ) stop &
         " error (get_DCGenAll): matrix is not a square matrix"
    
    if ( present(ao_val) ) then
      if ( size(ao_val) < ndim )  stop &
           " error (get_DCGenAll): array for eigenvalues too small"
      ao_val = ZERO
    end if
    
    if ( present(ao_lvec) ) then
      if ( minval(shape(ao_lvec)) < ndim )  stop &
           " error (get_DCGenAll): array for left eigenvector too small"
      ao_lvec = ZERO
    end if

    ! allocate mode independet fields
    allocate( idx(ndim) , rwork(2*ndim) , val(ndim) , matrix(ndim,ndim) )
    idx = 0
    rwork = ZERO
    val = ZERO
    ! assign submatrix to auxiliary field
    matrix = a_M(1:ndim,1:ndim)
    a_M = ZERO

    ! prepare all fields needed for the given mode
    select case(mode)
    case(0)
      jobvr = "V"
      jobvl = "N"
      
      nvl = ndim
      nvr = ndim

      if (.not.present(ao_val))  stop &
        " error (get_DCGenAll): array for eigenvalues needed (mode=0)"

      if (.not.present(ao_lvec))  stop &
        " error (get_DCGenAll): array for left eigenvector needed (mode=0)"
      
    case(1) ! -----------------------------
      jobvr = "V"
      jobvl = "N"
      
      nvl = 1
      nvr = ndim

      if (.not.present(ao_val))  stop &
        " error (get_DCGenAll): array for eigenvalues needed (mode=0)"

    case(2) ! -----------------------------
      jobvr = "N"
      jobvl = "N"
      
      nvl = 1
      nvr = 1

      if (.not.present(ao_val))  stop &
        " error (get_DCGenAll): array for eigenvalues needed (mode=0)"

    case default ! --------------------------
      stop " error (get_DCGenAll): invalid mode"
    end select


    allocate( vl(nvl,ndim ) , vr(nvr,ndim) )
    vl = ZERO
    vr = ZERO
    
    ! find optimal size for work array
    ! ---------------------------------
    allocate(work(1))
    call ZGEEV(jobvl,jobvr         &
               ,ndim,matrix,ndim   &
               ,val                &
               ,vl,nvl             &
               ,vr,nvr             &
               ,work,-1,rwork      &
               ,info               &
              )

    
    nwork = int(real(work(1)))
    deallocate(work)
    allocate(work(nwork))
    work= ZERO

    ! diagonalize matrix
    ! ------------------
    call ZGEEV(jobvl,jobvr         &
               ,ndim,matrix,ndim   &
               ,val                &
               ,vl,nvl             &
               ,vr,nvr             &
               ,work,nwork,rwork   &
               ,info               &
              )
    
    if (info/=0) then
      write(STDOUT,*) "Matrix Diagonalization ended with info =",info
      stop ""
    end if

    ! order orbitals with increasing real part of orbital energy
    ! -----------------------
    idx = 0
    idx(1) = minval(minloc(real(val)))
    do i=2,ndim
      info = idx(i-1)
      idx(i) = minval(minloc( real(val) , real(val)>real(val(info)) ))
    end do


    ! assign ordered array back to outgoing fields
    ! --------------------
    if (present(ao_val)) then
      ao_val(1:ndim) = val(idx)  ! eigenvalues
    end if
    
    ! assign right eigenvector
    if (jobvr == "V" ) then
      forall(i=1:ndim)
        a_M(1:ndim,i) = vr(1:ndim,idx(i))
      end forall
    end if

    ! assign left eigenvector
    if (jobvl == "V" ) then
      forall(i=1:ndim)
        ao_lvec(1:ndim,i) = vl(1:ndim,idx(i))
      end forall
    end if
  end subroutine Get_DCGenAll




  ! DRSymSelect = double real, symmetric real matrix
  !               ,all values,eigenvalue,eigenvector
  !               uses the LAPACK library
  subroutine get_DRSymSelect(a_M,ao_val,ao_order,ao_mode,ao_min,ao_max)
    ! calculate eigenvectors of the real matrix a_M
    ! When ao_min and/or ao_max are not given calculate all eigenvalues
    !
    ! Lapack routine DSYEVX is used:
    ! ------------------------------
    ! computes selected eigenvalues and, optionally, eigenvectors of
    ! a real symmetric matrix.
    !
    ! INPUT / OUTPUT
    !-----------------
    ! ao_order: (in) - optional
    !           leading order of the matrix 
    !           => diagonalize ao_order x ao_order junk of a_M
    !
    ! a_M     : (in/out) 
    !           IN : matrix to be diagonalized
    !           OUT: eigenstates (if wanted) ordered as eigenvalue
    !                second matrix index is vector index
    !
    ! ao_eval : (out) - optional
    !           Eigenvalues ordered with increasing Re(eigenvalue)
    !
    ! ao_min : (in) - optional
    !           minimal value/index of eigenvalue
    !
    ! ao_max : (in) - optional
    !           maximal value/index of eigenvalue
    !
    ! ao_mode: (in) - optional
    !          = xy [= 10 is default]
    !          x = 1 : use range eigenvalues should lie in
    !          x = 2 : use index range to select eigenvalues
    !          y = 0 : eigenvalues and eigenvectors
    !          y = 1 : eigenvalues only
    !
    implicit none
    integer,intent(in),optional :: ao_order,ao_mode
    real(dp),parameter :: abstol = -ONE

    real(dp),intent(inout) :: a_M(:,:)
    real(dp),intent(inout),optional :: ao_val(:)
    real(dp),intent(in),optional :: ao_min,ao_max

    character :: jobz,range,uplo
    integer :: i,il,iu,m,ldz,lwork,info,ndim
    integer,allocatable :: iwork(:),ifail(:)

    real(dp),allocatable :: matrix(:,:),w(:),z(:,:),work(:)
    real(dp) :: vl,vu

    ! get dimension
    ndim = size(a_M,1)
    if (present(ao_order)) ndim = ao_order
    if ( ndim < 1 ) then
      write(STDERR,*) " warning (get_DRSymSelect): matrix order < 1 =>&
           & nothing to do"
      return
    end if

    ! check dimensions
    if ( ndim /= size(a_M,2) ) stop &
         " error (get_DRSymSelect): matrix is not a square matrix"
    
    if ( present(ao_val) ) then
      if ( size(ao_val) < ndim )  stop &
           " error (get_DRSymSelect): array for eigenvalues too small"
      ao_val = ZERO
    end if


    !set up arguments
    !-----------------
    uplo = "U"
    jobz = "V"
    Range = "A"
      
    ! value range is default when limits are given
    if (present(ao_min) .or. present(ao_max)) Range = "V" 
    
    if (present(ao_mode)) then
      if ( mod(ao_mode,10) == 0 ) jobz = "V"
      if ( mod(ao_mode,10) == 1 ) jobz = "N"
      if ( mod(ao_mode,10) >  1 ) stop   &
           " error (get_DRSymSelect): invalid mode" 

      if (ao_mode/10 == 1 ) range = "V"
      if (ao_mode/10 == 2 ) range = "I"
      if (ao_mode/10  > 2 ) stop   &
           " error (get_DRSymSelect): invalid mode" 
    end if
    
        
    ! find range of eigenvalues
    ! -------------------------
    ! value range
    vl = -1.D10
    vu = 1.D10
    if (present(ao_min)) vl = ao_min
    if (present(ao_max)) vu = ao_max
    ! index range
    il = 1
    iu = ndim
    if (present(ao_min)) il = floor(ao_min)
    if (present(ao_max)) iu = ceiling(ao_max)
    
    ! if range is large unset eigenvalue limit
    if ( range=="V" .and. vu-vl==2.d10  ) range="A"
    if ( range=="I" .and. iu-il==ndim-1 ) range="A"
    

    ldz = 1
    if ( jobz == "V" ) ldz = ndim


    ! allocate all necessary field    
    allocate( w(ndim) ,  iwork(5*ndim) , ifail(ndim) , z(ldz,ndim))
    w = ZERO
    z = ZERO
    iwork = ZERO
    ifail = 0
    
    allocate(matrix(ndim,ndim))
    matrix = a_M(1:ndim,1:ndim)
    allocate(work(1))

    ! find optimal working arrray
    call DSYEVX (    &
         JobZ,       &
         Range,      &
         uplo,       &
         ndim,matrix,ndim,      &
         vl,vu,il,iu,&
         abstol,     &    ! ABSTOL=-1 means default value
         m,w,        &
         z,ldz,      &
         work,-1,    &
         iwork,      &
         ifail,info  &
         )
    
    lwork = int(work(1))
    deallocate(work)
    allocate(work(lwork))
    work = ZERO

    ! find eigenvalues/vectors
    call DSYEVX (    &
         JobZ,       &
         Range,      &
         uplo,       &
         ndim,matrix,ndim,      &
         vl,vu,il,iu,&
         abstol,     &    ! ABSTOL=-1 means default value
         m,w,        &
         z,ldz,      &
         work,lwork, &
         iwork,      &
         ifail,info  &
         )
    
    if ( info < 0 ) stop  &
         " error (get_DRSymSelect): illegal value occurred in DSYEVX"
 
    if ( info > 0 ) then      
      do i=1,ndim
        if (ifail(i) /= 0 ) write(STDERR,*) &
             " warning (get_DRSymSelect): unconverged eigenvalue no.",ifail(i)
      end do
    end if
    
    ! assign results to outgoing variables
    if (present(ao_val)) then
      ao_val = ZERO
      ao_val = w
    end if

    if ( jobz == "V" ) then
      a_M = ZERO
      a_M(1:ndim,1:m) = z(1:ndim,1:m)
    end if
    
  end subroutine get_DRSymSelect


  ! --------------------------------------------------------
  ! |  ARPACK routines  |
  ! --------------------------------------------------------


  ! DCGenSelect_mat = Double precision, general, complex matrix, selects part of the spectrum,
  !                   matrix is provided
  !                   uses the ARPACK library
  subroutine get_DCGenSelect_mat(a_n,a_nev,a_mat,a_values,ao_vectors,ao_ivector,ao_which,ao_info)
    ! Options:
    ! -------
    ! ao_vectors: eigenvectors (only returned if array is provided)
    ! ao_ivector: provide initial starting vector
    ! ao_which : default SM
    ! ao_info  : default 0 ; if additional convergence information requested 1
    !
    ! ARPACK - INFO
    ! -------------
    ! - znaupd  : computes nev eigenvalues and nconv Arnoldi vectors
    ! - zneupd  : computes and returnes Ritz values (eigenvalues) and
    !             Ritz vectors (eigenvectors) (if requested, "mode = .true.")
    ! - dznrm2  : Level 1 BLAS that computes the norm of a complex vector.
    ! - dlapy2  : LAPACK routine to compute sqrt(x**2+y**2) carefully.
    ! - zaxpy   : Level 1 BLAS that computes y <- alpha*x+y.
    !
    ! ----------------------------------------------------------------------
    implicit none

    integer, intent(in)          :: a_n,a_nev
    integer, intent(in),optional :: ao_info
    character(2), intent(in), optional :: ao_which
    complex(dp), intent(in), optional :: ao_ivector(:)


    complex(dp),intent(in) :: a_mat(:,:)
    complex(dp)            :: a_values(:)
    complex(dp),optional   :: ao_vectors(:,:)

    integer :: info
    character(2) :: which


    !  check size of input fields
    !  --------------------------
    ! matrix
    if ( size(a_mat,1)/=size(a_mat,2) )    &
      stop " ERROR(eigsolver): matrix is not quadratic"

    if ( size(a_mat,1) /= a_n )            &
       stop " ERROR(eigsolver): matrix size is inconsistent"


    info = 0
    if ( present(ao_info) ) info = ao_info

    which = "SM"
    if ( present(ao_which) ) which = ao_which


    if ( present(ao_vectors) ) then
      if ( present(ao_ivector) ) then
        call get_DCGenSelect_sbr(a_n,a_nev,matvec,a_values,ao_vectors,ao_ivector,ao_which=which,ao_info=info)
      else
        call get_DCGenSelect_sbr(a_n,a_nev,matvec,a_values,ao_vectors,ao_which=which,ao_info=info)
      end if
    else
      if ( present(ao_ivector) ) then
        call get_DCGenSelect_sbr(a_n,a_nev,matvec,a_values,ao_ivector=ao_ivector,ao_which=which,ao_info=info)
      else
        call get_DCGenSelect_sbr(a_n,a_nev,matvec,a_values,ao_which=which,ao_info=info)
      end if
    end if

    contains

       subroutine matvec(a_nn,a_init,a_final,ao_para)
        implicit none

        integer,intent(in) :: a_nn
        complex(dp)        :: a_final(a_nn),a_init(a_nn)
        real(dp),intent(in),optional :: ao_para

        a_final = matmul(a_mat,a_init)
      end subroutine matvec

  end subroutine get_DCGenSelect_mat



  ! DCGenSelect_sbr = Double precision, complex, general matrix, selects part of the spectrum,
  !                   subroutine for the matrix multiplication is provided
  !                   uses the ARPACK library 
  !                   (stop program if compiled without ARPACK library)
  subroutine get_DCGenSelect_sbr(a_n,a_nev,a_sbr,a_values,ao_vectors,ao_ivector,ao_which,ao_info,ao_para)
   ! Options:
   ! -------
   ! ao_vectors: eigenvectors (only returned if array is provided)
   ! ao_ivector: provide initial starting vector
   ! ao_which : default SM  (smallest magnitude)
   ! ao_info  : default 0 ; if additional convergence information requested 1
   ! ao_para  : additional parameter for the matrix-vector subroutine (e.g. external field)
    implicit none

    integer, intent(in)          :: a_n,a_nev
    integer, intent(in),optional :: ao_info
    character(2), intent(in), optional :: ao_which
    complex(dp), intent(in), optional :: ao_ivector(:)
    
    complex(dp),optional   :: ao_vectors(:,:)
    complex(dp)            :: a_values(:)
    
    integer  :: i


    ! external matrix-vector subroutine
    ! ---------------------------------
    real(dp),intent(in), optional :: ao_para
    external :: a_sbr


    !
    !     %--------------%
    !     | Local Arrays |
    !     %--------------%
    !
    integer :: iparam(11), ipntr(14)
    logical,allocatable      :: selec(:)

    complex(dp),allocatable  :: ax(:), d(:), v(:,:), workd(:)    &
                               ,workev(:), resid(:), workl(:)

    real(dp), allocatable    :: rwork(:), rd(:,:)
    !
    !     %---------------%
    !     | Local Scalars |
    !     %---------------%
    !
    character   :: bmat*1, which*2
    integer     :: ncv
    integer     :: ido, lworkl, info, iinfo, j,        &
                   ierr, nconv, maxitr, ishfts
    complex(dp) :: sigma
    real(dp)    :: tol, para
    logical     :: mode

    !
    !     %-----------------------------%
    !     | BLAS & LAPACK routines used |
    !     %-----------------------------%
    !
    real(dp) ::  dznrm2, dlapy2
    external ::  dznrm2, zaxpy, dlapy2



#ifndef ARPACK
   write(STDERR,*) ' ERROR(get_DCGenSelect_sbr): CANNOT USE ARPACK (COMPILED WITHOUT)'
   stop
#endif

    ! eigenvalues
    if ( size(a_values) < a_nev )         &
       stop " ERROR(eigsolver): field for eigenvalues is too small"

    ! eigenvectors
    mode = .false.
    if ( present(ao_vectors) )   mode = .true.

    if ( mode ) then
      if ( size(ao_vectors,1) < a_n )         &
         stop " ERROR(eigsolver): vector field is too small to store the entire vector"

      if ( size(ao_vectors,2) < a_nev )       &
         stop " ERROR(eigsolver): vector field is to small to store all vectors"
    end if

    iinfo = 0
    if ( present(ao_info) ) iinfo = ao_info

    para = ZERO
    if ( present(ao_para) ) para = ao_para

    !
    !     %-----------------------%
    !     | Executable Statements |
    !     %-----------------------%
    ncv   =  max( 2*a_nev , 3 )
    bmat  = 'I'
    which = 'SM'
    if ( present(ao_which) ) which = ao_which

    !     %---------------------------------------------------%
    !     | The work array WORKL is used in ZNAUPD as         |
    !     | workspace.  Its dimension LWORKL is set as        |
    !     | illustrated below.  The parameter TOL determines  |
    !     | the stopping criterion. If TOL<=0, machine        |
    !     | precision is used.  The variable IDO is used for  |
    !     | reverse communication, and is initially set to 0. |
    !     | Setting INFO=0 indicates that a random vector is  |
    !     | generated to start the ARNOLDI iteration.         |
    !     %---------------------------------------------------%
    allocate( resid(a_n) )
    ! weaker convergence criterion find more eigenstates (that seems to be exact)
    !  -> tol=0 has trouble finding all eigenstates requested
    tol    = 1.d-10  
    ido    = 0
    if ( present(ao_ivector) ) then
      if ( size(ao_ivector)<a_n )  &
        stop " ERROR(eigsolver): size of initial starting vector is too small"
      info  = 1
      resid = ao_ivector(1:a_n)
    else
      info  = 0
      resid = ZERO
    end if

    !    %---------------------------------------------------%
    !    | This program uses exact shift with respect to     |
    !    | the current Hessenberg matrix (IPARAM(1) = 1).    |
    !    | IPARAM(3) specifies the maximum number of Arnoldi |
    !    | iterations allowed.  Mode 1 of ZNAUPD is used     |
    !    | (IPARAM(7) = 1). All these options can be changed |
    !    | by the user. For details see the documentation in |
    !    | ZNAUPD.                                           |
    !    %---------------------------------------------------%
    lworkl = 3*ncv**2+5*ncv
    maxitr = 300 
    ishfts= 1
    iparam(1) = ishfts
    iparam(3) = maxitr
    iparam(7) = 1  ! see znaupd.f for more details ( type of eigenproblem)


    !
    !   Allocate local fields
    ! ------------------------
    allocate( ax(a_n), d(ncv), v(a_n,ncv), rd(ncv,3) )
    allocate( workev(3*ncv), workd(3*a_n), workl(lworkl) )
    allocate( rwork(ncv) )
    allocate(selec(ncv))
    ax = ZERO
    d  = ZERO
    v  = ZERO

    rd    = ZERO

    workev = ZERO
    workd  = ZERO
    workl  = ZERO
    rwork  = ZERO

    selec = .false.


    !
    !   %-------------------------------------------%
    !   | M A I N   L O O P (Reverse communication) |
    !   %-------------------------------------------%
    !
    j=0
    main_loop:do
      j = j + 1

      !
      !   %---------------------------------------------%
      !   | Repeatedly call the routine ZNAUPD and take |
      !   | actions indicated by parameter IDO until    |
      !   | either convergence is indicated or maxitr   |
      !   | has been exceeded.                          |
      !   %---------------------------------------------%
      !
#ifdef ARPACK
      call znaupd ( ido, bmat, a_n, which, a_nev, tol, resid, ncv,   &
              v, a_n, iparam, ipntr, workd, workl, lworkl,           &
              rwork , info )
#endif

      if ( abs(ido)/=1 ) exit
    
      !
      !    %-------------------------------------------%
      !    | Perform matrix vector multiplication      |
      !    |                y <--- OP*x                |
      !    | Matrix vector multiplication routine      |
      !    | that takes workd(ipntr(1)) as the input   |
      !    | vector, and returns the matrix vector     |
      !    | product to workd(ipntr(2)).               |
      !    %-------------------------------------------%
      !

    !  workd(ipntr(2):ipntr(2)+a_n-1) = matmul( a_mat , workd(ipntr(1):ipntr(1)+a_n-1) )
      call a_sbr( a_n , workd(ipntr(1)) , workd(ipntr(2)) , para )
 
    end do main_loop
    !
    !     %----------------------------------------%
    !     | Either we have convergence or there is |
    !     | an error.                              |
    !     %----------------------------------------%
    if ( info .lt. 0 ) then
      !   %--------------------------%
      !   | Error message, check the |
      !   | documentation in ZNAUPD  |
      !   %--------------------------%
      print *, ' '
      print *, ' Error with _naupd, info = ', info
      print *, ' Check the documentation of _naupd'
      print *, ' '
      stop
    end if
    !        %-------------------------------------------%
    !        | No fatal errors occurred.                 |
    !        | Post-Process using ZNEUPD.                |
    !        |                                           |
    !        | Computed eigenvalues may be extracted.    |
    !        |                                           |
    !        | Eigenvectors may also be computed now if  |
    !        | desired.  (indicated by mode = .true.)  |
    !        %-------------------------------------------%
    !
#ifdef ARPACK
    call zneupd (mode, 'A', selec, d, v, a_n, sigma,           &
              workev, bmat, a_n, which, a_nev, tol, resid, ncv,&
              v, a_n, iparam, ipntr, workd, workl, lworkl,     &
              rwork, ierr)
#endif
    !
    !        %----------------------------------------------%
    !        | Eigenvalues are returned in the one          |
    !        | dimensional array D.  The corresponding      |
    !        | eigenvectors are returned in the first NCONV |
    !        | (=IPARAM(5)) columns of the two dimensional  |
    !        | array V if requested.  Otherwise, an         |
    !        | orthogonal basis for the invariant subspace  |
    !        | corresponding to the eigenvalues in D is     |
    !        | returned in V.                               |
    !        %----------------------------------------------%

    if ( ierr .ne. 0) then
      !   %------------------------------------%
      !   | Error condition:                   |
      !   | Check the documentation of ZNEUPD. |
      !   %------------------------------------%
      print *, ' '
      print *, ' Error with _neupd, info = ', ierr
      print *, ' Check the documentation of _neupd. '
      print *, ' '
      stop
    end if

    ! assign eigenvalues and eigenvectors to output fields
    ! ----------------------------------------------------
    ! eigenvalues
      a_values = ZERO
      a_values(1:a_nev) = d(1:a_nev)
 
    ! eigenvectors
    if ( mode ) then
      ao_vectors = ZERO
      ao_vectors(1:a_n,1:a_nev) = v(1:a_n,1:a_nev)
    end if

    ! info output (if desired)
    ! ------------------------
    if_info : if ( iinfo==1 ) then
      nconv = iparam(5)
      do j=1, nconv
        !
        !    %---------------------------%
        !    | Compute the residual norm |
        !    |                           |
        !    |   ||  A*x - lambda*x ||   |
        !    |                           |
        !    | for the NCONV accurately  |
        !    | computed eigenvalues and  |
        !    | eigenvectors.  (iparam(5) |
        !    | indicates how many are    |
        !    | accurate to the requested |
        !    | tolerance)                |
        !    %---------------------------%
        !
        call a_sbr( a_n , v(1,j) , ax , para )
        !ax = matmul (a_mat, v(1:a_n,j) )

        call zaxpy(a_n, -d(j), v(1,j), 1, ax, 1)
        rd(j,1) = dble(d(j))
        rd(j,2) = dimag(d(j))
        rd(j,3) = dznrm2(a_n, ax, 1)
        rd(j,3) = rd(j,3) / dlapy2(rd(j,1),rd(j,2))
      end do


      !    %-----------------------------%
      !    | Display computed residuals. |
      !    %-----------------------------%
      !
#ifdef ARPACK
      call dmout(6, nconv, 3, rd, a_n, -6,  &
                    'Ritz values (Real, Imag) and relative residuals')
#endif

      !    %-------------------------------------------%
      !    | Print additional convergence information. |
      !    %-------------------------------------------%

      if (info == 1)  print *, ' Maximum number of iterations reached.'
      print *, ' '
      print *, 'Info'
      print *, '====== '
      print *, ' '
      print *, ' Size of the matrix is ', a_n
      print *, ' The number of Ritz values requested is ', a_nev
      print *, ' The number of Arnoldi vectors generated',  &
               ' (NCV) is ', ncv
      print *, ' What portion of the spectrum: ', which
      print *, ' The number of converged Ritz values is ',  &
                 nconv
      print *, ' The number of Implicit Arnoldi update',    &
               ' iterations taken is ', iparam(3)
      print *, ' The number of OP*x is ', iparam(9)
      print *, ' The convergence criterion is ', tol
      print *, ' '
    end if if_info

   end subroutine get_DCGenSelect_sbr



! ---------------------------------------------------------------------------------


  subroutine get_DCGenAll_Gen(a_mat,a_olap,a_ev,ao_job)
    !
    ! solve generalized eigenvalue problem of general complex matrix
    !
    ! a_job  : eigenvalues and/or left/right eigenvector
    !         = "N" : only eigenvalues  [default]
    !         = "L" : eigenvalues and left eigenvector
    !         = "R" : eigenvalues and right eigenvector
    use reduce_lapack, only : cholesky_decomp, reduce_decomposed
    use basics, only : sort
    use f95_precision
    use lapack95
    implicit none

    complex(dp), intent(inout) :: a_mat(:,:),a_ev(:)
    real(dp), intent(in) :: a_olap(:,:)
    character, intent(in),optional :: ao_job

    character :: mode

    integer :: n
    complex(dp), allocatable :: olap(:,:),vec(:,:)
    complex(dp), allocatable :: val(:,:)

    integer, allocatable :: map(:)
    
    
    mode='N'
    if ( present(ao_job) )   mode = ao_job
    
    n = size(a_mat,1)
    if ( n /= size(a_ev) )  stop ' ERROR: eigenvalue field has the wrong size'


    allocate(olap(n,n),vec(n,n))
    olap = a_olap
    vec = ZERO
    
    allocate(val(n,2))
    val = ZERO
    
    
    select case(mode)
    case("R")
      call GGEV(a_mat,olap,val(:,1),val(:,2),vr=vec)
    case("L")
      call GGEV(a_mat,olap,val(:,1),val(:,2),vl=vec)
    case("N")
      call GGEV(a_mat,olap,val(:,1),val(:,2))
    end select

    a_ev = val(:,1)/val(:,2)
    
    ! order eigenvalues (start with smallest)
    allocate(map(n))
    map = sort(a_ev)
    a_ev = a_ev(map)

    select case(mode)
    case("R","L")
      a_mat = vec(:,map)
    end select
    
  end subroutine get_DCGenAll_Gen



  subroutine get_DRSymAll_Gen(a_mat,a_olap,a_ev,ao_job)
    !
    ! solve generalized eigenvalue problem of symmetric real matrix
    !
    ! a_job  : eigenvalues and/or left/right eigenvector
    !         = "N" : only eigenvalues  [default]
    !         = "V" : eigenvalues and eigenvectors
    use reduce_lapack, only : cholesky_decomp, reduce_decomposed
    use basics, only : sort
    use f95_precision
    use lapack95
    implicit none

    real(dp), intent(inout) :: a_mat(:,:),a_ev(:)
    real(dp), intent(in) :: a_olap(:,:)
    character, intent(in),optional :: ao_job

    character :: mode

    integer :: n
    real(dp), allocatable :: olap(:,:)
    integer, allocatable :: map(:)
    

    mode='N'
    if ( present(ao_job) )   mode = ao_job
    
    n = size(a_mat,1)
    if ( n /= size(a_ev) )  stop ' ERROR: eigenvalue field has the wrong size'

    allocate(olap(n,n))
    olap = a_olap

    select case(mode)
    case("V")
      call SYGVD(a_mat,olap,a_ev,jobz="V")
    case("N")
      call SYGV(a_mat,olap,a_ev)
    end select

    ! order eigenvalues (start with smallest)
    allocate(map(n))
    map = sort(a_ev)
    olap(:,1) = a_ev(map)
    a_ev = olap(:,1)

    select case(mode)
    case("V")
      olap = a_mat
      a_mat = olap(:,map)
    end select

  end subroutine get_DRSymAll_Gen



  subroutine get_DCGenAll_GenDiag(a_m,a_ev,a_trafo)
    !
    !*PURPOSE
    !--------
    ! 
    ! Solve a generalized eigenvalue problem by first transforming it 
    ! into a normal eigenvalue problem (in a orthogonal representation)
    ! by using the tranformation matrix a_trafo
    !
    ! diagonalize FEM Hamiltonian by first going into the orthogonal DVR representation
    ! s.t. it becomes a normal eigenvalue problem
    ! -> imaginary parts of the orbitals in case of ECS are better
    !    meaning surpious positive imag. energies are smaller in magnitude
    !    when diagonalizing with geev for a normal eigenvalue problem
    !    than using the ggev LAPACK routine for a generalized eigenvalue problem
    !
    !*INPUT
    !------
    !  a_m : INPUT:  FEM matrix to be diagonalized
    !        OUTPUT: returns the eigenvectors
    !
    !  a_ev: OUTPUT: return the eigenvalues 
    !
    !  a_trafo: INPUT: transformation matrix from the non-orthogonal 
    !                  to the orthogonal basis 
    !
    use basics, only : sort
    
    use f95_precision
    use blas95
    use lapack95
    implicit none

    complex(dp), intent(inout) :: a_m(:,:),a_ev(:)
    complex(dp), intent(in) :: a_trafo(:,:)

    complex(dp), allocatable :: aux(:,:)
    integer, allocatable :: map(:)
    complex(dp) :: caux
    integer :: n,i


    n=size(a_ev)
    allocate(aux(n,n))

  
    ! express matrix in the DVR basis
    call gemm(a_m,a_trafo,aux)
    call gemm(a_trafo,aux,a_m,transa='t')

    ! diagonalize matrix in DVR basis
    call geev(a_m,a_ev,vr=aux)

    ! order eigenvalues (start with smallest real part)
    allocate(map(n))
    map = sort(a_ev)
    aux(:,1) = a_ev(map)
    a_ev = aux(:,1)
    
    ! normalize vectors
    do i=1,n
      caux = sum( aux(:,i)**2 )
      a_m(:,i) = aux(:,i) / sqrt(caux)
    end do

    ! express eigenstates in original FEM basis
    call gemm(a_trafo,a_m,aux)
       
    ! reorder eigenvectors
    a_m = aux(:,map)

  end subroutine get_DCGenAll_GenDiag


! ---------------------------------------------------------------------------------
! ---------------------------------------------------------------------------------


   ! lanczos_arnoldi_sbr = subroutine that performs the lanczos-arnoldi algorithm
   subroutine lanczos_arnoldi_sbr(a_n,a_nev,a_sbr,a_values,a_vectors,a_ivector,a_para)
    use constants
    use VarSystem, only : m_propmode,m_dt
    implicit none
    integer, intent(in)     :: a_n,a_nev
    complex(dp),intent(in)  :: a_ivector(:)
    complex(dp),allocatable :: a_vectors(:,:),a_vec(:),a_values(:),aux_kry(:,:),aux(:)
    complex(dp),allocatable :: r(:),aux_val(:),hess(:,:),v(:),q(:)
    integer     :: k,i
    ! external matrix-vector subroutine
    ! ---------------------------------
    real(dp),intent(in), optional :: a_para
    external :: a_sbr

    allocate(r(a_n),v(a_n),q(a_n),aux_kry(a_n,a_nev),hess(a_nev,a_nev))
    allocate(a_vectors(a_n,a_nev),a_values(a_nev))

    ! the three vectors for the algorithm
    r     = zero
    v     = zero
    ! coeff for trid matrix
    hess  = zero
    ! aux_kry stores the krylov basis
    aux_kry  = zero
    a_vectors= zero
    a_values = zero

    ! starting vector that is modified during routine
    q = a_ivector / sqrt(sum(abs(a_ivector)**2))
    aux_kry(:,1) = q

    ! perform H(a_para) |starting vector q>, result stored in r 
    call a_sbr(a_n,q,r,a_para)

    k=1
    hess(k,k)=dot_product(q,r)
    r = r - hess(k,k)*q
    hess(k,k+1) = sqrt(sum(abs(r)**2)) 

    if (sqrt(sum(abs(r)**2)) < 1.D-15) then
      forall (i=1:a_nev) a_values(i)  =  zero
      !a_vec = q
    else  
      hess(k+1,k) = hess(k,k+1) 

      !do k=2,a_nev-1  
      
      do while (k<a_nev-1 .and. abs(hess(k,k+1)) > 1.D-18)
        k=k+1
        ! v stores q_(k-1)  
        v=q ; q=r/hess(k-1,k) ; aux_kry(:,k) = q
        call a_sbr(a_n,q,r,a_para)
        hess(k,k)=dot_product(q,r)
        r=r-hess(k-1,k)*v-hess(k,k)*q
        hess(k,k+1) = sqrt(sum(abs(r)**2))
        hess(k+1,k) = hess(k,k+1)
        !if (abs(hess(k,k+1)) < 1.D-15)  return
      end do

      ! calculate the last diagonal entry of hess
      if (abs(hess(k,k+1)) > 1.D-18) then
        v=q ; q=r/hess(k-1,k) ; aux_kry(:,k) = q
        call a_sbr(a_n,q,r,a_para)
        hess(k,k)=dot_product(q,r)
      end if

      ! diagonalize Hessenberg   
      call get_DCGenAll( hess(1:k,1:k),a_values(1:k) )
      ! build QU  
      a_vectors(:,1:k) = matmul(aux_kry(:,1:k),hess(1:k,1:k))
      !forall (i=1:k) a_values(i)  = conjg(hess(1,i))*exp(-IMG*m_dt*a_values(i))
      !a_vec = matmul(a_vectors(:,1:k),a_values(1:k))
      !do i=1,k
      !  a_vec(:)  = a_vec(:) + a_values(i)*a_vectors(:,i)
      !end do
    end if

    end subroutine lanczos_arnoldi_sbr


end module EigSlv
