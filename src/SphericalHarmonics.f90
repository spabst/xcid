module SphericalHarmonics
  !
  !**PURPOSE
  !  Defines object type t_spherical and subroutines related this
  !  object
  !
  !**DESCRIPTION
  !  -----------
  !  The following subroutines (sbr) and functions (fct) are part of
  !  this module:
  !   -> (sbr) init_spherical   : initialize spherical object
  !   -> (sbr) deallo_spherical : reset object t_spherical
  !                               deallocate all arrays
  !                               and set variables to default value
  !   -> (fct) get_spherical_harmonics_obj :
  !                               calculate spherical harmonics
  !                               (based on provided object t_spherical)
  !   -> (fct) get_spherical_harmonics :
  !                               calculate spherical harmonics
  !
  !**AUTHOR
  !  ------
  !  (c) and written by Stefan Pabst
  !
  !**VERSION
  !  -------
  !  svn info: $Id: SphericalHarmonics.f90 2127 2015-11-11 17:02:15Z spabst $
  !-------------------------------------------------------------
  !
  use Constants
  implicit none
  
  type t_spherical
    ! lmax,mmax : maximum l and m quantum number
    integer :: lmax=0, mmax=0
    ! number of theta and phi points
    integer :: ntheta=1, nphi=1
    
    ! theta,phi : theta,phi values
    ! ct        : cos(theta)
    ! P_lm      : associate Legendre polynomials 
    !             1. index       - theta 
    !             2.,3. indicies - l, m ranges
    real(dblprec),allocatable :: theta(:)    &
                                ,phi(:)      &
                                ,ct(:)       &
                                ,P_lm(:,:,:)  
                            
    ! ep : e^(i*phi)                           
    complex(dblprec),allocatable :: ep(:)
    
  end type t_spherical


  ! ------------------------------------------------------
  ! ------------------------------------------------------
contains
  ! ------------------------------------------------------
  ! ------------------------------------------------------
  
  
  subroutine init_spherical(a_this,a_lmax,a_mmax,a_theta,a_phi)
    !**PURPOSE  
    !    Initialize spherical harmonics object
    !  
    !**DESCRIPTION
    !    Assign all internal variable and setup all fields in the object    
    !    
    !**INPUT/OUTPUT
    !    a_this   :  IN - OUT
    !                object of spherical harmonics
    !    a_lmax   :  INPUT
    !                max. angular momentum
    !    a_mmax   :  INPUT
    !                max. angular momentum projection
    !    a_theta(3): INPUT
    !                1. & 2. entry : boundaries of theta range
    !                3. entry : number of theta grid points
    !    a_phi(3) :  INPUT
    !                1. & 2. entry : boundaries of phi range
    !                3. entry : number of phi grid points
    !**AUTHOR  
    !    Stefan Pabst (University of Hamburg, DESY-CFEL)
    !**DATE  
    !    October 2012
    !
    use legendre, only : associated_Legendre_P
    implicit none
  
    type(t_spherical), intent(inout) :: a_this
    integer, intent(in)       :: a_lmax,a_mmax
    real(dblprec), intent(in) :: a_theta(3),a_phi(3)

    integer :: i,l,m,nt,np
  

    nt = nint(a_theta(3))
    np = nint(a_phi(3))
    
    if ( nt < 1 .or. np < 1 ) then
      stop " ERROR(init_associate_Legendre): number of theta/phi points &
                                            &are invalid (<1)"
    end if
    if ( a_lmax < 0 .or. a_mmax < 0 ) then
      stop " ERROR(init_associate_Legendre): max. l/m quantum number is negative"
    end if

    ! resert object
    call deallocate_spherical(a_this)
  
    a_this%lmax = a_lmax
    a_this%mmax = a_mmax 
    a_this%ntheta = nt
    a_this%nphi   = np


    ! theta
    ! ------
    allocate( a_this%theta(nt) , a_this%ct(nt) )
    allocate( a_this%p_lm(nt,0:a_lmax,0:a_mmax) )

    a_this%theta(1) = a_theta(1)
    forall(i=2:nt)
      a_this%theta(i) = a_theta(1) + (i-1) * (a_theta(2)-a_theta(1)) / (nt-1)
    end forall
    
    a_this%ct = cos(a_this%theta)
    
    forall( i=1:nt , l=0:a_lmax , m=0:a_mmax )
      a_this%p_lm(i,l,m) = associated_Legendre_P( l , m , a_this%ct(i) )
    end forall


    ! phi
    ! ----
    allocate( a_this%phi(np) , a_this%ep(np) )

    a_this%phi(1) = a_phi(1)
    forall(i=2:np)
      a_this%phi(i) = a_phi(1) + (i-1) * (a_phi(2)-a_phi(1)) / (np-1)
    end forall
    a_this%ep = exp( IMG*a_this%phi )
    
  end subroutine init_spherical
  
  ! ------------------------------------------------------

  subroutine deallocate_spherical(a_this)  
    !
    ! Reset object a_this
    !
    implicit none
    type(t_spherical) :: a_this
    
    
    a_this%lmax = 0
    a_this%mmax = 0
    a_this%ntheta = 1
    a_this%nphi   = 1
    
    if ( allocated(a_this%theta) )  deallocate(a_this%theta)
    if ( allocated(a_this%ct) )     deallocate(a_this%ct)
    if ( allocated(a_this%p_lm) )   deallocate(a_this%p_lm) 
    
    if ( allocated(a_this%phi) )    deallocate(a_this%phi)
    if ( allocated(a_this%ep) )     deallocate(a_this%ep)
  end subroutine deallocate_spherical
  
  ! ------------------------------------------------------
    
  elemental function get_spherical_harmonics_obj(a_this,a_l,a_m,a_itheta,a_iphi) Result(y_lm)
    ! 
    ! Calculate spherical harmonics
    !   -> be aware that high M states could lead to numerical problems
    !      ( due to factorial)
    !
    implicit none
    type(t_spherical),intent(in) :: a_this
    integer, intent(in) :: a_l,a_m
    integer, intent(in) :: a_itheta,a_iphi
  
    complex(dblprec) :: y_lm
    
    real(dblprec) :: fact
    integer :: i,mabs


    y_lm = ZERO
    mabs = abs(a_m)
    
    if ( a_itheta < 1 .or. a_itheta > a_this%ntheta ) return
    if (  a_iphi  < 1 .or.  a_iphi  >  a_this%nphi  ) return    

    ! prefactor of legendre polynomials : sqrt( (2l+1)/(4*pi) * (l-m)!/(l+m)! )
    fact = sqrt( (TWO*a_l+ONE)/(4*pi) )
    if ( mabs > 0 ) then
      fact = fact / product((/ (sqrt(i*(i+1)*ONE),i=a_l-mabs+1,a_l+mabs,2) /))
      y_lm = fact * a_this%p_lm(a_itheta,a_l,mabs) * a_this%ep(a_iphi)**mabs
    else
      ! spherical harmonics for positive m=|a_m|
      y_lm = fact * a_this%p_lm(a_itheta,a_l,mabs)
    end if



    ! negative m : Y_{l,m} = Y^*_{l,-m}
    if ( a_m<0 ) then
      if ( mod(a_m,2)==0 ) then
        y_lm = conjg(y_lm)
      else
        y_lm = -conjg(y_lm)
      end if
    end if
    
  end function get_spherical_harmonics_obj
 


  elemental function get_spherical_harmonics(a_l,a_m,a_theta,a_phi) Result(y_lm)
    ! 
    ! Calculate spherical harmonics y_{a_l,a_m} (a_theta,a_phi)
    !
    use legendre, only : associated_Legendre_P
    implicit none
    integer, intent(in) :: a_l,a_m
    real(dblprec), intent(in) :: a_theta,a_phi

    complex(dblprec) :: y_lm
    real(dblprec) :: factor
    integer :: i,mabs


    y_lm = ZERO
    mabs = abs(a_m)
    if ( mabs > a_l ) return
    
    ! prefactor of legendre polynomials : sqrt( (2l+1)/(4*pi) * (l-m)!/(l+m)! )
    factor = sqrt( (TWO*a_l+ONE)/(4*pi) )
    if ( mabs > 0 ) then
      factor = factor / product((/ (sqrt(i*(i+1)*ONE),i=a_l-mabs+1,a_l+mabs,2) /))
    end if
    y_lm = factor * associated_Legendre_P( a_l,mabs,cos(a_theta) )   &
                  * exp(IMG*mabs*a_phi)

    ! negative m : Y_{l,m} = (-1)^m Y^*_{l,-m}
    if ( a_m<0 )   y_lm = (1-2*mod(mabs,2)) * conjg(y_lm)
  end function get_spherical_harmonics

end module SphericalHarmonics



