module Propmethods
  !
  !**PRUPOSE
  !  -------
  !  Contains all propagation methods available in the TDCIS program
  !
  !**DESCRIPTION
  !  -----------
  !  This module contians the following (sbr) and functions(fct):
  !
  !  -> (sbr) iecm         : improved Euler-Cauchy method (propagation)
  !  -> (sbr) ecm          : Euler-Chauchy method (propagation)
  !  -> (sbr) tdiff2       : 2nd order finite-time differencing (propagation)
  !  -> (sbr) rk4          : 4th order Runge-Kutta (propagation)
  !  -> (sbr) lanarn       : Lanczos-Arnoldi (propagation)
  !                           
  !**AUTHOR
  !  ------
  !  (c) and written by Stefan Pabst 
  !
  !**VERSION
  !  -------
  !  svn info: $Id: PropMethods.f90 1662 2015-05-11 21:57:00Z spabst $
  !----------------------------------------------------------------------
  !
  use constants
  use eom
  implicit none



contains
        

  subroutine iecm(a_t0,a_dt,a_alpha0,a_alpha)
    !
    ! improved Euler-Cauchy Method
    ! t0  - time
    ! dt - size of time step
    !
    use varsystem,only : m_nmax,m_lmax
    use varwfct,only : m_ephase,m_i1p1hclass
    implicit none

    real(dblprec),    intent(in)    :: a_t0,a_dt
    complex(dblprec), intent(inout) :: a_alpha0,a_alpha(:,:,:)

    integer       :: i
    real(dblprec) :: t

    complex(dblprec) :: aux01,aux1(0:m_nmax,0:m_lmax,m_i1p1hclass) &
         ,aux02,aux2(0:m_nmax,0:m_lmax,m_i1p1hclass)



    aux01 = zero
    aux1  = zero
    aux02 = zero
    aux2  = zero


    ! initial time
    t=a_t0
    call derivative(a_alpha0,a_alpha,t,aux01,aux1,-IMG)

    ! intermediate time
    t = a_t0 + a_dt/2
    aux01 = a_alpha0 + a_dt/2 * aux01
    !$omp parallel workshare
    aux1  = a_alpha  + a_dt/2 * aux1
    aux1  = aux1 * m_ephase
    !$omp end parallel workshare

    call derivative(aux01,aux1,t,aux02,aux2,-IMG)

    ! final time : update phases
    !$omp parallel workshare
    aux2  = aux2  * m_ephase
    a_alpha = a_alpha * m_ephase**2

    ! final expression
    a_alpha0 = a_alpha0 + a_dt*aux02
    a_alpha  = a_alpha  + a_dt*aux2
    !$omp end parallel workshare
  end subroutine iecm



  subroutine ecm(a_t0,a_dt,a_alpha0,a_alpha)
    !
    ! Euler-Cauchy Method
    ! t0  - time
    ! dt - size of time step
    !
    use varsystem,only : m_nmax,m_lmax
    use varwfct,only   : m_i1p1hclass,m_ephase
    implicit none
    
    real(dblprec),    intent(in)    :: a_t0,a_dt
    complex(dblprec), intent(inout) :: a_alpha0,a_alpha(:,:,:)
    
    complex(dblprec) :: aux0,aux(0:m_nmax,0:m_lmax,m_i1p1hclass)
    integer :: i

    aux  = zero
    aux0 = zero

    ! initial time
    call derivative(a_alpha0,a_alpha,a_t0,aux0,aux,-IMG)
    
    ! final expression
    a_alpha0 = a_alpha0 + a_dt*aux0
    !$omp parallel workshare
    a_alpha  = a_alpha  + a_dt*aux
    a_alpha  = a_alpha * m_ephase
    !$omp end parallel workshare

  end subroutine ecm




  subroutine fdiff2(a_t0,a_dt,a_alpha0,a_alpha,a_prev0,a_prev)
    !
    ! 2. order time differences
    ! t0  - time
    ! a_dt - size of time step
    !
    ! see Eq. 54 in Greenman, PRA 82, 023406 (2011)
    !
    use varsystem,only : m_nmax,m_lmax
    use varwfct,only   : m_i1p1hclass,m_ephase
    implicit none
    
    real(dblprec),    intent(in)    :: a_t0,a_dt
    complex(dblprec), intent(inout) :: a_alpha0,a_alpha(:,:,:)
    complex(dblprec), intent(inout) :: a_prev0 ,a_prev(:,:,:)
    
    complex(dblprec) :: aux0,aux(0:m_nmax,0:m_lmax,m_i1p1hclass)
    integer       :: i
    real(dblprec) :: t

    aux  = zero
    aux0 = zero

    ! initial time
    t=a_t0
    if (t/=zero) call derivative(a_alpha0,a_alpha,t,aux0,aux,-IMG)

    ! intermediate time (reference prev comes from a_t0-a_dt/2)
    t = a_t0 + a_dt/2
    !$omp parallel workshare
    a_prev = a_prev * m_ephase**2 
    aux  = aux  * m_ephase

    a_prev0 = a_prev0 + a_dt * aux0
    a_prev  = a_prev  + a_dt * aux 
    !$omp end parallel workshare

    call derivative(a_prev0,a_prev,t,aux0,aux,-IMG)
    
    ! final time : update phases
    !$omp parallel workshare
    aux   = aux * m_ephase
    a_alpha = a_alpha * m_ephase**2

    ! final expression
    a_alpha0 = a_alpha0 + a_dt*aux0
    a_alpha  = a_alpha  + a_dt*aux
    !$omp end parallel workshare
  end subroutine fdiff2




  subroutine rk4(a_t0,a_dt,a_alpha0,a_alpha)
    !
    ! 4th order Runge-Kutta
    ! t0  - time
    ! a_dt - size of time step
    !
    use varsystem,only : m_nmax,m_lmax
    use varwfct,only   : m_i1p1hclass,m_ephase
    implicit none
    
    real(dblprec),    intent(in)    :: a_t0,a_dt
    complex(dblprec), intent(inout) :: a_alpha0,a_alpha(:,:,:)

    real(dblprec) :: t
    integer       :: i
    
    complex(dblprec) :: aux00,aux01,aux02,aux03,aux04
    complex(dblprec),allocatable,dimension(:,:,:) :: &
         aux0,aux1,aux2,aux3,aux4


    allocate(aux0(0:m_nmax,0:m_lmax,m_i1p1hclass))
    allocate(aux1(0:m_nmax,0:m_lmax,m_i1p1hclass))
    allocate(aux2(0:m_nmax,0:m_lmax,m_i1p1hclass))
    allocate(aux3(0:m_nmax,0:m_lmax,m_i1p1hclass))
    allocate(aux4(0:m_nmax,0:m_lmax,m_i1p1hclass))

    aux00 = ZERO
    aux01 = ZERO
    aux02 = ZERO
    aux03 = ZERO
    aux04 = ZERO
    aux0  = ZERO
    aux1  = ZERO
    aux2  = ZERO
    aux3  = ZERO
    aux4  = ZERO

    ! initial time
    ! --------------
    t=a_t0
    call derivative(a_alpha0,a_alpha,t,aux01,aux1,-IMG)
    
    ! intermediate time
    ! -----------------
    t = a_t0 + a_dt/two
    ! update phases
    !$omp parallel workshare
    a_alpha = a_alpha * m_ephase
    aux1  = aux1  * m_ephase

    aux00 = a_alpha0 + a_dt/two * aux01
    aux0  = a_alpha  + a_dt/two * aux1
    !$omp end parallel workshare

    call derivative(aux00,aux0,t,aux02,aux2,-IMG)

    !$omp parallel workshare
    aux00 = a_alpha0 + a_dt/two * aux02
    aux0  = a_alpha  + a_dt/two * aux2
    !$omp end parallel workshare

    call derivative(aux00,aux0,t,aux03,aux3,-IMG)

    ! final time
    !-----------
    t = a_t0 + a_dt 
    ! update phases
    !$omp parallel workshare
    aux1 = aux1 * m_ephase
    aux2 = aux2 * m_ephase
    aux3 = aux3 * m_ephase
    a_alpha = a_alpha * m_ephase

    aux00 = a_alpha0 + a_dt * aux03
    aux0  = a_alpha  + a_dt * aux3
    !$omp end parallel workshare

    call derivative(aux00,aux0,t,aux04,aux4,-IMG)

    
    ! final expression
    ! -----------------
    aux00 = aux01 + two*(aux02+aux03) + aux04
    aux0  = aux1  + two*(aux2 +aux3)  + aux4

    !$omp parallel workshare
    a_alpha0 = a_alpha0 + a_dt/6.D0*aux00
    a_alpha  = a_alpha  + a_dt/6.D0*aux0
    !$omp end parallel workshare
    !write(98,*) "time = ", t, a_alpha0, a_alpha
    !write(98,*) "--------------------------------"
  end subroutine rk4






  subroutine lanarn(a_t0,a_dt,a_alpha0,a_alpha)
    ! lanczos-arnoldi time propagation
    use EigSlv, only   : arn=>lanczos_arnoldi_sbr
    use diagCIS, only  : convert,Action_Hamiltonian
    use VarSystem,only : m_nmax,m_lmax,m_nmaxl,m_lanev
    use VarWfct,only   : m_i1p1hclass,m_nomaxl,m_wfct

    real(dblprec),    intent(in)    :: a_t0,a_dt
    complex(dblprec), intent(inout) :: a_alpha0,a_alpha(:,:,:)
    complex(dblprec),allocatable    :: startv(:),vec(:),olap(:) &
                                      ,evalues(:),evectors(:,:),auxv(:,:)
    real(dblprec) :: t
    integer       :: i,nmax
    t = a_t0 +a_dt/TWO
    
    !number of all alpha coefficients
    nmax=1+m_i1p1hclass *(sum(m_nmaxl+1)-sum(m_nomaxl+1))

    !allocate(evalues(m_lanev),evectors(nmax,m_lanev))
    allocate(startv(nmax),olap(m_lanev),vec(nmax))
    startv  = zero
    !evalues = zero
    !evectors= zero
    vec = zero

    ! create startvector from current wavefunction by converting into short format
    call convert(.false.,startv(:),a_alpha,a_alpha0)

    call arn(nmax,m_lanev,Action_Hamiltonian,evalues,evectors,startv,t) 

    ! multiplication of the QU with eigenvalue matrix, multiplication between
    ! Arnoldi basis matrix & Hessenberg eigenvector matrix with its transposed
    !aux = matmul(evectors,evalues)
    if (sum(abs(evalues)**2) == ZERO ) then
      vec = startv
    else 
      evalues(:) = exp(-IMG*a_dt*evalues(:)) 
      do i=1,m_lanev
        olap(i) = dot_product(evectors(:,i),startv(:))
        vec(:)  = vec(:) + olap(i) *evalues(i)*evectors(:,i)
      end do
      !aux = vec
    end if
    !write(32,*) t, vec
    
    call convert(.true.,vec,a_alpha,a_alpha0) 

  end subroutine lanarn




end module Propmethods
