module grid_type
  !
  !**PURPOSE
  !  -------
  !  Define grid object
  !
  !**DESCRIPTION
  !  -----------
  !  The following subroutines (sbr) and functions (fct) are part of 
  !  this module:
  !
  !  -> (sbr) init_cs_type  : Initialize complex scaling object
  !  -> (sbr) deallo_cs_type: Deallocate all fields and set all values to 0
  !                           that are in the complex scaling object
  !  -> (sbr) init_grid_type  : Initialize grid object (set everything to 0)
  !  -> (sbr) deallo_grid_type: Deallocate all field and set all values to 0
  !                             that are in the grid object  !
  !  -> (sbr) init_cplxscale : Setup complex scaling object
  !  -> (fct) ecs_path       : Calculate complex path and its derivaties
  !!**AUTHOR
  !  ------
  !  (c) and written by Stefan Pabst
  !
  !**VERISON
  !  -------
  ! Version info: $Id: Grid_type.f90 1660 2015-05-11 21:31:57Z spabst $
  !-----------------------------------------------------------------------
  !
  use Constants, dp=>dblprec
  implicit none


  ! complex scaling
  type :: t_cmplxscl
   ! r0     : start radius of the complex absorbing potential (CAP)
   !           position exterior complex scaling begins
   !           [atomic units]
   ! angle  : angle of complex scaling
   ! smooth : smoothness of exterior complex scaling 
    real(dp) :: r0     = ZERO     &
               ,angle  = ZERO     &
               ,smooth = ZERO

   !  complex path variables F(r),F'(r),...,F''''(r) occuring when
   !  exterior complex scaling is introduced. Fields are stored in the
   !  corresponding grid object.
   !  Higher derivaties are needed to calculated modified potential
   !  originating from the new Lapace operator.
   !    1. index : grid points  [1...m_ngrid]
   !    2. index : 0th-3th derivative of F(r)
    complex(dp), allocatable :: F(:,:) 
  end type t_cmplxscl


  type :: t_grid
    ! type : type of grid
    !        = "PSG" : pseudo spectral grid method  [default]
    !        = "FEM" : finite-element method (3rd order)
    character(3) :: type = ""
    
    ! nbasis : no. of basis functions
    !          -> for PSG : equal to npoints
    !          -> for FEM : 3*npoints-2
    ! norm   : norm of wfct coefficients s.t. orbitals is normalized
    !          -> for PSG: norm = sqrt( (npoints+1)*(npoints+2)/2 )
    !          -> for FEM: norm = 1
    integer  :: nbasis = 0
    real(dp) :: norm = ZERO

    ! npoints : number of radial grid points
    ! rmax  : maximum system radius  [atomic units]
    ! rmin  : minimum system radius  [atomic units]
    ! zeta  : r(x) ~ (1+x)/(1-x+zeta)
    !          -> only use with pseudo spectral grid
    integer  :: npoints = 0
    real(dp) :: rmax = ZERO   &
               ,rmin = ZERO   &
               ,zeta = ZERO

    ! file containg the grid points (used in FEM)
    character(clen) :: file=""

    ! r     : radial grid distribution
    ! root  : roots of P'_N (Legendre poly.) 
    !           grid points are defined by m_r(i):=r(m_roots(i))
    !           with r(x) = 2*m_rmax/m_zeta * (1+x)/(1-x+m_zeta)
    ! A2u   : factor to convert A^k_[n,l] coefficient to u_[n,l](r)
    real(dp), allocatable :: r(:),root(:),A2u(:)

    
    ! complex scaling
    !  cs        : complex scaling object
    !  absorbing : switch defining which absorbing method is used
    !               = "CAP"  : use complex absorbing potential (no cs object needed)
    !               = "ECS"  : use smooth exterior complex scaling (cs object needed)
    !               = "SPL"  : use splitting function
    type(t_cmplxscl), allocatable :: cs
    character(3) :: absorbing = ""

    ! grid parents/childs
    ! --------------------
    !  parent: underlying bigger grid
    !          needed when parent grid is truncated at high or small r regions
    !          as done in HF SCF routine
    type(t_grid), pointer :: parent => null()
    !  FEM_intgrid : finer integration grid needed to integrate FEM basis functions
    !                -> general grid that will be rescaled such that wanted 
    !                   integration limits are correct
    type(t_grid), pointer :: FEM_intgrid => null()
    ! DVR_grid  : reference grid points for the FEM function
    type(t_grid), pointer :: DVR_grid => null()

  contains
    procedure :: init_cplxscale
    procedure :: ecs_path
  end type t_grid



contains

  pure subroutine init_cs_type(a_this,a_nr)
    ! initialize complex scaling object
    implicit none
    type(t_cmplxscl),intent(inout) :: a_this
    integer, intent(in) :: a_nr

    call deallo_cs_type(a_this)
    allocate(a_this%F(a_nr,0:4))
    a_this%F = ZERO
  end subroutine init_cs_type


  pure subroutine deallo_cs_type(a_this)
    implicit none
    type(t_cmplxscl),intent(inout) :: a_this
    
    a_this%r0     = ZERO
    a_this%angle  = ZERO
    a_this%smooth = ZERO
    if ( allocated(a_this%F) ) deallocate(a_this%F)
  end subroutine deallo_cs_type




  pure subroutine init_grid_type(a_this,a_nr)
    ! don't create cs and parent field, since
    ! mostly they don't have it
    implicit none
    type(t_grid),intent(inout) :: a_this
    integer, intent(in) :: a_nr

    call deallo_grid_type(a_this)
    a_this%npoints = a_nr

    allocate(a_this%r(a_nr))
    allocate(a_this%root(a_nr))
    allocate(a_this%A2u(a_nr))

    a_this%r    = ZERO
    a_this%root = ZERO
    a_this%A2u  = ZERO
  end subroutine init_grid_type

  pure subroutine deallo_grid_type(a_this)
    implicit none
    type(t_grid),intent(inout) :: a_this

    a_this%type = ""
    a_this%npoints = 0
    a_this%nbasis  = 0
    a_this%norm = ZERO
    a_this%rmin = ZERO
    a_this%rmax = ZERO
    a_this%zeta = ZERO
    a_this%absorbing = ""
    a_this%file = ""

    if ( allocated(a_this%r) )      deallocate(a_this%r)
    if ( allocated(a_this%root) )   deallocate(a_this%root)
    if ( allocated(a_this%A2u) )    deallocate(a_this%A2u)
    if ( allocated(a_this%cs) )     deallocate(a_this%cs)

    if ( associated(a_this%parent) )     nullify(a_this%parent)
    if ( associated(a_this%FEM_intgrid) ) nullify(a_this%FEM_intgrid)
    if ( associated(a_this%DVR_grid) ) nullify(a_this%DVR_grid)
  end subroutine deallo_grid_type


  !----------------------------------------------------


  subroutine init_cplxscale( a_this , a_cs )
    !
    !  initialize exterior complex scaling mapping
    !
    !**INPUT/OUTPUT
    !  ------------
    !  a_this  :  TYPE(T_GRID) - INOUT 
    !             complex scaling object (see module grid_type for details)
    !
    !  a_cs(3) :  REAL(3) - IN 
    !             Including parameters (r0,theta,sm)
    !
    implicit none

    class(t_grid), intent(inout) :: a_this
    real(dp), intent(in) :: a_cs(3)
    
    integer :: i,sz

    sz = size(a_this%r)
    if ( .not.allocated(a_this%cs) )  allocate(a_this%cs)
    call init_cs_type(a_this%cs,sz)

    a_this%cs%r0     = a_cs(1)
    a_this%cs%angle  = a_cs(2)
    a_this%cs%smooth = a_cs(3)

    do i=0,3
      a_this%cs%F(:,i) = a_this%ecs_path(a_this%r,i)
    end do
  end subroutine init_cplxscale


  !----------------------------------------------------


  elemental complex(dp) function ecs_path(a_this,a_r,a_order) result(res)
    !
    !  initialize exterior complex scaling mapping
    !
    !     F(r) = r + c ( r + sm*[ln(1+exp((r0-r)/sm)) - ln(1+exp(r0/sm))] )  
    !
    !          , c = e( i theta ) - 1 ,  sm = smoothing parameter
    !
    ! INPUT
    ! -----
    !  a_r     : position on the real axis
    !  a_order : differential order n of the ECS path F(r) 
    !
    ! OUTPUT 
    ! ------
    !  output is F^[n](r) = d^n/dr^n F(r)
    !
    implicit none
    class(t_grid),intent(in) :: a_this
    real(dp), intent(in) :: a_r     ! position on the real axis
    integer, intent(in) :: a_order  ! compute  d_x^n F(x) where F(x) is the ECS path

    real(dp) :: r0,angle,smooth
    
    real(dp) :: e,e0,em,rmin
    complex(dp) :: c


    res = ZERO

    if ( .not.allocated(a_this%cs) )  then
      select case(a_order)
      case(0)
        res = a_r
      case(1)
        res = ONE
      end select
      return
    end if

    r0  = a_this%cs%r0
    angle  = a_this%cs%angle
    smooth = a_this%cs%smooth

    rmin = min(r0,a_r)
    c = exp( IMG*angle ) - ONE


    ! hard complex scaling
    if_smooth:if ( smooth==ZERO) then
      select case(a_order)
      case(0)
        ! F(r)
        if ( a_r < r0 ) then
          res = a_r
        else
          res = a_r + (a_r-r0) * c
        end if
      case(1)
        ! F'(r)
        if ( a_r < r0 ) then
          res = ONE
        else
          res = c + ONE
        end if
      case(2)
        ! F''(r)
        res = ZERO
      case(3)
        ! F'''(r)
        res = ZERO
      end select

    else

      e  = exp( -(r0-a_r)/smooth )    
      select case(a_order)
      case(0)
        ! F(r)
        ! old version :  a_this%F(:,0) = a_r + c * ( a_r + smooth * ( log(ONE+ONE/e)-log(ONE+ONE/e0)) ) 
        ! rewrite  
        !
        !         ln(1+e((r0-r)/sm)) - ln(1+e(r0/sm)) in F(r) 
        !
        ! due to numerical convergence problems (get rid of infinities/too large numbers)
        !
        !         -rmin/sm + ln(e(-(r-rmin)/sm)+e(-(r0-rmin)/sm)) - ln(1+e(-r0/sm))  , rmin=min(r0,a_r)
        !
        !
        e0 = exp( -r0/smooth )
        em = exp( -(r0-rmin)/smooth ) + exp( -(a_r-rmin)/smooth )
        res = a_r + c * ( a_r + smooth * (-rmin/smooth+log(em)-log(ONE+e0)) )
      case(1)
        ! F'(r)
        res = ONE + c * ( e/(ONE+e) )
      case(2)
        ! F''(r)
        res = c * e/smooth / (ONE+e)**2
      case(3)
        ! F'''(r)
        !res = F''(r) / smooth * (ONE-e)/(ONE+e)
        res = c * e/smooth**2 * (ONE-e)/(ONE+e)**3
      end select

    end if if_smooth

  end function ecs_path


end module grid_type

