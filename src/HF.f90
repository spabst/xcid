module HF
  !
  !**PURPOSE
  !  -------
  !  The MODULE HF contains all necessary routines and function to 
  !  perfom a SCF/HF calculation.
  !
  !**DESCRIPTION
  !  -----------
  !  The following subroutines (sbr) and functions (fct) are part of 
  !  this module:
  !
  !  -> (sbr) hf_scf(fd)    : find HF-SCF Mean-Field
  !  -> (sbr) occorb_set    : initialize occupied orbitals
  !  -> (sbr) hf_updateorbwfct   : get/update wfct for next iteration
  !  -> (sbr) hf_updateorbenergy : get energy of updated wfcts
  !  -> (sbr) hf_neworbitals     : calculate change in wfcts
  !  -> (sbr) hf_allorbital      : find all eigenstates,eigenenergies 
  !                                of the entire system
  !  -> (sbr) assignorbital      : store complex wfcts/energies in the
  !                                globally accessable field "m_orbital"
  !  -> (fct) hf_energy   : calculates the HF energy
  !  -> (sbr) hf_store    : store 1-particle wavefunction,energies in files
  !
  !**AUTHOR
  !  ------
  !  written by Stefan Pabst in April 2011
  !
  !**VERISON
  !  -------
  !  svn info: $Id: HF.f90 2127 2015-11-11 17:02:15Z spabst $
  !-----------------------------------------------------------------------
  !
  use constants, dp=>dblprec
  use varwfct, only : t_orbital
  use grid_type
  implicit none
  
  ! gridscf : grid object used for HF routine 
  !           grid is the basis of wfct objects orbold,orbnew
  type(t_grid),target,private :: mp_gridscf


  contains

  subroutine hf_scf
    ! perfom HF iteration to get converged occupied orbitals
    !  -> see Eq. 47 in Greenman, PRA 82. 023406 (2011) 
    ! 
    ! NOTE: this routine does not calculate 
    !       virtual orbitals
    !
    use VarSystem,only : m_niter, m_nelec , m_convE, m_convWF    &
                        ,m_rmaxscf, m_grid
    use basics, only : numberformat
    use grid, only : rgridscf_init
    implicit none

    real(dp), parameter :: weight0=0.80D0
    integer, parameter :: nrefine=25
    ! NEW
    ! routine adds two elecotrons (up and down spin)
    ! if the orbital where the new electrons are placed
    ! is of p,d or f character 
    integer :: aelec  ! active electron
    integer :: aelec0
    
    integer, parameter :: nstep=2
    integer :: i
    logical :: conv
    character(clen) :: txt
    real(dp) :: weight,convlimit(2)


    ! output format
    txt=trim(numberformat("I",ONE*m_niter))

    
    ! SCF grid
    ! --------
    !  occupied orbitals are confined and do not need large r
    call rgridscf_init(mp_gridscf,m_rmaxscf)

    ! SCF iteration
    ! -------------------------------------------------
    !  -> add 2 electrons till m_nelec is reached
    conv = .false.
    aelec=0
    txt=trim(numberformat("I",ONE*m_nelec))
    aelec0=aelec
    weight=weight0
    
    do while ( aelec+nstep <= m_nelec )

      ! increase number of electrons by nstep
      aelec = aelec + nstep  

      ! SCF for each number of electrons (to ensure convergence)
      do i=1,m_niter     
        conv = .false.

        if ( aelec/=aelec0) then
          ! header of iteration info output
          write(STDOUT,'(/,a,/,a,'//trim(txt)//',a,'//trim(txt)//',a/,a)')    &
              " -------------------------------------------------"            &
             ,"  | no. of mean-field electrons =",aelec, " of ",m_nelec," |"  &
             ," -------------------------------------------------"
          aelec0=aelec
        end if   
        
        if ( aelec==m_nelec ) then
          ! increasing weight 
          !weight = weight0/10.
          !weight = min( 1. , weight0*(2.+i/5)/5. )
          convlimit = (/ m_convE,m_convWF /)
        else if ( i>m_niter/4 .and. i>3 ) then
          cycle  ! for ion less interation needed
        else
          ! convergence limit doesn't need to be so strict for intermed. steps
          convlimit = (/ 1.D-2 , 1.D0 /)    ! don't use wfct as a criterion (not properly implemented)
        end if

        write(STDOUT,'(/,a,'//trim(txt)//',a,'//trim(txt)//')')  &
           ,"  iteration no. =",i, " of ",m_niter                 

        call hfiter_step(ao_elec=aelec,ao_weight=weight,ao_convlimit=convlimit,ao_conv=conv)
        aelec0=aelec
        if ( conv ) exit
      end do
    end do
    

    ! orbitals are not converged after max. number of iteration
    !  -> show warning
    if ( .not.conv .and. m_nelec>1 ) then
       write(STDOUT,*) " maximum number of HF iteration (niter) reached !!!"
       write(STDOUT,*) " orbitals/energy didn't convergered to the &
                        &specified accuracy"
       write(STDOUT,*)       
    end if

  end subroutine hf_scf

  ! -----------------------------------------------


  subroutine hfiter_step(ao_elec , ao_weight , ao_convlimit , ao_conv )
    !
    !**INPUT
    !  -----
    !  ao_elec  : number of occupied electrons
    !  ao_conv  : orbitals are converged
    !  ao_weight: weighting between old and new orbitals
    !
    !
    use varsystem, only : m_nelec,m_nuccharge,m_lmax  &
                         ,m_coulombmode
    use varwfct,only : m_ioccnl,m_occnl,m_lomax
    use basics, only : numberformat
    use Hamiltonian, only : buildH,latter_correction
    implicit none
    
    integer,intent(inout),optional :: ao_elec
    logical,intent(out),optional :: ao_conv
    real(dp), intent(in), optional :: ao_weight,ao_convlimit(2)
    
    integer :: aelec,nl_elec
      
    type(t_orbital), allocatable,save :: orbold(:),orbnew(:)
    real(dp),allocatable :: hamilton(:,:,:)
    complex(dp), allocatable :: ham(:,:)
    
    ! weight of new/old potential 
    real(dp) :: weight

    integer :: l,lm,nbasis
    integer :: i
    character(clen) :: txt
   

    ! optional argument
    aelec = m_nelec
    if ( present(ao_elec) )  aelec = min(ao_elec,m_nelec)
    weight = 0.60D0
    if ( present(ao_weight) )  weight = ao_weight
    if ( present(ao_conv) )   ao_conv = .false.
      
    
    ! initialize occ
    if ( .not.allocated(orbold) ) then
      allocate(orbold(1))
      orbold(1)%n = 0
      orbold(1)%l = 0
      orbold(1)%occ = 0
      orbold(1)%energy = 0
      orbold(1)%grid => mp_gridscf
      call set_occupied(orbold)

      ! initially the same as orbold
      allocate(orbnew(size(orbold)))
      orbnew = orbold
    
    else
      ! current orbitals become old orbitals
      deallocate(orbold)
      allocate(orbold(size(orbnew)))
      orbold = orbnew
    end if

    ! allocate one L higher if not all electrons are assigned
    nbasis = mp_gridscf%nbasis
    lm = m_lomax
    if ( aelec < m_nelec ) lm = min(lm+1,m_lmax)
    allocate(hamilton(nbasis,nbasis,0:lm))
    allocate(ham(nbasis,nbasis))
    hamilton = ZERO
    ham      = ZERO


    ! initialize HF matrix
    ! -> check also higher L (as long as not all electrons are assigned)
    do l=0,lm
      ham = ZERO
      select case(m_coulombmode)
      case("slater")
        ham = weight       * buildH(l,ao_orb=orbnew)  &
            + (ONE-weight) * buildH(l,ao_orb=orbold)  
      case default
        ham = buildH(l,ao_orb=orbnew)
      end select
      hamilton(:,:,l) = dble(ham)
    end do
    deallocate(ham)

    ! add new electrons s.t. open shell will be closed in next iteration
    ! but no more than m_nelec electrons
    l = orbold(size(orbold))%l
    nl_elec = orbold(size(orbold))%occ

    if ( nl_elec<4*l+2 .and. sum(orbold(:)%occ)>0 ) then  ! last shell is not closed
      aelec = sum(orbold(:)%occ) + min( 2 , 4*l+2-nl_elec)
      if ( aelec>m_nelec )  aelec = m_nelec
      
      ! header of iteration info output
      txt=trim(numberformat("I",ONE*m_nelec))
      write(STDOUT,'(/,a,/,a,'//trim(txt)//',a,'//trim(txt)//',a/,a)')    &
          " -------------------------------------------------"            &
         ,"  | no. of mean-field electrons =",aelec, " of ",m_nelec," |"  &
         ," -------------------------------------------------"
    end if
   

    ! find new set of occupied orbitals 
    call hf_neworbitals( orbnew , hamilton , aelec )

    call set_occupied(orbnew)
    aelec = sum(orbnew(:)%occ)

    ! interpolate bewteen new and old set of orbitals
    call hf_updateorbwfct( orbnew, orbold, .8d0 )
    
    ! calculate orbital energies of new occupied orbitals
    ! call hf_updateorbenergy( orbnew, hamilton )
    !!end if

    ! copy new orbitals into corresponding module array "m_orbital" 
    ! in module VarWfct
    call hfiter_assignorbital(orbnew)

    ! show some orbital information of current iteration step
    call hfiter_info( orbnew , orbold )

    ! test convergence
    if ( present(ao_conv) ) then
      ao_conv = .false.
      if ( size(orbnew)==size(orbold) ) then
        if ( present(ao_convlimit) ) then
          if ( all(hf_converged(orbnew,orbold                              &
                   ,ao_limitE=ao_convlimit(1),ao_limitWF=ao_convlimit(2))  &
                  )                                                        &
             )  ao_conv=.true.
        else
          if ( all(hf_converged(orbnew,orbold)) )  ao_conv=.true.
        end if
      end if
    end if
 
    ! return new number of electrons
    if ( present(ao_elec) )  ao_elec = aelec
  end subroutine hfiter_step
  
  
  ! ----------------------------------------------


  subroutine hf_updateorbwfct(a_orbn,a_orbo,a_step)
    ! Correct 1-particle orbital set by the difference to the
    ! new suggested orbital set and re-orthonormalize. 
    ! Initial and final norm of coefficients should be
    ! sqrt( (N+1)(N+2)/2 ) /= 1 , because u_(n,l)(r) should
    ! be normalized to 1 => norm of  A^[k]_(n,l) should not be 1.
    !
    ! INPUT
    ! -----
    ! orbn : out - array of new updated orbital set
    !         in - orbitals diagonalizing the current Hamiltonian
    ! orbo : array of old orbital set
    ! step : step size of orbital change
    !
    use orbital, only  :  get_orbital_norm
    implicit none

    real(dp),intent(in) :: a_step ! weight of orbital correction
    type(t_orbital),intent(inout) :: a_orbn(:)
    type(t_orbital),intent(in) :: a_orbo(:)

    real(dp), allocatable :: vec(:)
    real(dp) :: olap,a,b
    integer :: inew,iold,ii,jnew
    integer :: nbasisscf
    complex(dp) :: norm
    
    integer :: norb


    a = a_step
    b = ONE-a_step
    
    norb = size(a_orbn)

    !nbasisscf = a_orbo(1)%grid%nbasis
    !allocate( vec(nbasisscf) )
    !vec = ZERO

    ! new 1-particle orbitals
    do inew=1,norb
      ! find corresponding old orbital (otherwise do nothing)
      iold=0
      do ii=1,size(a_orbo) 
        if (a_orbo(ii)%n==a_orbn(inew)%n .and. a_orbo(ii)%l==a_orbn(inew)%l) then
          iold = ii
          exit
        end if
      end do
      
      if ( iold==0 ) then
        cycle  ! no corresp. orbital found -> skip and do try next orbital
      end if
      if (.not.allocated(a_orbo(iold)%wfct)) cycle
      
      ! corrected orbitals
      a_orbn(inew)%wfct = a * a_orbn(inew)%wfct + b * a_orbo(iold)%wfct

      ! orthogonalize (Gram-Schmidt orthonormalization) (within same L)
      !!vec = ZERO
      !!do jnew=1,inew-1
      !!  if ( a_orbn(inew)%l /= a_orbn(jnew)%l ) cycle  
      !!  print *, "ortho", a_orbn(inew)%l, a_orbn(inew)%n, a_orbn(jnew)%n

      !!  olap = sum( dble(a_orbn(jnew)%wfct*a_orbn(inew)%wfct) ) / norm**2 
      !!  vec = vec + olap * a_orbn(jnew)%wfct
      !!end do
      !!! make i-orbital orthogonal to lower-lying orbitals
      !!a_orbn(inew)%wfct = a_orbn(inew)%wfct - vec

      ! restore norm 
      norm = get_orbital_norm(a_orbn(inew))
      a_orbn(inew)%wfct = a_orbn(inew)%wfct / sqrt(norm)
    end do
  end subroutine hf_updateorbwfct



  ! ----------------------------------------------


  subroutine hf_neworbitals(a_orb,a_mat,ao_nelec)
    ! Calculate new set of mean-field 1-particle orbitals
    ! add two more electrons to system (as long as max. no. of e- not reached)
    !  -> if m_nelec=1 : add only 1 electron
    ! 
    ! INPUT
    ! -----
    ! a_orb  : object of new updated set of orbitals
    ! a_mat  : reference matrix (normally Hamiltonian)
    ! a_nelec: no. of occupied electrons
    !          = m_nelec [default]
    !
    use varwfct, only   : m_lomax,m_occnl,m_ioccnl
    use varsystem, only : m_nelec,m_cap_strength
    use EigSlv, only    : eigensolver
    use varmatrix, only : m_fem_ov
    use orbital, only   : get_orbital_norm
    use f95_precision
    use lapack95
    implicit none
    

    real(dp),intent(inout) :: a_mat(:,:,:)
    type(t_orbital),allocatable,intent(inout) :: a_orb(:)
    integer, intent(in), optional :: ao_nelec

    real(dp),allocatable :: val(:,:)
    integer :: i,n,l,nl(2)
    integer :: nbasisscf,nbasissys
    integer :: nelec
    real(dp) :: norm,energy

    ! occupied orbitals
    ! -----------------
    ! keep : keep old set of occ. orbtials or assign all orbtials each time again
    logical, parameter :: keep=.true.  
    integer :: nl_elec,aelec,oelec  ! no. of assigned electrons
    integer :: norb,norbn,nls
    integer, allocatable :: nlorb(:,:)
    ! orbital already assigned
    logical, allocatable :: assgn(:,:)

    real(dp), allocatable :: auxmat(:,:),auxmat2(:,:)


    ! optional argument
    nelec = m_nelec
    if ( present(ao_nelec) )  nelec = ao_nelec

    ! find new number of (n,l)-shells
    norb = size(a_orb)  ! old number (maybe need to add 1)
    
    ! check if a new (n,l)-shell is needed
    oelec=sum(a_orb(:)%occ)
    nl_elec = 2*(2*a_orb(norb)%l + 1)
    
    ! check if there is enough space left in the last (n,l)-shell
    norbn = norb
    if ( nelec-oelec > (nl_elec-a_orb(norb)%occ) ) then
      norbn = norb + 1
    end if


    ! for keeping occupied (n,l) shell once it gets occupied
    !  -> to do so, save (n,l) shell information
    allocate(nlorb(2,norb))
    forall(i=1:norb)
      nlorb(1,i) = a_orb(i)%n
      nlorb(2,i) = a_orb(i)%l
    end forall


    nbasisscf = a_orb(1)%grid%nbasis
    nbasissys = a_orb(1)%grid%nbasis
    if ( associated(a_orb(1)%grid%parent) ) then
      nbasissys = a_orb(1)%grid%parent%nbasis
    end if



    if ( size(a_mat,1) /= size(a_mat,2) )   &
      stop " error(hf_neworbitals): matrix is not quadratic"
    if ( size(a_mat,1) < nbasisscf)   &
         stop " error(hf_neworbitals): radial information of&
         & matrix is too small" 
    if ( size(a_mat,3)-1 < m_lomax)   &
         stop " error(hf_neworbitals): matrix includes not all ang. mom."
    

    ! auxil. arrays containing the orbital energies
    allocate(  val(nbasisscf,size(a_mat,3)))
    allocate(assgn(nbasisscf,size(a_mat,3)))  ! remembers if orbitals is already assigned
    val = ZERO
    assgn = .false.


    ! find new set of orbitals
    do l=1,size(a_mat,3)
      ! solve Eq. 47 in Greenman, PRA 82, 023406 (2011) for the
      ! current Fock operator
      nls=1+count(a_orb(:)%l==l-1) ! one radial eigenstate more than already occupied (per l)
      
      if ( mp_gridscf%type=="FEM" ) then
        allocate( auxmat(nbasisscf,nbasisscf) )
        allocate( auxmat2(nbasisscf,nbasisscf) )
        auxmat = m_fem_ov(1:nbasisscf,1:nbasisscf)
        !call SYGV(a_mat(1:nbasisscf,1:nbasisscf,l),auxmat,val(:,l),jobz="V")
        call SYGVX(a_mat(1:nbasisscf,1:nbasisscf,l),auxmat,val(:,l),z=auxmat2,iu=nls)  ! MKL: Jobz option doesn't exist (as in LAPACK95)
        a_mat(1:nbasisscf,1:nbasisscf,l) = auxmat2
        deallocate(auxmat)
        deallocate(auxmat2)
      else
        call eigensolver(a_mat(1:nbasisscf,1:nbasisscf,l),val(:,l),ao_max=nls*ONE,ao_mode=20)
      end if
    end do


    ! delete old orbital object
    if ( allocated(a_orb) ) deallocate(a_orb)
    allocate(a_orb(norbn))

    ! occupy energetically lowest orbitals
    aelec=0
    energy=-1D100  ! -1D100 means already used (assumes no orbital is actually tighter bound than -1D100+1)
    
    do i=1,norbn
      if ( i<=norb .and. keep ) then  ! keep old orbitals
        n = nlorb(1,i)
        l = nlorb(2,i)
        energy = val(n+1,l+1)
        assgn(n+1,l+1) = .true.

      else  ! find new orbital (with lowest orbital energy)

        !  -> assume no degeneracy within ang. mom. block
        ! find lowest orbital (each index starts with 1)
        nl=minloc(val,assgn.eqv..false. .and. val>=energy)
        n=nl(1)-1
        l=nl(2)-1
        energy = val(n+1,l+1)

        if ( energy >= ZERO ) then
          write(STDERR,*) "WARNING(HF): no bound state found"
          stop
        end if
       
        assgn(n+1,l+1) = .true.
      end if


      nl_elec = min(4*l+2,nelec-aelec)
      a_orb(i)%occ = nl_elec

      a_orb(i)%n = n
      a_orb(i)%l = l
      a_orb(i)%energy = energy
      a_orb(i)%grid => mp_gridscf      

      ! allocate new orbital wavefunction
      allocate(a_orb(i)%wfct(1:nbasisscf))  
      a_orb(i)%nbasis = nbasisscf

      ! normalize state
      a_orb(i)%wfct = a_mat(1:nbasisscf,n+1,l+1)
      norm = get_orbital_norm(a_orb(i))
      if ( norm == ZERO ) then
        write(STDOUT,'(a,2i)')  " WARNING(hf_neworbitals): orbital is 0 of state (nr,l)=",n-1,l
        stop
      end if
      a_orb(i)%wfct = a_orb(i)%wfct / sqrt(norm)

      ! make sure orbtials have same sign (important for hf_updateorbwfct)
      if ( dble(a_orb(i)%wfct(1))<ZERO )  a_orb(i)%wfct=-a_orb(i)%wfct
       
      ! increase no. of assigned electrons
      aelec = aelec + nl_elec
    end do
  end subroutine hf_neworbitals


  ! ----------------------------------------------

  subroutine hfiter_info(a_orbnew,a_orbold)
    ! print some orbital information of current occupied orbitals
    !
    implicit none
    type(t_orbital),intent(in) :: a_orbnew(:),a_orbold(:)
    
    real(dp) :: en,eo,de,etot,dorb
    integer :: i,j,n,l,occ
    logical :: new
    
    
    !! check no. of occupied orbitals + electrons
    !if ( size(a_orbnew)/=size(a_orbold)   .or.        &
    !     sum(a_orbnew(:)%occ)/=sum(a_orbold(:)%occ) ) &
    !  then
    !  write(STDOUT,'(a)') " no. of orbitals/electrons changed"  
    !end if

    ! orbital energies
    write(STDOUT,'(4(/,a))')                                         &
        " orbital energies:",                                        &
        " ------------------------------------------------------------------------------",    &
        "  n l  #e- : energy[E_h] (energy [eV]) |  energy change[%]  |  orbital change",    &
        " ------------------------------------------------------------------------------"

    do i=1,size(a_orbnew)
      n = a_orbnew(i)%n
      l = a_orbnew(i)%l
      en = dble(a_orbnew(i)%energy)
      occ = a_orbnew(i)%occ


      if ( min(n,l)<0 ) stop " ERROR(hf_info): invalid n or l value"

      ! find if occupied orbital is new
      new=.true.
      do j=1,size(a_orbold)
        if ( n==a_orbold(j)%n .and. l==a_orbold(j)%l ) then
          new=.false.
          
          ! energy change
          eo = dble(a_orbold(j)%energy)
          de = en-eo
          if ( eo/=ZERO ) de=de/eo ! relative change
        
          dorb = ZERO
          if ( allocated(a_orbnew(i)%wfct) .and.  & 
               allocated(a_orbold(j)%wfct) ) then
            ! orbital change
            dorb = sum((a_orbnew(i)%wfct-a_orbold(j)%wfct)**2)
          end if
          
          exit
        end if
      end do

      if ( new ) then
        write(STDOUT,'(3i3," :",ES15.6,"  (",ES15.6,")")')     &
           ,n,l,occ,en,en*au2ev
      else
        write(STDOUT,'(3i3," :",ES15.6,"  (",ES15.6,") ",2(" | ",ES10.3))')   &
           ,n,l,occ,en,en*au2ev,de*100,dorb
      end if
    end do
    write(STDOUT,*)

    ! -> need somehow a lot of time
    !etot = dble(hf_energy(ONE))
    !write(STDOUT,'(a,ES15.6,/)') " HF-Energy = ",etot
  end subroutine hfiter_info

  ! ----------------------------------------------

  elemental logical function hf_converged(a_orb1,a_orb2,ao_limitE,ao_limitWF) &
    Result(conv)
    ! check if orbital wavefunctions and energies are converged
    ! to the wanted accuracy
    !
    use varsystem, only : m_convE,m_convWF
    implicit none

    type(t_orbital), intent(in) :: a_orb1,a_orb2
    real(dp), intent(in), optional :: ao_limitE,ao_limitWF
    
    real(dp) :: diff,de,limitE,limitWF


    conv = .false.
    limitE = m_convE
    if ( present(ao_limitE) )   limitE = ao_limitE
    limitWF = m_convWF
    if ( present(ao_limitWF) )  limitWF = ao_limitWF

    if ( a_orb1%n/=a_orb2%n .or. a_orb1%l/=a_orb2%l    &
        .or. a_orb1%occ/=a_orb2%occ )  return

    ! absolute change in orbitals wfct
    if ( size(a_orb1%wfct)/=size(a_orb2%wfct) ) return
    diff = sum(abs(a_orb1%wfct - a_orb2%wfct)**2)
    if ( diff > limitWF ) return

    ! relative change in energies
    ! only when energy is 0 then use absolute change
    de = abs(a_orb1%energy-a_orb2%energy)
    if ( de/max(1.D-10,abs(a_orb2%energy)) > limitE ) return
          
    conv = .true.
  end function hf_converged


  ! ----------------------------------------------

  subroutine set_occupied(a_orb)
    ! initialize all parameters,fields for HF-SCF iteration
    ! - set the occupied shells according to orbitals defined in a_orb
    !   -> electrons must fill the shells
    !   -> partially open shell lead to termination of the program
    !
    use varsystem,only : m_nelec,m_nmax,m_lmax,m_lsum,m_lprx
    use varwfct,only   : m_occnl,m_ioccnl,m_lomax,m_nomax,m_moamax &
            ,m_noaminl,m_nomaxl,m_ioccall,m_ioccact
    use basics_append
    implicit none

    type(t_orbital),intent(in) :: a_orb(:)
    integer :: i,nt,n,l,ielec

  
    ! assign occupied orbitals to temporary field and count them (m_ioccnl)
    ielec    = 0
    m_ioccnl = 0
    m_ioccact= 0
    m_ioccall= 0
    if ( allocated(m_occnl)  ) deallocate(m_occnl)
    if ( allocated(m_noaminl)) deallocate(m_noaminl)
    if ( allocated(m_nomaxl) ) deallocate(m_nomaxl)


    if ( size(a_orb) < 1 )  stop " ERROR(set_occupied): size error if a_occ"
    
    ! count and set occupied (n,l) shells
    ielec = sum(a_orb(:)%occ)
    do i=1,size(a_orb)
      if ( a_orb(i)%occ>0 ) then
        n = a_orb(i)%n
        l = a_orb(i)%l

        if ( min(n,l)<0 ) cycle
        if (allocated(m_occnl)) then
          if (any(m_occnl(:,1)==n .and. m_occnl(:,2)==l) )   &
            stop " ERROR(set_occupied): (n,l)-shell is not unique"
        end if
         
        m_ioccnl = m_ioccnl + 1
        call append_allo(m_occnl,reshape((/n,l/),(/1,2/)),ao_dim=1)
        ! always all m-orbitals are equally populated for each (n,l)-shell
        m_ioccall = m_ioccall + 2*l+1
      end if      
    end do


    if (ielec>m_nelec) stop " error (set_occupied):&
         & number of electron exceed specified number"
    
    m_nomax = 0
    m_lomax = 0
    if ( m_ioccnl < 1 ) return  ! stop assigning if there is no occupied orbital

    ! assign all atom specific variables (occupied orbital related)
    m_nomax = maxval(m_occnl(:,1))
    m_lomax = maxval(m_occnl(:,2))
    m_moamax = m_lomax
    
    ! set lsum for coulomb matrix calculation
    m_lsum=2*m_lomax

    if (m_lomax > m_lmax) then
      write(STDOUT,*) " error(set_occupied): lmax<lomax "
      stop
    end if

    if (m_nomax > m_nmax)  stop " error (hf_scf): nmax<nomax !!!"

    ! assign all active occupied orbital related variables
    allocate(m_noaminl(0:m_lomax),m_nomaxl(0:m_lomax))
    
    m_noaminl = 0
    forall(l=0:m_lomax)
      m_nomaxl(l)  = maxval(m_occnl(:,1),mask = m_occnl(:,2)==l)
    end forall
  end subroutine set_occupied

  
  ! ---------------------------------------
  ! ---------------------------------------

 
  subroutine hf_allorbitals(ao_lmax)
    ! get all orbitals and orbital energies of the Fock operator
    ! defined in Eqs. 3, 31 in Greenman, PRA 82, 023406 (2011)
    !
    ! INPUT
    !-------
    ! eta : (in) - optional
    !       strength of CAP (if not given global value is used)
    ! ee  : (in) - optional
    !       weight of e-e interaction (if not given ee=1)
    ! lmax: INTEGER - in - optional
    !       diagonalize Hamiltonian up to this angular momentum
    !
    use Varsystem,only : m_nuccharge,m_cap_strength           &
              ,m_rmax,m_lmax,m_nelec                          &
              ,m_rminHighL,m_LminHighL                        &
              ,m_grid

    use varwfct,only   : t_orbital,m_orbital,m_ioccnl,m_occnl
    use Hamiltonian, only : buildH
    use EigSlv, only : eigensolver
    use Grid, only   : rgrid_init

    use varmatrix,only : m_fem_ov, m_femdvr_trafo
    use orbital, only  : init_orbital, get_orbital_norm
    use f95_precision
    use lapack95
    implicit none


    integer,intent(in),optional  :: ao_lmax
    
    complex(dp),allocatable :: hamc(:,:), evc(:)
    real(dp),allocatable    :: hamr(:,:), evr(:)
    complex(dp),allocatable :: hamc_sl(:,:), evc_sl(:)
    type(t_orbital), allocatable :: orb(:)
    
    integer :: nbasissys
    ! reduction of r-space (ignore small r for high L)
    integer :: sz,irmin
    integer :: i,n,l,lmax

    complex(dp) :: norm
    


    write(STDOUT,'(/,a,/,a)') " Generating all 1p-orbitals"   &
                            ," ----------------------------"
    
    lmax = m_lmax
    if (present(ao_lmax)) lmax = ao_lmax


    ! set grid for SCF-method
    ! -----------------------
    nbasissys = m_grid%nbasis

    ! initialize m_orbital
    if ( .not. allocated(m_orbital) ) then
      call init_orbital(m_orbital,0)
    end if

    ! initialize arrays
    ! -----------------
    allocate(hamc(nbasissys,nbasissys))
    allocate(evc(nbasissys))
    hamc = ZERO
    evc  = ZERO

    
    ! make sure always the same occupied orbitals are used to build the Hamiltonian
    if ( m_nelec==1 ) then
      m_ioccnl = 1
      allocate(orb(m_ioccnl))
      orb(1)%n = 0
      orb(1)%l = 0
      orb(1)%occ = 1
      orb(1)%energy = 0
      orb(1)%grid => m_grid
      call set_occupied(orb)
    else
      if ( m_ioccnl==0 ) stop " ERROR(hf_allorbitals): no occ. orbital informations"
  
      allocate(orb(m_ioccnl))
      do i=1,m_ioccnl
        orb(i) = m_orbital(m_occnl(i,1),m_occnl(i,2))
      end do
    end if


    ! save occupied orbitals separately
    ! --------------------------------
    lp_l : do l=0,lmax
      write(STDOUT,'(a,i3)') " -> run L =",l


      ! create Hamiltonian
      hamc = buildH(l,ao_orb=orb)

      ! neglect r<r_minHighL for L>) LminHighL due to convergence 
      ! problem in the diagonalization for high L 
      ! ----------------------------------------------------------
      sz = size(hamc,1)
      if ( l >= m_LminHighL ) then

        irmin = minloc(abs(m_grid%r(:)-m_rminHighL),1)
        write(STDOUT,'(4x,a,ES10.3)') "ignore r < ",m_grid%r(irmin)
        
        if ( m_grid%type=="FEM" ) then
          irmin = 3*(irmin-1)  !  ID of 1. FEM basis fct. considered
        end if
        sz = sz - (irmin-1)

        allocate(hamc_sl(sz,sz))
        hamc_sl = hamc(irmin:,irmin:)
        call move_alloc(hamc_sl,hamc)
      end if

      ! diagonalize hamiltonian
      ! -----------------------
      if (any(imag(hamc)/=ZERO)) then
        ! orbitals and their energies
        
        if ( m_grid%type=="FEM" ) then
          if ( allocated(hamc_sl) )  deallocate(hamc_sl)
          allocate(hamc_sl(sz,sz))
          hamc_sl = m_femdvr_trafo
          call eigensolver(hamc,evc(1:sz),hamc_sl)
          deallocate(hamc_sl)
        else
          call eigensolver(hamc,evc(1:sz))
        end if
        
      else
        
        allocate( hamr(sz,sz), evr(sz) )
        hamr = dble(hamc)

        if ( m_grid%type=="FEM" ) then
          call eigensolver(hamr,m_fem_ov,evr(1:sz),ao_job="V")
        else
          call eigensolver(hamr,evr)
        end if

        hamc      = hamr
        evc(1:sz) = evr
        deallocate(hamr,evr)
          
      end if

      ! resize hamc to its original size
      ! --------------------------------
      if ( l >= m_lminHighL ) then
        call move_alloc(hamc,hamc_sl)
        allocate(hamc(nbasissys,nbasissys))
        hamc = ZERO
        hamc(irmin:,1:sz) = hamc_sl
        deallocate(hamc_sl)
      end if


      ! assign to global variables
      ! --------------------------
      do n=1,sz
        ! check that all orbitals are allocated
        if ( n>size(m_orbital,1) .or. l>= size(m_orbital,2) )  &
          stop " ERROR(hf_allorbitals): system-wide orbital array is too small"

        if ( .not.allocated(m_orbital(n-1,l)%wfct) ) then
          call init_orbital(m_orbital,1,n-1,l)
        end if

        call assignorbital( m_orbital(n-1,l) , hamc(:,n) , evc(n) )
        
        ! normalize  
        norm = sqrt( get_orbital_norm(m_orbital(n-1,l)) )
        !if ( dble(m_orbital(n-1,l)%wfct(3)/norm)<ZERO )  norm = -norm
        if ( norm == ZERO )  norm = ONE  ! do nothing
        m_orbital(n-1,l)%wfct = m_orbital(n-1,l)%wfct / norm

      end do
    end do lp_l

    deallocate(hamc,evc)
  end subroutine hf_allorbitals

  ! ----------------------------------------------------------------

  subroutine occorb_info(ao_fid,ao_c)
    ! print information about occupied orbitals
    !
    ! INPUT
    ! -----
    !  ao_fid : INTEGER - in - optional
    !           pipe/file ID
    !  ao_c   : CHARACTER - in - optional
    !           leading character of each line printed
    !        
    use varwfct,only : m_ioccnl,m_occnl,m_orbital
    implicit none

    integer,intent(in),optional :: ao_fid
    character,intent(in),optional :: ao_c

    integer :: i,n,l,fid=STDOUT
    character :: c
    character(clen) :: orb

    c=" "
    if (present(ao_fid)) fid = ao_fid
    if (present(ao_c)) c = adjustl(ao_c)

    write(fid,'(a,2(/2a))') c                 &
        ,c,"Orbital information (n,l shells)" & 
        ,c,"--------------------------------"

    do i=1,m_ioccnl 
      ! create orbital symbol
      n = m_occnl(i,1)
      l = m_occnl(i,2)
      select case(l)
      case(0)
        orb="s"
      case(1)
        orb="p"
      case(2)
        orb="d"
      case(3)
        orb="f"
      case(4)
        orb="g" 
      case(5)
        orb="h"
      case(6)
        orb="i"
      case(7)
        orb="j" 
      case(8)
        orb="k" 
      case(9)
        orb="l"
      case(10)
        orb="m"
      case(11)
        orb="n" 
      case default
        orb="?"
      end select

      write(orb,'(X,i2,",",i2)') n,l

      write(fid,'(2a," :",F12.6," + I ",F10.6)') c,trim(orb)        &
          ,real(m_orbital(n,l)%energy),imag(m_orbital(n,l)%energy)
    end do

  end subroutine occorb_info

  ! ----------------------------------------------------------------

  function hf_energy(ao_fac)
    ! Calculate the HF-Energy, which can be complex due to the CAP
    ! No eqution for the HF energy is given in Greenman, PRA 82, 023406 (2011)
    !
    ! INPUT
    ! ------
    ! ao_fac - weighting factor of Coulomb correction
    !
    use varsystem,only : m_nelec,m_nocoulomb
    use varwfct,only : m_ioccnl,m_occnl,m_orbital
    use CoulombMatrix,only : m_coumat,coulomb
    implicit none

    complex(dp) :: hf_energy
    real(dp),intent(in),optional :: ao_fac

    real(dp) :: fac
    integer :: i,n,l

    fac = ONE
    if (present(ao_fac)) fac = ao_fac

    ! sum up orbital energies
    hf_energy = ZERO
    do i=1,m_ioccnl
      n = m_occnl(i,1)
      l = m_occnl(i,2)
      hf_energy = hf_energy + m_orbital(n,l)%energy * (2*l+1)
    end do

    ! H-like atom has no doubly occupied shell and has e-e contribution
    if (m_nelec == 1 ) return
    hf_energy = 2*hf_energy  ! doubly occupied (n,l,m) orbital


    ! only add Coulomb energy if Coulomb interaction is active
    if_coulomb:if ( .not.m_nocoulomb) then
      if (allocated(m_coumat)) deallocate(m_coumat) 
      call coulomb(-1,2,2,2,2)
      if (.not.allocated(m_coumat)) stop " hf_energy: coumat not allocated"

      ! Coulomb correction
      hf_energy = hf_energy                      &
                  - 2*fac *sum(m_coumat(:,:,1))  &
                  + fac * sum(m_coumat(:,:,2))

      deallocate(m_coumat)
    end if if_coulomb

    return
  end function hf_energy

  ! ----------------------------------------------
  
  subroutine hfiter_assignorbital(a_orb)
    ! assign orbitals (wfct + energies) to global orbital array
    ! m_orbital in module Varwfct
    !
    use varwfct, only : m_orbital
    use varsystem, only : m_grid
    use orbital, only : init_orbital
    implicit none

    type(t_orbital),intent(inout) :: a_orb(:)
    integer :: i,n,l,nbasis

    do i=1,size(a_orb)
      n = a_orb(i)%n
      l = a_orb(i)%l
      
      call init_orbital(m_orbital,1,n,l)
      call assignorbital(m_orbital(n,l),a_orb(i)%wfct,a_orb(i)%energy,a_orb(i)%occ)
      ! store the unreduced  wfct in a_orb (needed to build SCF Hamiltonian)  
      a_orb(i)%wfct = m_orbital(n,l)%wfct(1:size(a_orb(i)%wfct))
    end do      
  end subroutine hfiter_assignorbital

  ! -----------------------------------------------------------

  subroutine assignorbital(a_orb,a_wfct,a_energy,ao_occ)
    ! assign orbitals to global module array m_orbital (provided by a_orb)
    !
    ! INPUT
    ! -----
    ! a_orb    : target orbital object with index structure: 0...n,0...l
    ! a_wfct   : wfct of orbital 
    ! a_energy : energy of orbital 
    ! ao_occ   : occupation number of orbital
    !  
    use orbital, only : init_orbital
    use varsystem, only : m_grid
    use grid_type
    implicit none
    
    type(t_orbital),intent(inout) :: a_orb
    complex(dp), intent(in) :: a_wfct(:),a_energy
    integer,intent(in),optional :: ao_occ
    
    integer :: n

    
    n = size(a_wfct)
    if ( n > a_orb%nbasis ) stop " ERROR(assignorbital): wfct-entry is too small"


    a_orb%wfct(:) = ZERO
    a_orb%wfct(1:n) = a_wfct
    a_orb%energy = a_energy
    a_orb%energy_so = a_energy
    if ( present(ao_occ) )  a_orb%occ = ao_occ    

  end subroutine assignorbital

  ! ----------------------------------------------------------------------

  subroutine hf_store(a_mode)
    ! store orbital energies and wavefunctions in their correpsonding files
    !
    ! INPUT
    ! -----
    ! a_mode = 1 : all orbitals
    !        = 2 : all occupied orbitals
    !
    use varsystem,only : m_fenergy,m_forbu,m_forb,m_focc,m_storeOrbU       &
                         ,m_lmax
    use varwfct,only   : m_lomax,m_nomaxl,m_occnl,m_ioccnl,m_orbital
    use general,only   : printinfo,printvariables
    use orbital,only   : get_orbital_radial
    use inout,only     : opendir
    use grid_type
    implicit none

    integer,intent(in) :: a_mode
    
    integer :: i,l,n,ir,nmax
    integer :: npoints,nbasis
    type(t_grid), pointer :: grid => null()

    integer,allocatable :: idx(:,:)
    complex(dp) :: en
    complex(dp), allocatable :: energy(:,:)
! for DVR implementation
    integer :: j, fndx
    real(dp) :: xarr(3)
    real(dp),allocatable :: bf(:), r2eigf(:)


  
    write(STDOUT,'(/,X,a)')  "Storing orbital information" 
    if (.not.allocated(m_orbital)) then
      write(STDOUT,*)  " WARNING: No orbitals have been computed"
      stop
    end if

    ! shortcut for number of grid points
    if ( .not.associated(m_orbital(0,0)%grid) )   &
      stop " ERROR(hf_store): orbital (n=0,l=0) is not initialized"

    grid  => m_orbital(0,0)%grid
    npoints = grid%npoints
    nbasis  = grid%nbasis


    if (a_mode/=1 .and. a_mode/=2)  stop &
         " error(hf_store): mode is invalid"

    !
    ! write occupied file
    ! --------------------
    call opendir(99,m_focc)
    write(99,'(a,2(/,a))')                             &
        "# occupied orbitals (n,l)-shell"  &
       ,"# n - radial quantum number"      &
       ,"# l - angular momentum"
    do i=1,m_ioccnl
      n = m_occnl(i,1)
      l = m_occnl(i,2)
      write(99,'(2i4)') n,l
    end do
    close(99)


    !
    ! ------------------
    ! write energy file
    ! ------------------
    call opendir(100,m_fenergy)
    write(100,'(a)') "# 1-particle orbital energies"
    call printinfo(0,100,'#')
    call printvariables(1,100,'#')
    write(100,'(a)') "# "
    write(100,'(a)') "# n - radial quant. number"
    write(100,'(a)') "# l - ang. quant. number"
    write(100,'(a)') "# n , l , energy"
    write(100,'(a)') "# ---------------------------"
    write(100,'(a)') "# "
    
    allocate( idx(2,nbasis*(m_lmax+1)) )
    allocate( energy(0:nbasis-1 , 0:m_lmax ) )
    idx = 0
    energy = ZERO


    select case(a_mode)
    case(1)
      ! reorder energy s.t. the real part of the energies 
      ! are monotonically increasing

      ! assign energies to a field
      forall(n=0:nbasis-1,l=0:m_lmax)          
        energy(n,l) = m_orbital(n,l)%energy
      end forall

      ! create index mapping s.t. energies are ordered incleasingly w.r.t 
      ! their real part
      ! ------------------------------------------------------------------
      idx = -1
      do i=1,size(idx,2)
        idx(:,i) = minloc(real(energy)) - 1
        ! if energies are too large then stop
        if ( dble(energy(idx(1,i),idx(2,i))) >= huge(ONE) ) then
          exit
        else
          energy(idx(1,i),idx(2,i)) = huge(ONE) ! unset currently smallest entry
        end if
      end do

      ! -----------------------------------------------------------------
      ! write energies into a file
      ! -----------------------------------------------------------------
      do i=1,size(idx,2)
        n = idx(1,i)
        l = idx(2,i)

        ! n or l < 0 -> means not assigned -> skip
        if ( any(idx(:,i)<0) ) cycle  
        ! don't print orbital energies of orbitals that are not calculated
        if ( .not.allocated(m_orbital(n,l)%wfct) ) cycle

        en = m_orbital(n,l)%energy
        write(100,'(2i4,2ES20.10E3)') n,l,en
      end do

    case(2)

      do i=1,m_ioccnl
        n = m_occnl(i,1)
        l = m_occnl(i,2)
        en = m_orbital(n,l)%energy
        write(100,'(2i4,2ES20.10E3)') n,l,en
      end do

    end select
    close(100)

    !
    ! ------------------
    ! write orbital file
    ! ------------------
    !
    call opendir(100,m_forb)
    write(100,'(2a,3X)') "# 1-particle orbital wavefunction A^k_[n,l]"    &
          ,"(see Greenman, PRA 82, 023406, below Eq. 43)"

    if ( m_storeOrbU) then
      call opendir(101,m_forbu)
      write(101,'(2a,3X)') "# 1-particle orbital wavefunction u_[n,l](r)"   &
                          ,"(see Greenman, PRA 82, 023406, Eq. 43)"
    end if

    do i=0,1
      if ( i==1 .and. .not.m_storeOrbU ) cycle

      call printinfo(0,100+i,'#')
      call printvariables(1,100+i,'#')
      write(100+i,'(a)') "# "
      write(100+i,'(a)') "# each block contains all wfct for a given ang. mom."
      write(100+i,'(a)') "# - header of each block contains the corresponding&
                              & ang. mom."
      write(100+i,'(a)') "# - columns: r, (phi_i(r), i=1:N)"
      write(100+i,'(a)') "#"
    end do
    

    ! angular momentum block
    ! ----------------------
    lp_l:do l=0,m_lmax
      if (a_mode==2 .and. l>m_lomax) cycle

      nmax=-1
      do n=0,nbasis-1
        if ( .not.allocated(m_orbital(n,l)%wfct) ) cycle
        nmax = n
      end do
      if (nmax < 0 ) cycle  ! no wfct with this L exits

      if (a_mode==2)  nmax = m_nomaxl(l)  ! only loop over occupied orbitals

        
      ! write l block
      write(100,'(2/,a,i4)') " # l =",l
      if (m_storeOrbU) write(101,'(2/,a,i4)') " # l =",l

      ! set too small values (<1e-99) to 0  <- output problems when values is smaller than 1e-99
      do n=0,nmax
      do ir=1,nbasis
        if ( abs(imag(m_orbital(n,l)%wfct(ir)))<1.d-99 )   m_orbital(n,l)%wfct(ir) = dble(m_orbital(n,l)%wfct(ir))
        if ( abs(dble(m_orbital(n,l)%wfct(ir)))<1.d-99 )   m_orbital(n,l)%wfct(ir) = IMG*imag(m_orbital(n,l)%wfct(ir))
      end do
      end do
 

      ! write orbital wavefunction in terms of the basis functions [coefficients]
      ! -> in case of FEM : on average 3 basis functions per grid point
      ! -> in case of DVR : the coefficients correspond to DVR basis, not underlying FEM
      do ir=1,nbasis
        write(100,'(99999(2ES20.10E3))')   ( m_orbital(n,l)%wfct(ir) ,n=0,nmax  ) 
      end do

      ! write radial orbital wavefunction 

      if (m_storeorbU) then
        ! convert back to u(r)
        ! A^[nr]_[n,l] -> u_[n,l](nr) = P_(N+1)(nr)/sqrt(r'(nr)) * A^[nr]_[n,l]
        do_npoints: do ir=1,npoints
            write(101,'(ES12.5E2,99999(2X,2ES15.5E3))') grid%r(ir),         &
                 ( get_orbital_radial(m_orbital(n,l),ir), n=0,nmax )
        end do do_npoints
      end if
    end do lp_l

    close(100)
    close(101)

  end subroutine hf_store

end module HF
