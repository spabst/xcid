module analytic_integrator
!
  !**PURPOSE
  !  -------
  !  The analytic_integrators contains all necessary routines and function to 
  !  perfom analytic integration with FE basis functions
  !
  !**DESCRIPTION
  !  -----------
  !  The following subroutines (sbr) and functions (fct) are part of 
  !  this module:
  !
  !  -> (interface) int_caller - a caller interface
  !  -> (fct) analyt_int - implements straight analytical integration
  !                         gets into numerical inaccuracy for large x due to the terms x^5 and alternating signs
  !  -> (fct) analyt_int1 - implements analytical integration with shifting.
  !                         right now implemented only for n=0 (kinetic energy and overlap)
  !
  !**AUTHOR
  !  ------
  !  written by Arina Sytcheva and Stefan Pabst in July 2013
  !
  !**VERISON
  !  -------
  !  svn info: $Id: analytic_integrator.f90 1325 2014-10-09 20:17:20Z spabst $
  !-----------------------------------------------------------------------
  !
  use constants
  use grid_type
  use fem_basis, only : idx_basis_reverse, basiscoef_real, fem_3refpoints


  implicit none

  interface int_caller
    module procedure int_caller_real, int_caller_cmplx
  end interface


contains


  !--------------------------------------------------------
  !  integrator caller for complex matrices
  !-------------------------------------------------------
  subroutine int_caller_cmplx(a_matrix, a_grid, a_left, a_right, a_do_deriv, a_pot_n, ao_do_deriv, ao_sym)
    implicit none

    complex(dblprec),intent(inout) :: a_matrix(:,:)
    type(t_grid),intent(in)     :: a_grid
    real(dblprec),intent(in)    :: a_left, a_right
    logical,intent(in)          :: a_do_deriv             ! applys to the both. If ao_do_deriv present, apllys only to the left basis function
    integer,intent(in)          :: a_pot_n
    logical,intent(in), optional:: ao_do_deriv  ! applys to the right basis function
    logical,intent(in), optional:: ao_sym
    

    integer :: i, j, ifun, jfun, itype, jtype, fe_volume  
    real(dblprec) :: xarr_ai(3), xarr_aj(3)
    logical :: do_deriv_right  ! applys to the right basis function
    logical :: sym


    a_matrix = ZERO

    fe_volume = a_grid%nbasis

    do_deriv_right = a_do_deriv
    if ( present( ao_do_deriv) ) do_deriv_right = ao_do_deriv 

    sym = .true.
    if ( present(ao_sym) )  sym = ao_sym


    if ( sym ) then
      do i = 1, fe_volume
        call idx_basis_reverse(i, ifun, itype, fe_volume)
        do j = i, min(i+5, fe_volume)
          call idx_basis_reverse(j, jfun, jtype, fe_volume)
!          if ( a_pot_n < 0. .and. (ifun == 1 .or. jfun == 1) ) then
          !if ( a_pot_n < 0. .and. (xarr_ai(3) < ONE .or. xarr_aj(3) < ONE) ) then
          !  a_matrix(i,j) = analyt_int2(ifun, itype, jfun, jtype, a_grid, a_left, a_right, a_do_deriv, a_pot_n, do_deriv_right)
          !else
            a_matrix(i,j) = analyt_int1(ifun, itype, jfun, jtype, a_grid, a_left, a_right, a_do_deriv, a_pot_n, do_deriv_right)
          !end if
          a_matrix(j,i) = a_matrix(i,j)
        end do
      end do
    else
      do i = 1, fe_volume
        call idx_basis_reverse(i, ifun, itype, fe_volume)
        do j = i+1, min(i+5, fe_volume) !max(i-5,1), min(i+5, fe_volume)
          call idx_basis_reverse(j, jfun, jtype, fe_volume)
          a_matrix(i,j) = analyt_int1(ifun, itype, jfun, jtype, a_grid, a_left, a_right, a_do_deriv, a_pot_n, do_deriv_right)
          a_matrix(j,i) = - a_matrix(i,j)
        end do
      end do
    end if

  end subroutine int_caller_cmplx

  !------------------------------------------
  !  integrator caller for real matrices
  !------------------------------------------

  subroutine int_caller_real(a_matrix, a_grid, a_left, a_right, a_do_deriv, a_pot_n, ao_do_deriv, ao_sym)
    implicit none

    real(dblprec),intent(inout) :: a_matrix(:,:)
    type(t_grid),intent(in)     :: a_grid
    real(dblprec),intent(in)    :: a_left, a_right
    logical,intent(in)          :: a_do_deriv   ! applys to the both. If ao_do_deriv present, applies only to the left basis function
    integer,intent(in)          :: a_pot_n
    logical,intent(in), optional:: ao_do_deriv  ! applys to the right basis function
    logical,intent(in), optional:: ao_sym       ! matrix is symmetric (T, default) or anti-symmetric (F)
    

    integer :: i, j, ifun, jfun, itype, jtype, fe_volume  
    real(dblprec) :: xarr_ai(3), xarr_aj(3)
    logical :: do_deriv_right  ! applys to the right basis function
    logical :: sym
    
    
    a_matrix = ZERO

    fe_volume = a_grid%nbasis
    do_deriv_right = a_do_deriv
    if ( present( ao_do_deriv) ) do_deriv_right = ao_do_deriv 

    sym = .true.
    if ( present(ao_sym) )  sym = ao_sym


    if ( sym ) then

      do i = 1, fe_volume
        call idx_basis_reverse(i, ifun, itype, fe_volume)
        do j = i, min(i+5, fe_volume)
          call idx_basis_reverse(j, jfun, jtype, fe_volume)

          if ( a_pot_n < 0. .and. (ifun == 1 .or. jfun == 1) ) then
          !if ( a_pot_n < 0. .and. (xarr_ai(3) < ONE .or. xarr_aj(3) < ONE) ) then
            a_matrix(i,j) = analyt_int2(ifun, itype, jfun, jtype, a_grid, a_left, a_right, a_do_deriv, a_pot_n, do_deriv_right)
          else
            a_matrix(i,j) = analyt_int1(ifun, itype, jfun, jtype, a_grid, a_left, a_right, a_do_deriv, a_pot_n, do_deriv_right)
          end if
          a_matrix(j,i) = a_matrix(i,j)
        end do
      end do

    else

      do i = 1, fe_volume
        call idx_basis_reverse(i, ifun, itype, fe_volume)
        do j = i+1, min(i+5, fe_volume) !max(i-5,1), min(i+5, fe_volume)
          call idx_basis_reverse(j, jfun, jtype, fe_volume)
          a_matrix(i,j) = analyt_int1(ifun, itype, jfun, jtype, a_grid, a_left, a_right, a_do_deriv, a_pot_n, do_deriv_right)
          a_matrix(j,i) = - a_matrix(i,j)
        end do
      end do

    end if

  end subroutine int_caller_real




!--------------------------------
! analytical integration - with a shift to the FE's central point
!--------------------------------






  function analyt_int1(a_i, a_itype, a_j, a_jtype, a_grid, a_rleft, a_rright, a_do_deriv, a_pot_n, ao_do_deriv)

    use, intrinsic :: ieee_arithmetic
    use basics, only : binominal

    implicit none
    
    integer     ,intent(in) :: a_i, a_j   ! central points of the finite elements
    integer     ,intent(in) :: a_itype, a_jtype
    type(t_grid),intent(in) :: a_grid
    real(dblprec)    ,intent(in) :: a_rleft, a_rright
    logical     ,intent(in) :: a_do_deriv             ! applys to the both. If ao_do_deriv present, apllys to the left basis function
    logical     ,intent(in), optional :: ao_do_deriv  ! applys to the right basis function
    integer     ,intent(in) :: a_pot_n
    real(dblprec)           :: analyt_int1



    integer            :: i, j, k, l, n, np, np_max, pot_n
    integer, parameter :: degree_fe = 3
    real(dblprec)      :: xarr(degree_fe)                                   &  ! array to initialize the finite elements
                         ,coef_ai(0:5), coef_aj(0:5)                        &
                         ,coef_ai_l(0:5), coef_aj_l(0:5)                    &
                         ,coef_ai_r(0:5), coef_aj_r(0:5)                    &
                         ,g,h, aux1, aux2, x2                               &
                         ,xarr_aux(degree_fe)
    logical            :: do_deriv_right

    !if ( a_pot_n < 0 ) stop "ERROR(analyt_int1): analytic integrator works only for n>=0"
    analyt_int1 = 0.D0

    do_deriv_right = a_do_deriv
    if ( present(ao_do_deriv) ) do_deriv_right = ao_do_deriv
    
    if ( a_pot_n < 0 ) then
      np_max = 20
      pot_n = - a_pot_n
    else
      np_max = a_pot_n
      pot_n  = a_pot_n
    end if 

    if (a_rleft .ge. a_rright) then
      return
    end if

    if ( abs(a_i-a_j) .ge. 2) then
      return
    end if

    xarr(1:3) = fem_3refpoints(a_i, a_grid)

    if ( a_i == a_j) then
      coef_ai_l = basiscoef_real(xarr, a_itype, sum(xarr(1:2))/2,  a_do_deriv, .false.)  
      coef_aj_l = basiscoef_real(xarr, a_jtype, sum(xarr(1:2))/2,  do_deriv_right, .false.)
      coef_ai_r = basiscoef_real(xarr, a_itype, sum(xarr(2:3))/2,  a_do_deriv, .false.)  
      coef_aj_r = basiscoef_real(xarr, a_jtype, sum(xarr(2:3))/2,  do_deriv_right, .false.)
      g = xarr(3)-xarr(2)
      h = xarr(1)-xarr(2)
      x2 = xarr(2)

      if ( a_pot_n >= 0 ) then
        do np = 0, a_pot_n

          do k = 0, 5
          do l = 0, 5
            n = k + l + np  

            if ( a_i == 1 ) then 
              analyt_int1 = analyt_int1 + g**(np+1)*coef_ai_r(k) * coef_aj_r(l) /(n+1)*binominal(a_pot_n,np)*x2**(a_pot_n-np)
            else if ( a_i == a_grid%npoints) then
              analyt_int1 = analyt_int1 - h**(np+1)*coef_ai_l(k) * coef_aj_l(l) /(n+1)*binominal(a_pot_n,np)*x2**(a_pot_n-np)
            else 
              analyt_int1 = analyt_int1 +                                    &
                           (-h**(np+1)*coef_ai_l(k) * coef_aj_l(l) /(n+1) +  &
                             g**(np+1)*coef_ai_r(k) * coef_aj_r(l) /(n+1)) * &
                             binominal(a_pot_n,np)*x2**(a_pot_n-np)
            end if
            
          end do
          end do

        end do

      else  !  a_pot_n <0 
        do np = 0, np_max
          aux1 = 2*mod(np+1, 2)-1

          do k = 0, 5
          do l = 0, 5
            n = k + l + np  
            
            if ( a_i == 1 ) then 
              !analyt_int1 = analyt_int1 + g**(np+1)*aux2*binominal(a_pot_n,np)*x2**(a_pot_n-np)
              analyt_int1 = analyt_int1 + (g/x2)**(np+1) * coef_ai_r(k) * coef_aj_r(l) * binominal(pot_n + np -1,np) * aux1 /(n+1)
            else if ( a_i == a_grid%npoints) then
              !analyt_int1 = analyt_int1 - h**(np+1)*aux1*binominal(a_pot_n,np)*x2**(a_pot_n-np)
              analyt_int1 = analyt_int1 - (h/x2)**(np+1) * coef_ai_l(k) * coef_aj_l(l) * binominal(pot_n + np -1,np) * aux1 /(n+1)
            else 
              !analyt_int1 = analyt_int1 + (-h**(np+1)*aux1+g**(np+1)*aux2)*binominal(a_pot_n,np)*x2**(a_pot_n-np)
              analyt_int1 = analyt_int1 +                                             &
                             (-(h/x2)**(np+1) * coef_ai_l(k) * coef_aj_l(l) +         &
                               (g/x2)**(np+1) * coef_ai_r(k) * coef_aj_r(l) ) *       &
                                binominal(pot_n + np - 1,np) * aux1 / (n+1)
            end if
            if ( a_i == 2 .and. a_j == 2 ) then
              
            end if
          end do
          end do
       
        end do
        analyt_int1 = analyt_int1 / x2**(pot_n-1)

      end if  ! and a_pot_n if

    else ! if a_i /= a_j 

       x2 = xarr(2)
       if ( a_j>a_i) then 
         coef_ai = basiscoef_real(xarr, a_itype, sum(xarr(2:3))/2,  a_do_deriv, .false.)
         xarr_aux = fem_3refpoints(a_j,a_grid)
         coef_aj = basiscoef_real(xarr_aux, a_jtype, sum(xarr(2:3))/2, do_deriv_right, .false.)
         h = xarr(3)-xarr(2)
                  
       else if( a_j < a_i) then
         coef_ai = basiscoef_real(xarr, a_itype, sum(xarr(1:2))/2,  a_do_deriv, .false.)
         xarr_aux = fem_3refpoints(a_j,a_grid)  
         coef_aj = basiscoef_real(xarr_aux, a_jtype, sum(xarr(1:2))/2,  do_deriv_right, .false.)
         h = xarr(1)-xarr(2)
         
       end if
       
       if (a_pot_n >= 0 ) then

         do np = 0, a_pot_n
           do k = 0, 5
           do l = 0, 5
             n = k + l + np  
!   improve h**(np+1)*x2**(a_pot-np)
             analyt_int1 = analyt_int1 + coef_ai(k) * coef_aj(l) * h**(np+1)*x2**(a_pot_n-np) * binominal(a_pot_n,np)/binominal(k+l+np,l)/(n+1) 
         
           end do
           end do
         end do
    
       else  ! if a_pot_n <0
         
         !print *, "analyt_int1:ai,aj", a_i, a_j
         do np = 0, np_max
           aux1 = 2*mod( np+1, 2) - 1
           do k = 0, 5
           do l = 0, 5
             n = k + l + np  
             analyt_int1 = analyt_int1 + aux1 * (h/x2)**(np+1) * coef_ai(k) * coef_aj(l) * binominal(pot_n+np-1,np) / binominal(k+l+np,l) / (n+1) 
            if ( a_i ==2 .and. a_j ==2) then
              print *, "analyt_int1: k,l,n", k,l, np
              print *, "analyt_int1: coef's, h/x2", coef_ai(k), coef_aj(l), h/x2
              print *, "binom coef's", binominal(pot_n+np-1,np), binominal(k+l+np,l)
              print *, "interm", (h/x2)**(np+1) * coef_ai(k) * coef_aj(l) * binominal(pot_n+np-1,np) / binominal(k+l+np,l) / (n+1)
            end if
           end do
           end do
            
         end do

         !analyt_int1 = analyt_int1*h /x2**(pot_n)
         analyt_int1 = analyt_int1/x2**(pot_n-1)
       end if

      

    end if  ! a_i == a_j if

    return
    
  end function analyt_int1







!---------------------------------------
!  analytical integratio for small r - without shifting
!----------------------------------------


  function analyt_int2(a_i, a_itype, a_j, a_jtype, a_grid, a_rleft, a_rright, a_do_deriv, a_pot_n, ao_do_deriv)

    use, intrinsic :: ieee_arithmetic
    implicit none
    
    integer     ,intent(in) :: a_i, a_j   ! central points of the finite elements
    integer     ,intent(in) :: a_itype, a_jtype
    type(t_grid),intent(in) :: a_grid
    real(dblprec)    ,intent(in) :: a_rleft, a_rright
    logical     ,intent(in) :: a_do_deriv             ! applys to the both. If ao_do_deriv present, apllys to the left basis function
    logical     ,intent(in), optional :: ao_do_deriv  ! applys to the right basis function
    integer     ,intent(in) :: a_pot_n
    real(dblprec)           :: analyt_int2



    integer            :: i, j, k, l, n
    integer, parameter :: degree_fe = 3
    real(dblprec)      :: xarr(degree_fe)                                   & ! arrays to initialize the finite elements
                         ,coef_ai(0:5), coef_aj(0:5)                        &
                         ,rleft, rright, r1,r2 
    
    analyt_int2 = 0.D0
 
    if (a_rleft .ge. a_rright) then
      return
    end if

    if ( abs(a_i-a_j) .ge. 2) then
      return
    end if

    xarr(1:3) = fem_3refpoints(a_i, a_grid)
    rleft = max(a_rleft,xarr(1))  ! lower integration limit
    rright= min(a_rright,xarr(3)) ! upper integration limit

    do i = 1, 2
      ! check if grid segments needs to be considered
      if ( i==1 ) then
        if ( a_j>a_i .or. rleft>=xarr(2) ) cycle
        coef_ai = basiscoef_real(xarr, a_itype, sum(xarr(1:2))/2,  a_do_deriv, .true.)  
        coef_aj = basiscoef_real(fem_3refpoints(a_j,a_grid), a_jtype, sum(xarr(1:2))/2,  a_do_deriv, .true.)
        r1 = rleft
        r2 = min(xarr(2),rright)

      else if (i==2 ) then
        if ( a_j<a_i .or. rright<=xarr(2) ) cycle
        coef_ai = basiscoef_real(xarr, a_itype, sum(xarr(2:3))/2,  a_do_deriv, .true.)  
        coef_aj = basiscoef_real(fem_3refpoints(a_j,a_grid), a_jtype, sum(xarr(2:3))/2,  a_do_deriv, .true.)
        r1 = max(xarr(2),rleft)
        r2 = rright

      end if

      do k = 0, 5
      do l = 0, 5
        n = k + l + a_pot_n  
        if ( n == -1 ) then
          if ( r1 == ZERO) then
            analyt_int2 = analyt_int2 + coef_ai(k) * coef_aj(l) * log(r2)
          else   
            analyt_int2 = analyt_int2 + coef_ai(k) * coef_aj(l) * log(r2/r1)
          end if
        else
          if ( r1 == ZERO ) then
            analyt_int2 = analyt_int2 + coef_ai(k) * coef_aj(l) * r2**(n+1)/(n+1)
          else 
            analyt_int2 = analyt_int2 + coef_ai(k) * coef_aj(l) * (r2**(n+1) - r1**(n+1))/(n+1)
          end if
        end if
      end do
      end do

      
    end do

    return
    
  end function analyt_int2

end module analytic_integrator
