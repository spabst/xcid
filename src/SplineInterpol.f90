module SplineInterpol
  !
  !**PURPOSE
  !  -------
  ! The MODULE SplineInterpol contains routines and functions related to 
  ! spline interpolation.
  !
  !**DESCRIPTION
  !  ------------
  ! The following subroutines (sbr) and functions (fct) are part of 
  ! this module:
  !
  !  -> (fct) eval_spline_object : evaluate function at given point by interpolation
  !                                "t_spline" object is passed
  !  -> (fct) eval_spline : evaluate function at given point by interpolation
  !  -> (sbr) init_spline_object : initialize spline routine/arrays
  !                                "t_spline" object is passed
  !  -> (sbr) init_spline : initialize spline routine/arrays
  !
  !**AUTHOR
  !  ------
  ! passed to Stefan Pabst by Robin Santra (origin of procedures is unkown)
  ! rewritten into a module by Stefan Pabst in April 2011
  !
  !**VERSION
  ! svn info: $Id: SplineInterpol.f90 1316 2014-10-05 22:22:01Z spabst $
  ! ----------------------------------------------------------------------
  !
  use Constants, dp=>dblprec 
  implicit none

  type t_spline
    ! number of function stored in y
    integer :: nfct = 0

    ! curve f_n(x), n represents the 2. index in y [1...nfct]
    real(dp),allocatable :: x(:),y(:,:)
    
    ! spine array
    !  - 1. index : size of argument (= size of x)
    !  - 2. index : spline field [1...3]
    !  - 3. index : function ID [1...nfct]
    real(dp), allocatable :: spline(:,:,:)

    ! file name
    character(clen) :: file = ""
    logical         :: active = .false.
  end type t_spline

contains


  pure function eval_spline_object(a_x0,a_this,ao_fct,ao_mode) Result(res)
    !
    ! user friendly interface to eval_spline for one function stored in
    ! spline object (normally 1. function is used)
    !
    !**INPUT/OUTPUT
    !  -------------
    !
    !  a_x0    :  x value to be evaluated
    !
    !  a_this  :  spline interpolation object containing all the infos
    !
    !  ao_fct  :  function ID in spline object
    !             default : 1
    !
    !  ao_mode : what kind of function should be evaluated
    !            mode = 0 : fct(x)  [default]
    !            mode = 1 : fct'(x)
    !            mode =-1 : int^x dx' fct(x')
    !
    implicit none

    type(t_spline),intent(in) :: a_this
    real(dp), intent(in) :: a_x0
    integer,intent(in),optional :: ao_fct,ao_mode
    real(dp) :: res

    integer :: fct,mode


    res = ZERO

    ! default 1. function
    fct = 1
    if (present(ao_fct)) fct = ao_fct
    mode = 0
    if (present(ao_mode))  mode = ao_mode

    ! check consistency
    if ( .not.allocated(a_this%x) .or. .not.allocated(a_this%y) .or.  &
         .not.allocated(a_this%spline)                                &
       ) then

      return
      !write(STDERR,*) " ERROR(eval_spline): spline object is not allocated"
    else
      if ( size(a_this%spline,3) < fct ) return
      !write(STDERR,*) " ERROR(eval_spline): number of fct. in object is too small"
    end if

    res = eval_spline( a_x0,a_this%x,a_this%y(:,fct),a_this%spline(:,:,fct),mode )
    
  end function eval_spline_object


  pure function eval_spline(a_x0,a_x,a_y,a_spline,ao_mode) Result(res)
    ! this subroutine evaluates the cubic spline function
    !
    !    res = y(i) + b(i)*(u-x(i)) + c(i)*(u-x(i))**2 + d(i)*(u-x(i))**3
    !
    !    where  x(i) .lt. u .lt. x(i+1), using horner's rule
    !
    !  if  x0 <  x(1) then  res = y(1)
    !  if  x0 >= x(n) then  res = y(n)
    
    !
    ! INPUT
    !   x0     : argument at which the spline interpolation should be evaluated
    !   x,y    : arrays of argument and function values 
    !   spline : auxiliary spline fields 
    !            spline(:,1) = b
    !            spline(:,2) = c
    !            spline(:,3) = d
    !  ao_mode : what kind of function should be evaluated
    !            mode = 0 : fct(x)  [default]
    !            mode = 1 : fct'(x)
    !            mode =-1 : int^x dx' fct(x')
    !
    implicit none

    real(dp),intent(in) ::  a_x0,a_x(:),a_y(:)
    real(dp),intent(in) ::  a_spline(:,:)
    integer,intent(in),optional ::  ao_mode
    
    integer i, j, k,sz(2)
    real(dp) :: dx(3),res
    integer :: mode


    res = ZERO
    mode = 0
    if ( present(ao_mode) )  mode = ao_mode
    
    sz = shape(a_spline)
    if (sz(2) /= 3 ) return
    if (sz(1) /= size(a_x) .or. sz(1) /= size(a_y)) return

    if (a_x0 <= a_x(1)) then
      i = 1
      res = a_y(i)
      if (mode/=0) res = ZERO
      return
    else if (a_x0 >= a_x(sz(1))) then
      i = sz(1)
      res = a_y(i)
      if (mode/=0) res = ZERO
      return
    else
      ! find segment
      i = 1
      j = sz(1) + 1 
      do while (j>i+1)
        k = (i+j)/2
        if (a_x0 <  a_x(k))  j = k
        if (a_x0 >= a_x(k))  i = k
      end do 
    end if

    ! evaluate spline
    dx(1) = a_x0 - a_x(i)
    dx(2) = dx(1)**2
    dx(3) = dx(1)**3
    select case(mode)
    case(0)
      res = a_y(i) + sum(dx*a_spline(i,:))
    case(1)
      dx = [ ONE , dx(1)*TWO , dx(2)*THREE ]
      res = sum(dx*a_spline(i,:))
    case(-1)
      res = dx(1)*(a_y(i)+sum(dx*a_spline(i,:)/[TWO,THREE,FOUR])) 
      do j=1,i-1
        dx(1) = a_x(j+1) - a_x(j)
        dx(2) = dx(1)**2
        dx(3) = dx(1)**3  
        res = res + dx(1)*(a_y(j)+sum(dx*a_spline(j,:)/[TWO,THREE,FOUR]))
      end do
    end select

  end function eval_spline



  subroutine init_spline_object(a_this,ao_linear,ao_info)
    !
    ! user-friendly interface to init_spline for all functions stored in
    ! spline object
    !
    !**INPUT/OUTPUT
    !  -------------
    !
    !  a_this  :  TYPE(t_spline) - INOUT
    !             spline interpolation object containing all the infos/arrys
    !
    !  ao_linear: LOGICAL - IN  - OPTIONAL
    !             use linear (instead of cubic) interpolation
    !             default = .false.
    !
    !  ao_info :  LOGICAL - OUT - OPTIONAL
    !             true : successful finished
    !             false: unsuccessful finished
    !
    type(t_spline),intent(inout) :: a_this
    logical,intent(out),optional :: ao_info
    logical,intent(in),optional :: ao_linear

    integer :: i,nx,ny
    logical :: linear


    ! optional output info
    if (present(ao_info)) ao_info = .false.


    ! check x and y fields exist
    if ( .not.allocated(a_this%x) )             &
        stop " ERROR(init_spline_obj): x-field is not allocated"
    
    if ( .not.allocated(a_this%y) )             &
        stop " ERROR(init_spline_obj): y-field is not allocated"
    
    if ( size(a_this%y,1) /= size(a_this%x) )   &
        stop " ERROR(init_spline_obj): x,y-fields have different lengths"
          
    nx = size(a_this%x)
    ny = size(a_this%y,2)
    linear = .false.
    if ( present(ao_linear) )  linear = ao_linear

    ! check if spline field is allocated/large enough
    if ( allocated(a_this%spline) .and. ( size(a_this%spline,1)<nx     &
          .or. size(a_this%spline,3)<ny .or. size(a_this%spline,2)<3 ) &
       ) then

      deallocate(a_this%spline)
    end if
    ! allocate spline
    allocate( a_this%spline(nx,3,ny) )
    a_this%spline = ZERO


    ! loop through all function within the object
    do i=1,size(a_this%y,2)
      call init_spline(a_this%x,a_this%y(:,i),a_this%spline(:,:,i),ao_linear)
    end do


    ! successful termination
    if (present(ao_info)) ao_info = .true.
  end subroutine init_spline_object



  subroutine init_spline(a_x,a_y,a_spline,ao_linear)
    ! Initialize spline array (spline) for cubic spline interpolation
    ! spline contains the vectors b,c,d as mentioned below.
    ! The coefficients b(i), c(i), and d(i), i=1,2,...,n are computed
    !  
    ! DESCRIPTION
    ! -----------
    !
    !    s(x) = y(i) + b(i)*(x-x(i)) + c(i)*(x-x(i))**2 + d(i)*(x-x(i))**3
    !
    !    for  x(i) <= x <= x(i+1)
    !
    !  using  '  to denote differentiation,
    !
    !    y(i) = s(x(i))
    !    b(i) = s'(x(i))
    !    c(i) = s''(x(i))/2
    !    d(i) = s'''(x(i))/6 
    !
    ! INPUT
    ! -----
    !  x,y    : arrays of argument and function values 
    !  linear : use linear interpolation (function may be not smooth anymore)
    !           default :  .false.

    ! OUTPUT
    ! ------
    !  spline : auxiliary spline fields spline = ( b(:),c(:),d(:) )
    !
    implicit none

    real(dp), intent(in) :: a_x(:),a_y(:)
    logical, intent(in),optional  :: ao_linear

    real(dp), target  :: a_spline(:,:)
    real(dp), pointer :: b(:)=>null()   &
                        ,c(:)=>null()   &
                        ,d(:)=>null()
    real(dp) :: t
    integer :: n,nm1,i,ib

    n = size(a_x)
    if (n /= size(a_y)) stop " error(init_spline): size of argument&
       & and function field is different"
    
    if ( size(a_spline,1)<n .or. size(a_spline,2)<3 )                &
      stop " ERROR(init_spline): spline field is too small"

    a_spline = ZERO
    b => a_spline(:,1)
    c => a_spline(:,2)
    d => a_spline(:,3)

    ! linear interpolation
    ! --------------------
    ! only calculate 1. derivative (higher derivatives are zero)
    ! last 1. derivative is zero s.t. function is constant
    if ( present(ao_linear) ) then
    if ( ao_linear ) then
      forall(i=1:n-1,a_x(i+1)/=a_x(i))  ! avoid singularities
        b(i) = (a_y(i+1)-a_y(i)) / (a_x(i+1)-a_x(i))
      end forall
      return
    end if
    end if
   
    ! cubic spline
    ! -------------
    if ( n == 2 ) then
      ! --------------------------------------
      b(1) = (a_y(2)-a_y(1))/(a_x(2)-a_x(1))
      c(1) = 0.
      d(1) = 0.
      b(2) = b(1)
      c(2) = 0.
      d(2) = 0.
    elseif ( n > 2 ) then
      ! --------------------------------------
      nm1 = n-1
      !
      !  set up tridiagonal system
      !
      !  b = diagonal, d = offdiagonal, c = right hand side.
      !
      d(1) = a_x(2) - a_x(1)
      c(2) = (a_y(2) - a_y(1))/d(1)
      do i = 2, nm1
         d(i) = a_x(i+1) - a_x(i)
         b(i) = TWO*(d(i-1) + d(i))
         c(i+1) = (a_y(i+1) - a_y(i))/d(i)
         c(i) = c(i+1) - c(i)
      end do
      !
      !  end conditions.  third derivatives at  x(1)  and  x(n)
      !  obtained from divided differences
      !
      b(1) = -d(1)
      b(n) = -d(n-1)
      c(1) = ZERO
      c(n) = ZERO
      if ( n > 3 ) then
        c(1) = c(3)/(a_x(4)-a_x(2)) - c(2)/(a_x(3)-a_x(1))
        c(n) = c(n-1)/(a_x(n)-a_x(n-2)) - c(n-2)/(a_x(n-1)-a_x(n-3))
        c(1) = c(1)*d(1)**2/(a_x(4)-a_x(1))
        c(n) = -c(n)*d(n-1)**2/(a_x(n)-a_x(n-3))
      end if
      !
      !  forward elimination
      !
      do i = 2, n
         t = d(i-1)/b(i-1)
         b(i) = b(i) - t*d(i-1)
         c(i) = c(i) - t*c(i-1)
      end do
      !
      !  back substitution
      !
      c(n) = c(n)/b(n)
      do ib = 1, nm1
         i = n-ib
         c(i) = (c(i) - d(i)*c(i+1))/b(i)
      end do
      !
      !  c(i) is now the sigma(i) of the text
      !
      !  compute polynomial coefficients
      !
      b(n) = (a_y(n) - a_y(nm1))/d(nm1) + d(nm1)*(c(nm1) + TWO*c(n))
      do i = 1, nm1
         b(i) = (a_y(i+1) - a_y(i))/d(i) - d(i)*(c(i+1) + TWO*c(i))
         d(i) = (c(i+1) - c(i))/d(i)
         c(i) = THREE * c(i)
      end do
      c(n) = THREE * c(n)
      d(n) = d(n-1)
    end if
  end subroutine init_spline



  pure function diff1_object(a_x0,a_this,ao_fct) Result(res)
    !
    ! user-friendly interface to diff1_spline.
    ! calculates 1. derivative of functions in spline object
    !
    !**INPUT/OUTPUT
    !  -------------
    !
    !  a_x0    :  x value to be evaluated
    !
    !  a_this  :  spline interpolation object containing all the infos
    !
    !  ao_fct  :  function ID in spline object
    !             default : 1
    !
    implicit none

    type(t_spline),intent(in) :: a_this
    real(dp), intent(in) :: a_x0
    integer,intent(in),optional :: ao_fct
    real(dp) :: res

    integer :: fct


    res = ZERO

    ! default 1. function
    fct = 1
    if (present(ao_fct)) fct = ao_fct

    ! check consistency
    if ( .not.allocated(a_this%x) .or. .not.allocated(a_this%y) .or.  &
         .not.allocated(a_this%spline)                                &
       ) then

      return
      !write(STDERR,*) " ERROR(eval_spline): spline object is not allocated"
    else
      if ( size(a_this%spline,3) < fct ) return
      !write(STDERR,*) " ERROR(eval_spline): number of fct. in object is too small"
    end if

    res = diff1( a_x0,a_this%x,a_this%y(:,fct),a_this%spline(:,:,fct) )
    
  end function diff1_object


  pure function diff1(a_x0,a_x,a_y,a_spline) Result(res)
    ! this subroutine calculates the derivative of the cubic spline function
    !
    !    y(x) = y(i) + b(i)*(x-x(i)) + c(i)*(x-x(i))**2 + d(i)*(x-x(i))**3
    !
    !    where  x(i) .lt. u .lt. x(i+1), using horner's rule
    !
    !  if  x0 <  x(1) then  i = 1  is used.
    !  if  x0 >= x(n) then  i = n  is used.
    !
    !  1. derivative : y'(x) = b(i) + 2*c(i)*(x-x(i)) + 3*d(i)*(x-x(i))**2

    !
    ! INPUT
    !   n      : size of the arrays
    !   x0     : argument at which the spline interpolation should be evaluated
    !   x,y    : arrays of argument and function values
    !   spline : auxiliary spline fields
    !            spline(:,1) = b
    !            spline(:,2) = c
    !            spline(:,3) = d
    !
    implicit none

    real(dp),intent(in) ::  a_x0,a_x(:),a_y(:)
    real(dp),intent(in) ::  a_spline(:,:)
    integer i, j, k,sz(2)
    real(dp) :: dx(3),res

    res = ZERO

    sz = shape(a_spline)
    if (sz(2) /= 3 ) return
    if (sz(1) /= size(a_x) .or. sz(1) /= size(a_y)) return

    if (a_x0 <= a_x(1)) then
      i = 1
    else if (a_x0 >= a_x(sz(1))) then
      i = sz(1)
    else
      ! find segment
      i = 1
      j = sz(1) + 1
      do while (j>i+1)
        k = (i+j)/2
        if ( a_x0 <  a_x(k) ) j = k
        if ( a_x0 >= a_x(k) ) i = k
      end do
    end if

    ! evaluate derivative
    dx(1) = ONE
    dx(2) = TWO*(a_x0 - a_x(i))
    dx(3) = THREE*(dx(2)/TWO)**2

    res = sum(dx*a_spline(i,:))
  end function diff1


end module  SplineInterpol
