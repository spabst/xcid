module ReadMatrices
  !
  !**PURPOSE
  !  Collection of subroutines to read matrices from file. 
  !
  !**DESCRIPTION
  !  -----------
  !  The following subroutines (sbr) and functions (fct) are part of 
  !  this module:
  !   -> init_mweight     : initialize M-dependent part of the Wigner-Eckert
  !                         theorem
  !   -> read1p1hmatrices : read all 1p1h matrices from file and store them
  !                         in a globally accessable module arrays
  !   -> read2p2hmatrices : read 2p2h Coulomb matrix from file and store them
  !                         in a globally accessable module arrays
  !
  !**AUTHOR
  !  ------
  !  (c) and written by Stefan Pabst
  !
  !**VERSION
  !  -------
  ! svn info: $Id: ReadMatrices.f90 1148 2014-07-08 15:14:26Z spabst $
  !
  use constants
  use VarSystem, only : mformat=>m_matrix_format
  use inout, only : find_openpipe
  use omp_lib
  implicit none


contains

  subroutine init_mweight
    !
    ! Wigner-Eckart theorem is used for 1p1h matrices.
    ! However, the m1=m2=m3=0 matrix element is stored rather
    ! than the reduced matrix element. 
    ! Here the m-dependent factor is calculated. To obtain the matrix
    ! elements m1=m3, m2=0.
    !
    use varsystem,only : m_lmax
    use varwfct,only   : m_moamax
    use varmatrix, only : m_mweight
    use angmom,only : cleb
    implicit none

    integer :: l,m

    allocate(m_mweight(0:m_lmax,0:m_moamax))
    m_mweight=zero
    do l=1,m_lmax
      do m=0,min(m_moamax,l-1)
        ! symmetric in l<->l-1
        m_mweight(l,m) = cleb(2*l,2*m , 2,0 , 2*(l-1),2*m)   &
                       / cleb(2*l,0   , 2,0 , 2*(l-1),0)  
      end do
    end do
  end subroutine init_mweight



  subroutine read1p1hmatrices
    use varsystem,only : m_ev,m_corrfct                      &
                        ,m_LMgauge,m_vgauge,m_lgauge         &
                        ,m_fdipherm,m_fmomherm,m_fdipacc     &
                        ,m_folap,m_fsplit                    &
                        ,m_tsurf,m_ftsurfmat                 &
                        ,m_absorb, m_grid
    use varmatrix,only : m_dipherm,m_dipacc,m_momherm        &
                        ,m_olap,m_split,m_tsurf_mat
    implicit none
    
    
    !$omp parallel default(shared)
    !$omp sections 

    !$omp section
    ! Overlap 
    !   -> (l,m) independent matrices
    ! ----------------------------------
    ! overlap 
    call get_L0Operator(m_folap,m_olap)

    !$omp section
    ! Overlap with splitting function
    ! ----------------------------------
    if ( m_absorb == "SPL" ) then
      call get_L0Operator(m_fsplit,m_split)
    end if

    ! radial gradient (for t-surf)
    ! -> only needed for PSG grid
    if ( m_tsurf .and. m_grid%type=="PSG" ) then
      call get_L0Operator(m_ftsurfmat,m_tsurf_mat,ao_sym=-1)
    end if


    ! dipole operators (operators that transform like vectors)
    !  -> dl=+-1
    !  -> dm = 0 (only z-direction is of interest)
    !  -> hermitian inner product <- used for expectation value
    ! ---------------------------------------------------------
    
    !$omp section
    if ( m_ev(1).or.m_ev(2) .or. (m_corrfct.and.m_LMgauge==m_lgauge) ) then
      call get_L1Operator_hermitian(m_fdipherm,m_dipherm)
    end if
    !$omp section
    if (m_ev(3) .or. (m_corrfct.and.m_LMgauge==m_vgauge) ) then
      call get_L1Operator_hermitian(m_fmomherm,m_momherm)
    end if
    !$omp section
    if (m_ev(4)) call get_L1Operator_hermitian(m_fdipacc,m_dipacc)

    !$omp section

    ! dipole or momentum operator (symmetric)
    !  -> elements are structured in z_ij,z_ai,z_ab for efficiency
    !  -> used for equation of motion
    call get_L1Operator_symmetric

    !$omp end sections nowait
    !$omp end parallel 

  end subroutine read1p1hmatrices
    

  subroutine get_L0Operator(a_file,a_mat,ao_herm,ao_sym)
    !
    ! read overlap or CAP matrix
    ! -> see Eq. 11,A3 in Greenman, PRA 82, 023406 (2011)
    ! -> see between Eq. 28 and 29 in Greenman, PRA 82, 023406 (2011)
    !
    !**INPUT
    !  -----
    !  a_file  : CHARCTER(*) - IN
    !            matrix file
    !  a_mat   : COMPLEX(:,:,:) - IN/OUT - ALLOCATABLE
    !            array that holds the matrix
    !  ao_herm : LOGICAL - IN - OPTIONAL
    !            matrix is hermitian or symmetric
    !  ao_sym  : INTEGER - IN - OPTIONAL
    !            +1/-1 = symmetric/antisymmetric
    use varsystem,only : m_nmax,m_lmax
    implicit none

    character(clen),intent(in) :: a_file
    complex(dblprec),allocatable :: a_mat(:,:,:)
    logical, intent(in), optional :: ao_herm
    integer, intent(in), optional :: ao_sym

    logical :: ex,herm
    integer :: sym
    integer :: ioread
    integer :: n1,n2,l1,l2
    real(dblprec) :: mateler,matelei
    complex(dblprec) :: matele      
    character(clen) :: line

    integer :: fid



    inquire(file=a_file,exist=ex)
    if (.not.ex) then
      write(STDERR,*) " ERROR: file '",trim(a_file),"' doesn't exist"
      stop
    end if

! make sure every threat has a differnt pipe
#ifdef OPENMP
    fid = find_openpipe(ao_min=100+omp_get_thread_num()) 
#else
    fid = find_openpipe() 
#endif

    if ( trim(mformat)=="bin") then
      open(fid,file=a_file,status="old",action="read",form="unformatted")
    else
      open(fid,file=a_file,status="old",action="read")
    end if
    write(STDOUT,*) " -> reading file... ",trim(a_file)

    ! matrix hermitian or symmetric
    herm = .true.
    if ( present(ao_herm) ) herm = ao_herm

    sym = 1
    if ( present(ao_sym) ) sym = ao_sym

    ! only save M=0 contribution
    ! rest can be constructed by multiplying 
    ! with one clebsch-gordon coefficient
    allocate(a_mat(0:m_nmax,0:m_nmax,0:m_lmax))      
    a_mat=zero


    ioread=0
    lp_line:do while (ioread==0)

       if ( trim(mformat)=="bin" ) then
         read(fid,iostat=ioread)  n1,l1,n2,l2,mateler,matelei
         if ( ioread /= 0 ) exit
       else
         read(fid,'(a)',iostat=ioread) line
         if ( ioread /= 0 ) exit
         line = adjustl(line)
         if ( line(1:1) == "#" .or. len_trim(line) == 0 ) cycle
         read(line,*) n1,l1,n2,l2,mateler,matelei
       end if

       ! assign complex matrix element
       matele = mateler + img * matelei

       
       if ( max(n1,n2)>m_nmax .or. max(l1,l2)>m_lmax .or. l1/=l2) then 
          ! this range not needed => do nothing
       else  
         a_mat(n1,n2,l1) = matele
         if ( herm ) then
           a_mat(n2,n1,l1) = sym*dconjg(matele)
         else
           a_mat(n2,n1,l1) = sym*matele
         end if

         if (n1==n2) a_mat(n1,n2,l1) = dble(matele)
       end if
    end do lp_line


    close(fid)   
    if (ioread>0) then
      write(STDERR,*) " ERROR: reading file ",trim(a_file)
      stop
    end if
  end subroutine get_L0Operator

  ! -------------------------------------------

  subroutine get_L1Operator_hermitian(a_file,a_mat)
    ! 
    !  <p|op|q> = sum_r <p|r) (r|op|q> = sum_r o_p,r op_(r,q)
    !    o_p,r    - overlap matrix
    !    op_(r,q) - matrix element w.r.t. symmetric inner product
    !              (see Eq. A4 in Greenman, PRA 82, 023406 (2011))
    !
    !  All operators/observables op are dipole operators, meaning they
    !  have an angular momentum of L=1.
    !  This is true for the dipole operator r, dipole acceleration r_acc
    !  and the momentum operator p.
    !  See for more details Eq. A5 in Greenman, PRA 82, 023406 (2011)
    !  
    use varsystem,only : m_nmax,m_lmax
    use general,only : m_isact=>isact
    implicit none

    character(clen),intent(in) :: a_file
    complex(dblprec),allocatable :: a_mat(:,:,:,:)

    logical :: ex
    integer :: i,ioread,n1,n2,l1,l2
    real(dblprec) :: mateler,matelei
    complex(dblprec) :: matele      
    character(clen) :: line 

    integer :: fid


    inquire(file=a_file,exist=ex)
    if (.not.ex) then
      write(STDERR,*) " ERROR: file '",trim(a_file),"' doesn't exist"
      stop
    end if

! make sure every threat has a differnt pipe
#ifdef OPENMP
    fid = find_openpipe(ao_min=100+omp_get_thread_num()) 
#else
    fid = find_openpipe() 
#endif
    
    if ( trim(mformat)=="bin") then
      open(fid,file=a_file,status="old",action="read",form="unformatted")
    else
      open(fid,file=a_file,status="old",action="read")
    end if
    write(STDOUT,*) " -> reading file... ",trim(a_file)


    ! only save M=0 contribution
    ! rest can be constructed by multiplying 
    ! with one clebsch-gordon coefficient
    allocate(a_mat(0:m_nmax,0:m_nmax,0:m_lmax,0:1))      
    a_mat=zero
   

    ioread=0
    ldipherm:do while (ioread==0)
      
      if ( trim(mformat)=="bin" ) then
        read(fid,iostat=ioread)  n1,l1,n2,l2,mateler,matelei
        if ( ioread /= 0 ) exit
      else
        read(fid,'(a)',iostat=ioread) line
        if ( ioread /= 0 ) exit
        line = adjustl(line)
        if ( line(1:1) == "#" .or. len_trim(line) == 0 ) cycle
        read(line,*) n1,l1,n2,l2,mateler,matelei
      end if
      
      ! assign complex matrix element
      matele = mateler + img * matelei

      
      if ( max(n1,n2)>m_nmax .or. max(l1,l2)>m_lmax .or. abs(l1-l2)/=1) then
        ! do nothing, either orbital is not active
      else
        ! exclude m/=0 entry would lead to problems 
        ! if only m/=0 orbitals are active
        
        if (l1>l2) then
          a_mat(n1,n2,l1,0) = matele 
          a_mat(n2,n1,l2,1) = conjg(matele)  
        else if (l2>l1) then
          a_mat(n1,n2,l1,1) = matele
          a_mat(n2,n1,l2,0) = conjg(matele)  
        end if
      end if
    end do ldipherm
   

    close(fid)
    if (ioread>0) then
      write(STDERR,*) " ERROR: reading file ",trim(a_file)
      stop
    end if
  end subroutine get_L1Operator_hermitian
  
  ! -------------------------------------------

  subroutine get_L1Operator_symmetric(ao_lmax)
    !
    ! read dipole moment z_(p,q) or momentum [p_z]_(p,q)
    ! -> for z_(p.q) see Eq. A4 in Greenman, PRA 82, 023406 (2011) 
    ! -> for p_z the matrix is actually antisymmetric rather than symmetric
    !
    !
    !
    !**INPUT/OUTPUT
    !  ------------
    !  ao_lmax:  max. L to be read; once l>ao_lmax stop reading file
    !            assume L is ordered in matrix file
    !
    use varsystem,only   : m_fdipsymm,m_fmomsymm,m_nmax,m_lmax &
                          ,m_SOcoupl,m_LMgauge,m_lgauge,m_vgauge
    use varwfct,only   : m_i1p1hclass,m_id1p1hr=>m_id1p1hclassrev, m_fmoact,  m_moamax
    use varmatrix,only : m_dipij,m_dipab,m_dipai,m_mweight  
    use general,only : m_isact=>isact,m_isocc=>isocc,m_idocc=>idocc
    use angmom,only  : cleb
    implicit none

    integer, intent(in), optional :: ao_lmax ! max. l to be read

    logical :: ex
    integer :: ioread,n1,n2,l1,l2
    integer :: m1,m2,mi
    integer :: isgn,id1,id2,ids,lid
    real(dblprec) :: mateler,matelei,aux
    complex(dblprec) :: matele      
    character(clen) :: line,filename

    integer :: fid


    ! assign file name and symmetry of the operator
    isgn = 0
    if ( m_LMgauge == m_lgauge ) then
      filename = m_fdipsymm
      isgn = 1   ! symmetric
    elseif ( m_LMgauge == m_vgauge ) then
      filename = m_fmomsymm
      isgn = -1  ! antisymmetric
    end if



    ex = .FALSE.
    inquire(file=filename,exist=ex) 
    if (.not.ex) then
      write(STDERR,*) "  -> file doesn't exist"
      write(STDERR,*) "     file name: ",trim(filename)
      stop
    end if

    fid = find_openpipe()
    if ( trim(mformat)=="bin") then
      open(fid,file=filename,status="old",action="read",form="unformatted")
    else
      open(fid,file=filename,status="old",action="read")
    end if

    write(STDOUT,*) " -> reading dipole/momentum file (symmetric version)... " &
         ,trim(filename)



    ! dipole (symmetric version) fields
    allocate(m_dipij(m_i1p1hclass,m_i1p1hclass)    &
         ,m_dipai(0:m_nmax,0:1,m_i1p1hclass)       &
         ,m_dipab(0:m_nmax,0:m_nmax,0:1,0:m_lmax))
    m_dipij=zero
    m_dipai=zero
    m_dipab=zero


    ioread=0
    ldipsymm:do while (ioread==0)
       
      if ( trim(mformat)=="bin" ) then
        read(fid,iostat=ioread)  n1,l1,n2,l2,mateler,matelei
        if ( ioread /= 0 ) exit
      else
        read(fid,'(a)',iostat=ioread) line
        if ( ioread /= 0 ) exit
        line = adjustl(line)
        if ( line(1:1) == "#" .or. len_trim(line) == 0 ) cycle
        read(line,*) n1,l1,n2,l2,mateler,matelei
      end if
      
      ! assign complex matrix element
      matele = mateler + img * matelei

      

      if ( abs(l1-l2)/=1) then
      else if ( max(n1,n2)>m_nmax .or. max(l1,l2)>m_lmax) then
      else if ( present(ao_lmax) ) then
        ! stop reading the file when the ao_lmax is surpassed
        if ( max(l1,l2)>ao_lmax ) exit
      else if (m_isocc(n1,l1).and.m_isocc(n2,l2)) then

         ! loop through all act. occ. orbtial i,j with
         ! N_i=n1, L_i=l1 and N_j=n2, L_j=l2
         ! M^S_i = M^S_j - same spin component
         ! M^J_i = M^J_j - same M qunatum number
         do id1=1,m_i1p1hclass
           ! sign of m_L does not matter => use abs(m) for condition
           if (m_id1p1hr(id1)%n/=n1.or.m_id1p1hr(id1)%l/=l1 ) cycle 
           m1 = m_id1p1hr(id1)%ml
           
           do id2=1,m_i1p1hclass
             if (m_id1p1hr(id2)%n/=n2.or.m_id1p1hr(id2)%l/=l2 ) cycle 
             m2 = m_id1p1hr(id2)%ml

             if ( m_socoupl ) then
               ! spin must be the same component
               if (m_id1p1hr(id1)%ms/=m_id1p1hr(id2)%ms) cycle
               ! M^L and therefore M^J must be the same
               if (m_id1p1hr(id1)%mj/=m_id1p1hr(id2)%mj) cycle

               ! m^L = m^J - m^S
               ! spin down
               mi  = m_id1p1hr(id1)%mj + 1
               aux = cleb(2*l2,mi , 2,0 , 2*l1,mi)                     &
                   * m_id1p1hr(id1)%cleb(0) * m_id1p1hr(id2)%cleb(0)
               
               ! spin up
               mi  = m_id1p1hr(id1)%mj - 1
               aux = aux + cleb(2*l2,mi , 2,0 , 2*l1,mi)               &
                   * m_id1p1hr(id1)%cleb(1) * m_id1p1hr(id2)%cleb(1)

               aux = aux / cleb(2*l2,0 , 2,0 , 2*l1,0)
             else
               if ( m1 /= m2 ) cycle
               aux = m_mweight(max(l1,l2),m1)  
             end if
             
             
             m_dipij(id1,id2) = matele * aux
             m_dipij(id2,id1) = isgn * m_dipij(id1,id2) ! antisymmetric/symmetric

           end do
         end do
                 
         ! -----------------------
      else if (m_isocc(n1,l1).or.m_isocc(n2,l2)) then
         ! -----------------------
         
         ! z_ai
         ifzai:if (m_isocc(n1,l1)) then

            if (l2>l1) lid=1
            if (l1>l2) lid=0
            
            do id1=1,m_i1p1hclass
              ! sign of m^L does not matter => use abs(m) for condition
              if ( m_id1p1hr(id1)%n/=n1 .or. m_id1p1hr(id1)%l/=l1 ) cycle
              
              if ( m_socoupl ) then
                ! <L,M_L ; S,M_S | J,M_J>
                ! cleboa(id,id_s) : ID_s = 0,1 <-> S=-1/2,1/2
                ids = (m_id1p1hr(id1)%ms+1)/2
                aux = m_id1p1hr(id1)%cleb(ids) * m_mweight( max(l1,l2) , m_id1p1hr(id1)%ml )
                m_dipai(n2,lid,id1) = matele * aux
              else
                m_dipai(n2,lid,id1) = matele * m_mweight( max(l1,l2) , m_id1p1hr(id1)%ml )
              end if

              ! enforcing m_dipai(n_a,l_a,i) := z_(a,i) = isgn * z_(i,a)
              !  -> dipole operator   : isgn = +1 (symmetric)
              !  -> momentum operator : isgn = -1 (antisymmetric)
              m_dipai(n2,lid,id1) = isgn * m_dipai(n2,lid,id1)

            end do
            
         else if (m_isocc(n2,l2)) then
            
            if (l1>l2) lid=1
            if (l2>l1) lid=0
            
            do id2=1,m_i1p1hclass
              ! sign of m^L does not matter => use abs(m) for condition
              if (m_id1p1hr(id2)%n/=n2 .or. m_id1p1hr(id2)%l/=l2 ) cycle
               
              if ( m_socoupl ) then
                ! <L,M_L ; S,M_S | J,M_J>
                ! cleboa(id,id_s) : ID_s = 0,1 <-> S=-1/2,1/2
                ids = (m_id1p1hr(id2)%ms+1)/2
                aux = m_id1p1hr(id2)%cleb(ids) * m_mweight( max(l1,l2) , m_id1p1hr(id2)%ml )
                m_dipai(n1,lid,id2) = matele * aux
              else
                m_dipai(n1,lid,id2) = matele * m_mweight( max(l1,l2) , m_id1p1hr(id2)%ml )
              end if
            end do
         else
            stop " error in logic of reading in file"
         end if ifzai
         
         ! -----------------------
      else 
         ! -----------------------
         
         ! z_ab
         ifzab:if (l1>l2) then
            m_dipab(n2,n1,0,l1) = matele
            m_dipab(n1,n2,1,l2) = isgn*matele
         else
            m_dipab(n2,n1,1,l1) = matele
            m_dipab(n1,n2,0,l2) = isgn*matele
         end if ifzab
         
      end if
      
    end do ldipsymm
    
    
    close(fid)
    if (ioread>0) stop " error reading dipole(symmetric) file"
    
  end subroutine get_L1Operator_symmetric
  

! #############################################################
! #############################################################
! #############################################################


  subroutine mat_1p1h2actocc( a_matin , a_matout )
    !
    !  convert m_mat, which is stored in the 1p1h-ID basis, into the
    !  occ. act. orbital-ID basis (only necessary for SO-coupling is active)
    !  When SOcoupling is not active 1p1h-ID=occ.act.orb.-ID.
    !  Since for SOcoupl=active the 1p1h-IDs have additionally spin infos 
    !  about the excited electron only the spin up (of the excited electron) 
    !  channels will be used to reduce the index to the active occupied 
    !  orbital IDs. 
    !  Spin up is selected, since only |mj_+> = 1/sqrt(2) ( |mj> + |-mj> ) is
    !  populated and considered.
    !  
    !**INPUT/OUTPUT
    !  ------------
    !  a_matin  : COMPLEX(DP) [1p1hclassID,1p1hclassID] - IN 
    !             Matrix indicies are 1p1hclasses IDs
    !  a_matout : COMPLEX(dp) [actOrbID,actOrbID] - IN -ALLOCATABLE
    !             Matrix indicies are the act. occ. orbtial IDs
    !
    use varwfct, only : m_i1p1hclass,m_ioccact,m_1p1hr=>m_id1p1hclassrev
    implicit none
    
    complex(dblprec), intent(in)  :: a_matin(:,:)
    complex(dblprec), allocatable :: a_matout(:,:)

    integer :: sz
    integer :: i,j,ii,jj,si,sj

    ! check size of matin
    sz = size(a_matin,1)
    if ( sz /= size(a_matin,2) .or. sz /= m_i1p1hclass )   &
      stop " ERROR(mat_1p1h2actocc): size of a_matin is inconsistent"

    ! create output matrix
    if ( allocated(a_matout) )  deallocate(a_matout)
    allocate( a_matout( m_ioccact , m_ioccact ) )
    a_matout = ZERO


    do i=1,m_i1p1hclass
      do j=1,m_i1p1hclass

        ii = m_1p1hr(i)%idhole
        si = m_1p1hr(i)%ms
        jj = m_1p1hr(j)%idhole
        sj = m_1p1hr(j)%ms

        ! from some hole states (J=L+S,M_J=+/-J) the spin of active electron
        ! can be only +/-0.5. Due to symmetry M_S<0 or not considered and so
        ! the M_S=-0.5 could be 0, where the M_S=0.5 component is not.
        if ( si/=sj  ) cycle
        a_matout(ii,jj) = a_matin(i,j)
      end do
    end do

  end subroutine mat_1p1h2actocc



! #############################################################
! #############################################################
! #############################################################


  subroutine read2p2hmatrices
    !
    !**PURPOSE
    !  -------
    !  Read Coulomb matrix elements w/ respect to the symm. inner product
    !  see Eq. A7 in Greenman, PRA 82, 023406 (2011)
    !
    !  -> Coulomb matrix elements refer to the symmetry-adapted CIS configurations
    !     meaning terms are a bit more complicated than stated in 
    !     Eq. A7 in Greenman, PRA 82, 023406 (2011).
    !  -> symmetry-adapted matrix elements without spin-orbit are given in 
    !             see Eq. (5c) in Pabst, PRA 85, 023411
    !  -> symmetry-adapted matrix elements with spin-orbit
    !             see Eq. (10) in Pabst, PRA 86, 063411
    ! 
    !**DESCRIPTION
    !  -----------
    !  M-symmetries in Coulomb interaction use in the case w/ and w/o spin-orbit
    !  -> direct term: Vd^[m_a,m_b]=Vd^[-m_a,m_b]=Vd^[m_a,-m_b]
    !  -> excha. term: Ve^[m_a,m_b]=Ve^[-m_a,-m_b]
    !  -> symmetry (all m -> -m) is used s.t. m^L_a>=0 w/ and w/o spin-orbit
    !
    use varmatrix      
    use varwfct, only   : m_i1p1hclass,m_1p1h=>m_id1p1hclassrev
    implicit none

    integer, parameter :: ia=1  ! 1st index is always the a index (also named v1)
    integer, parameter :: ij=2  ! 2nd index is always the j index (also named o2)

    character(clen) :: line
    integer :: k1,k2  ! k* are dummy run indicies

    ! setup CIS Coulomb matrix
    if ( allocated(m_coulomb) ) deallocate(m_coulomb)
    allocate( m_coulomb(m_i1p1hclass,m_i1p1hclass) )
    forall(k1=1:m_i1p1hclass,k2=1:m_i1p1hclass) 
      ! index structure: j=o2 , i=o1
      m_coulomb(k1,k2)%lo2 = m_1p1h(k1)%l
      m_coulomb(k1,k2)%lo1 = m_1p1h(k2)%l
    end forall

    ! assemble direct and exchange parts incl. all prefactors
    ! s.t. m_coulomb w/o any prefactor is used

    ! add direct part : v_ajib
    call coulomb_direct
    
    ! add exchange part : v_ajbj
    call coulomb_exchange

    ! ####################################################
  contains
    ! ####################################################

    subroutine coulomb_direct
      !
      ! Read v_ajib
      !
      use varsystem,only : m_lprx,m_lsum,m_nmax  &
                          ,m_fcoudir             &
                          ,m_intercoupl,m_intracoupl &
                          ,m_SOcoupl
      use general,only : isocc,isact
      implicit none

      integer, parameter :: ib=4  ! 4th index is the b index (also named v2)
      integer, parameter :: ii=3  ! 4rd index is the i index (also named o1)

      logical :: ex
      integer :: ioread,k
      integer :: ido1,ido2,lv1id,lv2id
      integer :: fid
      
      real(dblprec) :: aux,mateler,matelei
      complex(dblprec) :: matele
      complex(dblprec),pointer  :: entry1 => null()  &
                                  ,entry2 => null()
      integer :: n(4),l(4),m(4)
      


      ! open file
      ! ---------
      inquire(file=m_fcoudir,exist=ex)
      if (.not.ex) stop "  -> Coulomb-direct file doesn't exist"

! make sure every threat has a differnt pipe
#ifdef OPENMP
      fid = find_openpipe(ao_min=100+omp_get_thread_num()) 
#else
      fid = find_openpipe() 
#endif

      if ( trim(mformat)=="bin") then
        open(fid,file=m_fcoudir,status="old",action="read",form="unformatted")
      else
        open(fid,file=m_fcoudir,status="old",action="read")
      end if
      write(6,*) " -> reading Coulomb (direct) file... ",trim(m_fcoudir)


      ! read each line
      ! --------------
      ioread=0
      lp_file:do while (ioread==0) 
        
        if ( trim(mformat)=="bin" ) then
          read(fid,iostat=ioread) (n(k),l(k),m(k),k=1,4) , mateler,matelei
          if ( ioread /= 0 ) exit
        else
          read(fid,'(a)',iostat=ioread) line
          if ( ioread /= 0 ) exit
          line = adjustl(line)
          if ( line(1:1) == "#" .or. len_trim(line) == 0 ) cycle
          read(line,*) (n(k),l(k),m(k),k=1,4),mateler,matelei
        end if
        
        ! assign matrix element
        matele = mateler + img * matelei

        
        if ( mod(sum(l),2)/=0 .or. any(l<abs(m)) ) then
        else if ( .not.(isocc(n(ij),l(ij)).and.isocc(n(ii),l(ii))) ) then
        else if ( isocc(n(ia),l(ia)).or.isocc(n(ib),l(ib))) then
        else if ( m(ia)/=m(ii).or.m(ij)/=m(ib) ) then
        else if ( any(l>m_lprx) .or. any(n>m_nmax) ) then
           ! max. l or max. n  exceeded
        else if ( abs(l(1)-l(3))>m_lsum .or. abs(l(2)-l(4))>m_lsum ) then
        
        else if ( all(isact(n,l,m)) ) then
          ! main part
          ! ---------
           
          
          ! allow only (a,j) index pair > (b,i) index pair
          ! ----------------------------------------------
          ! sould not be necessary since it has been taken care of by the
          ! generation of the matrix elements
          ! 
          ! enforce j>=i when a==b
          if ( all((/n(ia),l(ia),m(ia)/)==(/n(ib),l(ib),m(ib)/)) ) then
            if ( l(ij)==l(ii) .and. n(ij)<n(ii) ) cycle
            if ( l(ij)<l(ii) ) cycle

          ! enforce a>b, since a/=b
          elseif ( l(ia)==l(ib) ) then
            if ( n(ia)<n(ib) ) cycle
            if ( n(ia)==n(ib) .and.  m(ia)<m(ib) ) cycle
          elseif ( l(ia)<l(ib) ) then
            cycle
          end if

          
          
          ! loop over all possible hole configurations
          lp_j:do ido2=1,m_i1p1hclass !!! (b,j) 1p1h-class
            
            ! check n_j,l_j, m^L_b
            if (      m_1p1h(ido2)%n /= n(ij)   &
                 .or. m_1p1h(ido2)%l /= l(ij)   &
                 .or. m_1p1h(ido2)%ml/= m(ib)   &
               ) cycle
            

            lp_i:do ido1=1,m_i1p1hclass !!! (a,i) 1p1h-class

              ! only intrachannel coupling
              if ( .not.m_intercoupl .and. ido1/=ido2 ) cycle 
              if ( .not.m_intracoupl .and. ido1==ido2 ) cycle 

              ! check n_i,l_i, m^L_a
              if (      m_1p1h(ido1)%n /= n(ii)   &
                   .or. m_1p1h(ido1)%l /= l(ii)   &
                   .or. m_1p1h(ido1)%ml/= m(ia)   &
                 ) cycle
                                 
          
              ! check field is initialized
              if ( m_coulomb(ido2,ido1)%nlv2<1 ) then
                call init_coulomb( m_coulomb(ido2,ido1),l(ii),l(ij),m_lprx,m_nmax )
                call init_coulomb( m_coulomb(ido1,ido2),l(ij),l(ii),m_lprx,m_nmax )
                
                ! -> if still not initialized it means an error occurred
                if ( m_coulomb(ido2,ido1)%nlv2<1 .or. m_coulomb(ido1,ido2)%nlv2<1 ) then
                  write(STDERR,*) " ERROR(Read Coulomb): initialization of m_coulomb failed "
                  write(STDERR,*) " ido1,ido2,m_lprx:",ido1,ido2,m_lprx
                  stop
                end if
              end if
 
              ! find index of l(1) and l(4) used in m_coulomb
              lv2id = -1
              lv2id = minloc(array=m_coulomb(ido2,ido1)%lv2(:,l(ia)), dim=1  &
                             ,mask=m_coulomb(ido2,ido1)%lv2(:,l(ia))==l(ib)  &
                            )
              lv1id = -1
              lv1id = minloc(array=m_coulomb(ido1,ido2)%lv2(:,l(ib)), dim=1  &
                             ,mask=m_coulomb(ido1,ido2)%lv2(:,l(ib))==l(ia)  &
                            )
              if ( lv1id<1 .or. lv2id<1 ) cycle  ! no allowed l-combination
         

              ! calculate prefactors
              ! ---------------------
              ! set prefactor to 1
              aux = ONE                 
              if ( m_SOcoupl ) then
                ! <L,M_L ; S,M_S | J,M_J>
                !  -> see Eq. (10) in Pabst, PRA 86, 063411

                ! if a==b or a==-b enforce j>=i in the ido1 and ido2 loops
                if ( all((/n(ia),l(ia),m(ia)/)==(/n(ib),l(ib),abs(m(ib))/)) ) then
                  if ( l(ij)<l(ii) ) cycle
                  
                  if ( l(ij)==l(ii) ) then
                    if ( n(ij)<n(ii) ) cycle 
                    
                    if ( n(ij)==n(ii) ) then
                      if ( m_1p1h(ido2)%j<m_1p1h(ido1)%j ) cycle
                      if ( m_1p1h(ido2)%j==m_1p1h(ido1)%j  .and.  &
                           m_1p1h(ido2)%mj<m_1p1h(ido1)%mj        &                           
                         ) cycle
                    end if
                  end if
                end if

                aux = m_1p1h(ido1)%cleb((m_1p1h(ido1)%ms+1)/2) &
                    * m_1p1h(ido2)%cleb((m_1p1h(ido2)%ms+1)/2)
              else  ! no spin-orbit
                ! factor appearing by going into the m_g/u representation
                !  -> see Eq. (5c) in Pabst, PRA 85, 023411
                if (m(1)/=0) aux = aux * sqrt(TWO)
                if (m(2)/=0) aux = aux * sqrt(TWO)
              end if


              ! add contributions: 2*direct part
              ! --------------------------------
              entry1 => m_coulomb(ido2,ido1)%matrix( n(ib) , n(ia) , lv2id , l(ia) )
              entry2 => m_coulomb(ido1,ido2)%matrix( n(ia) , n(ib) , lv1id , l(ib) )
             
              entry1 = entry1 + TWO*aux*matele
              entry2 = entry1  ! index symmetry: v_ajib=v_bija



              nullify(entry1,entry2)
            end do lp_i
          end do lp_j
          
        end if
      end do lp_file

      close(fid)
      if (ioread>0) stop " error in reading Coulomb-direct file"
    end subroutine coulomb_direct

  
    ! #############################################################
    

    subroutine coulomb_exchange
      !
      ! Read v_ajbi
      !
      use varsystem,only : m_lprx,m_lsum,m_lmax,m_nmax   &
                          ,m_fcouexc                     &
                          ,m_intercoupl,m_intracoupl     &
                          ,m_SOcoupl,m_nmax
      use varmatrix,only : m_couexcdiag
      use general,only : isocc,isact
      implicit none

      integer, parameter :: ib=3  ! 3rd index is the b index (also named v2)
      integer, parameter :: ii=4  ! 4th index is the i index (also named o1)

      logical :: ex
      integer :: ioread,k
      integer :: ido1,ido2,lv1id,lv2id
      integer :: sigma  ! spin of holes i and j
      integer :: fid
      
      real(dblprec) :: aux,mateler,matelei
      complex(dblprec) :: matele
      complex(dblprec),pointer :: entry1 => null()  &
                                 ,entry2 => null()
      integer :: n(4),l(4),m(4)



      ! open file
      ! ---------
      inquire(file=m_fcouexc,exist=ex)
      if (.not.ex) stop " -> Coulomb-exchange file doesn't exist"

! make sure every threat has a differnt pipe
#ifdef OPENMP
      fid = find_openpipe(ao_min=100+omp_get_thread_num()) 
#else
      fid = find_openpipe() 
#endif

      if ( trim(mformat)=="bin") then
        open(fid,file=m_fcouexc,status="old",action="read",form="unformatted")
      else
        open(fid,file=m_fcouexc,status="old",action="read")
      end if
      write(6,*) " -> reading Coulomb (exchange) file... ",trim(m_fcouexc)


      ! read each line
      ! --------------
      ioread=0
      lp_file:do while (ioread==0)
        
        if ( trim(mformat)=="bin" ) then
          read(fid,iostat=ioread) (n(k),l(k),m(k),k=1,4) , mateler,matelei
          if ( ioread /= 0 ) exit
        else
          read(fid,'(a)',iostat=ioread) line
          if ( ioread /= 0 ) exit
          line = adjustl(line)
          if ( line(1:1) == "#" .or. len_trim(line) == 0 ) cycle
          read(line,*) (n(k),l(k),m(k),k=1,4),mateler,matelei
        end if
        
        ! read matrix element
        matele = mateler + img * matelei


        if_line:if ( mod(sum(l),2)/=0 ) then
        else if ( .not.(isocc(n(ij),l(ij)).and.isocc(n(ii),l(ii))) ) then
        else if ( isocc(n(ia),l(ia)).or.isocc(n(ib),l(ib)) ) then
        else if ( abs(l(3)-l(1))>l(2)+l(4) ) then
        else if ( sum(m(1:2))/=sum(m(3:4)) ) then
        else if ( any(l<abs(m)) ) then
        else if ( abs(l(1)-l(3))>m_lsum .or. abs(l(2)-l(4))>m_lsum ) then
        else if ( any(l>m_lmax) .or. any(n>m_nmax) ) then

        else if ( any(l>m_lprx) ) then
          ! Coulomb approximation
          ! ---------------------
          ! - one particle operator
          ! - hole and m independent

          if ( n(ii)/=n(ij) .or. l(ii)/=l(ij) .or. m(ii)/=m(ij) ) cycle
          if ( l(ia)/=l(ib) ) cycle

          if ( .not.allocated(m_couexcdiag) ) then
            allocate( m_couexcdiag(0:m_nmax,0:m_nmax,m_lprx+1:m_lmax) )
            m_couexcdiag = ZERO
          end if

          m_couexcdiag(n(ib),n(ia),l(ia)) = matele
          m_couexcdiag(n(ia),n(ib),l(ia)) = matele
          

        ! main part ( no approximations)
        ! ------------------------------
        else if ( all(isact(n,l,m)) ) then


          ! allow only certain m-combinations
          ! ---------------------------------
          ! sould not be necessary since it has been taken care of by the
          ! generation of the matrix elements
          !  -> enforce m(1)>=0 
          !  -> enforce m(2)>=0  if m(1)=0 or m(1)=m(3)
          !  -> enforce m(3)>=0  if m(1)=m(2)=0
          !  -> exclude this m-combination to avoid double counting, since
          !     it's not checked if a particular m-combination has been already used
          if ( m(1)<0 .or. (m(1)==0.and.m(2)<0) .or. (m(1)==m(3).and.m(2)<0) &
                  .or. (all(m(1:2)==0).and.m(3)<0)                           &
                 ) cycle
          
          ! allow only (a,j) index pair > (b,i) index pair
          ! ----------------------------------------------
          ! sould not be necessary since it has been taken care of by the
          ! generation of the matrix elements
          ! 
          ! enforce j>=i when a==b or a==-b 
          !  -> in the latter case trafo m->-m leads to double counting
          if ( all((/n(ia),l(ia),m(ia)/)==(/n(ib),l(ib),abs(m(ib))/)) ) then
            if ( l(ij)==l(ii) .and. n(ij)<n(ii) ) cycle
            if ( l(ij)<l(ii) ) cycle

          ! enforce a>b, since a/=b
          elseif ( l(ia)==l(ib) ) then
            if ( n(ia)<n(ib) ) cycle
            if ( n(ia)==n(ib) .and.  m(ia)<abs(m(ib)) ) cycle
          elseif ( l(ia)<l(ib) ) then
            cycle
          end if


          ! since only M^J>0 is used, we have to make
          ! sure we include the M^J<0 contributions
          lp_j:do ido2=1,m_i1p1hclass !!! (b,j) 1p1h-class
          ! -------------------------

            ! check here : N_j,L_j,m^L_b
            ! --------------------------
            ! sign of m^L_b matters for exchange term
            ! 1p1h-class may be transfered s.t. m^L_b is <0
            if (      m_1p1h(ido2)%n/=n(ij)        &
                 .or. m_1p1h(ido2)%l/=l(ij)        &
                 .or. m_1p1h(ido2)%ml/=abs(m(ib))  &
               ) cycle
            

            lp_i:do ido1=1,m_i1p1hclass !!! (a,i) 1p1h-class
            ! -------------------------

              ! check intrachannel coupling
              ! -------------------------
              if ( .not.m_intercoupl .and. ido1/=ido2 )  cycle 
              if ( .not.m_intracoupl .and. ido1==ido2 )  cycle 
              
              ! check here : N_i,L_i,m^L_a
              ! -------------------------
              ! m^L_a is always enforced to be >=0
              if (      m_1p1h(ido1)%n/=n(ii)    &
                   .or. m_1p1h(ido1)%l/=l(ii)    &
                   .or. m_1p1h(ido1)%ml/=m(ia)   &
                 ) cycle


              ! calculate prefactors
              ! --------------------
              ! set prefactor to 1
              aux = ONE                 
              if_so:if (.not.m_SOcoupl) then
                ! check M-symmetry: m^L_a = m^L_i ; m^L_b = m^L_j
                if ( m(ia)/=m(ii) .or. m(ij)/=m(ib) ) cycle
             
                ! if m(1)/=0/=m(2) or m(1)=0=m(2) no action is needed
                ! in the 1. case: both exch. terms are distrinct
                ! in the 2. case: both exch. terms refer to the same entry 
                !                 and because of the factor 1/2 only one
                !                 term is needed
                !
                ! if only m(1) or only m(2) is zero than sqrt(2) is needed
                !  -> see Eq. (5c) in Pabst, PRA 85, 023411
                if ( sum(m(1:2))/=0 .and. product(m(1:2))==0 ) then
                  ! two exchange terms in Eq. (5c) have the same value
                  aux = sqrt(TWO)
                end if
                 
              else   ! ---  SO splitting ---
                
                ! - |M^L_a-M^L_i|,|M^L_b-M^L_j|<=1
                ! - |M^L_b-M^L_j| is automatically checked
                !   since M^L_a-M^L_i = M^L_b-M^L_j 
                if ( abs(m(ii)-m(ia))>1 ) cycle

                
                ! if a==b or a==-b enforce j>=i in the ido1 and ido2 loops
                if ( all((/n(ia),l(ia),m(ia)/)==(/n(ib),l(ib),abs(m(ib))/)) ) then
                  if ( l(ij)<l(ii) ) cycle
                  
                  if ( l(ij)==l(ii) ) then
                    if ( n(ij)<n(ii) ) cycle 
                    
                    if ( n(ij)==n(ii) ) then
                      if ( m_1p1h(ido2)%j<m_1p1h(ido1)%j ) cycle
                      if ( m_1p1h(ido2)%j==m_1p1h(ido1)%j  .and.  &
                           m_1p1h(ido2)%mj<m_1p1h(ido1)%mj        &                           
                         ) cycle
                    end if
                  end if
                end if

                ! find which exchange term correspond to current matrix entry
                ! -> see term 2 and three in Eq. (10) in Pabst, PRA 86, 063411
                ! -> transform m's s.t. m^J_i is always positive
                if_spin:if ( m_1p1h(ido1)%ms==m_1p1h(ido2)%ms ) then                  
                  ! same spin -> means m^J_i,m^J_j>0
                  ! ---------------------------------
                  ! -> check M^sigma_1 is present [for M^sigma_1 see Eq. (10) in Pabst, PRA 86, 063411]

                  ! if m^L_a==0==m^L_i a m->-m flip could have occurred 
                  ! in opposite-spin section
                  if ( m(ia)==0 .and. m(ii)==0 .and. m(ij)<0) m=-m  
                  !  -> checks automatically: m(3)=m^L_b
                  if ( any(m<0) ) cycle  ! all m^L must be >=0

                  ! find which sigma term is present: sigma=m^J_i-m^L_i w/ m^L_i=m(4)
                  sigma = m_1p1h(ido1)%mj - 2*m(ii)  ! 2*sigma
                  if ( abs(sigma)/=1 )  cycle  ! current ido2=i hole is not consistent w/ m(4)
                  
                  ! check:  m(2)=m^L_j=m^J_j-sigma  
                  if ( 2*m(ij) /= m_1p1h(ido2)%mj-sigma )   cycle

                  ! convert sigma into spin index w/ values 0 (spin down) or 1 (spin up)
                  sigma = (sigma+1)/2

                  ! multiply Clebsch-Gordan coefficients
                  aux = m_1p1h(ido1)%cleb(sigma) * m_1p1h(ido2)%cleb(sigma)
                
                else  
                  ! opposite spin -> means m^J_j<0 (keep m^J_i>0)
                  ! ---------------------------------------------
                  ! -> check M^sigma_2 is present [for M^sigma_2 see Eq. (10) in Pabst, PRA 86, 063411]
                  !
                  ! -> means coupling between i and j hole with m^J_j<0<m^J_i
                  !    (m^J_j,m^L_b,sigma_b) -> (-m^J_j,-m^L_b,-sigma_b)

                  
                  ! enforce m^J_i>=0
                  !  and optionally if m^J_i==0 then m^J_j<0
                  if ( m(ia)==0 ) then
                    if ( m(ii)<0 ) m=-m  
                    if ( m(ii)==0 .and. m(ij)>0) m=-m  
                  end if
                  
                  ! check  m(3)=-m^L_b
                  if ( m(ib)/=-m_1p1h(ido2)%ml ) cycle
                  
                  ! find which sigma term is present: sigma=m^J_i-m^L_i w/ m^L_i=m(4)
                  sigma = m_1p1h(ido1)%mj - 2*m(ii)  ! 2*sigma
                  if ( abs(sigma)/=1 )  cycle  ! current ido2=i hole is not consistent w/ m(4)
                  
                  ! check  m(2)=m^L_j=-m^J_j-sigma 
                  if ( 2*m(ij) /= -m_1p1h(ido2)%mj-sigma )   cycle

                  ! convert sigma into spin index w/ values 0 (spin down) or 1 (spin up)
                  sigma = (sigma+1)/2

                  ! multiply Clebsch-Gordan coefficients
                  ! for j hole -sigma is used and for i sigma is used
                  aux = m_1p1h(ido2)%cleb(mod(sigma+1,2)) * m_1p1h(ido1)%cleb(sigma)
                  
                end if if_spin
              end if if_so
              

              ! check field is initialized
              if ( m_coulomb(ido2,ido1)%nlv2<1 ) then
                call init_coulomb( m_coulomb(ido2,ido1),l(ii),l(ij),m_lprx,m_nmax )
                call init_coulomb( m_coulomb(ido1,ido2),l(ij),l(ii),m_lprx,m_nmax )

                ! -> if still not initialized it means an error occurred
                if ( m_coulomb(ido2,ido1)%nlv2<1 .or. m_coulomb(ido1,ido2)%nlv2<1 ) then
                  write(STDERR,*) " ERROR(Read Coulomb): initialization m_coulomb failed"
                  write(STDERR,*) " ido1,ido2,m_lprx:",ido1,ido2,m_lprx
                  stop
                end if
              end if
          
              ! find index of l(1) and l(4) used in m_coulomb
              lv2id = -1
              lv2id = minloc(array=m_coulomb(ido2,ido1)%lv2(:,l(ia)), dim=1  &
                             ,mask=m_coulomb(ido2,ido1)%lv2(:,l(ia))==l(ib)  &
                            )
              lv1id = -1
              lv1id = minloc(array=m_coulomb(ido1,ido2)%lv2(:,l(ib)), dim=1  &
                             ,mask=m_coulomb(ido1,ido2)%lv2(:,l(ib))==l(ia)  &
                            )
              if ( lv1id<1 .or. lv2id<1 ) cycle  ! no allowed l-combination

              
              ! add contributions: -exchange part
              ! ---------------------------------
              entry1 => m_coulomb(ido2,ido1)%matrix( n(ib) ,n(ia) , lv2id , l(ia) )
              entry2 => m_coulomb(ido1,ido2)%matrix( n(ia) ,n(ib) , lv1id , l(ib) )
             
              entry1 = entry1 - aux*matele
              entry2 = entry1  ! index symmetry: v_ajbi=v_biaj

              nullify(entry1,entry2)
            end do lp_i
          end do lp_j

        end if if_line
      end do lp_file
      
      close(fid)
      if (ioread>0) stop " error in reading Coulomb-exchange file"
    end subroutine coulomb_exchange

  end subroutine read2p2hmatrices


end module ReadMatrices
