program HF_program
  !
  !**PURPOSE
  !  -------
  !  Perform closed-shell atomic Hartree-Fock to get firstly all occupied
  !  orbitals and secondly all 1-particle orbitals ( occupied + virtual) 
  !  and their orbital energies.
  !  
  !**DESCRIPTION
  !  -----------
  !  Eq. 47 in Greenman, PRA 82. 023406 (2011) is solved iteratively. 
  !  Depending on the program option given, all orbitals of the Fock
  !  operator including CAP (Eq. 3a,3b,31 in Greenman, PRA 82. 023406 (2011)).
  !  
  !
  !**AUTHOR
  !  -------
  !  (c) and written by Stefan Pabst, 2011
  !
  !**VERISON
  !  -------
  !  svn info: $Id: HF_program.f90 1711 2015-05-22 18:44:19Z spabst $
  !-----------------------------------------------------------------------
  !
  use constants
  use svn, only : m_projectrev
  use basics, only : timeinfo
  use VarSystem, only : g_program=>m_program,m_svnrev
  use grid, only : initialize_grid,rgridscf_init
  implicit none
  character(LEN=4) :: g_mode=""
  integer :: g_tp(8,3)=0


  call date_and_time(values=g_tp(:,1))

  ! HF is running
  g_program = 0 

  ! pass on-the-fly determined global svn version
  ! to module "general"
  m_svnrev = m_projectrev

  call prepare
  call date_and_time(values=g_tp(:,2))
  call modeselection

  call date_and_time(values=g_tp(:,3))
  call timeinfo(g_tp)

  print *
  print *, "--------------------------------------------"
  print *, "Hartree-Fock is done and thanks for the use."
  print *, "--------------------------------------------"
  print *, "'Ich mag sie nicht(,) und es tut mir leid,"
  print *, " dass ich jemals etwas mit ihr zu tun   "
  print *, " hatte.' [Erwin Schroedinger]"
  print *

  
  ! ####################################################
contains
  ! ####################################################

  subroutine prepare
    use varsystem,only : m_focc,m_forb,m_fenergy,m_lsum,m_lprx,m_grid  &
                        ,m_mp=>m_modelpot,m_nelec,m_nocoulomb 
    use varwfct,only   : m_lomax,m_actoccorb,m_orbital
    use general,only   : printinfo,init_idoccrev,defaultvalue,printvariables   &
                        ,assign_active_orbitals
    use read,only : readoccupied,readorb,readenergy,readsystempara
    
    use inout, only    : readfile,inquireopen,flength
    use splineinterpol,only : init_spline_object, spline => eval_spline_object
    use FEM_Hamiltonian,only : init_FEM_matrices, local_pot
    use femdvr_hamil,only : init_femdvr_matrices

    implicit none

    character(clen) :: input
    logical :: ex
    integer :: nlines, i
    integer, parameter :: fid=34

    ! print header
    call printinfo(g_program,ao_header=-1)

    ! check input file
    call getarg(1,input)
    ! show help and exit program
    if ( trim(input) == "-v" ) then
      call printhelp
      stop
    end if

    ! check existence of the input file
    ex = .FALSE.
    inquire(file=input,exist=ex)

    ! input file given/not given
    if (.not.ex) then
      ! input file doesn't exist => assume is mode info
      g_mode = input
    else
      ! 2. argument defines which calculation will be performed
      call getarg(2,g_mode)  
    end if

    !check validity of mode
    select case (g_mode)
    case("0","1","1.1","1.2","2")
    case default
      call printhelp
      if (ex) then
        write(STDERR,'(/,x,a)') " ERROR: invalid mode"
      else
        write(STDERR,'(a)') " ERROR: input file and/or mode is invalid"
      end if
      stop
    end select


    ! set default values
    call defaultvalue    
    ! read input file and flag arguments
    call readSystemPara(input)

    if ( g_mode == "0" ) then
      m_nelec = 1
      m_nocoulomb = .true.
    end if

    ! print parameters for HF but no header (ao_header=0)
    call printinfo(g_program,ao_header=0)

    ! initialize grid (m_grid , m_gridscf , m_subgrid)
    ! -------------------------------------------------
    call initialize_grid(m_grid)


    ! for mode=1.2 and 2 orbitals and occupation must be predefined
    !  -> orbitals are read later s.t. printvariable is executed before 
    !     orbitals are read (since that can take a moment for big systems)
    if (trim(g_mode)=="1.2" .or. trim(g_mode)=="2") then 

      ! define occupied and active orbitals
      print *, "reading occupation file... ",trim(m_focc)
      call readoccupied(m_focc)
      print *, "reading energy file... ",trim(m_fenergy)
      call readenergy(m_fenergy)
     
      !print *, "assign active orbitals" 
      !call assign_active_orbitals(m_actoccorb)
   
    else
      
      ! only even number of electrons (except 1-electron)
      if ( mod(m_nelec,2)/=0 .and. m_nelec>1)  then
        stop " ERROR: odd number of elec."
      end if

      ! set m_lsum: up to orbitals with l=m_lprx matrix element is exact
      m_lsum = 2*m_lprx
    end if

    ! print important system variables
    call printvariables

    ! read model potential
    if ( m_mp%active ) then
      write(STDOUT,'(2a)') " reading model potential file... ", trim(m_mp%file)

      nlines = flength(m_mp%file,"E","#")
      ! if file is empty object data has 1 empty element
      if ( nlines == 0 ) then
        nlines = 1
        write(STDERR,'(2x,a)') "WARNING: model potential file has no data information"
      end if

      allocate( m_mp%x(nlines) , m_mp%y(nlines,1) )
      m_mp%x = ZERO
      m_mp%y = ZERO

      call inquireopen(ex,fid,m_mp%file,ao_action="read")
      call readfile(fid,m_mp%x,m_mp%y(:,1))
      close(fid)

      ! initialize spline
      call init_spline_object(m_mp,ao_linear=.true.)
   end if
    

    ! for mode=1.2 and 2 orbitals must be read
    if (trim(g_mode)=="1.2" .or. trim(g_mode)=="2") then 

      print *, "reading orbital file... ",trim(m_forb)
      call readorb(m_forb,m_orbital,m_grid,m_lomax)
    end if
    
    ! initialize FEM matrices
    if ( m_grid%type=="FEM" )  call init_fem_matrices(0)
    ! initialize FEM-DVR matrices
    if ( m_grid%type=="DVR" )  call init_femdvr_matrices(0)
    
  end subroutine prepare


  subroutine modeselection
    use varsystem,only : m_nuccharge,m_nelec
    use varwfct,only   : m_lomax
    use hf,only : hf_scf,hf_allorbitals,hf_energy,hf_store,occorb_info
    implicit none

    complex(dblprec) :: energyhf
    integer :: i
    
    select case(g_mode)
    case("0")  ! H-like atom
      i = m_nelec
      m_nelec = 1  ! set to hydrogen atom
      call hf_allorbitals
      m_nelec = i
      call hf_store(1)
    case("1")  ! SCF + all-orbital calculation
      call hf_scf
      call hf_allorbitals
      call hf_store(1)
    case("1.1") ! only SCF
      call hf_scf
      call hf_store(2)
    case("1.2") ! only all-orbital calculation
      call hf_allorbitals
      call hf_store(1)   
    case("2")   ! HF energy    
      energyhf=hf_energy()
      call occorb_info

      write(STDOUT,'(X,/,a,F12.5," + I ",F12.5,/)')             &
           " Hartree-Fock Energy E_HF [a.u.] = ",real(energyhf),imag(energyhf)
    case default
      call printhelp
      write(STDERR,'(/,x,a)') " mode is invalid!!!"
      stop
    end select
  end subroutine modeselection


  subroutine printhelp
    implicit none

    write(STDOUT,*)  
    write(STDOUT,*) ' usage: hf.x [input file / -v ] [mode]'
    write(STDOUT,*) ' ----------------------------------------'
    write(STDOUT,*) ' mode = 0   - H-like atom [no e-e interaction]'
    write(STDOUT,*) '      = 1   - Closed-shell Hartree-Fock [complete run] '
    write(STDOUT,*) '      = 1.1   -> find mean-field/occ. orb.'
    write(STDOUT,*) '      = 1.2   -> find all 1-particle orbitals &
                                     &[occ. pre-defined]'
    write(STDOUT,*) '      = 2   - HF-Energy &
                   &[occfil,orbfil(r/i),enfil must exist]'
    write(STDOUT,*)  

  end subroutine printhelp

end program HF_program
