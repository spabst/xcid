module TDCIS
  !
  !**PURPOSE
  !  -------
  !  Contains subroutines solely necessary for the CIS time proagation.
  !
  !**DESCRIPTION
  !  -----------
  !  This module contians the following (sbr) and functions(fct):
  !
  !  -> (sbr) IDMcorr_update   : add CAP-correction to ion density matrix
  !  -> (sbr) CalcWriteExptVal :
  !                          store the expectation values, the electric field,
  !                          and the ion density matrix elements in a files
  !                          at each time step defined by dtprint
  !
  !  -> (sbr) init_wavefunction :
  !                          initialize alpha coefficients, 
  !                          ion density matrix, and phase factor per time
  !                          step dt araising from the Heisenberg picture
  !
  !  -> (fct) calc_idm   :   calculate uncorrected IDM
  !  -> (fct) calc_ev    :   calculate expectation values
  !  -> (fct) calc_ev_ion:   calculate expectation values of the ion
  !  -> (fct) get_overlap:   calculate overlap between two CIS wfcts
  !
  !**AUTHOR
  !  ------
  !  (c) and written by Stefan Pabst 
  !
  !**VERSION
  !  -------
  !  svn info: $Id: TDCIS.f90 2127 2015-11-11 17:02:15Z spabst $
  !----------------------------------------------------------------------
  !
  use constants
  use varwfct, only : t_wfct
  implicit none

contains


  subroutine IDMcorr_update(a_wfct,a_dt)
    ! perform time integration to get CAP correction for 
    ! the ion density matrix
    !
    ! see Eq. 29 in Greenman, PRA 82, 023406 (2011)
    !
    use varwfct,only : m_ioccact,m_i1p1hclass,m_idphr=>m_id1p1hclassrev  &
                      ,m_orbital,m_ioar=>m_idoccactrev                   &
                      ,t_wfct
    use varsystem,only : m_lmax,m_cap_strength,m_nmax
    use varmatrix,only : m_olap
    implicit none

    real(dblprec),intent(in) :: a_dt
    type(t_wfct) :: a_wfct

    complex(dblprec) :: newstep(0:m_lmax),vec(0:m_nmax)
    complex(dblprec) :: ea(0:m_nmax)
    integer :: n,t,i,j,l,ioa,joa

    ! multiply phase factor from field-free propagation
    ! --------------------------------------------------
    forall (i=1:m_ioccact,j=1:m_ioccact)
      a_wfct%idmcorr(i,j) = a_wfct%idmcorr(i,j)              &
        * exp( IMG * a_dt *                                  &
               ( conjg(m_ioar(i)%energy)- m_ioar(j)%energy ) &
             )
    end forall

    if (a_wfct%store_IDMcorrL) then
      forall (i=1:m_ioccact,j=1:m_ioccact)
      a_wfct%idmcorrL(i,j,:) = a_wfct%idmcorrL(i,j,:)          &
          * exp( IMG * a_dt *                                  &
                 ( conjg(m_ioar(i)%energy)- m_ioar(j)%energy ) &
               )
      end forall
    end if


    ! loop over every IDM element
    do i=1,m_i1p1hclass
       do j=i,m_i1p1hclass  !only upper triangle part -> use hermitian symmetry

          ! m^L_a = m^L_b <-> m^L_i = m^L_j (with/without SO coupling)
          if (m_idphr(i)%ml/=m_idphr(j)%ml) cycle
          ! m^S_a = m^S_b (with SO coupling)
          if (m_idphr(i)%ms/=m_idphr(j)%ms) cycle

          ioa = m_idphr(i)%idhole
          joa = m_idphr(j)%idhole
 
          if (max(ioa,joa)>m_ioccact) then
             print *, i,j,ioa,joa,m_ioccact
             stop " error (IDMupdate): ID hole is too large"
          end if

          ! calculate new contribution for this time step t
          newstep=zero

          !$omp parallel do default(shared) private(l,n,vec,ea) &
          !$omp schedule(static)
          do l=0,m_lmax
            ! new method: envolves only energies
            ea = ZERO
            forall(n=0:m_nmax)
              ea(n) = m_orbital(n,l)%energy
            end forall

            ! calculate non-hermiticity via orbital energies
            ! -> this measure can be used as a correction of the norm loss
            ! -> no extra exp(-energy_ion *dt) necessarz since rho(t+dt) is used
            !    to get derivative
            vec = matmul( m_olap(:,:,l),a_wfct%alpha_ai(:,l,i)*ea )       &
                  - conjg(ea) * matmul( m_olap(:,:,l),a_wfct%alpha_ai(:,l,i) ) 
                    
            newstep(l) = dot_product(a_wfct%alpha_ai(:,l,j),vec)

          end do
          !$omp end parallel do

          a_wfct%idmcorr(ioa,joa) = a_wfct%idmcorr(ioa,joa) + IMG*a_dt*sum(newstep)


          ! assign rho_(j,i) : use hermiticity
          if(ioa/=joa) a_wfct%idmcorr(joa,ioa) = conjg(a_wfct%idmcorr(ioa,joa))
                  
          if (a_wfct%store_IDMcorrL) then
             ! add new contribution
             a_wfct%idmcorrL(ioa,joa,:) =   &
                a_wfct%idmcorrL(ioa,joa,:) + IMG*a_dt*newstep
             ! hermitian
             if(ioa/=joa) a_wfct%idmcorrL(joa,ioa,:) = conjg(a_wfct%idmcorrL(ioa,joa,:))
          end if
       end do
    end do
  end subroutine IDMcorr_update



  subroutine splitwfct(a_wfct,a_wfctsplit)
    use varsystem, only : m_lmax,m_nmax
    use varwfct, only   : m_i1p1hclass,t_wfct
    use varmatrix, only : m_split
    implicit none

    type(t_wfct)  :: a_wfct,a_wfctsplit
    integer :: i,l

    ! calculate psi_II
    forall(i=1:m_i1p1hclass,l=0:m_lmax)
       ! use symmetry of m_split to sum over the first index of m_split
       a_wfctsplit%alpha_ai(:,l,i) = matmul( a_wfct%alpha_ai(:,l,i),m_split(:,:,l) )
    end forall


    ! subtraction of psi_II from the wfct
    a_wfct%alpha_ai = a_wfct%alpha_ai - a_wfctsplit%alpha_ai

    ! correct the idm for the absorbed part
    ! delta rho_ij = <phi_i^a|phi_j> alpha_i^a.(alpha_j^a)*
    call split_corr

  contains

    subroutine split_corr
      ! calculates the correction to the idm
      use varwfct,only    : m_i1p1hclass,m_idphr=>m_id1p1hclassrev  &
                           ,m_ioccact,m_ioar=>m_idoccactrev
      use varsystem,only  : m_lmax,m_nmax,m_SOcoupl,m_split_dt
      use varmatrix, only : m_olap

      implicit none
      integer          :: i,l,j,ioa,joa 
      complex(dblprec) :: vec(0:m_nmax)
      complex(dblprec) :: idmnew(0:m_lmax)


      ! add current IDM correction
      ! --------------------------
      do i=1,m_i1p1hclass
        do j=i,m_i1p1hclass
          ! m^L_a = m^L_b(with/without SO coupling)
          if (m_idphr(i)%ml/=m_idphr(j)%ml) cycle  
          ! m^S_a =m ^S_a (with SO coupling)
          if (m_SOcoupl .and. m_idphr(i)%ms/=m_idphr(j)%ms) cycle

          ! occupied orbitals ID
          ioa = m_idphr(i)%idhole
          joa = m_idphr(j)%idhole

          do l=0,m_lmax
            ! overlap of outer parts
            vec = matmul(m_olap(:,:,l),a_wfctsplit%alpha_ai(:,l,i))
            idmnew(l) = dot_product(a_wfctsplit%alpha_ai(:,l,j),vec)
            ! overlap between inner of i and outer part of j
            vec = matmul(m_olap(:,:,l),a_wfct%alpha_ai(:,l,i))
            idmnew(l) = idmnew(l) + dot_product(a_wfctsplit%alpha_ai(:,l,j),vec)
            ! overlap between inner of j and outer part of i
            vec = matmul(m_olap(:,:,l),a_wfctsplit%alpha_ai(:,l,i))
            idmnew(l) = idmnew(l) + dot_product(a_wfct%alpha_ai(:,l,j),vec)
          end do

          ! add new contribution
          a_wfct%idmcorr(ioa,joa) = a_wfct%idmcorr(ioa,joa) + sum(idmnew)
          if(ioa/=joa) a_wfct%idmcorr(joa,ioa) = a_wfct%idmcorr(ioa,joa)
          
          ! l-resolved contribution 
          if (a_wfct%store_IDMcorrL) then
             ! add new contribution
             a_wfct%idmcorrL(ioa,joa,:) = a_wfct%idmcorrL(ioa,joa,:) + idmnew
             ! hermitian
             if(ioa/=joa) a_wfct%idmcorrL(joa,ioa,:) = conjg(a_wfct%idmcorrL(ioa,joa,:))
          end if
        end do
      end do
      
      ! multiply phase factor from field-free propagation
      ! --------------------------------------------------
      ! -> apply after splitting because propagation is done later
      forall (i=1:m_ioccact,j=1:m_ioccact)
        a_wfct%idmcorr(i,j) = a_wfct%idmcorr(i,j)              &
          * exp( IMG * m_split_dt *                            &
                 ( conjg(m_ioar(i)%energy)- m_ioar(j)%energy ) &
               )
      end forall
     
      if (a_wfct%store_IDMcorrL) then
        forall (i=1:m_ioccact,j=1:m_ioccact)
        a_wfct%idmcorrL(i,j,:) = a_wfct%idmcorrL(i,j,:)          &
            * exp( IMG * m_split_dt *                            &
                   ( conjg(m_ioar(i)%energy)- m_ioar(j)%energy ) &
                 )
        end forall
      end if

    end subroutine split_corr
  end subroutine splitwfct



  function calc_idm( a_wfct ) Result(idm)
    !
    ! calculate uncorrected IDM from current CIS wfct
    !  -> see Eq. 17 in Greenman, PRA 82, 023406 (2011)
    !
    !**INPUT
    !  -----
    !   a_wfct : TYPE(T_WFCT) - IN
    !            CIS wfct
    use varsystem,only : m_SOcoupl,m_nmax,m_lmax
    use varwfct,only   : m_idphr=>m_id1p1hclassrev,m_i1p1hclass,m_ioccact  &
                        ,t_wfct
    use varmatrix,only : m_olap
    implicit none

    type(t_wfct), intent(in) :: a_wfct

    complex(dblprec) :: idm(m_ioccact,m_ioccact)
    integer :: i,j,l,n,ioa,joa
    complex(dblprec) :: element


    idm = ZERO

    do i=1,m_i1p1hclass
      do j=i,m_i1p1hclass
        ! m^L_a = m^L_b(with/without SO coupling)
        if (m_idphr(i)%ml/=m_idphr(j)%ml) cycle
        ! m^S_a =m ^S_a (with SO coupling)
        if (m_SOcoupl .and. m_idphr(i)%ms/=m_idphr(j)%ms) cycle

        ! occupied orbitals ID
        ioa = m_idphr(i)%idhole
        joa = m_idphr(j)%idhole
        if ( max(ioa,joa)>m_ioccact )  stop "calc_idm: occupied ID out of range"

        ! calc IDM element
        element = ZERO
        !$omp parallel do private(l,n) default(shared) &
        !$omp collapse(2) schedule(static) reduction(+:element)
        do l=0,m_lmax
          do n=0,m_nmax
            element = element +   a_wfct%alpha_ai(n,l,i) *   &
                      dot_product(a_wfct%alpha_ai(:,l,j),m_olap(:,n,l))
          end do
        end do
        !$omp end parallel do
        idm(ioa,joa) = idm(ioa,joa) + element

        ! IDM is hermitian
        if(ioa/=joa) idm(joa,ioa) = conjg(idm(ioa,joa))
      end do
    end do

  end function calc_idm


  ! ---------------------------------------------


  subroutine CalcWriteExptVal(a_wfct)
    ! calculate and print expectation values of 
    !  1. dipole moment z
    !  2. dipole acceleration z_acc=Z/r^3*z
    ! and print the reduced ion density matrix rhoidm
    !
    use varsystem,only : m_ev,m_fIDev,m_LMgauge,m_lgauge,m_vgauge    &
                        ,m_afield,m_efield,m_fieldtype               &
                        ,m_fIDcorr,m_corrfct,m_tsurf
    use varmatrix,only : m_dipherm,m_dipij,m_dipacc,m_momherm
    use varwfct, only  : t_wfct,m_wfctref
    use pulse, only    : getpulse
    use write, only    : store_IDMs
    use SplineInterpol,only : diff1
    implicit none


    type(t_wfct) :: a_wfct
    real(dblprec) :: ev,efield,afield


    ! ----------------------
    !         IDM
    ! ----------------------
    ! calculate & print rho^IDM_ij(t) - reduced density matrix
    ! -> NOTE: update IDM (with getidm) before expectation values
    !          are calculated, since some of them need IDM (e.g. z_ion)
    call store_IDMs( a_wfct , a_wfct%time )



    ! ----------------------
    !   expectation values
    ! ----------------------
    ! -> append each element in the line
    efield = getpulse(a_wfct%time,m_efield)
    afield = getpulse(a_wfct%time,m_afield)

    
    ! time
    write(m_fIDev,'(ES17.7E3)',advance="no") a_wfct%time


    ! dipole moments
    if_dipole : if ( m_ev(1) .or. m_ev(2) ) then
       ev = ZERO
      
       if (.not.allocated(m_dipherm)) &
            stop "error (calcWriteExptVal): hermitian dipole matrix is not &
                  &allocated!!!"

       ! calculate <z>(t) - dipole moment
       if ( m_ev(1) ) then
         ev = calc_ev(a_wfct,m_dipherm)
         if (abs(ev) < 1D-99 ) ev = ZERO  ! avoid too tiny numbers

         write(m_fIDev,'(ES17.7E3)',advance="no") ev
       end if

       ! dipole moment of ion
       if ( m_ev(2) ) then
         ev = calc_ev_ion(a_wfct,m_dipij)
         if (abs(ev) < 1D-99 ) ev = ZERO  ! avoid too tiny numbers
         write(m_fIDev,'(ES17.7E3)',advance="no") ev
       end if
    end if if_dipole 


    ! dipole velocity / momentum
    if_momentum : if ( m_ev(3) ) then
      ev = ZERO

       if (.not.allocated(m_momherm)) &
            stop "error (calcWriteExptVal): hermitian dipole matrix is not &
                  &allocated!!!"

      if ( m_LMgauge == m_lgauge ) then
        ev = calc_ev(a_wfct,m_momherm)
      elseif ( m_LMgauge == m_vgauge ) then
        ev = calc_ev(a_wfct,m_momherm,-afield)
      end if

      if (abs(ev) < 1D-99 ) ev = ZERO  ! avoid too tiny numbers
      write(m_fIDev,'(ES17.7E3)',advance="no") ev
    end if if_momentum


    ! dipole acceleration
    if_acceleration : if ( m_ev(4) ) then
      ev = ZERO

      if (.not.allocated(m_dipacc)) &
           stop "error (calcWriteExptVal): hermitian dipole matrix is not &
                 &allocated!!!"

      ev = calc_ev(a_wfct,m_dipacc,-efield)
      ! no shift by the driving field 
      ! -> prevent large shift in HHG spectrum yield
      !ev = calc_ev(a_wfct,m_dipacc)  
      if (abs(ev) < 1D-99 ) ev = ZERO  ! avoid too tiny numbers
      write(m_fIDev,'(ES17.7E3)',advance="no") ev
    end if if_acceleration

    ! pulse
    ! -----
    write(m_fIDev,'(3ES17.7E3)',advance="yes") efield,afield

    ! correlation function
    ! ---------------------
    if ( m_corrfct ) then
      !write(m_fIDcorr,'(10ES17.7E3)') a_wfct%time,get_overlap(m_wfctref,a_wfct,"herm")
      write(m_fIDcorr,'(10ES17.7E3)') a_wfct%time,get_overlap(m_wfctref,a_wfct)
    end if


    ! t-surf
    ! ---------
    if ( m_tsurf )   call tsurf_write(a_wfct)
  end subroutine CalcWriteExptVal



  subroutine tsurf_write(a_wfct)
    use varsystem, only : m_lmax,m_nmaxl,m_tsurf_r,m_grid  &
                         ,m_fIDtsurf,m_ftsurf
    use varwfct, only : m_orbital,m_i1p1hclass
    use varmatrix, only : m_tsurf_mat
    use orbital, only : get_orbital_radial
    implicit none
    type(t_wfct), intent(in) :: a_wfct

    complex(dblprec) :: wfct(2,0:m_lmax)
    integer :: l,n,i,ir
    character(clen) :: ffile
    logical :: ex



    ! location of t-surface sphere
    ir = minloc(abs(m_grid%r-m_tsurf_r),dim=1)


    do i=1,m_i1p1hclass

      ! every channel is written in an own file
      write(ffile,*) i
      ffile = trim(m_ftsurf)//"_"//trim(adjustl(ffile))

      inquire(file=trim(ffile),opened=ex)
      if ( .not.ex ) open(m_fIDtsurf+i,file=trim(ffile))

      wfct = ZERO
      do l=0,m_lmax
        do n=0,m_nmaxl(l)
          ! value of wfct at m_tsurf_r
          wfct(1,l) = wfct(1,l) + a_wfct%alpha_ai(n,l,i)   &
                      * get_orbital_radial(m_orbital(n,l),ir)
        end do
      end do
                       
      ! value of derivative at m_tsurf_r
      select case( m_grid%type)
      case("PSG")
        do l=0,m_lmax
        do n=0,m_nmaxl(l)
          ! -> use anti-symmetry of derivative to sum over first index
          wfct(2,l) = wfct(2,l) - get_orbital_radial(m_orbital(n,l),ir)      &
                      * sum(a_wfct%alpha_ai(:,l,i)*m_tsurf_mat(:,n,l))
        end do
        end do

      case("FEM","DVR")
        do l=0,m_lmax
        do n=0,m_nmaxl(l)
          wfct(2,l) = wfct(2,l) + a_wfct%alpha_ai(n,l,i)                     &
                                * get_orbital_radial(m_orbital(n,l),ir,.true.)
        end do
        end do
      case default
        wfct(2,:) = ZERO
      end select

      write(m_fIDtsurf+i,'(999ES17.7)')  a_wfct%time, (wfct(:,l) ,l=0,m_lmax)
    end do
  end subroutine tsurf_write




  function calc_ev( a_wfct , a_matrix , ao_scalar ) Result(ev)
    !
    ! Calculate expectation value.    !
    ! <M> = 2*Re( <HF|M|Phi^a_i> 
    !         + sum_i sum_a>b <Phi^a_i|M|Phi^b_i> 
    !         + sum_a sum_i>j <Phi^a_i|M|Phi^a_j> )
    !       + 2 <ao_scalar>  [ optional]
    !
    !  see Eq. 12 in Greenman, PRA 82, 023406 (2011)
    !
    use varsystem,only : m_nmax,m_nmaxl,m_lmax,m_SOcoupl,m_nelec
    use varwfct,only   : m_i1p1hclass,m_idphr=>m_id1p1hclassrev   &
                        ,m_ioccact,m_idoar=>m_idoccactrev         &
                        ,t_wfct
    use varmatrix,only : m_olap,m_mweight
    implicit none

    type(t_wfct), intent(in) :: a_wfct
    complex(dblprec), intent(in) :: a_matrix(0:m_nmax,0:m_nmax,0:m_lmax,0:1)
    real(dblprec), intent(in),optional :: ao_scalar
    real(dblprec) :: ev

    complex(dblprec) :: aux,vec(0:m_nmax)
    integer :: i,j,no1,lo1,no2,lo2,mjo1,mjo2,idmso1
    integer :: lv1,lv2,loid,mlv1,mlv2,nv


    ev=zero

    !$omp parallel do default(shared) private(i,j,no1,no2,lo1,lo2,loid) &
    !$omp private(mjo1,mjo2,idmso1,lv1,lv2,mlv1,mlv2,aux,nv)        & 
    !$omp schedule(dynamic) reduction(+:ev)
    lp_i:do i=1,m_i1p1hclass
      no1 = m_idphr(i)%n
      lo1 = m_idphr(i)%l
      mlv1= m_idphr(i)%ml
      mjo1   = m_idphr(i)%mj 

      ! for spin orbitals additional informations are needed in <i|m|j> 
      ! and <i|m|a>.
      if (m_SOcoupl) then
        idmso1 = (m_idphr(i)%ms+1)/2
      end if

      ! <a|M|i> term
      lp_zai:do loid=0,1
        ! loid = 0 : l>lo
        !     = 1 : l<lo
        lv1=lo1+1-2*loid
        if (lv1<0 .or. lv1>m_lmax) cycle
 
        aux = dot_product(a_wfct%alpha_ai(:,lv1,i),a_matrix(:,no1,lv1,loid))
        aux = aux * a_wfct%alpha_0 * m_mweight(max(lv1,lo1),mlv1)
 
        ! use m_g/u and spin symmetry
        ! -> each symmetry gives an additional factor of sqrt(2)
        ! -> SOcoupl=true : m_g/u and spin symmetries are coupled
        !                   (and not independent) just one time sqrt(2)
        ! -> Hydrogen atom : no symmetries (just one electron) => no factor
        ! -> odd number of electrons: assume only outermost orbital containing only
        !        one electron is active
        if ( mod(m_nelec,2) == 1 ) then
          ! do nothing
        elseif (mlv1==0 .or. m_SOcoupl) then
          aux = aux * sqrt(two)
        elseif (mlv1/=0.and..not.m_SOcoupl) then
          aux = aux * two
        end if
 
        if (m_SOcoupl) aux = aux * m_idphr(i)%cleb(idmso1)
 
        ! update expectation value
        ev = ev + two*dble(aux)  ! 2*Re(aux) <- use complex conjug. symm.
      end do lp_zai
 

      ! <a|M|b> term
      lp_zab:do lv1=0,m_lmax-1
        ! sum over l_b>l_a
        !  -> l_a > l_b gives the complex conjugated result
        !     leading to 2*Re(aux)
        !  -> l_a=l_b is not possible for dipole transition
        lv2=lv1+1

        aux = ZERO
        do nv=0,m_nmaxl(lv1)
          aux = aux + a_wfct%alpha_ai(nv,lv1,i)  * &
                dot_product(a_wfct%alpha_ai(:,lv2,i),a_matrix(:,nv,lv2,0))
        end do
        aux = aux * m_mweight(lv2,mlv1)

        ev = ev + two*dble(aux)
      end do lp_zab
      
    end do lp_i
    !$omp end parallel do


    ! <i|M|j> term
    !  -> use IDM to evaluate occupied-occupied transitions
    lp_ij:do i=1,m_ioccact
      no1 = m_idoar(i)%n
      lo1 = m_idoar(i)%l
      mlv1= m_idoar(i)%ml
      mjo1= m_idoar(i)%mj
      do j=i+1,m_ioccact
        no2 = m_idoar(j)%n
        lo2 = m_idoar(j)%l
        mlv2= m_idoar(j)%ml
        mjo2= m_idoar(j)%mj

        ! consider only dipole transition with dm=0
        if ( abs(lo2-lo1)/=1 .or. mlv1/=mlv2 )   cycle
        ! check automatically the spin condition (no effect w/o spin-orbit)
        if ( m_SOcoupl ) then
          if( mjo1/=mjo2 .or. m_idoar(j)%ms/=m_idoar(i)%ms)  cycle  
        end if
        loid=(lo2-lo1+1)/2


        ! use IDM entry
        aux = a_wfct%idm(i,j) * a_matrix(no1,no2,lo1,loid)

        ! multiply magnetic part
        if ( m_SOcoupl ) then
          aux = aux * sum(m_idoar(i)%cleb(0:1) * m_idoar(j)%cleb(0:1)  &
                          * m_mweight(max(lo1,lo2),mjo1/2+1:mjo1/2:-1) &
                         )
        else
          aux = aux * m_mweight(max(lo1,lo2),mlv1)
        end if

        ev = ev - two*dble(aux)
      end do
    end do lp_ij



    ! add scalar contribution
    if ( present(ao_scalar) .and. ao_scalar/=ZERO ) then
      ! add ground state contribution
      ev = ev + ao_scalar * a_wfct%alpha_0*conjg(a_wfct%alpha_0)

      ! add channel contributions
      do i=1,size(a_wfct%IDM,1)
        ev = ev + ao_scalar * a_wfct%IDM(i,i)
      end do 
    end if

  end function calc_ev



  function calc_ev_ion( a_wfct,a_mat_IDM) Result(ev)
    ! Calculate the expectation value in the ion subsystem.
    ! To do so use the cap-corrected ion density matrix.
    ! Matrix of the observable is a hermitian matrix.

    ! The <i|M|j> should have only real matrix elements. 
    ! The separation of m_mat and m_dipij is only made because this routine
    ! was written only for m_dip_IDM and m_dip_IDM is already a results of 
    ! angular momenta algebra that doesn't need to be done again.
    !
    use varsystem, only : m_lmax,m_nmax,m_SOcoupl
    use Varwfct,only : t_wfct,wfct_getidm
    use readmatrices, only : mat_1p1h2actocc
    implicit none

    type(t_wfct), intent(in) :: a_wfct
    complex(dblprec),intent(in) :: a_mat_IDM(:,:)

    complex(dblprec),allocatable, dimension(:,:) :: mat
    real(dblprec) :: ev


    ! convert m_mat, which is stored in the 1p1h-ID basis, into the
    ! occ. act. orbital-ID basis (only necessary for SO-coupling is active)
    ! When SOcoupling is not active 1p1h-ID=occ.act.orb.-ID
    call mat_1p1h2actocc( a_mat_IDM , mat )

    ! perform the trace over IDM and matrix of observable
    !  <i|M|j>
    ! ----------
    ! <z> = Tr( rho * z) = sum_i <i| rho* z |i> = sum_ij rho_ij z_ji
    !  -> use mat is symmetric (normally: mat must be transposed)
    ev = dble(sum( mat*wfct_getidm(a_wfct) ))

  end function calc_ev_ion




  function get_overlap(a_wfct1,a_wfct2,ao_mode) Result(olap)
    ! 
    !**DESCRIPTION
    !  -----------
    !  Build the overlap between two CIS wfcts wfct1,wfct2.
    !             (wfct1 | wfct2)   mode=symm
    !             <wfct1 | wfct2>   mode=herm
    ! 
    !**INPUT
    !  -----
    !  a_wfct1 :  TYPE(T_WFCT) - IN 
    !             "bra" state
    !  a_wfct2 :  TYPE(T_WFCT) - IN 
    !             "ket" state
    !  ao_mode :  CHARCTER(:) - IN
    !             type of inner product
    !              = "symm"  : symmetric inner product  [default]
    !              = "herm"  : hermitian inner product
    !
    use varsystem,only : m_nmax,m_lmax,m_SOcoupl,m_nelec
    use varwfct,only   : t_wfct     &
                        ,m_i1p1hclass,m_idphr=>m_id1p1hclassrev
    use varmatrix,only : m_olap
    implicit none

    type(t_wfct),intent(in) :: a_wfct1,a_wfct2
    character(*), intent(in), optional :: ao_mode
    complex(dblprec) :: olap
    character(4) :: mode

    integer :: i,n,l
    integer :: lmax,nmax,n1p1h

    mode="symm"
    if ( present(ao_mode) ) mode=adjustl(ao_mode)

    olap = zero

    if ( mode == "symm" ) then

#if OPENMP
      !$omp parallel do default(shared) &
      !$omp collapse(2) reduction(+:olap) 
      do i=1,m_i1p1hclass
      do l=0,m_lmax
        olap = olap + sum(a_wfct1%alpha_ai(:,l,i)*a_wfct2%alpha_ai(:,l,i))
      end do
      end do
      !$omp end parallel do
      olap = olap + a_wfct1%alpha_0*a_wfct2%alpha_0
#else
      olap = sum(a_wfct1%alpha_ai*a_wfct2%alpha_ai)   &
           + a_wfct1%alpha_0*a_wfct2%alpha_0
#endif

    elseif ( mode == "herm" ) then
      nmax = size(a_wfct1%alpha_ai,1)-1
      lmax = size(a_wfct1%alpha_ai,2)-1
      n1p1h= size(a_wfct1%alpha_ai,3)

      !$omp parallel do private(i,n,l) default(shared) &
      !$omp schedule(static) collapse(3) reduction(+:olap)
      do i=1,n1p1h
      do n=0,nmax
      do l=0,lmax
        olap = olap + a_wfct2%alpha_ai(n,l,i) *   &
                      dot_product(a_wfct1%alpha_ai(:,l,i),m_olap(:,n,l))
      end do
      end do
      end do      
      !$omp end parallel do

      olap = olap + conjg(a_wfct1%alpha_0)*a_wfct2%alpha_0
    else
      stop "get_overlap: invalid mode"
    end if

  end function get_overlap


  subroutine init_wavefunction(ao_backup,ao_corrfct)
    !
    !**PURPOSE
    !  --------
    !  initializing the wavefunction and density matrix arrays 
    !
    !**DESCRIPTION
    !  -----------
    !  ao_backup is a backup object that contains the wavefunction at a
    !  certain time from which the propagation continues
    !
    use varsystem,only : m_SOcoupl,m_nmax,m_nmaxl,m_lmax,m_dt,m_propmode &
         ,t_backup, m_probe, m_probe_E                                   &
         ,m_refstates_rootname,m_refstates_segment,m_refstates_mode      &
         ,m_refstates_file,m_refstates_use                               &
         ,m_storeIDMcapL,m_absorb                                        &
         ,m_LMgauge,m_vgauge
    
    use varwfct,only : m_ioccact,m_i1p1hclass,m_idphr=>m_id1p1hclassrev  &
         ,m_ephase,wfct_init,m_wfct,m_wfctref,m_wfctprev,m_wfctsplit     &
         ,m_orbital,t_wfct


    use general,only : m_isocc=>isocc
    use eom, only : action_lightfield
    use read, only : read_backup
    implicit none


    type(t_backup), optional :: ao_backup

    logical, optional :: ao_corrfct
    logical           :: corrfct,readbackup

    integer :: i,n,l
    complex(dblprec) :: ei,ea
    real(dblprec) :: dt1
    
    complex(dblprec) :: olap
    type(t_wfct), allocatable :: wfcttmp,wfcttmp2
    logical :: ex


    dt1=zero
    ei=zero
    ea=zero

    corrfct=.false.
    if ( present(ao_corrfct) ) corrfct = ao_corrfct


    ! initialize wfct
    ! ----------------------------
    ! -> initial sate is be default the HF ground state
    call wfct_init(m_wfct,m_nmax,m_lmax,m_i1p1hclass,m_ioccact,m_storeIDMcapL)
    m_wfct%alpha_0 = ONE

    ! create wfct object for splitting routine
    if (m_absorb == 'SPL') then
      call wfct_init(m_wfctsplit,m_nmax,m_lmax,m_i1p1hclass,m_ioccact,m_storeIDMcapL)
    end if


    ! read wavefunction + IDM from backup file
    ! ----------------------------------------
    readbackup = .false.
    if ( present(ao_backup) ) readbackup = ao_backup%read

    if ( readbackup ) then  ! read backup
      write(STDOUT,*) "reading backup file... ",trim(ao_backup%file_read)
      call read_backup( ao_backup%file_read , m_wfct )
    end if


    ! perform a delta-like probe/dipole step at current wavefunction
    ! --------------------------------------------------------------
    if ( m_probe .and. m_probe_E/=ZERO ) then
      allocate(wfcttmp)
      wfcttmp = m_wfct
      wfcttmp%alpha_0  = ZERO
      wfcttmp%alpha_ai = ZERO
      
      ! perform H|Psi(t)>  without Coulomb interaction
      call action_lightfield( m_probe_E         &
        , m_wfct%alpha_0, m_wfct%alpha_ai       &
        , wfcttmp%alpha_0 , wfcttmp%alpha_ai    &
        )
      
      m_wfct%alpha_0  = m_wfct%alpha_0  - IMG * wfcttmp%alpha_0
      m_wfct%alpha_ai = m_wfct%alpha_ai - IMG * wfcttmp%alpha_ai
      deallocate(wfcttmp)
    end if


    ! for correlation function
    ! create reference state :  |ref> = d |wfct> 
    !  -> reference state |ref> rather than |wfct> will be propagated  
    !  -> d = z or p_z  depending on LMgauge
    !  -> normally |wfct> = |HF>
    !     but when readbackup or m_probe is used |wfct> is different
    ! ----------------------------------------
    if ( corrfct ) then
      
      ! create empty reference state
      m_wfctref = m_wfct  ! copy structure
      m_wfctref%alpha_0  = ZERO
      m_wfctref%alpha_ai = ZERO

      ! calculate state: z |HF> 
      call action_lightfield( -ONE                &
         ,m_wfct%alpha_0   ,m_wfct%alpha_ai       & 
         ,m_wfctref%alpha_0,m_wfctref%alpha_ai    &
         )

      ! propagte the state z|wfct>
      m_wfct = m_wfctref
    end if


    ! use reference states
    ! --------------------
    if ( m_refstates_use ) then
      
      write(STDOUT,*) 
      select case (m_refstates_mode)
      case("add")
        write(STDOUT,*) " Add reference states to the initial wavefunction"
      case("sub")
        write(STDOUT,*) " Subtract reference states from the initial wavefunction"
      case("lim")
        write(STDOUT,*) " Keep only reference states in the initial wavefunction (remove the rest)"
        allocate(wfcttmp2)
        wfcttmp2 = m_wfct
        m_wfct%alpha_0  = ZERO
        m_wfct%alpha_ai = ZERO
      case("only")
        write(STDOUT,*) " Use only reference states for the initial wavefunction"
        ! use only reference states
        m_wfct%alpha_0  = ZERO
        m_wfct%alpha_ai = ZERO
      end select
      
      allocate( wfcttmp)      
      wfcttmp = m_wfct  ! to have the correct array sizes in wfcttmp

      do i=1,size(m_refstates_file)
        inquire( file=m_refstates_file(i)%txt , exist=ex )
        if ( .not.ex )  then
          write(STDOUT,*)  "  -> file ",trim(m_refstates_file(i)%txt)," doesn't exist"
          cycle  ! skip file that doesn't exist
        else
          write(STDOUT,*)
          write(STDOUT,*)  "  -> read file ",trim(m_refstates_file(i)%txt)
        end if
        call read_backup( m_refstates_file(i)%txt , wfcttmp )

        select case (m_refstates_mode)
        case("add","only")
          olap = ONE / sqrt(get_overlap(wfcttmp,wfcttmp))
          m_wfct%alpha_0  = m_wfct%alpha_0  + olap * wfcttmp%alpha_0
          m_wfct%alpha_ai = m_wfct%alpha_ai + olap * wfcttmp%alpha_ai 
        case("sub")
          olap = get_overlap(wfcttmp,m_wfct) / get_overlap(wfcttmp,wfcttmp)
          write(STDOUT,'(a,2ES14.4," ,",ES14.4)') '  overlap w/ initial state (value,abs. value) = ', olap,abs(olap)
          m_wfct%alpha_0  = m_wfct%alpha_0  - olap * wfcttmp%alpha_0
          m_wfct%alpha_ai = m_wfct%alpha_ai - olap * wfcttmp%alpha_ai           
        case("lim")
          olap = get_overlap(wfcttmp,wfcttmp2) / get_overlap(wfcttmp,wfcttmp)
          write(STDOUT,'(a,2ES14.4," ,",ES14.4)') '  overlap w/ initial state (value,abs. value) = ', olap,abs(olap)
          m_wfct%alpha_0  = m_wfct%alpha_0  + olap * wfcttmp%alpha_0
          m_wfct%alpha_ai = m_wfct%alpha_ai + olap * wfcttmp%alpha_ai           
        end select
      end do

      if ( corrfct )  m_wfctref = m_wfct
      
      deallocate(wfcttmp)
      if ( allocated(wfcttmp2) )  deallocate(wfcttmp2)
    end if


    ! needed for 2. order finite differencing
    ! ---------------------------------------
    if ( m_propmode == 11 ) then
      call wfct_init(m_wfctprev,m_nmax,m_lmax,m_i1p1hclass,m_ioccact,m_storeIDMcapL)
    end if


    ! ephase = exp(-i(e_a-e_i)dt1))
    ! ----------------------------------------
    !  -> dt1 = dt/2 (propmode=1,2)
    !  -> if other propagation schemes are implemented 
    !     dt1 can be set accordingly
    !  -> not needed for Lanczos propagation
    allocate(m_ephase(0:m_nmax,0:m_lmax,m_i1p1hclass))
    m_ephase = ZERO

    select case (m_propmode)
    case(0,3)
       dt1 = m_dt
    case(1,11,2)
       dt1 = m_dt/2
    case default
       ! termination by wrong propmode ID is done in subroutine propagation
       stop " propagation: propagation method ID unknown"
    end select

    if (m_propmode/=3) then
      do i=1,m_i1p1hclass
        ei = dconjg(m_idphr(i)%energy)  ! neg. imag. energy leads to a decay
        do l=0,m_lmax
          do n=0,m_nmaxl(l)
            ea=m_orbital(n,l)%energy
            if (m_isocc(n,l)) cycle
            
            m_ephase(n,l,i) = exp(-IMG*dt1*(ea-ei))
          end do
        end do 
      end do
    end if
 
  end subroutine init_wavefunction


end module TDCIS
