module CoulombMatrix
  !
  ! ------------------------------------------------------------------
  ! |  This module contains routines related to the Coulomb matrix   |
  ! |  generation and storage.                                       |
  ! |  Only the main routine "coulomb" should be externally called.  |
  ! |  All other routines are auxiliary routines.                    |
  ! ------------------------------------------------------------------
  !
  ! This module contains the following functions (fct) and 
  ! subroutines (sbr) :
  !
  !  -> (sbr) coulomb  : main routine to be called to generate
  !                      coulomb matrix elements
  !                      as stated in Eq. 6b in
  !                      Greenman, PRA 82, 023406 (2011).
  !                      An explicit compact expression of the general
  !                      matrix element is not given.
  !
  !  -> (sbr) writeout2p2h : 
  !                      Assemble radial and angular part and store
  !                      it in a file. And in some cases it is stored
  !                      in the publically accessable module array
  !                      "m_coumat".
  !  -> (sbr) angular2p2h :
  !                      Calculate the angular part of the coulumb 
  !                      matrix and store it in a private module array 
  !                      "m_angmom".
  !  -> (sbr) radial2p2h  :
  !                      Calculate the radial part of the coulomb
  !                      matrix and store it in a private module array
  !                      "m_rmat".
  !  -> (sbr) fem_reduce_coulomb :
  !                      Calculate one of the 2 radial integrals
  !  -> (sbr) fem_2dint_coulomb :
  !                      Calculate both radial Coulomb integrals
  !  -> (sbr) genrmima : Generate the matrix r_<^l/r_>^(l+1) and store it
  !                      in the private module array "m_rmima".
  !
  ! AUTHOR INFORMTION
  ! -----------------
  !  (c) written by Stefan Pabst in 2010
  !
  ! Version info: $Id: CoulombMatrix.f90 1711 2015-05-22 18:44:19Z spabst $
  ! ----------------------------------------------------------------------
  !
  use constants
  implicit none

  complex(dblprec),private,allocatable,target :: m_rmat(:,:,:,:,:)
  real(dblprec),private,allocatable,target :: m_angmom(:,:,:,:)
  complex(dblprec),private,allocatable :: m_rmima(:,:,:)  
  
  ! coumat :
  !  -> store all Coulomb matrix elements in an array
  !  -> only when all orbx are occupied orbitals (=2,3)
  !    -> used in Hartree-Fock program to calculate HF energy
  !    -> only v_ijij (direct) and v_ijji (exchange) are needed
  !  -> index structure: coumat(ID_i,ID_j,1/2)
  !    -> ID: occupied orbital ID for (n,l,m) , m=-L,...,+L
  !    -> 1 : v_ijij (direct)
  !    -> 2 : v_ijji (exchange)
  complex(dblprec),allocatable :: m_coumat(:,:,:) 

contains

  subroutine coulomb(a_fid,a_orb1,a_orb2,a_orb3,a_orb4   &
       ,ao_condi1,ao_condi2,ao_condi3)
    ! Purpose:
    ! --------
    ! calculation of V_c(orb1,orb2,orb3,orb4)=(orb1,orb2|1/r12|orb3,orb4)
    !  -> Coulomb interaction connects : orb1<->orb3 (r1) and orb2<->orb4 (r2)

    ! Input:
    ! ------
    ! fid (required)
    !  - is the output id of the file
    !  - fid=-1 : store matrix elements v_ijij,v_ijji in m_coumat
    !             -> applies only when all orbital types (orbx) refer to 
    !                occupied orbitals
    !             -> otherwise, do nothing
    !
    ! orbx (required)
    !  - defines which type of orbitals are used for position x
    !   = -1: active virtual + all occupied orbitals
    !   = 0 : all active orbitals
    !   = 1 : active virtual orbitals
    !   = 2 : all occupied orbitals
    !   = 3 : active occupied orbitals
    !
    ! condix (optional)
    !  - additional conditions on m values (maximal 3 conditions)
    !   =  0 : no conditions
    !   = pq : m_p=m_q  (pq = 10*p+q, p/=q, 0<p<q<5)
    !   else : program stops
        

    ! Output:
    ! -------
    ! - no value will be returned 
    !   all non-zero matrix elements are written in a given file
    ! - exception fid=-1 and all orbital indices refer to occupied orbitals
    !    -> then matrix elements are stored in m_coumat


    ! Symmetries (if enabled):
    ! -----------------------
    ! symmetries that can be used for matrix elements appearing wihtin CIS
    ! configuration space (i.e. [m_a=m_c,m_b=m_d] or [m_a=m_d,m_b=m_c])
    !
    ! symmetries used: abcd = cdab = dcba ( = cbad)
    ! orb1/2/3/4 -> type(a/b/c/d)

    !
    ! symmetries are only used if orbital types (orbx) of the orbitals 
    ! that switched positions are the same
    !  -> e.g.: type(a) = type(c) or/and type(b)=type(d)
    !
    ! symm. are used such that the most left index 
    ! of the orbital doublet has
    ! >= angular momentum l , for equal l then >= radial number n
    ! , for equal l and  n then >= qunatum number m
    !  -> example type(a) = type(c): 
    !        l_a>=lc
    !        if l_a=l_c then n_a>=n_c 
    !        if l_a=l_c and n_a=n_c then m_a>=m_c
    !
    ! remark on symm.no.1:
    !    usually used for the exchange Coulomb term    
    !    require: type(a) = type(c) and type(b)=type(d)
    !    enforce: (a,b)>=(c,d) w/ symmetry v_abcd = v_cdab
    !   	
    ! remark on symm.no.2:
    !    usually used for the direct Coulomb term
    !    require: type(a) = type(d) and type(b)=type(c)
    !    enforce: (a,b)>=(c,d) w/ symmetry v_abcd = v_dcba
    !
    ! remark on symm.no.3:  (NOT USED)
    !    require: symm.no.1 and all orbital m_b=m_d (=> m_a=m_c) are the same
    !    enforce: b>=d w/ symmetry v_abcd = v_adcb 
    !
    
    ! SO-splitting (SOcoupl=true)
    ! -> when SO splitting in the occupied orbital is enabled
    !    then it is possible for the exchange part (v_ajbi) that 
    !    m_a/=m_i and m_b/=m_j , however, |m_a-m_i|,|m_b-m_j| <= 1
    ! -> in the writting routine this restricted m range will be employed
    !    s.t. not a lot unnecessary elements are written out

    use varsystem,only : m_lmax,m_lsum,m_lprx,m_grid,m_useSymm,m_useMsymm
    use varwfct,only : m_lomax,m_ioccall
    use general,only : findmrange,findnrange
    implicit none
    
    integer,intent(in) :: a_fid,a_orb1,a_orb2,a_orb3,a_orb4
    integer,intent(in),optional :: ao_condi1,ao_condi2,ao_condi3

    integer :: orb(4),condiv(3),condim(3,2)
    integer,dimension(4) :: l,in,nmin,nmax,im,mmin,mmax,lma
    integer :: l1,l2,l3,l4
    integer :: lcmin,lcmax,ilc
    integer :: i
    logical :: calc,symm(3)
    integer :: excdiag_l


    orb(1)=a_orb1
    orb(2)=a_orb2
    orb(3)=a_orb3
    orb(4)=a_orb4
    
    ! check input correctness
    do i=1,4 
       select case (orb(i))
       case (-1,0,1)
          lma(i)=m_lmax
       case(2,3)
          lma(i)=m_lomax
       case default
          stop "coulomb: orb specifier is invalid"
       end select       
    end do


    ! symmetry can be only used if the corresponding entries 
    ! are of the same type and symmetry use is enabled
    !  -> symm.no.3 is later (in writing routine)
    symm=.false. 
    if (m_useSymm) then
       if (orb(1)==orb(3).and.orb(2)==orb(4)) symm(1)=.true.
       if (orb(1)==orb(4).and.orb(2)==orb(3)) symm(2)=.true.
    end if

    ! when calculating terms for HF energy don't use any symmetry
    if ( a_fid==-1 ) then
      symm=0
      ! DON'T use m symmetry when matrix elements are stored in
      ! m_coumat for calculating HF energy (a_fid=-1)
      m_useMsymm = .false.
    end if


    ! convert m-condition input in the condim field
    ! condiv is an auxaliary field
    condiv=0
    if (present(ao_condi1)) condiv(1)=ao_condi1
    if (present(ao_condi2)) condiv(2)=ao_condi2
    if (present(ao_condi3)) condiv(3)=ao_condi3

    condim=1
    do i=1,3
       if (condiv(i)==0) cycle
       if (condiv(i)<10 .or. condiv(i)>100) &
            stop "coulomb: condi input is invalid"

       ! force later : condim(i,1)=condim(i,2)
       condim(i,1)=condiv(i)/10       ! 1. m value in constraint i
       condim(i,2)=mod(condiv(i),10)  ! 2. m value in constraint i
    end do
    if (any(condim>4)) stop "coulomb: condi input is out of range"

    ! generatee (r</r>)^l/r>
    if (.not.allocated(m_rmima) .and. m_grid%type/="FEM") then
       if (a_fid/=-1) print *, "generating r_</r_> matrix..."
       allocate(m_rmima(m_grid%nbasis,m_grid%nbasis,0:m_lsum))
       m_rmima = ZERO
       call genrmima
    end if

    ! allocate array m_coumat
    if (a_fid==-1.and.(all(orb==2).or.all(orb==3))) then
       if (allocated(m_coumat)) deallocate(m_coumat)
       allocate(m_coumat(m_ioccall,m_ioccall,2))
       m_coumat = ZERO
    end if


    ! Coulomb approximation : 1/||r_1-r_2|| -> 1/||r|| when l>lprx
    !   -> only one hole state is necessary for v_ajbi when l>lprx
    !      - for each l1,l3 combination only one l2,l4 comb. needed
    excdiag_l = -1  ! first possible l(2)=l_j 

    ! loop over all angular momenta
    lo1:do l1=0,lma(1)
      lo2:do l2=0,lma(2)
        lo3:do l3=0,lma(3)
          lo4:do l4=0,lma(4)
           
            ! enforce quant. no. l is larger in (a,j) pair than in (b,i) pair
            if (symm(1) .and. l3>l1 )  cycle  ! v_ajbi
            if (symm(2) .and. l4>l1 )  cycle  ! v_ajib

            ! check parity is not violated    
            if (mod(l1+l2+l3+l4,2)==1) cycle
            calc=.true.

            l(1)=l1
            l(2)=l2
            l(3)=l3
            l(4)=l4

            ! find lcoulomb (no approximations)
            ! ------------------------
            lcmin=max(abs(l3-l1),abs(l4-l2))
            lcmax=min(l3+l1,l4+l2,m_lsum)  ! limit lcmax to lsum
            ! impose Coulomb approximation 
            ! (note: lsum can be larger than lprx)
            if (any(l>m_lprx)) then
              lcmax=0
            end if

            if (lcmax<lcmin) calc=.false.
            ilc=(lcmax-lcmin)/2 + 1

            
            ! find m range
            ! ------------
            call findmrange(4,orb,l,mmin,mmax)
            im=mmax-mmin+1
            if (minval(im)<1) calc=.false.

            ! find n range
            ! ------------
            call findnrange(4,orb,l,nmin,nmax)
            in=nmax-nmin+1
            if (minval(in)<1) calc=.false.

            ! hole independent Coulomb when l>lprx
            ! ------------------------------------
            ! use only first hole state i when l>lprx for long-range 
            ! Coulomb 
            ! v_ajbi <- potential becomes ionic state independent 
            !           (as it is used in tdcis).
            !  -> don't move this part before n,m range is found since 
            !     it is important that this l combination has an active 
            !     hole state
            if ( calc .and. any(l>m_lprx) ) then
              if ( excdiag_l==-1 ) then
                ! remember first possible l of act.occ.orb
                excdiag_l = l(2) 
              elseif ( excdiag_l/=l(2) )  then
                calc=.false.
              end if
            end if


            ! check if calculation should be skipped
            if (.not.calc) cycle

            ! info output
            if (a_fid/=-1) write(6,'(3X,a,4i5)') &
                "Coulomb matrix (l1,l2,l3,l4):",l

            ! start calculating entries
            allocate(m_angmom(ilc,im(1),im(2),im(3)))
            allocate(m_rmat(ilc,in(1),in(2),in(3),in(4)))
            m_angmom=ZERO
            m_rmat=ZERO
            
            ! perform angular integration/ summation over angular momenta
            call angular2p2h(lcmin,lcmax,l,mmin,mmax,condim)
            ! perform radial integration
            ! -> see Eq. A7 in Greenman, PRA 82, 023406 (2011)
            call radial2p2h(lcmin,lcmax,l,nmin,nmax,symm) 
            ! write matrix elements into a file
            call writeout2p2h &
                 (a_fid,l,nmin,nmax,mmin,mmax,orb,symm,condim)
                            
            !clear arrays
            deallocate(m_rmat,m_angmom)
                 
          end do lo4
        end do lo3
                
      end do lo2
    end do lo1

    if (allocated(m_rmima)) deallocate(m_rmima)
    return
    
  end subroutine coulomb
    

  subroutine writeout2p2h(a_fid,a_l,a_nmin,a_nmax,a_mmin,a_mmax,a_orb &
       ,a_symm,ao_mcondi)
    !
    ! write matrix elements out
    ! check : use of symmetries, indexing method
    !
    use varsystem,only : m_useMsymm,m_llimit2p,m_SOcoupl,m_lprx   &
                        ,m_matrix_format
    use general,only   : m_isocc=>isocc,m_isact=>isact,m_idocc=>idocc
    implicit none
    
    integer,intent(in) :: a_fid
    integer,intent(in),dimension(4) :: a_nmin,a_nmax,a_mmin,a_mmax,a_orb
    integer,intent(in),target :: a_l(4)
    integer,intent(in),optional :: ao_mcondi(3,2)
    logical,intent(in) :: a_symm(3)

    integer :: m1,m2,m3,m4,n1,n2,n3,n4
    integer :: m(4),n(4),nid(4),mid(3)
    integer :: i,nlmid(4)
    integer :: ib,ii   ! used to enforce (a,j)>=(b,i) pair

    complex(dblprec) :: matele

    character(LEN=clen) :: form
    logical :: condi,skip,symm(3)

    ! coulomb approximation: l>lprx
    logical :: excdiag_m
    integer :: excdiag_n 

    form="(4i7,2ES15.7)"
    symm=a_symm
    

    condi=.false.
    if (present(ao_mcondi).and.any(ao_mcondi(:,1)-ao_mcondi(:,2)/=0)) &
         condi=.true.
            
    ! Coulomb approximation : 1/||r_1-r_2|| -> 1/||r|| when l>lprx
    !   -> only one hole state is necessary for v_ajbi when l>lprx
    excdiag_n = -1  ! n quantum no. of 1. active occ. orbital
    
    n_first:do n1=a_nmin(1),a_nmax(1)
      nid(1)=n1-a_nmin(1)+1
      n(1)=n1
      do n2=a_nmin(2),a_nmax(2)
        nid(2)=n2-a_nmin(2)+1
        n(2)=n2
        do n3=a_nmin(3),a_nmax(3)
          nid(3)=n3-a_nmin(3)+1
          n(3)=n3
          n_last:do n4=a_nmin(4),a_nmax(4)
            nid(4)=n4-a_nmin(4)+1
            n(4)=n4


            ! for diag. Coulomb only one m-combi neccessary
            ! since potential 1/r_> is m-independent
            excdiag_m=.false.  

            m_first:do m1=a_mmin(1),a_mmax(1)
              mid(1)=m1-a_mmin(1)+1
              m(1)=m1
              do m2=a_mmin(2),a_mmax(2)
                mid(2)=m2-a_mmin(2)+1
                m(2)=m2
                m_last:do m3=a_mmin(3),a_mmax(3)
                  mid(3)=m3-a_mmin(3)+1
                  m(3)=m3
                  m4=m1+m2-m3
                  m(4)=m4
                  if (m4<a_mmin(4) .or. m4>a_mmax(4)) cycle
          

                  ! enforce m constrains
                  ! --------------------
                  ! -> should be already done in the soubroutine angu
                  !    and corresponding matrix elements are set 0
                  ! -> to skip calculation and increase speed
                  !    they are enforced here again
                  if (condi) then                          
                    if ( m(ao_mcondi(1,1))/=m(ao_mcondi(1,2)) ) cycle
                    if ( m(ao_mcondi(2,1))/=m(ao_mcondi(2,2)) ) cycle
                    if ( m(ao_mcondi(3,1))/=m(ao_mcondi(3,2)) ) cycle
                  end if

                  ! check M symmetries should be used 
                  ! ----------------------------------
                  ! whether matrix elements with negative M should be 
                  ! stored
                  !   -> m_1 >= 0 always possible
                  !   -> m_2 >= 0 only possible when m_1=m_3 or m1=0
                  !   -> m_3 >= 0 only when m_1=m_2=0 (only applies w/ spin-orbit)
                  if_m:if ( m_useMsymm ) then
                    if (m1<0) cycle
                    if (m1==m3 .and. m2<0) cycle
                    if (m1==0) then
                      if (m2<0) cycle
                      if (m2==0 .and. m3<0) cycle
                    end if
                  end if if_m


                  ! if only active orbtials should be calculated
                  ! skip passive occupied orbital
                  skip=.false.
                  do i=1,4
                    if ( a_orb(i)==0.or.a_orb(i)==3 ) then
                      if ( m_isocc(n(i),a_l(i))           &
                           .and.                          &
                           .not.m_isact(n(i),a_l(i),m(i)) &
                         ) skip=.true.
                    end if
                  end do
                  if (skip) cycle
                  

                  ! enforce index symmetries 
                  ! ------------------------
                  ! do not check symm.no.3 anymore: 
                  !  -> hard to unfold again
                  !!symm(3)=.false.
                  !!if (symm(1) .and. m2==m4) symm(3)=.true.
                  !!! -> restrict l 
                  !!if (symm(3).and.a_l(4)>a_l(2)) cycle
                  

                  ! enforce (a,j) pair >= (b,i) pair
                  ! --------------------------------
                  ! -> first force a>=b, if a==b force j>=i
                  ! -> large means the following quant. no. are larger
                  !    in the given order
                  !      1. l  (already done in sbr. coulomb)
                  !      2. n
                  !      3. m
                  ! -> this restriction can be exploited for symm(1)
                  !    and symm(2)
                  if_ab:if ( any(symm(1:2)) ) then
                    ! find b index
                    if ( symm(1) ) then
                      ib=3
                      ii=4
                    elseif ( symm(2) ) then
                      ib=4
                      ii=3
                    end if

                    ! if b==a or -b=a then enforce j>=i
                    ! -> transformation m->-m  can lead to v_ajbi=v_aibj
                    !    when a=-b
                    ! -> if li=lj only n criterion must be enforced
                    !    mi=mj is automatically fulfilled, since ma=mb (ma+mj=mb+mi)
                    if ( all((/n(ib),a_l(ib),abs(m(ib))/)==(/n(1),a_l(1),m(1)/)) ) then
                      if ( a_l(ii)==a_l(2) .and. n(ii)>n(2) ) cycle
                      if ( a_l(ii)>a_l(2) ) cycle  ! should be already enforced (just make sure)
                    
                    ! first enforce a>=b,-b
                    elseif ( a_l(ib)==a_l(1) ) then
                      if ( n(ib)>n(1) ) cycle
                      if ( n(ib)==n(1) .and. abs(m(ib))>m(1) ) cycle

                    elseif ( a_l(ib)>a_l(1) ) then
                      cycle  ! should be already enforced (just make sure)
                    end if
                  end if if_ab

                  
                  ! SO-splitting (SOcoupl=true) and exchange part
                  ! ---------------------------------------------
                  ! -> when SO splitting in the occupied orbital 
                  !    is enabled then it is possible for the 
                  !    exchange part (v_ajbi) that 
                  !    m_a/=m_i and m_b/=m_j 
                  ! -> however: |m_a-m_i|,|m_b-m_j| <= 1
                  !    restrict m values to this difference
                  ! -> DO NOT apply this, when all orbitals are 
                  !    occupied orbitals
                  if ( m_SOcoupl .and. .not.all(m_isocc(n,a_l))          &
                       .and. a_orb(1)==a_orb(3) .and. a_orb(2)==a_orb(4) &
                     ) then
                    if ( abs(m(4)-m(1))>1 .or. abs(m(3)-m(2))>1 )  cycle
                  end if


                  ! enforce hole-independent potential potential
                  ! --------------------------------------------
                  !  - when any l > m_lprx  (affects only v_ajbi)
                  !      potential becomes 1/r_> and spherically symmetric 
                  !      meaning no m dependence
                  !  - make sure j=i :  l is done but n, m quantum numbers are left
                  !  - only one hole state i is needed
                  !
                  if ( any(a_l>m_lprx) ) then
                    ! enforce same hole (l is already checked)
                    if ( n(2)/=n(4) .or. m(2)/=m(4) ) cycle  
                    if ( .not.m_isocc(n(2),a_l(2)) ) cycle
                      
                    ! only one m-combi needed
                    if ( excdiag_m )  cycle   
                    ! only one ionic hole is needed
                    if ( excdiag_n>=0 .and. excdiag_n/=n(2) )  cycle
                  end if


                  ! sum over lcoulomb
                  ! -----------------
                  matele=sum(m_rmat(:,nid(1),nid(2),nid(3),nid(4))  &
                       * m_angmom(:,mid(1),mid(2),mid(3)))


                  ! write matrix elements
                  ! ---------------------
                  if_small:if (abs(matele)>m_llimit2p) then    
                     excdiag_m = .true. ! one non-zero m-combi found
                     if (excdiag_n==-1) excdiag_n = n(2)   ! 1. possible n of act.occ.orb.
                    

                     if_fid:if (a_fid/=-1) then
                       
                       if ( trim(m_matrix_format)=="bin") then
                         write(a_fid) (n(i),a_l(i),m(i),i=1,4)    &
                                     ,dble(matele),dimag(matele)
                       else
                         write(a_fid,"(4(i6,i4,i5),2ES16.8)")  &
                              (n(i),a_l(i),m(i),i=1,4)         &
                              ,dble(matele),dimag(matele)
                       endif

                     else !----------------
                                                   
                       do i=1,4
                          ! mode=1 : passive+active occupied orbital
                          ! orbital index is (n,l,m)-triplet
                          nlmid(i)=m_idocc(1,n(i),a_l(i),m(i))
                       end do

                       ! direct term
                       ijij:if (nlmid(1)==nlmid(3) .and.  &
                                nlmid(2)==nlmid(4)) then  
                          m_coumat(nlmid(1),nlmid(2),1) = matele
                          m_coumat(nlmid(2),nlmid(1),1) = matele
                       end if ijij
                       
                       ! exchange term
                       ijji:if (nlmid(1)==nlmid(4) .and.  &
                                nlmid(2)==nlmid(3)) then
                         
                         m_coumat(nlmid(1),nlmid(3),2) = matele
                         m_coumat(nlmid(3),nlmid(1),2) = matele
                        
                       ! this special symmetry does not need to be used 
                       !else if (nlmid(1)==nlmid(2) .and.  &
                       !         nlmid(3)==nlmid(4)) then
                       !
                       !  m_coumat(nlmid(1),nlmid(3),2) = matele
                       !  m_coumat(nlmid(3),nlmid(1),2) = matele
                       !  
                       end if ijji
                     end if if_fid

                  end if if_small

                end do m_last
              end do
            end do m_first
          end do n_last
        end do 
      end do
    end do n_first

  end subroutine writeout2p2h



  subroutine angular2p2h(a_lcmin,a_lcmax,a_l,a_mmin,a_mmax,ao_condition)
    !
    ! Perform angular integration analytically with the help of
    ! Clebsch-Gordan coefficients
    !
    use angmom,only : m_cleb=>cleb
    implicit none
    
    integer,intent(in) :: a_lcmin,a_lcmax
    integer,intent(in),target :: a_l(4),a_mmin(4),a_mmax(4)
    integer,intent(in),optional :: ao_condition(3,2)

    integer,pointer :: l1,l2,l3,l4
    integer :: lc,lid,vorz
    integer :: m(4),m1,m2,m3,m4, m1id,m2id,m3id,m4id
    real(dblprec) :: cgc
    
    l1=>a_l(1)
    l2=>a_l(2)
    l3=>a_l(3)
    l4=>a_l(4)
    
    if (.not.allocated(m_angmom)) stop "angular: array angmom not allocated"
    m_angmom=ZERO
    
    if (mod(l1+l2+l3+l4,2)==1) return
        
    
    do lc=a_lcmin,a_lcmax,2
       lid=(lc-a_lcmin)/2 + 1       

       cgc=m_cleb(2*l3,0,2*lc,0,2*l1,0) * m_cleb(2*l4,0,2*lc,0,2*l2,0)

       do m1=a_mmin(1),a_mmax(1)
          m1id=m1-a_mmin(1)+1
          m(1)=m1
          do m2=a_mmin(2),a_mmax(2)
             m2id=m2-a_mmin(2)+1
             m(2)=m2
             do m3=a_mmin(3),a_mmax(3)
                m3id=m3-a_mmin(3)+1
                m(3)=m3

                m4=m1+m2-m3
                m4id=m4-a_mmin(4)+1
                m(4)=m4
                
                if (m4<a_mmin(4) .or. m4>a_mmax(4)) cycle
                if (present(ao_condition)) then
                   if ( m(ao_condition(1,1))/=m(ao_condition(1,2)) ) cycle
                   if ( m(ao_condition(2,1))/=m(ao_condition(2,2)) ) cycle
                   if ( m(ao_condition(3,1))/=m(ao_condition(3,2)) ) cycle
                end if

                ! sign: (-1)**(m1-m3)
                vorz=1
                if (mod(m1+m3,2)/=0) vorz=-1

                m_angmom(lid,m1id,m2id,m3id) = vorz * cgc  *  &
                     m_cleb(2*l3,2*m3, 2*lc,2*(m1-m3), 2*l1,2*m1) &
                     * m_cleb(2*l4,2*m4, 2*lc,2*(m2-m4), 2*l2,2*m2)
             end do
          end do
       end do
    end do
    
    cgc=(TWO*l3+1)*(TWO*l4+1)/( (TWO*l1+1)*(TWO*l2+1) )
    cgc=sqrt(cgc)
    m_angmom= m_angmom*cgc
    return
  end subroutine angular2p2h
    

  subroutine radial2p2h(a_lcmin,a_lcmax,a_l,a_nmin,a_nmax,a_symm)
    !
    ! - Calculate the 2D radial integral
    ! - Integration pairs are (1,3) and (2,4)
    ! - Meaning orbitals refered in l(1) and l(3) have same radius r1
    !   and orbital refered in l(2) and l(4) have same radius r2
    ! - see Eq. A7 in Greenman, PRA 82, 023406 (2011)
    ! 
    !**INPUT
    !  -----
    !  lcmin, lcmax - minimum and maximum angular momenta L of the multipole
    !                 expansion of 1/|r_1-r_2| =: 1/r12
    !                 see Eq. A6 in Greenman, PRA 82, 023406 (2011)
    !
    !  l            - angular momenta l_p,l_q,l_s,l_t of the orbitals p,q,s,t 
    !                 as stated in Eq. A7 in Greenman, PRA 82, 023406 (2011)
    !
    !  nmin, nmax   - minimum and maximum radial quantum numbers for which 
    !                 the Coulomb matrix elements should be calculated
    !
    !  a_symm       - defines if certain symmetries in the coefficient should 
    !                 be exploited during the calculation
    !                 See beginning of the module for more details
    !
    use varsystem,only : m_grid
    use varwfct,only : m_orbital
    use FEM_Hamiltonian, only : coulomb_r12
    implicit none
    
    integer, intent(in) :: a_lcmin,a_lcmax
    integer,intent(in),target :: a_l(4),a_nmin(4),a_nmax(4)
    logical,intent(in) :: a_symm(3)

    complex(dblprec),pointer :: rcontri(:,:,:,:,:)

    integer :: n1,n2,n3,n4
    integer :: nid(4),pn(4)
    integer :: i,lc
    complex(dblprec),allocatable :: rrmima(:,:),orb(:,:)

    ! FEM
    integer :: im,id,fem1,ifem3
    complex(dblprec),allocatable :: auxcoul(:,:,:), coul(:,:)

    ! order of loops levels are switched
    logical :: switch


    if (.not.allocated(m_rmat))   stop " radial2p2h: m_rmat is not allocated"
    m_rmat = ZERO
   
    allocate(orb(m_grid%nbasis,4))
    orb    = ZERO

    ! find which pair (13 or 24) has less n states (in1*in3 or in2*in4)
    ! such that the amount of 2D loops to get rrmima is minimized
    n1=a_nmax(1)-a_nmin(1)+1
    n2=a_nmax(2)-a_nmin(2)+1
    n3=a_nmax(3)-a_nmin(3)+1
    n4=a_nmax(4)-a_nmin(4)+1


    ! reorder n such that integration/loops is/are most efficient
    ! reordering n results in reording symm index as well
    if (n1*n3<=n2*n4) then
      switch=.false.
 
      do i=1,4
        pn(i)=i
      end do
    else
      ! symmetries 1 and 2 have to be changed
      !   -> l(pn(1))<l(pn(3)) becomes now l(pn(2))<l(pn(4))
      !   -> l(pn(1))<l(pn(4)) becomes now l(pn(2))<l(pn(3))
      switch=.true.
 
      pn(1)=2
      pn(3)=4
      pn(2)=1
      pn(4)=3
    end if
      

    select case(m_grid%type)
    case("DVR","PSG")
      
      if (.not.allocated(m_rmima))  stop " radial2p2h: m_rmima is not allocated"
      allocate(rrmima(m_grid%nbasis,0:(a_lcmax-a_lcmin)/2))
      rrmima = ZERO
      
     
      ! r1 integration first
      r1:do n1=a_nmin(pn(1)),a_nmax(pn(1))
         nid(pn(1))= n1 - a_nmin(pn(1))+1
         orb(:,1)  = m_orbital(n1,a_l(pn(1)))%wfct
    
         do n3=a_nmin(pn(3)),a_nmax(pn(3))
            if (a_symm(1) .and. .not.switch .and. a_l(pn(3))>a_l(pn(1)) ) cycle
            
            nid(pn(3))= n3 - a_nmin(pn(3))+1
            orb(:,3)  = orb(:,1) * m_orbital(n3,a_l(pn(3)))%wfct
                      
            
            do lc=a_lcmin,a_lcmax,2
               forall(i=1:m_grid%nbasis)
                  rrmima(i,(lc-a_lcmin)/2)=sum(m_rmima(:,i,lc)*orb(:,3)) 
               end forall
            end do
            
            ! r2 integration
            r2:do n2=a_nmin(pn(2)),a_nmax(pn(2))
    
               nid(pn(2))= n2 - a_nmin(pn(2))+1
               orb(:,2)  = m_orbital(n2,a_l(pn(2)))%wfct
    
               do n4=a_nmin(pn(4)),a_nmax(pn(4))
                  if (a_symm(2).and..not.switch.and.a_l(pn(4))>a_l(pn(1)) ) cycle
                  if (a_symm(2).and.     switch.and.a_l(pn(3))>a_l(pn(2)) ) cycle
                  if (a_symm(1).and.     switch.and.a_l(pn(4))>a_l(pn(2)) ) cycle
    
                  nid(pn(4))= n4 - a_nmin(pn(4))+1
                  orb(:,4)  = orb(:,2) * m_orbital(n4,a_l(pn(4)))%wfct
    
                  forall(lc=0:(a_lcmax-a_lcmin)/2)
                     m_rmat(lc+1,nid(1),nid(2),nid(3),nid(4)) &
                          = sum(rrmima(:,lc)*orb(:,4))
                  end forall
               end do
            end do r2
         end do
      end do r1


    case ("FEM")


      allocate(auxcoul(m_grid%nbasis,6,0:(a_lcmax-a_lcmin)/2))
      allocate(coul(m_grid%nbasis,6))

      do n1=a_nmin(pn(1)),a_nmax(pn(1))
        nid(pn(1))= n1 - a_nmin(pn(1)) + 1
        orb(:,1)  = m_orbital(n1,a_l(pn(1)))%wfct
    
        do n3=a_nmin(pn(3)),a_nmax(pn(3))
          if (a_symm(1) .and. .not.switch .and. a_l(pn(3))>a_l(pn(1)) ) cycle
          
          nid(pn(3))= n3 - a_nmin(pn(3)) + 1
          orb(:,2)  = m_orbital(n3,a_l(pn(3)))%wfct
     

          auxcoul = ZERO
          do lc=a_lcmin,a_lcmax,2
          do id=1,6
          do im=1,m_grid%nbasis - id + 1

            ! load Coulomb matrix
            coul = ZERO
            do ifem3=1,6
            do fem1=1,m_grid%nbasis - ifem3 + 1
              coul(fem1,ifem3) = coulomb_r12(fem1,im,fem1+ifem3-1,im+id-1,lc)
            end do
            end do

            ! perform one radial integral
            auxcoul(im,id,(lc-a_lcmin)/2)   &
              = fem_reduce_coulomb( orb(:,1:2) , coul )
          end do
          end do
          end do
        

          ! r2 integration
          do n2=a_nmin(pn(2)),a_nmax(pn(2))
    
            nid(pn(2))= n2 - a_nmin(pn(2))+1
            orb(:,3)  = m_orbital(n2,a_l(pn(2)))%wfct
    
            do n4=a_nmin(pn(4)),a_nmax(pn(4))
              if (a_symm(2).and..not.switch.and.a_l(pn(4))>a_l(pn(1)) ) cycle
              if (a_symm(2).and.     switch.and.a_l(pn(3))>a_l(pn(2)) ) cycle
              if (a_symm(1).and.     switch.and.a_l(pn(4))>a_l(pn(2)) ) cycle
    
              nid(pn(4))= n4 - a_nmin(pn(4))+1
              orb(:,4)  = m_orbital(n4,a_l(pn(4)))%wfct
          
              do i=0,(a_lcmax-a_lcmin)/2
                m_rmat(i+1, nid(1),nid(2),nid(3),nid(4))  &
                  = fem_reduce_coulomb( orb(:,3:4) , auxcoul(:,:,i) )
              end do

            end do
          end do 

        end do
      end do 

    end select

  end subroutine radial2p2h



  pure function fem_reduce_coulomb(a_orb,a_mat)  result(res)
    implicit none
    complex(dblprec),intent(in) :: a_orb(:,:),a_mat(:,:)
    complex(dblprec) :: res

    ! orbaux(s,d) = phi_n(s)*phi_m(s+d) + phi_n(s+d)*phi_m(s)
    ! for pair (n,m) in a_orb
    complex(dblprec), allocatable :: orbaux(:,:)
    integer :: i,n,ni

    res = ZERO
    n = size(a_orb,1)
 
    !------NEW-----
    allocate(orbaux(n,6))
    orbaux = ZERO
    
    ! d=(i1-i3)=(i2-i4)=0
    orbaux(:,1) = a_orb(:,1)*a_orb(:,2)
    do i = 2,6  ! i = d+1
      ni = n - i + 1
      orbaux(1:ni,i) = a_orb(1:ni,1)*a_orb(i:n,2) + a_orb(1:ni,2)*a_orb(i:n,1)
    end do

    res = sum( orbaux * a_mat )

  end function fem_reduce_coulomb



  subroutine genrmima
    ! generate matrices m(l): r_<**l / r_>**(l+1) for all l=0,lsum
    !
    use varsystem,only : m_grid,m_lsum
    implicit none
    
    integer :: l,i,j
    complex(dblprec) :: rminmax
    complex(dblprec), allocatable :: rgrid(:)


    ! Initialize mapping in case complex scaling is used
    ! ------------------------------------------------------
    allocate(rgrid(m_grid%nbasis))
    rgrid = ZERO

    if ( m_grid%absorbing == "CAP" .or. m_grid%absorbing == "SPL" ) then
      rgrid(:) = m_grid%r
    else if ( m_grid%absorbing == "ECS" ) then
      rgrid = m_grid%cs%F(:,0)
    end if


    do j=1,m_grid%nbasis
      do i=1,j
        rminmax = rgrid(i)/rgrid(j)
        do l=0,m_lsum
          m_rmima(i,j,l) = rminmax**l/rgrid(j)
          m_rmima(j,i,l) = m_rmima(i,j,l)
        end do
      end do
    end do
    
    !rminmax = 2.D0/(ONE*(m_grid%nbasis+1)*(m_grid%nbasis+2))
    rminmax = ONE / m_grid%norm**2 
    m_rmima = m_rmima *rminmax**2
  end subroutine genrmima

end module CoulombMatrix
