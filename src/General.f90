module general
  !
  !**PURPOSE
  !  -------
  !
  !**DESCRIPTION
  !  -----------
  !  The following subroutines (sbr) and functions (fct) are part of 
  !  this module:
  !
  !  -> (sbr) findmrange    : Find M-range for a given L
  !  -> (sbr) findnrange    : Find N-range for a given L
  !  -> (fct) isact         : Check whether the 1p-1h class is active
  !  -> (fct) isocc         : Check whether the (n.l) shell is an occupied 
  !                           shell
  !
  !  -> (sbr) assign_active_orbitals_file: 
  !                           Assign all occupied orbitals as active, 
  !                           which are stated active in the file 
  !
  !  -> (sbr) assign_active_orbitals: 
  !                           Assign all occupied orbitals as active, 
  !                           which are in the range given in the arguments
  !
  !  -> (sbr) set_active_orbital:
  !                           Convert range of orbital statement (first and last orbital)
  !                           from a string into integer numbers (quantum numbers)
  !
  !  -> (sbr) get_nlmj      : Convert information of one orbital 
  !                           from string into its qunatum numbers
  !
  !  -> (fct) calc_IDMLength: Number of IDM elements that non-zero
  !  
  !  -> (fct) idocc         : Find ID of 1p-orbitals or 1p1h configuration
  !
  !  -> (sbr) init_idoccrev : Initialize arrays with the index being the 
  !                           orbital ID. Each elements contains all the 
  !                           information of this orbital
  !
  !                           within the orbital type
  !  -> (sbr) init_associate_Legendre :
  !                            
  !
  !  -> (sbr) defaultvalue  : Set default values for globally accessable
  !                           system variabeles
  !  -> (sbr) postprocess   : Analyze input parameters and bring them in the 
  !                           final form for the program
  !  -> (sbr) printinfo     : Print all system variables (that can be 
  !                           specified in the input file)
  !  -> (sbr) printvariables: Print information about the occupied orbitals
  !                           and internally defined variables.
  ! 
  !**AUTHOR
  !  ------
  !  (c) and written by Stefan Pabst
  !
  !**VERSION
  !  -------
  !  svn info: $Id: General.f90 2176 2015-12-16 04:13:40Z spabst $
  !-----------------------------------------------------------------------
  !
  use constants, dp=>dblprec
  implicit none

contains

  subroutine findmrange(a_len,a_orb,a_l,a_mmin,a_mmax)
    ! a_orb (required)
    !  - defines which type of orbitals are used for position x
    !  - only restriction to active orbitals is of interest
    !  = -1: active virtual + all occupied orbitals
    !  = 0 : all active orbitals
    !  = 1 : active virtual orbitals
    !  = 2 : all occupied orbitals
    !  = 3 : active occupied orbitals
    !
   use varwfct,only : m_moamax,m_mmax,m_lomax
    implicit none
    
    integer,intent(in) :: a_len
    integer,intent(in),dimension(a_len) :: a_orb,a_l
    integer,dimension(a_len) :: a_mmin,a_mmax
    integer :: i
    
    do i=1,a_len

      select case( a_orb(i) ) 
      case(-1) 
        ! do not restrict to active orbitals
        a_mmax(i) = min(a_l(i), max(m_mmax,m_lomax) )
      case(0,1)
        a_mmax(i) = min(a_l(i),m_mmax)
      case(2)
        a_mmax(i) = min(a_l(i),m_lomax)
      case(3)
        a_mmax(i) = min(a_l(i),m_moamax)
      end select

      a_mmin(i) = -a_mmax(i)
    end do
  end subroutine findmrange
    
  
  subroutine findnrange(a_len,a_orb,a_l,a_nmin,a_nmax)
    ! return the range of n values for given ang. momentum l
    ! a_orb (required)
    !  - defines which type of orbitals are used for position x
    !  = -1: active virtual + all occupied orbitals
    !  = 0 : all active orbitals
    !  = 1 : active virtual orbitals
    !  = 2 : all occupied orbitals
    !  = 3 : active occupied orbitals
    !
    use varsystem,only : m_grid,m_nmaxl
    use varwfct,only : m_nomax,m_fmoact,m_nomaxl,m_noaminl,m_lomax
    implicit none

    integer,intent(in) :: a_len    
    integer,intent(in),dimension(a_len) :: a_orb,a_l
    integer,dimension(a_len) :: a_nmin,a_nmax
    integer :: i,nn
    
    a_nmin=m_grid%nbasis
    a_nmax=0
    
    do i=1,a_len
       select case (a_orb(i))
       case(-1)  ! active virtual + all occupied
          a_nmax(i)=m_nmaxl(a_l(i))
          a_nmin(i)=0
       case(0)  ! all active orbitals
          a_nmax(i)=m_nmaxl(a_l(i))
          a_nmin(i)=0
          if (a_l(i)<=m_lomax) a_nmin(i)=m_noaminl(a_l(i))
       case(1)  ! active virtual
          a_nmax(i)=m_nmaxl(a_l(i))
          a_nmin(i)=0
          if (a_l(i)<=m_lomax) a_nmin(i)=m_nomaxl(a_l(i))+1
       case(2)  ! occupied orbitals  
           a_nmin(i)=0
           if (a_l(i)>m_lomax) stop "error (findnrange): l>lomax for &
                                     &occ. orbital"
           a_nmax(i)=m_nomaxl(a_l(i))
       case(3)  ! only active occupied orbitals 
           if (a_l(i)>m_lomax) stop "error (findnrange): l>lomax for &
                                     &occ. orbital"
           a_nmax(i)=m_nomaxl(a_l(i))
          do nn=0,m_nomax
             if (any(m_fmoact(nn,a_l(i),:))) a_nmax(i)=max(a_nmax(i),nn)
             if (any(m_fmoact(nn,a_l(i),:))) a_nmin(i)=min(a_nmin(i),nn)
          end do
       end select
    end do
  end subroutine findnrange
  


  elemental function isocc(a_n,a_l)
    ! isocc = 0 - orbital class |n,l> contains virtual orbitals
    ! isocc = 1 - orbital class |n,l> contains occupied orbitals
    ! remark: our model describes only closed-shell systems
    !         -> |n,l> can't contain a mix of virtual and occupied orbitals
    !
    use varwfct, only : m_nomax,m_lomax,m_occnl
    implicit none

    integer,intent(in) :: a_n,a_l
    logical :: isocc
    
    isocc=.false.

    if (a_l>m_lomax) return
    if (a_n>m_nomax) return

    if ( any(m_occnl(:,1)==a_n .and. m_occnl(:,2)==a_l) ) isocc=.true.
    return 
  end function isocc
  

  elemental function isact(a_n,a_l,a_m,ao_j)
    !
    ! isact = true  - |nlmj> occupied orbital is active
    !         false - |nlmj> occupied orbital is frozen
    !       
    ! NOTE: If occ. orbital is active the 1p1h-class is too
    !       regrardless of the spin of the electron.
    !       Coulomb coupling can lead to m^L_a > l_i
    !       for m^J_i = j_i
    !
    ! Description
    ! -----------
    ! all inproper passing returns a false.
    ! Normally program terminates but that is not allowed 
    ! for an elemental function. 
    !
    ! Symmetries
    ! -----------
    ! impose +-M symmetry -> use always |m|
    !
    ! input
    ! -------
    ! j is not given:orbitals are |n,l,m>
    !  -> n = N, l = L , m= M_L
    ! j given => orbitals are |n,l,j,m> (Spin-Orbit-splitting)
    !  -> j = 2*J | m = 2*M_J    
    !
    use varwfct, only : m_moamax,m_fmoact_so,m_fmoact
    use varsystem,only : m_lmax,m_nmaxl,m_SOcoupl
    implicit none

    integer, intent(in) :: a_n,a_l,a_m
    integer,intent(in),optional :: ao_j
    logical :: isact
    integer :: mab,ij


    isact=.false.

    ! check l and n range
    if ( a_l<0 .or. a_l>m_lmax      ) return
    if ( a_n<0 .or. a_n>m_nmaxl(a_l)) return



    if (present(ao_j)) then
       ! SO splitting is not considered in virtual orbitals
       !  => assume orbital should be an occupied orbital
       if (.not.isocc(a_n,a_l)) return
       if (.not.allocated(m_fmoact_SO)) return

       ! check SO coupling is active
       if (.not.m_SOcoupl) then
         !stop " error (isact): SO-coupling is not active!!!" 
         return
       end if

       ! check j=l+-s , s=0.5
       if (abs(ao_j-2*a_l)/=1) then
         !stop " error (isact): J /= L +/- S , S=0.5"
         return
       end if
       ! check M range
       if (abs(a_m)>ao_j) return  ! M_J range

       mab = abs(a_m/2)      ! m index = m/2 ^= |M_J|-.5
       ij = (ao_j-2*a_l+1)/2 ! ij = 0,1 ^= L-S,L+S 

       isact=m_fmoact_SO(a_n,a_l,mab,ij)

    else ! ----------------

       ! check M range
       mab=abs(a_m)
       if (mab>min(m_moamax,a_l)) return


       if (isocc(a_n,a_l)) then ! occupied orbital
         if (.not.allocated(m_fmoact)) return
         isact = m_fmoact(a_n,a_l,mab)
       else ! virtual orbital
         isact = .true.    
       end if

    end if
  end function isact


  subroutine assign_active_orbitals_file(a_file)
    !
    !**PURPOSE
    !  -------
    !  Read active ocucpied orbital informations stored in file a_file
    !  and set corresponding orbitals active (in m_fmoact and m_fmoact_so) 
    !  and setup active-occupied-orbital related parameters.
    !
    !**DESCRIPTION
    !  -----------
    !  Loop through file and look for the key "##ORB" at the beginning of 
    !  the orbital lines.
    !  Convert orbital information into integers stored in the array nlmj
    !  (see below for format description).
    !  Close file at the end.
    !
    !**INPUT
    !  -----
    !  a_file : CHARACTER(*) - IN
    !             File name 
    ! ----------------------------------------------------------------
    !  
    use varsystem,only : m_SOcoupl
    use varwfct, only  : m_orbformat,m_orbformat_p,m_orbformat_r
    use basics, only   : getelements
    implicit none

    character(*),intent(in) :: a_file
    integer, parameter :: ID=20


    type(t_string), allocatable :: orbline(:)
    character(clen) :: txt
    integer :: stat
    
    ! nlmj format
    ! 1. index: 1 - N <- radial quantum number
    !           2 - L
    !           3 - M_L or M_J-0.5 (w/o or w/ spin-orbit)
    !           4 - -1  or J - 0.5
    ! 2. index: number of tuples
    integer :: nlmj(4)

    real :: aux1,aux2
    
    ! act : active orbitals
    ! found : checks wether any active orbitals were found
    logical :: act,found


    stat = 0
    found = .FALSE.
    
    ! read through header to find lines with orbital information
    open(id,file=a_file)


    do while (stat==0)
      
      read(id,'(a)',iostat=stat) txt
      if ( stat/=0 ) exit
      txt=adjustl(txt)
      
      
      if ( len_trim(txt)==0 ) cycle  ! empty line

      ! no comment or blank line => data section => stop reading
      
      if ( txt(1:1)/="#" ) exit         
      ! no orbital information line
      if ( txt(1:5)/="##ORB" ) cycle  ! no orbital information
     

      ! read orbital information
      ! ------------------------
      ! - only orbital information lines get it through to this point    
      act = .FALSE.
      call getelements(orbline,txt," ")

      read(orbline(2)%txt,*) nlmj(1)
      read(orbline(3)%txt,*) nlmj(2)
      read(orbline(4)%txt,*) aux1
      read(orbline(5)%txt,*) aux2
      read(orbline(8)%txt,*) act
      
      ! convert principal quant. no. in radial quant. no. (if necessary)
      if ( m_orbformat==m_orbformat_p )  nlmj(1)=nlmj(1)-nlmj(2)-1

      ! check that format is OK
      if ( mod(2*aux1,1.0)/= 0 .or. mod(2*aux2,1.0)/=0 ) then
        write(STDERR,*) " WARNING(assg_act_orb_file): orbital information&
                                                    & has invalid format"
        write(STDERR,*) "  -> line reads : ",trim(txt)
        cycle
      end if

      ! check if spin orbital setting is the same as in the program
      if ( m_SOcoupl ) then

        if ( mod(4*aux1*aux2,2.0)/=1.0 )           &
            stop " ERROR(assg_act_orb_file): spin orbitals should be used"

        nlmj(3) = int(aux2-0.5)
        nlmj(4) = int(aux1-0.5)

      else 

        if ( mod(aux2,1.0)/=0.0 .or. aux1/=0.0 )   &
            stop " ERROR(assg_act_orb_file): spin orbitals shouldn't be used"

        nlmj(3) = int(aux2)
        nlmj(4) = -1

      end if


      ! assign active orbitals
      if ( act ) then
        call set_active_orbitals(nlmj,nlmj)
        found = .TRUE.
      end if

    end do

    
    ! check wether any active orbitals were found
    if ( found ) then
      ! setup active-orbital-related parameter
      call parameter_active_orbitals
    else
      write(STDERR,*) " WARNING(assg_act_orb_file): no active orbital in &
                      &backup file '",trim(a_file),"'"
      stop
    end if

    close(id)

  end subroutine assign_active_orbitals_file



  subroutine assign_active_orbitals(a_string)
    !
    !**PURPOSE
    !  -------
    !  Decode orbital information from a string format into integer.
    !  Then set corresponding orbitals active (in m_fmoact and m_fmoact_so) 
    !  and setup active-occupied-orbital related parameters.
    !
    !**DESCRIPTION
    !  -----------
    !  Loop through the list of active orbital ranges in a_string. 
    !  The lower and upper limit of each range is stored in ces and cee,
    !  respectively.  The format of the elements "cse" and "cee" have the format
    !  NL_Ml (without Spin orbitals) or NLJ_Mj (with spin-orbitals). 
    !    N : principal quantum number        (integer: one character)
    !    L : spatial angular quantum number  (single character : S,P,D,...)
    !    Ml: spatial magnetic angular quantum number (same format as L)
    !    J : total   angular quantum number  (single digit number <- e.g. 0.5,1.5)
    !    Mj: total magnetic angular quantum number (same format as J)
    !  The format of the elements can be also found in VarWfct. 
    !
    !**INPUT
    !  -----
    !  a_string : CHARACTER(*) - IN
    !             collection of active orbitals 
    !             (For format infos see module VarWfct)
    ! ----------------------------------------------------------------
    !  
    use varwfct,only :  m_nomax,m_lomax,m_fmoact,m_fmoact_so
    use varsystem,only : m_SOcoupl
    implicit none

    character(*),intent(in) :: a_string
    character(clen) :: txt,element
    character(clen) :: ces,cee
    integer :: ipos
    
    ! nlmj format
    ! 1. index: 1 - N <- radial quantum number
    !           2 - L
    !           3 - M_L or M_J-0.5 (w/o or w/ spin-orbit)
    !           4 - -1  or J - 0.5
    ! 2. index: number of tuples
    integer :: nlmj(4,2)=0

   
    ! set up fields, which define which occupied orbital
    ! is active, when not already done in readoccupied in module read
    if ( .not. allocated(m_fmoact) ) then
      ! needed also when m_SOcoupl=.true.
      allocate(m_fmoact(0:m_nomax,0:m_lomax,-m_lomax:m_lomax))
      m_fmoact = .FALSE.
    end if

    ! only needed when m_SOcoupl=.true.
    if ( m_SOcoupl .and. .not. allocated(m_fmoact_SO) ) then
      allocate(m_fmoact_so(0:m_nomax,0:m_lomax,0:m_lomax,0:1))
      m_fmoact_SO = .FALSE.
    end if
  

    ! go through list of active occupied orbitals
    txt = trim(adjustl(a_string))
    occact:do while (len_trim(txt)/=0)
      ! load next element
      ipos = scan(txt,":")
      nextele:if (ipos==0) then
        element = trim(adjustl(txt))
        txt=" "  ! no more elements 
      else
        element = trim(adjustl(txt(1:ipos-1)))
        txt    = adjustl(txt(ipos+1:))
      end if nextele
      ! element is empty
      if ( len_trim(element) == 0 ) cycle
      
      ! create everything in orbital ranges
      !  -> if just one element: start element = end element
      ipos = scan(element,"-")
      if (ipos == 0 ) then ! just one element
        ces = element
        cee = element
      else
        ces = adjustl(element(1:ipos-1))
        cee = adjustl(element(ipos+1:))
        if ( len_trim(ces) == 0 ) ces = "1s"  ! lower bound: lowest orbital
        if ( len_trim(cee) == 0 ) cee = "end" ! upper bound: highest orbital
      end if

      nlmj = -1
      ! get orbital info converted into integers
      call get_nlmj( ces , nlmj(:,1) , "L")  ! lower bound
      call get_nlmj( cee , nlmj(:,2) , "U")  ! upper bound

      ! evaluate range of orbitals and set them active
      call set_active_orbitals( nlmj(:,1) , nlmj(:,2) )
    end do occact

    !
    ! setup active-occupied-orbital related parameter
    !
    call parameter_active_orbitals
  end subroutine assign_active_orbitals


  subroutine parameter_active_orbitals
    !
    !**PURPOSE
    !  -------
    !  Setup all parameters that are related/depend on the active occupied
    !  orbitals.
    !
    !**DESCRIPTION
    !  -----------
    !  Use arrys m_fmoact and m_fmoact_so as reference which orbitals are
    !  active.
    !
    use varwfct,only : m_nomax,m_moamax,m_mmax,m_lomax     &
                      ,m_noaminl,m_fmoact,m_fmoact_so      &
                      ,m_ioccact,m_i1p1hclass
    use varsystem,only : m_SOcoupl,m_coulombmode
    implicit none
    
    integer :: is,i,mact,n,l,ij



    if ( .not.allocated(m_fmoact))  stop &
        " ERROR(para_act_orb): 'm_fmoact' array isn't allocated"

    if ( m_SOcoupl .and. .not.allocated(m_fmoact_so))  stop &
        " ERROR(para_act_orb): 'm_fmoact_so' array isn't allocated"


    m_ioccact = 0
    m_moamax  = -(m_lomax+1) ! out of range (means unassigned)


    ! number of 1p1h class
    m_ioccact = 0
    m_i1p1hclass = 0
    if (.not.m_socoupl) then
      ! NOTE: m_fmoact also sets negative M value to true
      ! m_ioccact only counts m>=0
      do i=0,m_lomax
        m_ioccact = m_ioccact + count(m_fmoact(:,:,i))
      end do

      m_i1p1hclass = m_ioccact
    else
      ! m_fmoact has only MJ>0
      m_ioccact = count(m_fmoact_so)
      m_i1p1hclass = 2*m_ioccact

      ! remove j=l+1/2, mj=j,spin=-1/2 class (unphysical)
      !  -> would require ml>l
      do l=0,m_lomax
        m_i1p1hclass = m_i1p1hclass - count(m_fmoact_so(:,l,l,1))
      end do
    end if


    ! initialize arrays for max. and min. active occupied 
    if (allocated(m_noaminl)) deallocate(m_noaminl)
    allocate(m_noaminl(0:m_lomax))
    m_noaminl = -1  ! out of range (means unassigned)

    do l=0,m_lomax
      do n=m_nomax,0,-1 ! 
        if ( any(m_fmoact(n,l,:)) ) m_noaminl(l) = n
      end do
    end do


    ! find moamax
    do i=0,m_lomax
      if ( any(m_fmoact(:,:,i)) ) m_moamax=max(m_moamax,i)
    end do
    if ( m_moamax<0 )  stop " ERROR(para_act_orb): No active orbital"

    ! check if mmax = moamax +1
    if ( m_SOcoupl .and. m_coulombmode=="all" ) then
      ! j = l+0.5 must be active
      do l=0,m_lomax
        if ( any(m_fmoact_so(:,l,l,1)) )  then
          m_mmax = max(m_moamax,l+1)
        end if
      end do
    else
      m_mmax = m_moamax
    end if

  end subroutine parameter_active_orbitals


  subroutine set_active_orbitals( a_start , a_end )
    !
    ! assign active occupied orbitals to the globally accessable
    ! array m_fmoact and m_fmoact_so
    !
    !**INPUT
    !  -----
    !  a_start : INTEGER(4) - IN
    !            lower bound of orbital range
    !  a_end   : INTEGER(4) - IN
    !            upper bound of orbital range
    !
    !**DESCRIPTION
    !  -----------
    !  Get all orbitals that lie in the range starting with a_start and ending
    !  with a_end. Orbital energies are provided via m_orbital.
    !
    !  The format of a_start and a_end is:
    !    1. index: 1 - N <- radial quantum number (!!! read description below)
    !              2 - L
    !              3 - M_L or M_J-0.5
    !              4 - -1  or J - 0.5   
    !    2. index: number of tuples  
    !
    use varsystem,only : m_SOcoupl
    use varwfct,only : m_fmoact,m_fmoact_so,m_lomax,m_nomax  &
                      ,m_orbital,m_ioccnl,m_occnl
    implicit none

    integer,intent(in) :: a_start(4),a_end(4)

    integer :: i,m
    integer :: n(2),l(2),is(2),nn,ll,iis
    real(dp) :: en(2),energy


    n(1) = a_start(1)
    l(1) = a_start(2)
    is(1)= 1
    en(1)= dble(m_orbital(n(1),l(1))%energy)

    n(2) = a_end(1)
    l(2) = a_end(2)
    is(2)= 1
    en(2)= dble(m_orbital(n(2),l(2))%energy)

    if ( m_SOcoupl ) then
      ! convert j info and get energy_so 
      is(1) = 1 + a_start(4) - a_start(2) 
      en(1) = dble(m_orbital(n(1),l(1))%energy_so(is(1)))

      is(2) = 1 + a_end(4) - a_end(2)
      en(2) = dble(m_orbital(n(2),l(2))%energy_so(is(2)))
    end if
    
    
    ! loop through occ. orbitals
    lp_i:do i=1,m_ioccnl
      nn = m_occnl(i,1)
      ll = m_occnl(i,2)
      energy = dble(m_orbital(nn,ll)%energy)

      lp_spin:do iis=0,1
        if ( m_SOcoupl ) then
          energy = dble(m_orbital(nn,ll)%energy_so(iis))
        elseif (iis==0) then
          cycle  ! iis=1 for no spin-orbit mode
                 !  -> m runs up to l 
        end if
        ! check energy range
        if ( energy>maxval(en) .or. energy<minval(en) ) cycle

        ! check l,j range
        ! assume no radial n degeneracy
        if ( energy==en(1) ) then
          if ( ll<l(1) ) cycle
          if ( ll==l(1) .and. iis<is(1) ) cycle
        end if
        if ( energy==en(2) ) then
          if (ll>l(2) ) cycle
          if (ll==l(2) .and. iis>is(2) ) cycle
        end if

        lp_m:do m=0,ll-(1-iis)  ! max. mj < l, here: m=mj-.5
          if (nn==n(1).and.ll==l(1).and.iis==is(1)) then
            if ( m<a_start(3) )  cycle
          end if
          if (nn==n(2).and.ll==l(2).and.iis==is(2)) then
            if ( m>a_end(3) )  cycle
          end if

          m_fmoact(nn,ll,m)  = .true.
          m_fmoact(nn,ll,-m) = .true.
          if ( m_SOcoupl )  then
            m_fmoact(nn,ll,:)  = .true.
            m_fmoact_so(nn,ll,m,iis)  = .true.
          end if
          
        end do lp_m
      end do lp_spin
    end do lp_i
    
  end subroutine set_active_orbitals



  subroutine get_nlmj(a_str,a_nlmj,a_mode)
    !
    ! convert orbital information in integer values
    !
    ! INPUT/OUTPUT
    ! ------------
    !  a_str   : CHARACTER(*) - IN
    !            Orbital informations are given in a string in the format 
    !            "NLJ_M" or "N,L,J_M". M-statement "_M" and J-statement "J"
    !            are optional. If they are not provided all sub-orbitals 
    !            are included.
    !
    !              N : principal or radial quantum number
    !              L : spatial angular momentum
    !              J : total angular momentum
    !              M : magnetic quantum number
    !              
    !            If no commas are used N and L is only a single character long
    !            and J is 3-character long. Otherwise the comma format has to 
    !            be used.
    !            N,J,M are always numbers. L can be a number or a letter.
    !            could be a letter.
    !            J is a odd half-integer written like "i.5".
    !            M is an integer when no J is provided otherwise it is an odd
    !            half-integer.
    !
    !            Examples:
    !            "5p" or "3s_0" or "4d1.5_0.5" or "22,30,13.5" or "2,1,3.5_1.5" 
    !
    !  a_nlmj  : INTEGER(4) - OUT
    !            orbital information are stored as integer array: 
    !              n, l, m , -1          [no spin-orbital]
    !              n, l, m_j-0.5, j-0.5  [spin orbital]
    !              n : radial quantum number
    !
    !  a_mode  : CHARACTER - IN
    !            defines wether the lowest ("L") or highest ("U") M 
    !            [or M_J,J values] should be used when only a subshell
    !            and not a specific orbital is given (e.g. 4d [or 2p_1.5])
    !
    use varsystem, only : m_SOcoupl
    use varwfct,only    : m_nomax,m_lomax,m_orbital   &
                        ,m_orbformat,m_orbformat_p,m_orbformat_r
    use basics, only    : StrUpCase,getelements
    implicit none

    character(*),intent(in) :: a_str,a_mode
    integer,intent(out) :: a_nlmj(4)

    logical :: low  ! lower bound is assumed
    character(clen) :: orb
    character(3) :: cl=""
    integer :: l,np,nl(2)
    real :: j,mj

    type(t_string),allocatable :: ele(:)


    if (a_mode=="U" .or. a_mode=="u") then
      low = .false.
    elseif (a_mode=="L" .or. a_mode=="l") then
      low = .true.
    else 
      stop " ERROR(get_nlmj): invalid mode"
    end if

    orb = trim(adjustl(a_str))
    call StrUpCase(orb)
    a_nlmj(:) = -1  ! not assigned

    ! when "end" is specified find highest orbital (in m_occnl).
    if ( trim(orb) == "END" ) then
      nl = maxloc( real(m_orbital%energy) , isocc(m_orbital%n,m_orbital%l) )
      nl = nl - 1
      a_nlmj = (/ nl(1) , nl(2) , nl(2) , -1  /)
      if (m_SOcoupl) a_nlmj(4) = nl(2)
      
      return
    end if


    call getelements(ele,orb,"_")

    ! m statement
    ! -----------
    if ( size(ele)==2 ) then
      read(ele(2)%txt,*)  mj
      if ( m_Socoupl .and. mod(2*mj-1,2.)/=0 ) then
          stop "ERROR(get_nlmj): mj is not an odd half-integer"
      elseif ( .not.m_Socoupl .and. mod(2*mj,2.)/=0 ) then
           stop "ERROR(get_nlmj): m is not an integer"
      end if
      a_nlmj(3) = int(mj)
    end if

    ! nlj statement
    ! -------------
    orb = ele(1)%txt
    call getelements(ele,orb,",")
    if ( size(ele)>1 ) then
      ! comma format
      read(ele(1)%txt,*) a_nlmj(1)
      read(ele(2)%txt,*) cl

      if ( size(ele)>2 ) then ! j info
        read(ele(3)%txt,*) j
        if ( mod(2*j-1,2.)/=0 )  &
            stop "ERROR(get_nlmj): j is not an odd half-integer"
        j = int(j)
        a_nlmj(4) = int(j)
      end if

    else
      ! no-comma format        
      ! read n and l
      read(ele(1)%txt,'(i1,a1)') a_nlmj(1),cl

      ! read j
      ele(1)%txt = ele(1)%txt(3:)
      if ( len_trim(ele(1)%txt)/=0 ) then
        read(ele(1)%txt,*) j
        if ( mod(2*j-1,2.0)/=0.0 )  stop " ERROR(get_nlmj): j is not an odd half-integer"
        j = int(j)
        a_nlmj(4) = int(j)
      end if
    end if

    ! find l (check if numerb or letter format
    cl = adjustl(cl)
    if ( len_trim(cl)>1 ) then  ! must be number format
      read(cl,*) l

    elseif ( ichar(cl(1:1))<ichar('A') .or. ichar(cl(1:1))>ichar('Z') ) then
      read(cl,*) l

    else
      select case (cl)
      case("S") 
        l = 0
      case("P") 
        l = 1
      case("D") 
        l = 2
      case("F") 
        l = 3
      case("G") 
        l = 4
      case("H") 
        l = 5
      case("I") 
        l = 6
      case("J") 
        l = 7
      case("K") 
        l = 8
      case("L") 
        l = 9
      case("M") 
        l = 10
      case("N") 
        l = 11
      case("O") 
        l = 12
      case default  
        stop " ERROR(assignactive): Element format for L must be S,P,D or F"
      end select
    end if
    a_nlmj(2) = l

    ! set j of not already done
    if ( m_SOcoupl .and. a_nlmj(4)<0 ) then
      if ( a_nlmj(3)>=0 )  stop "ERROR(get_nlmj): mj but not j is stated"

      a_nlmj(4) = l
      if ( low ) a_nlmj(4) = max(0,l-1)
    else if (.not.m_SOcoupl .and. a_nlmj(4)/=-1) then
      stop "ERROR(get_nlmj): j is stated (no spin-orbit mode)"
    end if

    ! m value if not specified in a_str
    if ( a_nlmj(3)<0 ) then
      a_nlmj(3) = l
      if ( low ) a_nlmj(3)=0
    end if

    ! use radial quantum number
    if ( m_orbformat==m_orbformat_p ) then
      a_nlmj(1) = a_nlmj(1) - a_nlmj(2) - 1
    end if

  end subroutine get_nlmj



  integer function calc_IDMLength( a_nlmj , ao_id ) Result(length)
    !
    !**PURPOSE
    !  -------
    !  Find number of non-zero matrix elements in IDM.
    !  If ao_id is given, the pair of occupied orbitals corresponding to each
    !  non-zero matrix element entry is stored in ao_id
    !
    !**INNPUT/OUTPUT
    !  -------------
    !  a_nlmj  :  INTEGER(:,:) - IN
    !             set of occupied orbitals (2.index) each defined by 
    !             a set of quantum numbers (1. index) 
    !             1. index: 1 - N <- principle value (period)
    !                       2 - L
    !                       3 - M_L or M_J-0.5 (w/o or w/ Spin-orbit)
    !                       4 - -1  or J - 0.5 (not necessary w/o Spin-orbit)
    !             2. index: number of tuples
    !
    !  ao_id   :  INTEGER(:,:) - OUT - OPTIONAL - ALLOCATABLE
    !             assign a pair of occupied orbital IDs (2.index in a_nlmj) 
    !             to each non-zero matrix elements. 
    !             Shape of ao_id will be (2,length) where the 1. index refers
    !             to the i and j in rho^IDM_ij
    !
    use VarSystem, only : m_SOcoupl
    use basics_append, only : append_allo
    implicit none

    integer, intent(in) :: a_nlmj(:,:)
    integer, intent(inout), optional, allocatable :: ao_id(:,:)
    integer :: i,j,sz


    ! initialize
    length = 0 


    if ( present(ao_id) ) then
      if ( allocated(ao_id) ) deallocate(ao_id)
    end if

    ! check sizes
    if ( size(a_nlmj,1) < 3 .and. .not.m_SOcoupl ) return
    if ( size(a_nlmj,1) < 4 .and. m_SOcoupl) return

    sz = size(a_nlmj,2)

    ! loop through orbitals and count the cases where m_i = m_j
    do i=1,sz
      do j=i,sz  ! only upper matrix triangle

        ! m_i=m_j : 3rd entrie is the M entry (w/ and w/o Spin-orbit)
        if ( a_nlmj(3,i) /= a_nlmj(3,j) ) cycle 

        length = length + 1
        if ( present(ao_id) ) then
          call append_allo( ao_id , reshape((/ i , j /),(/2,1/)) )
        end if

      end do
    end do

  end function calc_IDMLength




  integer function idocc(a_mode,a_n,a_l,a_m,ao_j,ao_ms)
    ! all channels (ion state [+ spin state of the electron]) are ordered 
    ! with increasing (nt,l,[j],m) referring to the ion state, where
    ! nt=n+l+1 is the principal quantum number, l is the angular momentum,
    ! j the total angular moment (only applies when SO-splitting is active),
    ! and m is the magnetic angular momentum. n refers to the radial 
    ! quantum number. 
    ! The spin state of the electron ao_ms is only considered when 
    ! spin orbitals are considered.
    !
    ! no SO-splitting: 
    ! ----------------
    ! channels correspond to directly to occupied orbitals, since both spin
    ! states are equally likely and only a superposition is stored.
    ! virtual & occupied orbitals are both respresented in 
    ! the n,l,m basis and m is the mag. angular momentum
    ! for the virtual and occupied orbital.
    ! 
    ! SO-coupling:
    ! -------------
    ! considered when j,ms are given and SO-coupling is active
    !   j : = 2*J (to make it integer)
    !   m : = 2*M_J
    !   ms: = 2*M_S (of virtual orbital) [only needed for mode=3]
    !
    ! only active orbitals with m>=0 are considerd
    ! if m<0 then n,l,-m,(-ms) is considered (exception a_mode==1)
   
    ! INPUT:
    !  a_mode = 1 - consider all occupied orbitals (passive and active)
    !  (required)   and m<0 as well
    !               -> j is given   : count them in the N,L,J,M_J basis
    !               -> j is not giv.: count them in the N,L,M_L   basis
    !                    -> for both cases : Socoupl=true,false
    !
    !        = 2 - calculate active occupied orbital ID 
    !               -> no SO-coupling: #{1p1h class}=#{act.occ.orb.} 
    !                                  -> as cmode=1
    !               -> SO-coupling   : m_s is ignored 
    !                                  -> N,L,J,M_J defined occupied orbital
    !
    !        = 3 - calculate 1p1h configuration class ID 
    !        
    !
    ! output:
    !   return= id  number of act. occupied orbital
    !         = 0   if input orbital |nl(j)m> is not active
    !
    use varwfct,only : m_nomax,m_lomax,m_ioccact,m_ioccall,m_i1p1hclass
    use varsystem,only : m_SOcoupl
    implicit none

    integer,intent(in) :: a_mode,a_n,a_l,a_m
    integer,intent(in),optional :: ao_j,ao_ms

    integer,allocatable, save :: occid(:,:,:)
    integer :: sgn


    idocc=0
    
    ! check idocc mode
    if (a_mode/=1.and.a_mode/=2.and.a_mode/=3) then
       write(0,*) " error(idocc): invalid mode =",a_mode
       stop " "
    end if
    
    ! check spin-orbit dependent input
    if ( m_SOcoupl) then
      ! ms must be given only when ID of 1p1h-class (with spin-orbit) should be found
      ! mj must be given when ms is given (but not vice versa)
      if ( a_mode==3 .and. .not.present(ao_ms))          &
         stop " error (idocc): ms must be given for 1p1h-config. ID"
    
      if ( .not.present(ao_j) )          &
         stop " error (idocc): j must be given with spin-orbit coupling on"
   
    else  ! no Spin-Orbit -----

      ! check that j and ms are not given when Spin-orbit coupling is not set
      if (present(ao_j).or.present(ao_ms))  &
           stop " error (idocc): no SO-coupling but j is stated!"

    end if
    
    
    ! check valid range and if orbital is occ. + active orbital
    ! if active orbitals or 1p1h config. should be counted, make sure that 
    ! asked orbital is active as well
    if ( a_mode==1 ) then
      if (.not.isocc(a_n,a_l)) return
    elseif (m_SOcoupl) then
      if (.not.isact(a_n,a_l,a_m,ao_j)) return
      sgn=1
      if ( a_m<0 ) sgn=-1  ! need to flip sign of spin
      ! unphysical 1p1h-class where j=l+1/2 with mj=j and spin -1/2 
      ! is not an active 1p1h-class
      if ( a_mode==3 .and. ao_j==2*a_l+1 .and. abs(a_m)==ao_j .and. ao_ms==-1 ) return
    else
      if (.not.isact(a_n,a_l,a_m)) return
    end if


    ! create permanent auxiliary field
    if ( .not.allocated(occid) )  call init_occid 
    
    ! find ID
    ! --------
    idocc = occid(a_n,a_l,a_mode)

    if ( present(ao_j) ) then
      select case(a_mode)
      case(1)
        if ( ao_j>2*a_l )  idocc = idocc + 2*a_l
        idocc = idocc + (a_m+ao_j)/2
      case(2)
        if ( ao_j>2*a_l )  idocc = idocc + a_l
        idocc = idocc + (abs(a_m)-1)/2
        idocc = idocc - nonact(2,a_n,a_l,abs(a_m),ao_j)
      case(3)
        if ( ao_j>2*a_l )  idocc = idocc + 2*a_l
        idocc = idocc + (abs(a_m)-1)
        ! skip unphysical 1p1h where j=mj=2l+1 and spin=-1/2
        if ( ao_j/=2*a_l+1 .or. abs(a_m)/=ao_j .or. ao_ms/=1 )   idocc = idocc + (sgn*ao_ms+1)/2
        idocc = idocc - nonact(3,a_n,a_l,abs(a_m),ao_j)
      end select

    else  ! no spin-orbit

      select case(a_mode)
      case(1)
        idocc = idocc + a_m + a_l
      case(2,3)
        idocc = idocc + abs(a_m) 
        idocc = idocc - nonact(2,a_n,a_l,abs(a_m))
      end select

    end if


    idocc = idocc + 1
    if (a_mode==3 .and. idocc>m_i1p1hclass) then
       write(STDERR,*) idocc,">",m_i1p1hclass
       stop " idocc: more active 1p1h classes than &
            &initially determined"
    
    else if (a_mode==2.and.idocc>m_ioccact) then
       write(STDERR,*) idocc,">",m_ioccact
       stop " idocc: more active orbitals than &
            &initially determined"
    
    else if (a_mode==1.and.idocc>m_ioccall) then
       write(STDERR,*) idocc,">",m_ioccall
       stop " idocc: more occupied orbitals than &
            &initially determined"
    end if

  contains

    subroutine init_occid
      ! find ID of first element in each (n,l) shell
      use varwfct, only : m_occnl,m_ioccnl,m_orbital
      implicit none
      integer :: n1,l1,n2,l2
      integer :: i
      integer, allocatable :: ii(:)
      real(dp),allocatable :: energy(:)


      allocate(occid(0:m_nomax,0:m_lomax,3))
      occid=0

      ! tempor. fields
      allocate( ii(m_ioccnl) , energy(m_ioccnl) )
      ii=0
      energy = ONE

      ! find energy-ordered series of occupied orbitals
      forall(i=1:m_ioccnl) 
        energy(i) = dble(m_orbital(m_occnl(i,1),m_occnl(i,2))%energy)
      end forall
      do i=1,m_ioccnl
        ii(i)=minval(minloc(energy,energy<ZERO))
        if ( ii(i)<1 .or. ii(i)>m_ioccnl )  &
          stop " ERROR(init_occid): occ. orb. energies are incorrect"
        energy(ii(i)) = ONE  ! unset entry
      end do
      deallocate(energy)


      do i=2,m_ioccnl
        ! previous n,l-shell
        n1 = m_occnl(ii(i-1),1)
        l1 = m_occnl(ii(i-1),2)
        ! current n,l-shell
        n2 = m_occnl(ii(i),1)
        l2 = m_occnl(ii(i),2)

        if ( i==2 ) then
          occid(n1,l1,:) = 0
        end if
        
        if ( m_SOcoupl ) then
          occid(n2,l2,1) = occid(n1,l1,1) + 4*l1+2
          occid(n2,l2,2) = occid(n1,l1,2) + 2*l1+1
          occid(n2,l2,3) = occid(n1,l1,3) + 4*l1+2
        else
          occid(n2,l2,1)   = occid(n1,l1,1)   + 2*l1+1
          occid(n2,l2,2:3) = occid(n1,l1,2:3) + l1+1
        end if

        occid(n2,l2,2) = occid(n2,l2,2) - nonact(2,n1,l1)
        occid(n2,l2,3) = occid(n2,l2,3) - nonact(3,n1,l1)
      end do
    end subroutine init_occid


    integer function nonact(a_mode,a_n,a_l,ao_m,ao_j) result(res)
      use varwfct, only : m_fmoact_so,m_fmoact
      implicit none
      integer, intent(in) :: a_mode,a_n,a_l,ao_m,ao_j
      optional :: ao_m,ao_j

      integer :: i,m,mmax,j


      res = 0
      if ( a_mode/=2 .and. a_mode/=3 ) return

      if ( m_SOcoupl ) then
        if ( present(ao_m) .and. .not.present(ao_j) )  &
            stop " ERROR(nonact): mj but not j is defined"

        j=2*a_l+1
        if ( present(ao_j) ) j=ao_j        

        ! run over j=l-0.5
        if ( j>2*a_l) then
          do m=1,j-2,2
            if ( .not.isact(a_n,a_l,m,j-2) ) res=res+1
          end do
          if ( a_mode==3 )  res=2*res
        end if
        
        ! run over given j
        mmax=j
        if ( present(ao_m) ) mmax=abs(ao_m)-2
        do m=1,mmax,2
          if ( .not.isact(a_n,a_l,m,j) ) then
            res=res+1 
            if ( a_mode==3 ) res=res+1
          else if ( a_mode==3 .and. j==2*a_l+1 .and. m==j )  then
            ! remove unphysical 1p1h-class 
            ! where 2j=2l+1 , mj=j , spin=-1 (would require ml>l)
            res=res+1
          end if
        end do

      else  ! no Spin-orbit

        mmax = a_l
        if ( present(ao_m) )  mmax = ao_m-1
        do i=0,mmax
          if ( .not.isact(a_n,a_l,i) ) res = res + 1
        end do

      end if
    end function nonact

  end function idocc




  subroutine init_idoccrev
    ! initialize field which has the reverse function to idocc(id,idx)
    ! -----------------------------------------------------------------
    ! id  : active occupied orbital ID
    ! idx (w/o SO) : 1-radial no. (n); 2-ang. mom. (l); 3-z ang. mom. (m)
    ! idx (w SO)   : 1-radial no. (n); 2-ang. mom. (l); 3-z ang. mom. (m_l)
    !                4-ang. mom. (2*j);5-z ang. mom. (2*m_j);
    !                6-z spin (0:-1/2,1:1/2);  
    !
    use varsystem,only : m_SOcoupl
    use varwfct,only : m_orbital,m_lomax,m_nomax             &
         ,m_ioccall,m_ioccact,m_i1p1hclass                   &
         ,m_idoallr=>m_idoccallrev,m_idoactr=>m_idoccactrev  &
         ,m_id1p1hr=>m_id1p1hclassrev
    use angmom,only : m_cleb=>cleb
    use orbital, only : init_orbital
    implicit none

    integer :: iall,iact,i1p1h,idj,ms,n,l,ml,mj,j
    complex(dp) :: energy
    
    ! orbital field must be allocated before 
    if (.not.allocated(m_orbital))  call init_orbital(m_orbital,0)

    ! if already allocated reset field
    if (allocated(m_idoallr)) deallocate(m_idoallr)
    if (allocated(m_idoactr)) deallocate(m_idoactr)
    if (allocated(m_id1p1hr)) deallocate(m_id1p1hr)

    allocate(m_idoallr(m_ioccall),m_idoactr(m_ioccact))
    allocate(m_id1p1hr(m_i1p1hclass))


    m_idoallr(:)%n=0
    m_idoallr(:)%l=0
    m_idoallr(:)%ml=0
    ! not applicable for no SO-coupling
    ! however, should be initialized anyway
    m_idoallr(:)%idhole=0
    m_idoallr(:)%j=0
    m_idoallr(:)%mj=0
    m_idoallr(:)%ms=0
    m_idoallr(:)%cleb(0)=ZERO
    m_idoallr(:)%cleb(1)=ZERO
    m_idoallr(:)%energy=ZERO


    m_idoactr(:)%n=0
    m_idoactr(:)%l=0
    m_idoactr(:)%ml=0
    ! not applicable for no SO-coupling
    ! however, should be initialized anyway
    m_idoactr(:)%idhole=0
    m_idoactr(:)%j=0
    m_idoactr(:)%mj=0
    m_idoactr(:)%ms=0
    m_idoactr(:)%cleb(0)=ZERO
    m_idoactr(:)%cleb(1)=ZERO
    m_idoactr(:)%energy=ZERO


    m_id1p1hr(:)%n=0
    m_id1p1hr(:)%l=0
    m_id1p1hr(:)%ml=0
    ! not applicable for no SO-coupling
    ! however, should be initialized anyway
    m_id1p1hr(:)%idhole=0
    m_id1p1hr(:)%j=0
    m_id1p1hr(:)%mj=0
    m_id1p1hr(:)%ms=0
    m_id1p1hr(:)%cleb(0)=ZERO
    m_id1p1hr(:)%cleb(1)=ZERO
    m_id1p1hr(:)%energy=ZERO


    lp_n:do n=0,m_nomax
      lp_l:do l=0,m_lomax
        if (.not.isocc(n,l)) cycle
    
        if_so:if (.not.m_SOcoupl) then
          do ml=-l,l
            
            iall = idocc(1,n,l,ml)
            iact = idocc(2,n,l,ml)
            i1p1h= idocc(3,n,l,ml)

            if (iall<=0 .or. iall>m_ioccall) then
               write(STDERR,*) "iall,m_ioccall:",iall,m_ioccall
               stop " error(init_idoccrev): wrong IDoccall"
            end if
            m_idoallr(iall)%idhole = iact
            m_idoallr(iall)%n      = n
            m_idoallr(iall)%l      = l
            m_idoallr(iall)%ml     = ml
            m_idoallr(iall)%mj     = ml
            m_idoallr(iall)%energy = m_orbital(n,l)%energy   

            if (ml>=0 .and. isact(n,l,ml)) then                   
               if (iact<=0 .or. iact>m_ioccact) then
                  write(STDERR,*) "iact,m_ioccact:",iact,m_ioccact
                  stop " error(init_idoccrev): wrong IDoccact"
               end if
               if (i1p1h<=0 .or. i1p1h>m_i1p1hclass) then
                  write(STDERR,*) "i1p1h,m_i1p1hclass:",i1p1h,m_i1p1hclass
                  stop " error(init_idoccrev): wrong ID1p1hclass"
               end if

               m_idoactr(iact)%idhole = iact
               m_idoactr(iact)%n      = n
               m_idoactr(iact)%l      = l
               m_idoactr(iact)%ml     = ml
               m_idoactr(iact)%mj     = ml
               m_idoactr(iact)%energy = m_orbital(n,l)%energy
               
               m_id1p1hr(i1p1h) = m_idoactr(iact) 

            end if
          end do
      
        else  ! SO coupling ----------------------------

          lp_j:do j=abs(2*l-1),2*l+1,2 ! j = 2*J
            ! idj = 0,1 <-> J = L-S, L+S
            idj = j/2-l+1 
            
            do mj=-j,j,2  ! mj = 2*M^J 

              
              iall = idocc(1,n,l,mj,j)
              iact = idocc(2,n,l,mj,j)

              if (iall<=0 .or. iall>m_ioccall) then
                write(STDERR,*) "iall,m_ioccall:",iall,m_ioccall
                stop " error(init_idoccrev): wrong IDoccall"
              end if

              ! set energy of classes 
              energy = m_orbital(n,l)%energy
              if (m_SOcoupl) then
                energy = m_orbital(n,l)%energy_so(idj)
              end if

              m_idoallr(iall)%idhole = iact
              m_idoallr(iall)%n    = n  !(of occupied orbital)
              m_idoallr(iall)%l    = l  !(of occupied orbital)
              m_idoallr(iall)%j    = j  ! 2J    (of occupied orbital)
              m_idoallr(iall)%mj   = mj ! 2*M^J (of occupied orbital)
              m_idoallr(iall)%energy = energy
                 
              ! m>0
              ! ---
              ! for active occupied orbitals and 1p-1h classes
              ! only ac. occ. orbitals with mj>0 are needed 
              if (mj<0 .or. .not.isact(n,l,mj,j))  cycle

              
              if (iact<=0 .or. iact>m_ioccact) then
                write(STDERR,*) "iact,m_ioccact:",iact,m_ioccact
                stop " error(init_idoccrev): wrong IDoccact"
              end if

              m_idoactr(iact)%idhole = iact
              m_idoactr(iact)%n    = n  !(of occupied orbital)
              m_idoactr(iact)%l    = l  !(of occupied orbital)
              m_idoactr(iact)%j    = j  !2J (of occupied orbital)
              m_idoactr(iact)%mj   = mj !2M^J(of occupied orbital)
              m_idoactr(iact)%energy = energy


              ! m^S_a : spin of electron 
              do ms=-1,1,2 ! ms = 2*M^S
                ml=(mj-ms)/2 !ml = M^L
                if ( ml>l ) cycle
                
                i1p1h = idocc(3,n,l,mj,j,ms)
                if (i1p1h<=0 .or. i1p1h>m_i1p1hclass) then
                  write(STDERR,*) "i1p1h,m_i1p1hclass:",i1p1h,m_i1p1hclass
                  stop " error(init_idoccrev): wrong ID1p1hclass"
                end if
                 
                m_id1p1hr(i1p1h)%idhole = iact
                m_id1p1hr(i1p1h)%n   = n  !(of occupied orbital)
                m_id1p1hr(i1p1h)%l   = l  !(of occupied orbital)
                m_id1p1hr(i1p1h)%j   = j  !2J (of occupied orbital)
                m_id1p1hr(i1p1h)%mj  = mj !2M^J(of occupied orbital)
                m_id1p1hr(i1p1h)%ml  = ml ! M^L(of virtual orbital)
                m_id1p1hr(i1p1h)%ms  = ms !2M^S(of virtual orbital)
                m_id1p1hr(i1p1h)%energy = energy

                ! clebsch-gordan coefficients of occupied orbital
                ! NOTE: idoar(3,i),idoar(6,i) are the spin of the 
                !       virtual orbital, however, each i has always
                !       a spin up and down component nevertheless
                !       of the spin value of the virtual orbital
                ! ms=-1/2
                m_id1p1hr(i1p1h)%cleb(0) =           &
                     m_cleb(2*l,mj+1, 1,-1, j,mj)

                ! ms=+1/2
                m_id1p1hr(i1p1h)%cleb(1) =           &
                     m_cleb(2*l,mj-1, 1,+1, j,mj)

                ! also assign these clebsch-gordan to the 
                ! occ. act. orbital object
                ! -> can be useful for calculating z_ij in calc_ev
                m_idoactr(iact)%cleb = m_id1p1hr(i1p1h)%cleb
              end do

            end do
          end do lp_j
        end if if_so
        
      end do lp_l
    end do lp_n
    
  
    ! check that all occupied orbitals are bound
    if ( any(real(m_idoallr(:)%energy)>=ZERO) ) &
         write(STDERR,*) "warning (init_idoccrev): some occupied orbitals &
                   &are not set or bound"

  end subroutine init_idoccrev
  


  subroutine defaultvalue
    ! define default values
    use varsystem
    use varwfct
    use varmatrix
    use pulse
    implicit none
 
    integer :: i


    ! system information
    m_nuccharge   = 2.0
    m_nelec       = -1
    m_npoints     = 400
    m_FEM_nintpts = 20
    m_Rmax        = 60.0
    m_zeta        = 0.5
    
    m_grid_type = "PSG"
    m_grid_rdfiles = .false.

    ! Spin-Orbit Interaction
    m_SOcoupl  = .false.
    m_fenergy_new = ""

    ! light-matter interaction
    m_LMgauge = m_lgauge

    ! CAP information
    m_absorb  = "CAP"
    m_Rabsorb = 40.0
    m_cap_strength = 1.e-3
    m_cs_angle  = 45  ! [°]
    m_cs_smooth = 1.D0
    ! Splitting function
    m_split_r = 40.0
    m_split_smooth = 1.D0
    m_split_write = .false.
    m_split_dt = ONE
    ! tsurf
    m_tsurf_r = 40.0
    m_tsurf   = .false.

    ! local potential
    m_modelpot%file = ""
    m_modelpot%active = .false.

    ! probe step
    m_probe = .false.
    m_probe_E = ZERO

    ! HF parameter
    m_niter     = 150
    m_rmaxscf   = 20.D0
    m_hfstep    = 0.8D0
    m_convE     = 1.D-06
    m_convWf    = 1.D-06
    m_rminHighL = 5.D-2
    m_LminHighL = 80


    ! all orbitals are active by default
    m_actoccorb = "1s-"
    m_orbformat = m_orbformat_p


    ! angular momentum restrictions
    m_lmax = 3
    m_lprx = 5
    m_lsum = -1 ! get assigned


    ! cut off limits
    m_ecut     = 10.0
    m_llimit1p = 0.D0
    m_llimit2p = 0.D0


    ! time propagation
    m_tmax     = -ONE  ! will be defined later
    m_dt       = 5.D-2
    m_dtprint  = 2.D-1
    m_propmode = 2  ! RK4
    m_lanev    = 5  ! Lanczos space dimension

    ! pulse parameter
    m_npulse = 10  ! cannot be defined from outside  (setup up only one pulse)
    if (allocated(m_fieldtype))  deallocate(m_fieldtype)
    allocate(m_fieldtype(m_npulse))
    if (allocated(m_efield))  deallocate(m_efield)
    allocate(m_efield(m_npulse))
    if (allocated(m_afield))  deallocate(m_afield)
    allocate(m_afield(m_npulse))

    do i=1,m_npulse
      m_fieldtype(i)       = "E"   ! read E(t)  (=A , read A(t) )
      m_efield(i)%strength = 0.D-3  
      m_efield(i)%duration = 0.D0  
      m_efield(i)%frequency= 0.D0   
      m_efield(i)%CEP      = 0.D0    
      m_efield(i)%t0       = 0.D0
      m_efield(i)%file     = ""
      m_efield(i)%rdfile   = .FALSE.
      m_efield(i)%smooth   = .FALSE.
      m_efield(i)%atomicunits= .TRUE. ! parameters and/or file data is given in a.u.
    end do
    m_afield = m_efield

    ! Coulomb interaction mode (w/ interhannel coupling)
    m_coulombmode = "all"

    ! +-M symmetry used files
    m_useMsymm = .true.
    m_useSymm  = .true.  ! use index symmetry (e.g. symmetric, ...)

    ! output files
    m_fout         = "data"
    m_storeOrbU    = .FALSE.
    m_storeEV      = "all"
    m_ev           = .false.
    m_storeIDMcapL = .FALSE.
    m_corrfct      = .FALSE.

    m_matrix_format = "bin"
    m_matrix_folder = "matrix"
    m_orbital_folder= "orbital"


    ! backup file (in time propagation)
    m_backup%dt    = 40.D0  ! ~ 1 fs
    m_backup%write = .TRUE. 
    m_backup%read  = .FALSE.
    m_backup%append  = .FALSE.
    m_backup%actorbfile = .TRUE. 
    m_backup%file_write = ""   ! get assigned later to trim(m_fout)//".backup"
    m_backup%file_read  = ""

    ! diagonalize CIS Hamiltonian
    m_diagCIS    = .FALSE.
    m_diagField = ZERO

    ! reference states
    m_refstates_rootname = ""
    m_refstates_segment  = ""
    m_refstates_mode     = "" ! don't use reference states

    ! spatial grid parameters
    m_theta = (/ ZERO, ZERO, ONE /)
    m_phi   = (/ ZERO, ZERO, ONE /)
    m_energy = (/ ZERO, ZERO, ONE /)
    m_rskip = 1
    m_tskip = 1 
  
    m_omp_nthreads = 1
  end subroutine defaultvalue



  subroutine postprocess
    use basics, only : strUpCase,strLowCase,getelements
    use VarSystem
    use VarWfct
    use omp_lib
    implicit none

    type(t_string),allocatable :: string(:)
    integer :: i,n

    if ( m_nelec<0)  m_nelec = int(m_nuccharge)

    ! 'residual' Coulomb modes
    if ( m_nelec==1) m_coulombmode = "none"
    ! model potential does not exclude Coulomb interaction 
    ! if ( m_modelpot%active ) M_coulombmode = "none"


    m_nocoulomb = .false.
    m_intercoupl = .true.
    m_intracoupl = .true.
    call StrLowCase(m_coulombmode)
    select case(trim(m_coulombmode))
    case('all')
    case('none','slater')
      m_nocoulomb = .true.
      m_intercoupl = .false.
      m_intracoupl = .false.
    case('intra')
      m_intercoupl = .false.
    case('inter')
      m_intracoupl = .false.
    case default
      write(STDERR,*) " ERROR: invalid Coulomb mode"
      write(STDERR,*) "        input was : ",trim(m_coulombmode)
      write(STDERR,*) "        possible options are coulombmode=all,intra,none"
      stop
    end select


    ! check propagation mode
    select case(m_propmode)
    case(0,1,11,2,3)
    case default
      write(STDERR,*) " ERROR: invalid Propagation mode"
      write(STDERR,*) "        input was : ",m_propmode
      write(STDERR,*) "        possible options are propmode=0,1,11,2,3"
      stop
    end select


    ! check Light-Matter gauge
    select case(m_LMgauge)
    case(m_lgauge,m_vgauge)
    case default
      write(STDERR,*)      " ERROR: invalid light-mattter gauge"
      write(STDERR,*)      "        input was : ",trim(m_LMgauge)
      write(STDERR,'(5a)') "        possible options are LMgauge = "    &
                                    ,trim(m_lgauge),", ",trim(m_vgauge)
      stop
    end select


    ! file where the existing backup is stored (continue propagation from this point)
    if ( len_trim(m_backup%file_read) == 0 ) m_backup%file_read=trim(m_fout)//".backup"


    ! Grid for finding Hatree-Fock ground state
    m_rmaxscf = min(m_rmax,m_rmaxscf)

    ! grid type
    ! ---------
    !  -> assign grid-type-dependent parameters
    m_grid_type = adjustl(m_grid_type)
    call StrUpCase(m_grid_type)
    select case(trim(m_grid_type))
    case('PSG')
      if ( m_grid_rdfiles ) then
        m_grid_rdfiles=.false.
        write(STDERR,*) " Warning: for pseudo-spectral grid method grid points cannot be defined via file"
      end if
      m_nmax = m_npoints
    case('FEM')
      m_nmax = 3*m_npoints-2
    case('DVR')
      m_nmax = 3*m_npoints-2
    case default
      stop " ERROR: invalid grid type (only 'FEM','PSG','DVR' are allowed)"
    end select 
    if ( .not.m_grid_rdfiles ) then
      m_grid_files=""
    else
      m_nmax = -1
    end if

    ! assign information aboutactive radial wavefunction
    ! all orbitals are active if not set otherwise
    !  -> be overwritten in readenergy if used
    if ( allocated(m_nmaxl) ) deallocate(m_nmaxl)
    allocate(m_nmaxl(0:m_lmax))
    m_nmaxl = m_nmax


    ! lprx < lmax
    ! lsum is assigned later (when m_lomax is known)
    !  -> lsum is never larger than 2*lmax
    m_lprx = min(m_lprx,m_lmax)


    ! Absorbing methods
    ! -----------------
    ! use only capital letters
    call StrUpCase(m_absorb)
    select case(m_absorb)
    case("CAP")
      m_split_write = .false.   ! no splitting is happening here
    case("ECS")
      ! CAP is unset
      m_cap_strength = ZERO
      m_split_write = .false.
    case("SPL")
      m_cap_strength = ZERO
    case default
      write(STDERR,*) " ERROR: invalid absorbing method"
      write(STDERR,*) "        input was : ",trim(m_absorb)
      write(STDERR,*) "        possible options are absorb = CAP, ECS, SPL"
      stop
    end select


    ! time steps
    ! ----------
    ! print rather shorter than longer intervals but at least 1*dt
    m_dtprint   = max( 1 , floor(  m_dtprint /m_dt) ) * m_dt
    m_backup%dt = max( 1 , floor( m_backup%dt/m_dt) ) * m_dt
    m_split_dt  = max( 1 , floor(  m_split_dt/m_dt) ) * m_dt


    ! if HF is running, SOcoupl will be deactivated
    if (m_program == 0 ) then
      m_SOcoupl = .false.
    end if


    ! convert phase into rad
    m_efield%CEP = m_efield%CEP * pi/180.D0
    m_cs_angle   = m_cs_angle   * pi/180.D0
    m_phi(1:2)   = m_phi(1:2)   * pi/180.D0
    m_theta(1:2) = m_theta(1:2) * pi/180.D0


    ! External Field
    ! --------------
    ! convert pulse parameter in atomic units
    ! convert intensity FWHM to E-field FWHM

    do i=1,m_npulse 
      m_efield(i)%Duration = m_efield(i)%Duration * sqrt(two)
      if (.not.m_efield(i)%atomicunits) then ! units of pulse parameter
        m_efield(i)%Duration  = m_efield(i)%Duration  * attosec2AU
        m_efield(i)%t0        = m_efield(i)%t0        * attosec2AU
        m_efield(i)%strength  = m_efield(i)%strength  * V_m2au
        m_efield(i)%Frequency = m_efield(i)%Frequency * eV2au
      end if    
      call StrUpCase(m_fieldtype(i))
      m_afield(i) = m_efield(i)
    end do

    ! remove entries that have no field
    n=0
    do i=1,m_npulse
      if ( m_efield(i)%strength /= ZERO  .or. m_efield(i)%rdfile )  then
        ! this pulse is not defined  and skip all the rest (assume no more pulses are defined)  
        n = n+1
        m_afield(n) = m_efield(i)
      end if
    end do
    
    m_npulse = max(1,n)  ! at least one pulse
    deallocate(m_efield)
    allocate(m_efield(m_npulse))
    m_efield = m_afield(1:m_npulse)
    deallocate(m_afield)
    allocate(m_afield(m_npulse))
    m_afield = m_efield


    ! expectation value
    ! ------------------
    call getelements(string,m_storeEV,":")
    do i=1,size(string)
      call strlowcase(string(i)%txt)
      select case(trim(string(i)%txt))
      case("all")
        m_ev    = .true.
      case("z")
        m_ev(1) = .true.
      case("zion")
        m_ev(2) = .true.
      case("mom")
        m_ev(3) = .true.
      case("zacc")
        m_ev(4) = .true.
      case default
        write(STDERR,*) " ERROR: expectation value argument is invalid"
        write(STDERR,*) "        only value: all,z,zion,zacc,mom are allowed"
        stop
      end select
    end do

    ! reference states
    ! ----------------
    call StrLowCase(m_refstates_mode)
    m_refstates_use = .true.
    ! empty arrays of no ref. states should be used
    select case (trim(m_refstates_mode))
    case('','null')
      m_refstates_mode = ""
      m_refstates_rootname = ""
      m_refstates_segment = ""
      m_refstates_use = .false.
    case('add','sub','only','lim')
      m_refstates_use = .true.
      m_refstates_rootname = adjustl(m_refstates_rootname)
    case default
      write(STDERR,*)  'ERROR: refstates_mode (=',trim(m_refstates_mode),') is invalid'
      stop '' 
    end select


    ! outputs
    ! ----------
    m_fev        = trim(m_fout)//".ev"
    m_frho1      = trim(m_fout)//".idm.1"
    m_frho2      = trim(m_fout)//".idm.2"
    m_fwfct      = trim(m_fout)//".wfct"
    m_fwfctsplit = trim(m_fout)//".split"
    m_ftsurf     = trim(m_fout)//".tsurf"
    m_fcorr      = trim(m_fout)//".corr"
    m_backup%file_write = trim(m_fout)//".backup"
    m_fdens      = trim(m_fout)//".dens"
    m_fpes       = trim(m_fout)//".pes"

    ! matrix file format
    ! ------------------
    m_matrix_format = adjustl(m_matrix_format)
    call strlowcase(m_matrix_format)
    select case (trim(m_matrix_format))
    case("bin","txt")
    case default
      write(STDERR,*) "ERROR(postprocess): matrix format is invalid: ",trim(m_matrix_format)
      stop
    end select

    ! file names
    ! ----------
    ! -> add directory information
    m_orbital_folder = adjustl(m_orbital_folder)
    m_focc     = trim(m_orbital_folder)//"/"//m_focc 
    m_forbu    = trim(m_orbital_folder)//"/"//m_forbu    
    m_forb     = trim(m_orbital_folder)//"/"//m_forb     
    m_fenergy  = trim(m_orbital_folder)//"/"//m_fenergy  
                            
    m_matrix_folder = adjustl(m_matrix_folder)
    m_fcouocc  = trim(m_matrix_folder)//"/"//m_fcouocc  
    m_fcoudir  = trim(m_matrix_folder)//"/"//m_fcoudir  
    m_fcouexc  = trim(m_matrix_folder)//"/"//m_fcouexc  
    m_folap    = trim(m_matrix_folder)//"/"//m_folap    
    m_fsplit   = trim(m_matrix_folder)//"/"//m_fsplit
    m_ftsurfmat= trim(m_matrix_folder)//"/"//m_ftsurfmat
    m_fdipsymm = trim(m_matrix_folder)//"/"//m_fdipsymm 
    m_fdipherm = trim(m_matrix_folder)//"/"//m_fdipherm 
    m_fdipacc  = trim(m_matrix_folder)//"/"//m_fdipacc  
    m_fmomsymm = trim(m_matrix_folder)//"/"//m_fmomsymm 
    m_fmomherm = trim(m_matrix_folder)//"/"//m_fmomherm 



    ! small letter for m_orbformat
    call strlowcase(m_orbformat)
    select case(m_orbformat)
    case(m_orbformat_p,m_orbformat_r)
    case default
      write(STDERR,*) "ERROR(postprocess): orbital format is invalid: ",trim(m_orbformat)
      stop
    end select
    
    ! diagonalize CIS Hamiltonian
    ! ---------------------------
    if ( m_diagCIS ) then
       if ( m_diagnev < 1 ) stop " ERROR(DiagCIS): no eigenvalue should be calculated" 
    end if


    ! OPENMP: set up number of threads
    ! ---------------------------------
#if OPENMP
    if (  m_omp_nthreads > 0 ) then
      ! otherwise use default value
      call OMP_SET_NUM_THREADS( m_omp_nthreads ) 
    else
      m_omp_nthreads = OMP_GET_MAX_THREADS()
    end if
#else
    m_omp_nthreads=1  ! no openmp => only one core
#endif

  end subroutine postprocess



  subroutine printinfo(ao_mode,ao_pipe,ao_str,ao_header,ao_check)
    ! Print program parameters on screen. The option a_mode
    ! defines for which purpose/rogram the output should be printed,
    ! and adjust the output accordingly.
    ! 
    ! INPUT
    ! -----
    ! ao_mode  : INTEGER - IN - OPTIONAL
    !            ID of which program is running
    !            if not given, program-wide parameter "m_program" is used.
    !            For details to "m_program" see module VarSystem.
    !      = 0 : HF calculation output
    !      = 1 : Matrix elements calculation output
    !      = 2 : Propagation output
    !      = 3 : density matrix postprocessing
    !
    ! ao_pipe  : INTEGER - IN - OPTIONAL
    !            file unit
    !            [ default : ao_pipe = STDOUT ]
    !
    ! ao_str   : CHARACTER - IN - OPTIONAL
    !            character/symbol put in front of each line
    !            [ default : ao_str = " " ]
    !
    ! ao_header: INTEGER - IN - OPTIONAL
    !            show header and/or body
    !     = -1 : only header 
    !     =  0 : only body
    !     =  1 : all ( header + body ) [ default ]
    !
    ! ao_check : LOGICAL- IN - OPTIONAL
    !            check existence of files (default: false)
    use varsystem
    use varwfct
    use varmatrix
    use pulse
    implicit none

    integer,intent(in),optional :: ao_mode
    integer,intent(in),optional :: ao_pipe
    character,intent(in),optional :: ao_str
    logical,intent(in),optional :: ao_check
    integer,optional :: ao_header

    character(2) :: str
    integer :: fid,mode,header,i
    logical :: ex,check
    logical :: l0,l1,l2,l3   ! switched correspodnig to mode
    character(clen) :: txt


    l0 = .false.
    l1 = .false.
    l2 = .false.
    l3 = .false.

    fid = STDOUT
    if ( present(ao_pipe) ) fid = abs(ao_pipe)

    str = ""
    if (present(ao_str)) str = ao_str

    mode = m_program
    if (present(ao_mode)) mode = ao_mode

    header = 1
    if (present(ao_header)) header = ao_header
    
    check = .false.
    if (present(ao_check)) check = ao_check

    ! only -1,0,1 is allowed
    if ( header**2 > 1 )      &
      stop " ERROR(printinfo) : header option is invalid"

    txt = ""
    select case (mode)
    case(0)
      txt = "Hartree-Fock for closed-shell atoms"
      l0 = .true.
    case(1)
      txt = "Matrix generation"
      l1 = .true.
    case(2)
      txt = "TDCIS-(SOo) propagation"
      l2 = .true.
    case(3)  
      txt = "Density/Spectrum calculation"
      l3 = .true.
    case default
      stop " ERROR(printinfo) : mode is unknown"
    end select


    ! header
    ! ------
    if ( header/=0 ) then

      write(fid,'(a,/,2a,/,4a)') str                                    &
            , str,"XCID package"                                        &
            , str,"project version: ",trim(m_svnrev) 

      if ( len_trim(txt)>0 ) write(fid,'(2a)') str,trim(txt)

      write(fid,'(2a,/,2a)')                                            &
              str,"(c) 2010-2013 Stefan Pabst"                          &
             ,str, "--------------------------------------------------" 
#if OPENMP
      write(fid,'(2a,/)')  str," -> runs in OpenMP mode"
#elif CUBLAS
      write(fid,'(2a,/)')  str," -> runs in CUDA mode"
#endif      
    end if
  
    if ( header==-1 )  return  ! show only header


    ! system parameter
    ! -----------------
#if OPENMP
    write(fid,'(a)')  str
    write(fid,'(2a)') str,"OpenMP"
    write(fid,'(2a)') str,"------"
    write(fid,'(2a,i5)') str,"number of threads/cores :",m_omp_nthreads
#endif

    write(fid,'(a)')  str
    write(fid,'(2a)') str,"Atom"
    write(fid,'(2a)') str,"----"
    write(fid,'(2a,ES12.4)') str,"nuclear charge (Z) [e]      :",m_nuccharge
    write(fid,'(2a,i5)')     str,"number of electrons         :",m_nElec

    ! Spin interaction
    write(fid,'(2a,L3)') str,"spin-orbit splitting        :",m_SOcoupl
    if ( l0 ) then
      write(fid,'(2a)')  str," -> Spin-orbit is always deactivated&
                                  & in HF program."
    end if


    ! light-matter interaction
    ! -------------------------
    write(fid,'(1a)') str
    write(fid,'(3a)') str,"Light-Matter Interaction    : ", trim(m_LMgauge)
    write(fid,'(2a)') str," -> LMgauge = 'length'      : E(t) * z"
    write(fid,'(2a)') str," -> LMgauge = 'velocity'    : A(t) * p"


    ! Coulomb interaction
    ! -------------------------
    write(fid,'(1a)') str
    write(fid,'(3a)') str,"Coulomb interaction mode    : ", trim(m_coulombmode)
    write(fid,'(2a)') str," -> mode = 'all'   : no approx. [default]"
    write(fid,'(2a)') str," -> mode = 'intra' : only intrachannel coupl."
    write(fid,'(2a)') str," -> mode = 'slater': Hatree-Slater potential (only hf.x)"
    write(fid,'(2a)') str," -> mode = 'none'  : no residual Coulomb inter."


    ! model potential
    ! ----------------
    write(fid,'(a,/,2a,l2)') str,str,"Use model potential in Hamiltonian:"  &
                                   ,m_modelpot%active 
    if ( m_modelpot%active ) then
      write(fid,'(3a)')          str,"File name of model potential      = " &
                                  ,trim(m_modelpot%file)
      inquire(file=m_modelpot%file,exist=ex)
      if (.not.ex) stop "  -> model potential file doesn't exist"
    end if


    write(fid,'(a)')  str
    write(fid,'(2a)') str,"Grid parameter"
    write(fid,'(2a)') str,"--------------"
    write(fid,'(2a,ES12.4)') str,"system radius (r)r [a.u.]   :",m_rmax
    write(fid,'(2a,i5)')     str,"radial grid points          :",m_npoints
    write(fid,'(2a,i5)')     str,"no. of radial functions     :",m_nmax
    select case(m_grid_type)
    case("PSG")
      write(fid,'(2a)') str,"grid type (PSG or FEM)      : pseudo spectral grid (=PSG)" 
      write(fid,'(2a,ES12.4)') str,"r(x)=(1+x)/(1-x+zeta), zeta =",m_zeta
    case("FEM")
      write(fid,'(2a)') str,"grid type (PSG or FEM)      : finite-element method (=FEM)" 
      if ( m_grid_rdfiles ) then
        write(fid,'(3a)') str,"read grid points from files : ",trim(m_grid_files)
      else
        write(fid,'(2a,ES12.4)') str,"r(x)=(1+x)/(1-x+zeta), zeta =",m_zeta
      endif
      write(fid,'(2a,i4)') str,"number of FEM integration points: ",m_fem_nintpts
    case("DVR")
      write(fid,'(2a)') str,"grid type (PSG, FEM or DVR)  : discrete variable representation (=DVR)" 
      if ( m_grid_rdfiles ) then
        write(fid,'(3a)') str,"read grid points from files : ",trim(m_grid_files)
      else
        write(fid,'(2a,ES12.4)') str,"r(x)=(1+x)/(1-x+zeta), zeta =",m_zeta
      endif
    end select

    write(fid,'(a)')  str
    write(fid,'(2a)') str,"Absorbing parameter"
    write(fid,'(2a)') str,"-------------------" 
    write(fid,'(3a)')        str,"Absorbing method            : ",trim(m_absorb)
    write(fid,'(2a,ES12.4)') str,"Absorb. region r[a.u.]>=    :",m_Rabsorb
    
    if ( m_absorb == "CAP" ) then
      write(fid,'(2a,ES12.4)') str,"CAP strength (eta)          :"   &
                                  ,m_cap_strength
      if (m_tsurf) then
        write(fid,'(2a,ES12.4)') str,"tsurf radius                :"   &
                                  ,m_tsurf_r
      end if
    else if (m_absorb == "SPL") then
      write(fid,'(2a,ES12.4)') str, "Splitting smoothness is    :"   &
                                  ,m_split_smooth
      write(fid,'(2a,ES12.4)') str, "Splitting radius is        :"   &
                                  ,m_split_r
      write(fid,'(2a,ES12.4)') str, "Splitting time steps       :"   &
                                  ,m_split_dt
      write(fid,'(2a,L2)') str, "Store absorbed wavefunction    :"   &
                                  ,m_split_write
    else
      write(fid,'(2a,ES12.4)') str,"Complex-scaling angle [deg] :"   &
                                  ,m_cs_angle/pi*180
      write(fid,'(2a,ES12.4)') str,"Complex-scaling smoothness  :"   &
                                  ,m_cs_smooth
    end if

  

    if ( l0 ) then
       write(fid,'(a)')  str
       write(fid,'(2a)') str,"Hartree-Fock parameter"
       write(fid,'(2a)') str,"----------------------"
       write(fid,'(2a,i5)')     str,"max. number of iteration    :",m_niter
       write(fid,'(2a)')        str," -> = -1 : no iteration limit"
       !write(fid,'(a)')  str
       write(fid,'(2a,ES12.4)') str,"max. radius for HF iteration:",m_rmaxscf
       !write(fid,'(a)')  str
       write(fid,'(2a,ES12.4)') str,"min. radius for L (>=LminHighL):"  &
                                   ,m_rminHighL
       write(fid,'(2a,i5)')     str,"LminHighL                      :"  &
                                   ,m_LminHighL
       write(fid,'(a)')  str
       write(fid,'(2a,ES12.4)') str,"weight for updating orbitals :",m_hfstep
       write(fid,'(2a)')        str," -> 0 < x <= 1"
       write(fid,'(2a,ES12.4)') str,"energy convergence criterion=",m_convE
       write(fid,'(2a,ES12.4)') str,"wavefct. converg.  criterion=",m_convWF
    end if

    
    if ( l2 .or. l3 ) then
       write(fid,'(a)')  str
       write(fid,'(2a)') str,"Pulse parameter"
       write(fid,'(2a)') str,"---------------"
       write(fid,'(2a,i2)')  str,"no. of pulses: ",m_npulse
       do i=1,m_npulse
         ! no pulse defined  (should not occur already taken care of in postprocess)
         if ( .not.m_efield(i)%rdfile .and. m_efield(i)%strength==ZERO )  cycle   
         write(fid,'(a)')  str
         write(fid,'(2a,i2)')  str,"pulse no.: ",i
         write(fid,'(2a)')     str,"----------"
         write(fid,'(2a,L1)')    str,    &
                 "atomic/SI units [T/F]               : ",m_efield(i)%atomicunits
                              
         if (m_efield(i)%rdfile) then
           write(fid,'(3a)')  str       &
               ,"field type (E/A)                    : ",trim(m_fieldtype(i))
           write(fid,'(2a,L1)') str,    &
                "smooth pulse ends                   : ",m_efield(i)%smooth

           write(fid,'(3a)')    str,    &
                "read pulse file                     : ",trim(m_efield(i)%file)

           inquire(file=m_efield(i)%file,exist=ex)
           if (.not.ex) stop "  -> file doesn't exist"

         else

           write(fid,'(3a)')  str       &
               ,"field type (E/A)                    : ",trim(m_fieldtype(i))
           write(fid,'(2a,9ES12.4)') str,&
                "max. field strength [a.u.]          : ", m_efield(i)%strength
           write(fid,'(2a,9ES12.4)') str,&
                "Intensity FWHM-duration [a.u.]      : ", m_efield(i)%duration/sqrt(TWO)
           write(fid,'(2a,9ES12.4)') str,&
                "(mean) photon energy [a.u.]         : ", m_efield(i)%frequency
           write(fid,'(2a,9ES12.4)') str,&
                "pulse centered at time t0 [a.u.]    : ", m_efield(i)%t0
           write(fid,'(2a,9ES12.4)') str,&
                "Carrier-Envelop Phase (CEP) [deg]   : ", m_efield(i)%CEP/pi*180
           write(fid,'(2a)')        str,&
                " -> tau/=0 : cos(w*t+CEP)*e^(-4*ln(2)*t^2/tau^2)"
           write(fid,'(2a)')        str,&
                " -> tau=0  : cos(w*t+CEP), t>=0"
         end if
       end do
    end if
 
 
    if ( l2 ) then
       if ( m_diagCIS ) then
         ! diagonlize CIS Hamiltonian
         write(fid,'(a)')  str
         write(fid,'(2a)') str,"Diagonalize CIS Hamiltonian"
         write(fid,'(2a)') str,"---------------------------"
         write(fid,'(2a)') str," -> no propagation"         
         write(fid,'(2a,i5)')      str," -> number of eigenstates                :", m_diagnev
         write(fid,'(2a,3ES13.4)') str," -> field strength (start/step size/end) :", m_diagField         
       else
         ! propagation 
         write(fid,'(a)')  str
         write(fid,'(2a)') str,"Propagation parameter"
         write(fid,'(2a)') str,"---------------------"
         write(fid,'(2a,ES12.4)') str,"max. time       [a.u.]          : ",m_tmax
         write(fid,'(2a,ES12.4)') str,"time steps (dt) [a.u.]          : ",m_dt
         write(fid,'(2a,ES12.4)') str,"displayed time step [a.u.]      : ",m_dtprint
         write(fid,'(2a)')        str," -> will be a multiple of dt"
         write(fid,'(2a,i3)') str,"propgation scheme ID            : ",m_propmode
         write(fid,'(2a)')    str," -> propmode =  0 : Euler method"
         write(fid,'(2a)')    str," -> propmode =  1 : improved Euler method"
         write(fid,'(2a)')    str," -> propmode = 11 : time differencing (2nd order)"
         write(fid,'(2a)')    str," -> propmode =  2 : Runge-Kutta (4th order)"    
         write(fid,'(2a)')    str," -> propmode =  3 : Lanczos-Arnoldi"
       
         write(fid,'(a)')  str
         write(fid,'(2a,2L)')     str,"dipole step before propagation : ",m_probe
         write(fid,'(2a,ES12.4)') str," -> strength of dipole step    : ",m_probe_E
       end if
    end if

    write(fid,'(a)')  str
    write(fid,'(2a)') str,"Cut-off parameter"
    write(fid,'(2a)') str,"-----------------"
    write(fid,'(2a,i3)')  str,"max. system angular momentum    : ",m_lmax
    if ( .not.l0 ) then
       write(fid,'(2a,i3)')  str,"Coulomb approx. beyond ang. mom.: ",m_lprx
       write(fid,'(2a,i3)')  str,"max. change in ang.mom.(Coloumb): ",m_lsum
       write(fid,'(2a,F8.3)')   str,"energy cut [a.u.]     :",m_ecut
       write(fid,'(2a,ES13.4)') str,"1p1h matrices cut-off :",m_llimit1p
       write(fid,'(2a,ES13.4)') str,"2p2h matrices cut-off :",m_llimit2p
    end if


    if ( l2 ) then
       write(fid,'(a)')  str
       write(fid,'(2a)') str,"Propagation files"
       write(fid,'(2a)') str,"-----------------"
       write(fid,'(3a)') str,"prefix of output filenames  = ",trim(m_fout)
       write(fid,'(2a)') str," -> store corrected ion density matrix  in *.idm.1"
       write(fid,'(2a)') str," -> store ion density matrix correction in *.idm.2"


       if (m_storeIDMcapL) then
         write(fid,'(2a)') str," -> store angular mom. resolved absorption in &
                                    &*.idm.lxx"
       else
         write(fid,'(2a)') str," -> don't store angular mom. resolved absorption"
       end if


       write(fid,'(2a)') str," -> store expectation values and/or E-field in *.ev"
       if (m_ev(1)) write(fid,'(2a)') str,"   -> store dipole moment"
       if (m_ev(2)) write(fid,'(2a)') str,"   -> store ionic dipole moment"
       if (m_ev(3)) write(fid,'(2a)') str,"   -> store momentum expectation value"
       if (m_ev(4)) write(fid,'(2a)') str,"   -> store dipole acceleration"
       write(fid,'(2a)')              str,"   -> store E-field"


      if (m_storeWfct) then
         write(fid,'(2a)') str," -> store CI-coefficients of wavefunction in &
                                    &*.wfct with time steps backup_dt"
       else
         write(fid,'(2a)') str," -> don't store CI-coefficients of wavefunction"
       end if

      if (m_corrfct) then
         write(fid,'(2a)') str," -> store correlation function <HF|zG(t,0)z|HF> &
                                    &in *.corr"
      end if
      
      ! backup
      if ( m_backup%write ) then 
        write(fid,'(2a)') str," -> backup CI-coefficients of wavefunction in &
                                            &*.backup"
        write(fid,'(2a,ES12.3)') str,"    - backup time step [a.u.]:"        &
                                    , m_backup%dt
      else
        write(fid,'(2a)') str," -> don't backup CI-coefficients of wavefunction"
      end if


      ! read backup
      if ( m_backup%read ) then
        write(fid,'(3a)') str," -> continue propagation from backup point &
                                   &stored in : ",trim(m_backup%file_read)
        

        inquire(file=m_backup%file_read,exist=ex)
        if (.not.ex) then
          write(fid,*) "   -> file '",trim(m_backup%file_read),"' doesn't exist"
          ! stop  ! <- don't stop program
        end if
        

        write(fid,'(2a,2L)') str,"    -> append data if 'fout' is kept unchanged [T/F] :"   &
                             ,m_backup%append

        write(fid,'(2a,2L)') str,"    -> use act. occ. orb. of file/input [T/F] :"   &
                             ,m_backup%actorbfile
      end if

    end if



    ! Parameter when running the density program
    ! -------------------------------------------
    if (l3 ) then
      write(fid,'(a)')  str
      write(fid,'(2a)') str,"Visualization Parameters (density.x)"
      write(fid,'(2a)') str,"------------------------------------"
      write(fid,'(2a,i2)') str," show every nth time step,    n = ", m_tskip
      write(fid,'(2a,i2)') str," show every nth radial point, n = ", m_rskip
      write(fid,'(2a)')    str," angular grids"
      write(fid,'(2a,2ES12.3,i5)')   str,"  -> theta [rad] (start/final/#steps) : ",m_theta(1:2),nint(m_theta(3))
      write(fid,'(2a,2ES12.3,i5)')   str,"  -> phi [rad]   (start/final/#steps) : ",m_phi(1:2),nint(m_phi(3))
    end if

    ! file infos and check existence
    ! -------------------------------
    write(fid,'(a)')  str
    write(fid,'(2a)') str,"Orbital/Matrix files"
    write(fid,'(2a)') str,"--------------------"
    write(fid,'(3a)') str,"occupation file                = ",trim(m_focc)
    inquire(file=m_focc,exist=ex)
    if ( .not.l0 .and. .not.ex .and. check) stop "  -> file doesn't exist"
    
    write(fid,'(3a)') str,"orbital energy file            = ",trim(m_fenergy)
    inquire(file=m_fenergy,exist=ex)
    if ( .not.l0 .and. .not.ex .and. check) stop "  -> file doesn't exist"

    if ( .not. l2) then
      write(fid,'(2a,6x,2a)') str,"orbital file (A^k_[n,l])"," = ",trim(m_forb)
      inquire(file=m_forb,exist=ex)
      if ( .not.l0 .and. .not.ex .and. check) stop "  -> file doesn't exist"
      if ( l0  .and. m_storeOrbU ) then
        write(fid,'(2a,8x,2a)') str,"orbital file (u_[n,l])"," = ",trim(m_forbu)
      end if
    end if



    if ( l2 ) then
      
      if (len_trim(m_fenergy_new)/=0) then
        ! check existence in subroutine and if it doesn't exist then skip
        ! reading it. It results in that the HF-orbital energy will be used instead.
        write(fid,'(3a)') str,"energy file (overwrite selec. orb. energies) = ",trim(m_fenergy_new)
      else
        write(fid,'(2a)') str,"No Spin-Orbit config./energy file is provided"
      end if
    end if
       

    if ( l1 .or. l2 ) then
       write(fid,'(3a)') str,"overlap matrix file            = ",trim(m_folap)
       inquire(file=m_folap,exist=ex)
       if (l2 .and. .not.ex .and. check) stop "  -> file doesn't exist"
    end if

    if ( l1 .or. l2 .or. l3 ) then
      write(fid,'(3a)') str,"dipole matrix file (symmetric) = ",trim(m_fdipsymm)
      inquire(file=m_fdipsymm,exist=ex)
      if (l2 .and. .not.ex .and. check .and. m_LMgauge==m_lgauge) stop "  -> file doesn't exist"

      write(fid,'(3a)') str,"momentum operator file (symmetric) = ",trim(m_fmomsymm)
      inquire(file=m_fmomsymm,exist=ex)
      if (l2 .and. .not.ex .and. check .and. m_LMgauge==m_vgauge) stop "  -> file doesn't exist"
    end if

    if ( l1 .or. l2 ) then
       if (m_ev(1) .or. m_ev(2) ) then
          write(fid,'(3a)') str,"dipole matrix file (hermitian) = ",trim(m_fdipherm)
          inquire(file=m_fdipherm,exist=ex)
          if (l2 .and. .not.ex .and. check) stop "  -> file doesn't exist"
        end if

       if (m_ev(3)) then
          write(fid,'(3a)') str,"momentum operator (hermitian)  = ",trim(m_fmomherm)
          inquire(file=m_fdipacc,exist=ex)
          if (l2 .and. .not.ex .and. check) stop "  -> file doesn't exist"
        end if
          
       if (m_ev(4)) then
          write(fid,'(3a)') str,"dipole accel. matrix file      = ",trim(m_fdipacc)
          inquire(file=m_fdipacc,exist=ex)
          if (l2 .and. .not.ex .and. check) stop "  -> file doesn't exist"
       end if
       
       ! hydrogen-like atoms don't need Coulomb terms
       ! when Coulomb interaction is switched off, files are not required
       if ( m_nelec /= 1 .and. .not.m_nocoulomb ) then
         write(fid,'(3a)') str,"Coulomb matrix file (direct)   = ",trim(m_fcoudir)
         inquire(file=m_fcoudir,exist=ex)
         if (l2 .and. .not.ex .and. check) stop "  -> file doesn't exist"
       
         write(fid,'(3a)') str,"Coulomb matrix file (exchange) = ",trim(m_fcouexc)
         inquire(file=m_fcouexc,exist=ex)
         if (l2 .and. .not.ex .and. check) stop "  -> file doesn't exist"
       end if
    end if
    
    write(fid,'(a)') str
    flush(fid)
  end subroutine printinfo




  subroutine printvariables(ao_mode,ao_fid,ao_str,ao_orb)
    ! print important system variables
    !
    ! INPUT
    ! ------
    !  ao_mode : (in) - optional
    !      = 1 : show max. quantum numbers and occupied orbitals
    !            [default]
    !      = 2 : show max. quantum number only
    !      = 3 : show occupied orbitals only
    !
    !  ao_fid  : (in) - optional
    !            output/file unit [default = screen]
    !  ao_str  : (in) - optional
    !            character/symbol put in front of each line
    !            [default: ao_str = " "]
    !  ao_orb  : (in) - optional
    !            string following ao_str just for the lines that contain the
    !            orbital information. In that way these lines can be easily 
    !            found.
    !
    use VarSystem
    use VarWfct
    implicit none
    integer,intent(in),optional :: ao_mode,ao_fid
    character(*),intent(in),optional :: ao_str,ao_orb

    character(clen) :: str,orb,txt_n
    character(2) :: qn_n
    integer :: qn_n_mode

    logical :: showmax,showocc
    integer :: mode,i,ij,fid
    integer :: n,np,nn,l,j,jid,ml,mj,ms
    complex(dblprec) :: energy


    fid = STDOUT
    if (present(ao_fid)) fid = ao_fid

    str = " "
    if (present(ao_str)) str = trim(adjustl(ao_str))

    orb = " "
    if (present(ao_orb)) orb = trim(adjustl(ao_orb))


    showmax = .false.
    showocc = .false.

    mode = 1
    if (present(ao_mode)) mode = ao_mode
      if (mode<1 .or. mode>3) stop " error(printvar.): invalid mmode"

    if (mode==1 .or. mode==2) showmax = .true.
    if (mode==1 .or. mode==3) showocc = .true.
    if (.not.allocated(m_fmoact)) showocc = .false.
    if (.not.allocated(m_idoccactrev)) showocc = .false.


    if (showmax) then
      write(fid,'(a)') trim(str)
      write(fid,'(2a,/,2a)') trim(str)," Further important variables"              &
                           ,trim(str)," --------------------------------"
      write(fid,'(2a,i5)')  trim(str)," max. system   radial number  = ",m_nmax
      write(fid,'(2a,i5)')  trim(str)," max. system   ang. momentum  = ",m_lmax
      write(fid,'(2a,i5)')  trim(str)," max. occupied radial number  = ",m_nomax
      write(fid,'(2a,i5)')  trim(str)," max. occupied ang. momentum  = ",m_lomax
      write(fid,'(2a,i5)')  trim(str)," max. act. occ. mag. ang. mom.= ",m_moamax
      write(fid,'(2a,i5)')  trim(str)," number of act. occ. orbitals = ",m_ioccact
      write(fid,'(2a,i5)')  trim(str)," number of 1p1h classes       = ",m_i1p1hclass
    end if


    if (showocc) then
      
      qn_n_mode=0
      if (m_orbformat==m_orbformat_p) then
        txt_n = "  -> np=n+l+1 is the principal quantum number" 
        qn_n  = "np"
        qn_n_mode = 1
      elseif (m_orbformat==m_orbformat_r) then
        txt_n = "  -> n =  is the radial quantum number"
        qn_n  = " n"
        qn_n_mode = 2
      end if

      write(fid,'(a)') trim(str)
      if ( .not.m_SOcoupl) then
        write(fid,'(4a,/,2a)')                                                                    &
               trim(str)," occupied orbitals (",qn_n,",l,0,m,energy,act.[T]/froz.[F],act.occ.ID)" &
              ,trim(str)," ---------------------------------------------------------------"
      else
        write(fid,'(4a,/,2a)')                                                                    &
               trim(str)," occupied orbitals (",qn_n,",l,j,mj,energy,act.[T]/froz.[F],act.occ.ID)"&
              ,trim(str)," ----------------------------------------------------------------" 
      end if
      write(fid,'(2a)') trim(str),trim(txt_n)


      lp_i:do i=1,m_ioccall
        n  = m_idoccallrev(i)%n
        l  = m_idoccallrev(i)%l
        j  = m_idoccallrev(i)%j
        ml = m_idoccallrev(i)%ml
        mj = m_idoccallrev(i)%mj
        energy = m_idoccallrev(i)%energy

        jid = (j-2*l+1)/2
        nn = -1
        if ( qn_n_mode==1 ) then
          nn = n+l+1
        else
          nn = n
        end if

        if ( mj < 0 .or. ml<0 ) cycle

        if (m_SOcoupl) then
          write(fid,'(a,2i3,2F5.1,2F13.5,l2,i4)')  trim(str)//trim(orb)//" "  &
              , nn , l , j/TWO , mj/TWO                                       &
              , energy , m_fmoact_so( n, l, (mj-1)/2, jid )                   &
              , m_idoccallrev(i)%idhole 
        else
          write(fid,'(a,4i3,2F13.5,l2,i4)')  trim(str)//trim(orb)//" "        &
              , nn ,l,0,ml,energy                                             &
              , m_fmoact(n,l,ml)                                              &
              , m_idoccallrev(i)%idhole 
        end if
      end do lp_i


      ! display 1p-1h configurations
      write(fid,'(a)') trim(str)
      write(fid,'(2a,/,2a)') trim(str)," 1p-1h config ID : occ. orb. ID , elec. spin" &
                            ,trim(str)," -------------------------------------------"

      do i=1,m_i1p1hclass
        write(fid,'(a,X,i3," :",i3," ,",f5.1)')  trim(str),i,m_id1p1hclassrev(i)%idhole  &
                                    ,m_id1p1hclassrev(i)%ms/TWO
      end do

    end if

    write(fid,'(a)') trim(str)
    flush(fid)
  end subroutine printvariables

end module general
