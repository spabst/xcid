module eom
  !
  !**PRUPOSE
  !  -------
  !  Contains the equation of motion of the TDCIS method
  !
  !**DESCRIPTION
  !  -----------
  !  This module contians the following (sbr) and functions(fct):
  !
  !  -> (sbr) derivative   : calculate the time derivative of the 
  !                          alpha coeffeficient
  !  -> (sbr) action_H0    : calculate  H_0 |wfct> 
  !  -> (sbr) action_lightfield : 
  !                           calculate E(t) z |wfct> or A(t) p_z |wfct>
  !  -> (sbr) coulomb_ph_ph : calculate H_1 |wfct> 
  !                           
  !**AUTHOR
  !  ------
  !  (c) and written by Stefan Pabst 
  !
  !**VERSION
  !  -------
  !  svn info: $Id: EOM.f90 2127 2015-11-11 17:02:15Z spabst $
  !----------------------------------------------------------------------
  !
  use constants
  use omp_lib
  implicit none


contains


  subroutine derivative(a_init0,a_init,a_time,a_final0,a_final,ao_fact)
    !
    ! calculate the time derivative of all time coefficients alpha0,alpha^a_i
    !   init0  , init  <-> alpha_0 , all alpha^a_i
    !   final0 , final <-> d/dt alpha_0, d/dt alpha^a_i
    !
    !  ao_fact : factor multiplied on to a_final
    !  a_time  : current time (needed to determine the current field strength)
    !            -> if m_diagCIS : a_time refers directly to the field strength 
    !
    ! REFERENCE:
    ! ---------
    ! The corresponding equations are described in :
    !   Eqs. (5) in L. Greenman PRA 82, 023406 (2010)
    !
    use varsystem,only : m_nocoulomb,m_diagCIS           &
                        ,m_efield,m_afield,m_propmode    &
                        ,m_LMgauge,m_vgauge,m_lgauge
    use pulse, only    : getpulse
    implicit none


    real(dblprec),intent(in) :: a_time

    complex(dblprec) :: a_init0,a_final0
    complex(dblprec),dimension(:,:,:) :: a_init,a_final

    intent(in) :: a_init0,a_init
    intent(out) :: a_final0,a_final

    complex(dblprec),optional,intent(in) :: ao_fact
    complex(dblprec) :: fact
    real(dblprec) :: field

    fact = ONE
    if ( present( ao_fact) ) fact = ao_fact

    fact = ONE
    if ( present( ao_fact) ) fact = ao_fact

    a_final0 = zero
    a_final  = zero

    ! field strength 
    ! --------------
    if ( m_diagCIS) then
      field = a_time  ! time file contain elec.-mag. field
    else
      ! get current field strength
      field = ZERO
      if ( m_LMgauge == m_lgauge ) then
        field = getpulse(a_time,m_efield)
      elseif ( m_LMgauge == m_vgauge ) then
        field = getpulse(a_time,m_afield)
      end if
    end if 

    ! efield transitions
    ! ------------------

    if ( field /= ZERO ) then
      call action_lightfield( field , a_init0, a_init , a_final0, a_final )
    end if

    ! Coulomb transitions
    ! -------------------
    if ( .not.m_nocoulomb ) then
      call coulomb_ph_ph( a_init0,a_init , a_final0,a_final ) 
    end if

    ! calculate action of H_0
    ! -----------------------
    if ( m_diagCIS .or. m_propmode==3 ) then
      call action_H0( a_init , a_final)
    end if
    
    ! multiplication of a overall factor
    if ( fact /= ONE ) then
      a_final0 = fact * a_final0
      a_final  = fact * a_final
    end if

  end subroutine derivative
  

  subroutine action_H0( a_init , a_final )
    use varsystem, only : m_nmax,m_lmax
    use varwfct, only   : m_i1p1hclass,m_idphr=>m_id1p1hclassrev,m_orbital
    use general, only   : isocc,isact
    implicit none

    integer :: i,n,l
    complex(dblprec) :: de

    complex(dblprec),dimension(0:m_nmax,0:m_lmax,m_i1p1hclass) :: a_final,a_init

    intent(inout) :: a_final
    intent(in)    :: a_init

    do l=0,m_lmax
    do n=0,m_nmax
      ! virtual index is not allowed to refer to occupied orbital
      if ( isocc(n,l) .or. .not.isact(n,l,0) ) cycle  
      forall(i=1:m_i1p1hclass)
        ! orbital energy of the hole: complex conjugte is needed so that 
        ! the hole lifetime (neg. imag. energy) leads to 
        ! configuration lifetime (neg. imag. energy)
        a_final(n,l,i) = a_final(n,l,i)  &
           + a_init(n,l,i) * (m_orbital(n,l)%energy-conjg(m_idphr(i)%energy))
      end forall
    end do
    end do
 
  end subroutine action_H0


  subroutine action_lightfield( a_field , a_init0,a_init , a_final0,a_final )
    !
    !  REF.: all terms involving dipole transitions in Eqs.5 in PRA 82, 023406 (2010)
    !
    implicit none

    real(dblprec), intent(in) :: a_field
    complex(dblprec)          :: a_init0,a_final0
    complex(dblprec),dimension(:,:,:) :: a_init,a_final

    intent(in) :: a_init0,a_init
    intent(inout) :: a_final0,a_final
    ! z_ai terms
    call field_ground_ph( a_field , a_init0,a_init , a_final0,a_final )
    ! z_ab  terms
    call field_virtual(  a_field , a_init0,a_init , a_final0,a_final )
    ! z_ij  terms
    call field_occupied( a_field , a_init0,a_init , a_final0,a_final )
  end subroutine action_lightfield


  subroutine field_ground_ph( a_efield , a_init0,a_init , a_final0,a_final )
    ! ################
    ! # ground state #
    ! ################
    !  + dipole (d)excitation from/(to) the ground state 
    !
    !  REF.: Eq.5a + "z_ai" term of Eq.5b in PRA 82, 023406 (2010)
    !
    use varsystem,only : m_nmax,m_nmaxl,m_lmax,m_SOcoupl,m_nelec   &
                        ,m_LMgauge,m_lgauge,m_vgauge
    use varwfct,only   : m_i1p1hclass,m_idphr=>m_id1p1hclassrev
    use varmatrix,only : m_dipai
    implicit none

    real(dblprec), intent(in) :: a_efield
    complex(dblprec)          :: a_init0,a_final0
    complex(dblprec),dimension(0:m_nmax,0:m_lmax,m_i1p1hclass) :: a_init,a_final

    intent(in) :: a_init0,a_init
    intent(inout) :: a_final0,a_final

    integer :: i,lo,mo,lid,l,n
    integer :: isgn
    complex(dblprec) :: fact
   

    isgn = 0
    if ( m_LMgauge == m_lgauge ) then
      isgn = 1  ! symmetric
    else if ( m_LMgauge == m_vgauge ) then
      isgn = -1 ! antisymmetric
    end if


    !$omp parallel do private(i,lo,mo,l,lid,fact) &
    !$omp default(shared) reduction(+:a_final0)
    gs_occ:do i=1,m_i1p1hclass
       lo=m_idphr(i)%l
       mo=m_idphr(i)%ml

       ! happens w/ spin-orbit: this state can only be reached via Coulomb
       if ( mo>lo ) cycle  

       if (mod(m_nelec,2)==1) then 
         ! hydrogen-like system [1 electron => no sqrt(2) ]
         !  -> electron must be in a m=0 state 
         !     [otherwise +/- M symmetry is broken]
         ! -> odd number of electrons: assume only outermost orbital containing only
         !        one electron is active                         
         fact = -a_efield
       else if (mo==0 .or. m_SOcoupl) then
         fact = -sqrt(TWO) * a_efield
       else
         fact = -TWO * a_efield
       end if
      

       gs_lvirt:do lid=0,1
          l=lo-1+2*lid
          if ( l<0 .or. l>m_lmax ) cycle
          
          ! Eq. (5a)
          ! z_(i,a) needed : m_dipai(n,l,i):=z_(a,i)
          a_final0 = a_final0 + isgn * fact * sum(m_dipai(:,lid,i)*a_init(:,l,i))

          ! Eq. (5b)
          ! z_ai needed
          forall(n=0:m_nmaxl(l))
            a_final(n,l,i) = a_final(n,l,i) + (fact*a_init0)*m_dipai(n,lid,i)
          end forall
       end do gs_lvirt
    end do gs_occ
    !$omp end parallel do
   
  end subroutine field_ground_ph


  subroutine field_virtual( a_efield , a_init0,a_init , a_final0,a_final )
    ! ######################
    ! # dipole transitions #
    ! ######################
    !  + within occupied and within virtual space
    !
    ! REF.: "z_ab" terms of Eq.5b in PRA 82, 023406 (2010)
    !
    use varsystem,only : m_lmax,m_nmaxl,m_nmax,m_nmaxl,m_SOcoupl
    use varwfct, only  : m_i1p1hclass,m_idphr=>m_id1p1hclassrev
    use varmatrix,only : m_dipab,m_mweight
    implicit none

    real(dblprec)    :: a_efield
    complex(dblprec) :: a_init0,a_final0
    complex(dblprec),dimension(0:m_nmax,0:m_lmax,m_i1p1hclass) :: a_init,a_final

    intent(in) :: a_efield,a_init0,a_init
    intent(inout) :: a_final0,a_final


    complex(dblprec) :: vecaux(0:m_nmax)
    real(dblprec) :: fact
    integer :: i,n
    integer :: lo,mo,lid,l,lp
    

    
    ! -z_ab contribution
    !$omp parallel do private(i,lo,mo,l,lid,lp,n,vecaux,fact) &
    !$omp default(shared) schedule(auto) collapse(2)
    ph_dip :do i=1,m_i1p1hclass
      ph_zabl:do l=0,m_lmax

      ph_zablp:do lid=0,1  !<- refers to same a_alpha(:,l,i) [parallization problematic?]
        lp=l-1+2*lid
        if (lp<0 .or. lp>m_lmax) cycle
      
        !lo=m_idphr(i)%l
        mo=m_idphr(i)%ml
      
        fact = -a_efield * m_mweight(max(l,lp),mo)

        !vecaux=ZERO
        !forall(n=0:m_nmaxl(l))
        !  vecaux(n)=sum(m_dipab(:,n,lid,l)*a_init(:,lp,i))
        !end forall
        !a_final(:,l,i) = a_final(:,l,i) + fact * vecaux

        forall(n=0:m_nmaxl(l))
          a_final(n,l,i) = a_final(n,l,i)   &
                         + fact * sum(a_init(:,lp,i)*m_dipab(:,n,lid,l))
        end forall
        
      end do ph_zablp
      
      end do ph_zabl
    end do ph_dip
    !$omp end parallel do

  end subroutine field_virtual


  subroutine field_occupied( a_efield , a_init0,a_init , a_final0,a_final )
    ! ######################
    ! # dipole transitions #
    ! ######################
    !  + within occupied and within virtual space
    !
    ! REF.: "z_ij" terms of Eq.5b in PRA 82, 023406 (2010)
    !
    use varsystem,only : m_lmax,m_nmax,m_nmaxl,m_SOcoupl
    use varwfct, only  : m_i1p1hclass,m_idphr=>m_id1p1hclassrev
    use varmatrix,only : m_dipij,m_mweight
    implicit none

    real(dblprec)    :: a_efield
    complex(dblprec) :: a_init0,a_final0
    complex(dblprec),dimension(0:m_nmax,0:m_lmax,m_i1p1hclass) :: a_init,a_final

    intent(in) :: a_efield,a_init0,a_init
    intent(inout) :: a_final0,a_final

    real(dblprec) :: fact
    integer :: i,j
    integer :: lo,mo
    

    !$omp parallel do default(shared) private(i,j,mo,fact)
    ph_dip :do i=1,m_i1p1hclass
      !lo=m_idphr(i)%l
      mo=m_idphr(i)%ml

      ! z_ji contribution
      ph_zij:do j=1,m_i1p1hclass
        
        ! m must be the same 
        if (mo/=m_idphr(j)%ml) cycle  
        ! SO-coupling: 
        !    virtual orbitals must be the same
        !    m_a,sigma_a <-> m_idphr(3,i),m_idphr(6,i) unchanged
        if (m_SOcoupl .and. m_idphr(i)%ms/=m_idphr(j)%ms) cycle
        if ( m_dipij(j,i)==ZERO ) cycle

        fact = -a_efield * m_dipij(j,i)            

        !!$omp workshare
        a_final(:,:,i) = a_final(:,:,i) - fact * a_init(:,:,j)
        !!$omp end workshare nowait
      end do ph_zij          
    end do ph_dip
    !$omp end parallel do

  end subroutine field_occupied



  subroutine coulomb_ph_ph( a_init0,a_init , a_final0,a_final )
    ! #######################
    ! # Coulomb interaction #
    ! #######################
    !
    ! perform operation
    !
    !   \sum_b,j  <Phi^a_i| H_1 |Phi^b_j> for each a,i configuration
    !
    ! - internal: a=v1 , i=o1 , b=v2 , j=o2
    !
    ! exchange and direct terms are already combined in m_coulomb
    ! no prefactor needed
    !  -> l>lprx : - no direct Coulomb terms exist
    !              - exchange term is approximated with 1/r 
    !                -> spherically symmetric potential
    !                -> hole and m independent potential
    !              - see Eq. 48 in Greenman, PRA 82, 023406 (2011)
    !
    use varsystem,only : m_nmax,m_lmax,m_nmaxl,m_lprx              &
                        ,m_intercoupl,m_intracoupl,m_SOcoupl
    
    use varwfct,only   : m_lomax,m_i1p1hclass,m_1p1h=>m_id1p1hclassrev

    use varmatrix,only : m_coulomb,m_couexcdiag
    implicit none

    complex(dblprec)          :: a_init0,a_final0
    complex(dblprec),dimension(0:m_nmax,0:m_lmax,m_i1p1hclass) :: a_init,a_final

    intent(in) :: a_init0,a_init
    intent(inout) :: a_final0,a_final

    integer :: o1,o2
    integer :: lv1,lv2,lv2id
    complex(dblprec) :: vecaux(0:m_nmax)
    
    ! auxiliary loop variables
    integer :: cpl,n1,n2


    !$omp parallel private(o1,o2,lv1,lv2,lv2id,n1,n2) default(shared) 
    !!$omp parallel do private(o1,o2,lv1,lv2,lv2id,n1,n2) default(shared) &
    !!$omp schedule(dynamic) collapse(2)
    lp_ai: do o1=1,m_i1p1hclass
    do lv1=0,m_lmax  !m_lmax

      if ( lv1 < m_1p1h(o1)%ml )  cycle  ! l<m^L
      
      ! skip empty 1p1h-classes (looks like no speed up)
      !if (.not.m_intercoupl) then 
      !  if ( all(a_init(:,:,o1)==ZERO) ) cycle
      !end if

      if_lprx:if (lv1<=m_lprx) then
        lp_j: do o2=1,m_i1p1hclass
          if (.not.m_intercoupl .and. o1/=o2) cycle
          if (.not.m_intracoupl .and. o1==o2) cycle

          lp_lb:do lv2id=1,m_coulomb(o2,o1)%nlv2
            ! if m_coulomb(j,i)%nlv2>0 means automatically 
            !    m_coulomb(j,i)%matrix is allocated

            lv2 = m_coulomb(o2,o1)%lv2(lv2id,lv1)
            if ( lv2<0 .or. lv2>m_lprx) cycle  
            ! lv2 > lprx should not occur 
            ! but lv2<0 can occur when no further possible lv2 exists
            if ( lv2 < m_1p1h(o2)%ml )  cycle  ! l<m^L


! switch between gpu and intel mkl version of BLAS
!-----------
#ifdef CUBLAS
            ! blas matrix-vector multiplication
            ! a_final = a_final + m_coulomb * a_init
            n1=m_nmaxl(lv1)+1  ! reduce matrix
            n2=m_nmaxl(lv2)+1
            call cublas_ZGEMV( "T",n2,n1,CONE,m_coulomb(o2,o1)%matrix(0:n2-1,0:n1-1,lv2id,lv1),n2  &
                       ,a_init(:,lv2,o2),1,CONE,a_final(:,lv1,o1),1)
#else

            ! equivalent version of calculating the Coulomb action
            ! ------------------------
            ! -> VERSION 1: forall loop
            !vecaux=zero
            !forall(n1=0:m_nmaxl(lv1))
            !  vecaux(n1)=sum( m_coulomb(o2,o1)%matrix(:,n1,lv2id,lv1) * a_init(:,lv2,o2) )
            !end forall
            !a_final(:,lv1,o1) = a_final(:,lv1,o1) + vecaux

            ! -> VERSION 2: BLAS routine
            !call ZGEMV( "T",n2,n1,CONE,m_coulomb(o2,o1)%matrix(0:n2-1,0:n1-1,lv2id,lv1),n2  &
            !           ,a_init(:,lv2,o2),1,CONE,a_final(:,lv1,o1),1)
            
            ! -> VERSION 3: use matmul intrinsic function
            ! use intrinsic matmul routine
            !a_final(:,lv1,o1) = a_final(:,lv1,o1) +  &
            !  matmul(a_init(:,lv2,o2),m_coulomb(o2,o1)%matrix(:,:,lv2id,lv1))

            ! -> VERSION 4: do loop in combination with openmp statements
            !$omp do schedule(static)
            do n1=0,m_nmaxl(lv1)
            !!$omp workshare  <- not as efficient as 'omp do'
            !forall(n1=0:m_nmaxl(lv1))
              a_final(n1,lv1,o1) = a_final(n1,lv1,o1) +  &
                sum( a_init(:,lv2,o2)*m_coulomb(o2,o1)%matrix(:,n1,lv2id,lv1) )
            !end forall
            !!$omp end workshare
            end do
            !$omp end do nowait
#endif  
!-----------
           
          end do lp_lb
        end do lp_j
         

      else if (allocated(m_couexcdiag)) then
        !
        !  l>lprx
        !
        ! exchange Coulomb part (l,lp>lprx - Coulomb approximation)
        !  see Eq. 48 in Greenman, PRA 82, 023406 (2011)
          
#ifdef CUBLAS
        n1=m_nmaxl(lv1)+1  ! reduce matrix
        call cublas_ZGEMV( "T",n1,n1,-CONE,m_couexcdiag(0:n1-1,0:n1-1,lv1),n1  &
                   ,a_init(:,lv1,o1),1,CONE,a_final(:,lv1,o1),1)
#else
        !vecaux=zero
        !forall(n1=0:m_nmaxl(lv1))
        !  vecaux(n1)=sum( m_couexcdiag(:,n1,lv1) * a_init(:,lv2,o2) )
        !end forall
        !a_final(:,lv1,o1) = a_final(:,lv1,o1) - vecaux 

        !a_final(:,lv1,o1) = a_final(:,lv1,o1) -    &
        !  matmul( a_init(:,lv2,o2) , m_couexcdiag(:,:,lv1) )
        
        !$omp do schedule(static)
        do n1=0,m_nmaxl(lv1)
          a_final(n1,lv1,o1) = a_final(n1,lv1,o1) -    &
            sum( a_init(:,lv1,o1) * m_couexcdiag(:,n1,lv1) )
        end do
        !$omp end do nowait
#endif  
!-----------
          
      end if if_lprx
    end do
    end do lp_ai
    !!$omp end parallel do
    !$omp end parallel 

  end subroutine coulomb_ph_ph

end module eom
