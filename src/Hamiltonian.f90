module Hamiltonian
  !
  !**PURPOSE
  !  -------
  !  The MODULE Hamiltonian contains all necessary routines and function to 
  !  build the Hamiltonian
  !
  !**DESCRIPTION
  !  -----------
  !  The following subroutines (sbr) and functions (fct) are part of 
  !  this module:
  !
  !  -> (sbr) buildH(fd,fda) : build the system Hamiltonian
  !  -> (sbr) buildT(fd,fda) : build radial kinetic Operator
  !  -> (sbr) buildU(fd,fda) : build local potential + angular potential
  !  -> (sbr) buildV(fd,fda) : mean-field potential
  !
  !**AUTHOR
  !  ------
  !  written by Stefan Pabst in April 2011
  !
  !**VERISON
  !  -------
  !  svn info: $Id: Hamiltonian.f90 1660 2015-05-11 21:31:57Z spabst $
  !-----------------------------------------------------------------------
  !
  use constants, dp=>dblprec
  use varwfct, only : t_orbital
  use grid_type
  implicit none


contains

  function buildH(a_l,ao_orb,ao_z,ao_fac,ao_cap,ao_grid) Result(ham)
    ! Build the Hamiltonian for a given angular momentum up to
    ! a given maximum radius (doesn't need to be maximum system radius)
    !
    ! Hamiltonian is defined in Eq. 31,47 (mode detailed) in
    ! Greenman, PRA 82, 023406 (2011)
    !
    ! INPUT
    ! -----
    !  a_l      - angular momentum block
    ! ao_orb   - set of occupied orbitals
    !            if not given, global m_orbital from varwfct is used
    !            to get set of occupied orbitals
    ! ao_z      - nuclear charge
    ! ao_fac    - weight of electron-electron interaction
    ! ao_cap    - CAP strength
    ! ao_grid   - grid object that defines the underlying spatial grid
    !
    ! OUTPUT
    ! ------
    ! a_matrix - calculated Hamiltonian
    !
    use varsystem, only : m_nocoulomb,m_coulombmode   &
                         ,m_nuccharge,m_nelec         &
                         ,m_cap_strength,m_grid
    use varwfct,only   : m_ioccnl,m_orbital,m_occnl
    use varmatrix, only: m_fem_ke,m_fem_u
    use fem_hamiltonian, only : buildVFEM
    use reduce_lapack, only :   reduce_decomposed
    use grid_type

    use, intrinsic :: ieee_arithmetic

    implicit none

    complex(dp),allocatable :: ham(:,:)

    real(dp),intent(in),optional :: ao_z,ao_fac,ao_cap
    integer,intent(in) :: a_l
    type(t_orbital),intent(in),optional,target :: ao_orb(:)
    type(t_grid), intent(in),optional,target :: ao_grid

    complex(dp),allocatable :: diag(:),ndiag(:,:)
    type(t_orbital),pointer :: orb(:)=>null()
    type(t_grid), pointer :: grid => null()    
    real(dp) :: z,fac,cap
    
    integer :: i,j,nelec,nbasis
    integer :: no,lo

    logical :: err


    err = .false.


    ! Hartree-Slater (nocoulomb must be set to .false.)
    ! when building the Hamiltonian
    if ( m_coulombmode=="slater" )  m_nocoulomb=.false.


    ! optional arguments
    ! ------------------
    fac = ONE
    if ( present(ao_fac) ) fac = ao_fac
    z = m_nuccharge
    if ( present(ao_z) ) z = ao_z
    cap = m_cap_strength
    if ( present(ao_fac) ) cap = ao_cap

    ! get grid object
    if ( present(ao_grid) ) then
      grid => ao_grid
    else if ( present(ao_orb) ) then
      grid => ao_orb(1)%grid
    else if ( associated(m_orbital(0,0)%grid) ) then
      grid => m_orbital(0,0)%grid
    else
      grid => m_grid
    end if

!    if ( grid%type == "DVR") grid =>grid%DVR_grid
    nbasis = grid%nbasis

    ! allocate matrices
    ! ----------------------------
    allocate(ham(nbasis,nbasis))
    ham = ZERO
    allocate(diag(nbasis))
    diag = ZERO 

    ! ----------------------
    ! | build kinetic part |
    ! ----------------------
    ! -> 1. line of Eq. 47
    select case(grid%type)
    case("PSG")
      ham = buildT(a_l,grid)
    case("FEM")
      ham = m_fem_ke(1:nbasis,1:nbasis)
    case("DVR")
      ham = m_fem_ke(1:nbasis,1:nbasis)
    end select


    ! --------------------------
    ! | add diagonal potential |
    ! | 1-particle Hamiltonian |
    ! --------------------------
    ! -> 2. line of Eq. 47
    select case(grid%type)
    case("PSG")
      diag = buildU(z,cap,grid)
    case("FEM")
      deallocate(diag)
      ham = ham + m_fem_u(1:nbasis,1:nbasis,a_l)
    case("DVR")
      deallocate(diag)
!      diag = buildU(z,cap,grid)
      ham = ham + m_fem_u(1:nbasis,1:nbasis,a_l)
    end select

    ! find number of electrons in the system
    nelec = m_nelec
    if ( present(ao_orb) ) then
      nelec = sum(ao_orb(:)%occ)
    end if

    ! --------------------------------
    ! | residual Coulomb interaction |
    ! --------------------------------
    if_meanfield:if ( fac/=ZERO  .and. .not.m_nocoulomb   &
         .and. m_ioccnl>0 .and. nelec>1) then
      
      if (present(ao_orb)) then
        orb => ao_orb
      else if ( allocated(m_orbital) ) then
        allocate(orb(m_ioccnl))
        do i=1,m_ioccnl
          no=m_occnl(i,1)
          lo=m_occnl(i,2)
          if (.not.allocated(m_orbital(no,lo)%wfct))   &
              stop " error(buildH): orbital wavefunctions are not initialized"
          orb(i) = m_orbital(no,lo)
        end do     
      end if

      ! add Coulomb interaction
      ! -> 3. - 5. line of Eq. 47
      if_slater:if ( m_coulombmode=="slater" ) then
        if ( grid%type=="FEM" )  stop " ERROR(buildH): Hartree-Slater is not implemented for FEM"

        allocate(ndiag(nbasis,nbasis))

        ! latter correction
        do i=1,nbasis
          ndiag(i,i) = ndiag(i,i) + diag(i)
        end do
        call latter_correction(ndiag,grid)

        ! assign
        ham = ham + fac * ndiag

        deallocate(diag)  ! is already in ndiag
        deallocate(ndiag)
      
      else
      
        select case(grid%type)
        case("PSG")
          ham = ham + fac * buildV(a_l,orb)
        case("FEM")
          allocate(ndiag(nbasis,nbasis))
          ndiag = ZERO

          call buildVFEM( ndiag , a_l, orb, grid )
          ham = ham + fac * ndiag

          deallocate(ndiag)
        case("DVR")
          ham = ham + fac * buildV(a_l, orb)
        end select
      
      end if if_slater
    end if if_meanfield


    ! add diagonal part
    if ( allocated(diag) ) then
      forall(i=1:nbasis)  ham(i,i) = ham(i,i) + diag(i) 
    end if


    
    ! FEM only: transform (also named "reduce") FEM basis to an orthogonal basis
    !           in order to use standard diagionalization routines instead 
    !           of a generalized one where overlap matrix is not the unity matrix
    if ( grid%type=="FEM" ) then
      !call reduce_decomposed( ham , m_fem_ovup(1:nbasis,1:nbasis) )
    end if

  end function buildH



  function buildT(a_l,a_grid) Result(mat)
    !
    ! generate the kinetic operator (incl. angular potential)
    ! as defined in line 1 in Eq. 47 in Greenman, PRA 82, 023406 (2011)
    !
    !  T = d^2_r + l(l+1)/2r**2 
    !
    ! INPUT
    ! -----
    !  a_l    - angular momentum
    !  a_grid - grid object
    !
    use grid_type
    implicit none

    complex(dp),allocatable  :: mat(:,:)
    type(t_grid),intent(in),target :: a_grid
    integer, intent(in)     :: a_l
    
    real(dp) :: fac,zeta,rmaxsys

    ! ecs_f : - is derivative of the mapping function from the 
    !           complex-scaled path to the solely real r-path 
    !         - has to be applied before and after the original kinetic operator
    !  rp   : inverse derivatives of all coordinate trafos applied before and 
    !         after the kinetic operator
    complex(dp),allocatable :: rp(:), ecs_f(:)  
    complex(dp), pointer :: r(:)=>null()

    integer :: i,j,nbasis,nbasissys

  
    ! init grid object
    zeta  = a_grid%zeta
    nbasis = a_grid%nbasis
    if ( associated(a_grid%parent) ) then
      nbasissys = a_grid%parent%nbasis
      rmaxsys  = a_grid%parent%rmax
    else
      nbasissys = a_grid%nbasis
      rmaxsys  = a_grid%rmax
    end if
    
    fac = rmaxsys/TWO * zeta   

    
    ! allocate resulting matrix
    allocate(mat(nbasis,nbasis))
    mat = ZERO

    ! CAP or ECS
    ! ----------
    allocate( ecs_f(nbasis))
    ecs_f = ONE   ! no mapping 
    if ( a_grid%absorbing == "ECS" ) then
      ecs_f = a_grid%cs%F(:,1)
      r => a_grid%cs%F(:,0)
    else
      allocate(r(nbasis))
      r = a_grid%r
    end if


    ! rp(x) = 1 / r'(x) / f(x)
    !  -> r'(x)     : derivative of the mapping between the real-r path and 
    !                 the [-1:1] region for Gauss-Lobatto
    !  -> ecs_f(x)  : derivative of the mapping between the complex-r path and
    !                 the real r-path ( only needed when complex scaling is used)
    allocate(rp(nbasis))
    rp = ZERO
    forall(i=1:nbasis)   
      rp(i) =  (ONE-a_grid%root(i)+zeta)**2  / (fac*(TWO+zeta)) / ecs_f(i) 
    end forall


    ! off-diagonal part  
    ! ------------------
    forall(i=1:nbasis,j=1:nbasis,i/=j)
      mat(j,i) = rp(i)*rp(j) / (a_grid%root(i)-a_grid%root(j))**2  
    end forall


    ! diagonal part
    !  -> incl. angular potential
    ! ----------------------------
    fac = (nbasissys+1)*(nbasissys+2)
    forall(i=1:nbasis)
      mat(i,i) = fac * rp(i)**2 / (6.D0 * (1-a_grid%root(i)**2))   &
                 + HALF*a_l*(a_l+1)/r(i)**2
    end forall 
  end function buildT



  function buildU(a_z,a_cap,a_grid) Result(mat)
    ! generate local potential part of the Hamiltonian,
    ! which consists of the nuclear potential and the complex absorbing 
    ! potential
    !
    !   U = -Z/r                          , r<Rabsorb
    !     = -Z/r - i cap * (r-Rabsorb)**2 , r>Rabsorb
    !
    ! As defined in line 2 in Eq. 47 in Greenman, PRA 82, 023406 (2011)
    !
    ! INPUT
    ! -----
    !  z  - nuclear charge
    ! cap - CAP strength
    ! grid- grid object
    !
    use varsystem,only : m_Rabsorb,m_mp => m_modelpot
    use grid_type
    use splineinterpol, only : spline => eval_spline_object
    implicit none
    
    complex(dp),allocatable :: mat(:)

    real(dp),intent(in) :: a_z,a_cap
    type(t_grid),intent(in) :: a_grid

    integer :: i,nbasis
    complex(dp), allocatable :: r(:,:)  ! local assignment of r


    nbasis = a_grid%nbasis

    ! set up matrix
    allocate(mat(nbasis))
    mat = ZERO

    ! CAP or ECS
    ! ----------
    allocate(r(nbasis,0:3))
    r = ZERO
    if ( a_grid%absorbing == "ECS" ) then
      r = a_grid%cs%F
    else
      r(:,0) = a_grid%r
      r(:,1) = ONE
      r(:,2:3) = ZERO
    end if


    if ( m_mp%active ) then
      ! model potential
      ! ---------------
      !  -> multiply r/F(r) on model potential (/= 1 only for complex scaling)
      !  -> assuming the complex scaling starts where model potential
      !     has the behavior ~ 1/r [substitude with 1/F(r) for complex scaling]
      !
      forall(i=1:nbasis)  mat(i) = spline(a_grid%r(i),m_mp) * (a_grid%r(i)/r(i,0))
      
      !!print model potential
      !open(30,file="modelpotential.info")  
      !do i=1,nbasis
      !  write(30,*) a_grid%r(i),spline(a_grid%r(i),m_mp)
      !end do
      !close(30)
    else 
      ! nuclear potential
      ! -----------------
      mat = -a_z/r(:,0)
    end if


    ! Complex Absorbing region
    ! -------------------------
    if ( a_grid%absorbing == "CAP" ) then   
      ! CAP
      ! ----
      do i=1,nbasis
        if ( abs(r(i,0)) < m_Rabsorb ) cycle
          mat(i) = mat(i) - IMG*a_cap * (r(i,0)-m_Rabsorb)**2
      end do
    else if ( a_grid%absorbing == "ECS" ) then   
      ! complex scaling
      ! ---------------    
      mat = mat - (TWO*r(:,3)*r(:,1) - THREE*r(:,2)**2) / (8.D0*r(:,1)**4)
    end if
  end function buildU



  function buildV(a_l,a_orb) result(mat)
    ! generate mean-field potential defined by
    ! occupied orbitals given in orbital as defined in 
    ! Eq. 47 in Greenman, PRA 82, 023406 (2011)
    !
    ! INPUT
    ! -----
    !  a_V   - direct and exhange part of the mean-field potential
    !  a_orb - set of occupied orbitals
    !   l  - angular momentum of sub Hamilton matrix
    !
    use VarSystem, only : m_coulombmode
    implicit none

    complex(dp),allocatable :: mat(:,:)
    type(t_orbital),intent(in) :: a_orb(:)

    integer,intent(in) :: a_l
    complex(dp), allocatable :: aux(:)
    integer :: i,nbasis


    ! check information 
    if ( size(a_orb) < 1 )  then
      ! no occupied orbital informations
      stop " ERROR(buildV): no orbital information present"
    end if
    nbasis = a_orb(1)%grid%nbasis
    if ( nbasis<1 )   stop " ERROR(buildV): no. of grid points is <1"

    allocate(mat(nbasis,nbasis))
    mat = ZERO
    allocate(aux(nbasis))
    aux = ZERO

    ! 3. line of Eq. 47 in Greenman, PRA 82, 023406 (2011)
    aux = MFdirect(a_orb)

    if ( m_coulombmode=="slater" ) then
      aux = aux + MFexchange_slater(a_orb)
    else
      ! 4.-5. line of Eq. 47 in Greenman, PRA 82, 023406 (2011)
      mat = MFexchange(a_orb,a_l)
    end if
      
    ! add diagonal
    do i=1,nbasis
      mat(i,i) = mat(i,i) + aux(i)
    end do
  end function buildV


  function MFdirect(a_orb) Result(mat)
    !
    ! generate direct part of mean-field potential
    ! as defined in 3. line of Eq. 47 in Greenman, PRA 82, 023406 (2011)
    !
    ! mfdi(r) = sum_r'  2/(nbasis+1)/(nbasis+2) 1/max(r,r') 
    !            * sum_i (4*l_i+2) |A^r'_i|
    !
    implicit none
    complex(dp),allocatable :: mat(:),aux(:)
    type(t_orbital),intent(in) :: a_orb(:)

    complex(dp),allocatable :: rg(:,:)
    real(dp) :: weight,occ
    integer :: nbasis,nbasissys
    complex(dp), pointer :: r(:)=>null()

    integer :: i,j,lo
  

    ! check information 
    if ( size(a_orb) < 1 )  then
      ! no occupied orbital informations
      stop " ERROR(MFdirect): no orbital information present"
    end if

    ! grid informations
    nbasis = a_orb(1)%grid%nbasis
    if ( nbasis<1 )   stop " ERROR(MFdirect): no. of grid points is <1"
    nbasissys = nbasis
    if (associated(a_orb(1)%grid%parent)) nbasissys=a_orb(1)%grid%parent%nbasis
    
    if ( a_orb(1)%grid%absorbing == "ECS" ) then
      r => a_orb(1)%grid%cs%F(:,0)
    else
      allocate(r(nbasis))
      r = a_orb(1)%grid%r
    end if
    
    ! set up matrix
    allocate(mat(nbasis))
    mat = ZERO

    ! Gaus-Lobatto weight
    !weight = TWO / ( (nbasissys+1)*(nbasissys+2) )
    weight = ONE/a_orb(1)%grid%norm/a_orb(1)%grid%norm
    

    allocate(aux(nbasis),rg(nbasis,nbasis))
    aux = ZERO
    rg = ZERO

    ! 1/r>
    forall(i=1:nbasis,j=1:nbasis) 
      rg(j,i) = ONE/r(max(i,j))
    end forall
    
    
    orbital:do i=1,size(a_orb)
      lo  = a_orb(i)%l
      occ = a_orb(i)%occ/TWO /(TWO*lo+ONE)
      if ( occ==0 )  cycle

      forall(j=1:nbasis)
        aux(j) = TWO*sum(a_orb(i)%wfct(1:nbasis)**2 * rg(1:nbasis,j) )  
      end forall

      mat = mat + aux*occ * (TWO*lo+ONE)*weight
    end do orbital

  end function MFdirect



  function MFexchange(a_orb,a_l) Result(mat)
    !
    ! generate exchange part of mean-field potential
    ! as defined in Eq. 47 in Greenman, PRA 82, 023406 (2011)
    !
    use varwfct,only : m_ioccnl,m_lomax
    use angmom,only : cleb
    implicit none
    
    complex(dp), allocatable :: mat(:,:)
    integer,intent(in) :: a_l
    type(t_orbital) :: a_orb(:)
    
    real(dp) :: weight

    complex(dp) :: rs,rg
    complex(dp), pointer :: r(:)=>null()
    integer :: nbasis,nbasissys

    complex(dp),allocatable :: aux(:)
    real(dp), allocatable :: clebsch(:,:)
    integer, allocatable :: lc(:,:)

    integer :: ilc
    integer :: nocc,i,j,k,lo

    
    ! check information 
    if ( size(a_orb) < 1 )  then
      ! no occupied orbital informations
      stop " ERROR(MFdirect): no orbital information present"
    end if

    ! grid informations
    nbasis = a_orb(1)%grid%nbasis
    if ( nbasis<1 )   stop " ERROR(MFdirect): no. of grid points is <1"
    nbasissys = nbasis
    if (associated(a_orb(1)%grid%parent)) nbasissys=a_orb(1)%grid%parent%nbasis
    if ( a_orb(1)%grid%absorbing == "ECS" ) then
      r => a_orb(1)%grid%cs%F(:,0)
    else
      allocate(r(nbasis))
      r = a_orb(1)%grid%r
    end if
    
    ! set up matrix
    allocate(mat(nbasis,nbasis))
    mat = ZERO    

    ! Gaus-Lobatto weight
!    weight = TWO / ( (nbasissys+1)*(nbasissys+2) )
    weight = ONE/a_orb(1)%grid%norm/a_orb(1)%grid%norm
    !weight = a_orb(1)%grid%norm

    ! auxil. stuff
    nocc = size(a_orb)
    allocate(aux(nocc))
    aux = ZERO

    allocate(clebsch(m_lomax+1,nocc),lc(m_lomax+1,nocc))
    clebsch = ZERO
    lc = -1


    ! set up clebsch-gordon coefficient
    do i=1,nocc
      lo = a_orb(i)%l
      do ilc=1,m_lomax+1
        lc(ilc,i) = a_l + lo - 2*(ilc-1)
        if ( lc(ilc,i)<0 ) cycle  ! no negative ang. momentum
        clebsch(ilc,i) = cleb(2*a_l,0, 2*lc(ilc,i),0, 2*lo,0)**2
      end do
    end do


    ! set up r,r' matrix
    do j=1,nbasis
      do k=1,j
        
        ! define r<,r>
        rg=r(j) 
        rs=r(k)

        ! sum over L=|lo-a_l|,lo+a_l
        !  -> weight by orbital occupation
        forall( i=1:nocc , a_orb(i)%occ>1 )
          aux(i) = sum(clebsch(:,i)*(rs/rg)**lc(:,i))/rg * a_orb(i)%occ/(FOUR*a_orb(i)%l+TWO) 
        end forall

        ! sum over all occupied states
        do i=1,nocc
          mat(j,k) = mat(j,k) - a_orb(i)%wfct(j)*a_orb(i)%wfct(k)*aux(i) * weight
        end do
        mat(k,j) = mat(j,k)
      end do
    end do
  end function MFexchange
 


  function MFexchange_slater(a_orb) Result(mat)
    !
    ! generate Slater exchange potential 
    ! as defined in 3. line of Eq. 47 in Greenman, PRA 82, 023406 (2011)
    !
    ! U_ex(r) = -3/2 * [3/pi * rho(r)]**1/3
    !
    implicit none
    complex(dp),allocatable :: mat(:)
    type(t_orbital), intent(in) :: a_orb(:)

    integer :: nbasis,nbasissys
    complex(dp), pointer :: r(:)=>null()

    real(dp) :: weight,occ,aux
    integer :: i,j,lo
  
    
    ! check information 
    if ( size(a_orb) < 1 )  then
      ! no occupied orbital informations
      stop " ERROR(MFdirect): no orbital information present"
    end if

    ! grid informations
    nbasis = a_orb(1)%grid%nbasis
    if ( nbasis<1 )   stop " ERROR(MFdirect): no. of grid points is <1"
    nbasissys = nbasis
    if (associated(a_orb(1)%grid%parent)) nbasissys=a_orb(1)%grid%parent%nbasis
    if ( a_orb(1)%grid%absorbing == "ECS" ) then
      r => a_orb(1)%grid%cs%F(:,0)
    else
      allocate(r(nbasis))
      r = a_orb(1)%grid%r
    end if
    
    ! set up matrix
    allocate(mat(nbasis))
    mat = ZERO

    ! Gaus-Lobatto weight
    !weight = TWO / ( (nbasissys+1)*(nbasissys+2) )
    weight = ONE/a_orb(1)%grid%norm/a_orb(1)%grid%norm
        
    orbital:do i=1,size(a_orb)
      lo = a_orb(i)%l
      occ = a_orb(i)%occ/TWO /(TWO*lo+ONE)
      if ( occ==0 )  cycle

      aux = TWO*(TWO*lo+ONE)/FOUR/pi
      do j=1,nbasis
        mat(j) = mat(j) + (aux*occ)*a_orb(i)%wfct(j)**2
      end do
    end do orbital

    mat = -THREE/TWO * (THREE/pi*mat)**(ONE/THREE) &
          * weight
  end function MFexchange_slater


  subroutine latter_correction(a_mat,a_grid)
    implicit none
    complex(dp), intent(inout) :: a_mat(:,:)
    type(t_grid), intent(in) :: a_grid

    integer :: i,i2,dim

    
    if ( size(a_mat,2)==1 ) then
      dim=1
    else if ( size(a_mat,1)==size(a_mat,1) ) then
      dim=2
    else
      stop " ERROR(latter_corr.): uncompatible size"
    end if

    if ( size(a_mat,1)/=a_grid%nbasis )  &
      stop " ERROR(latter_corr.): size inconsistency"

    do i=1,a_grid%nbasis
      i2=1
      if (dim==2)  i2=i

      if ( dble(a_mat(i,i2))>-ONE/a_grid%r(i) ) then
        a_mat(i,i2)=-ONE/a_grid%r(i)
      end if
    end do
  end subroutine latter_correction

end module Hamiltonian
