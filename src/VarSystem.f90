module VarSystem
  !
  !**PURPOSE
  !  -------
  !  Collection of global variables, excluding variables directly related
  !  to the wavefunction [VarWfct] and/or operators [VarMatrix].
  !
  !**AUTHOR
  !  ------
  !  (c) and written by Stefan Pabst
  !
  !**VERSION
  !  -------
  !  svn info: $Id: VarSystem.f90 1748 2015-06-03 02:22:53Z spabst $
  !-----------------------------------------------------------------------
  !
  use constants, only : dp=>dblprec,clen
  use splineinterpol, only : t_spline
  use pulse_type
  use grid_type
  use sphericalharmonics, only : t_spherical
  implicit none
  

  ! ---------
  ! | TYPES | 
  ! ---------
  
  type t_backup
    ! file   : file name containing last backup (time point + wave function)
    character(clen) :: file_read="", file_write=""
    
    ! write  : store alpha coefficients in backup file (overwrite previous backup images)
    ! read   : read stored alpha coefficients from backup file (overwrite old one)
    ! actorbfile : use active occupied orbitals from the file  ( = true  )
    !                                       from the input ( = false )
    ! append : append output (=true) otherwise overwrite file (= false)
    logical :: read = .false.,write = .false.,actorbfile=.false.
    logical :: append = .false.
    
    ! dt     : time step of backup [+ final time step]
    real(dp) :: dt = ZERO
    ! time   : time point of the backup
    real(dp) :: time = ZERO
  end type t_backup



  ! -------------------
  ! -------------------
  ! ||   Variables   ||
  ! -------------------
  ! -------------------


  ! Program
  ! ---------
  ! m_program : information about which program is running
  !  =-1 : unassigned
  !  = 0 : Hartree-Fock
  !  = 1 : Matrix elements
  !  = 2 : Time-propagation
  !  = 3 : Density/Correlation program
  integer :: m_program = -1
  ! m_svnrev  : keeps the svnrev number of the current status of the package
  character(clen) :: m_svnrev=""

  ! System
  ! ----------------
  ! m_nElec : number of electrons in the system
  !           = 1 : hydrogen-like atom is assumed, meaning sqrt(2) in 
  !                 equation of motion is dropped
  
  ! m_nuccharge : charge of nuclei 
  !             (can be tuned artifically to non-integer number)
  integer,save  :: m_nElec=-1
  real(dp),save :: m_nuccharge=2

  ! m_rmax  : system radius (max. radius) [atomic units]
  ! m_zeta  : r(x) ~ (1+x)/(1-x+zeta)
  real(dp),save :: m_rmax = 50.D0    &
                  ,m_zeta = 0.5D0  
  
  ! m_npoints     : no. of radial points of the overall grid
  ! m_fem_nintpts : no. of radial points for FEM integration
  integer, save :: m_npoints = 300  &
                  ,m_fem_nintpts = 20

  ! m_grid_type  :  type of grid 
  !                 = "FEM" : FEM grid
  !                 = "PSG" : pseudo spectral grid method (default)
  character(clen), target, save :: m_grid_type=""

  ! m_grid  : system-wide grid 
  type(t_grid),save,target :: m_grid

  ! m_grid_files : contains file names of grids stored in m_grids
  logical, save :: m_grid_rdfiles = .false.
  character(clen),target,save :: m_grid_files=""


  ! angular information
  ! --------------------
  ! spherical harmonics object 
  !  -> contains angular grid and spherical harmoncis Y^l_m(theta,phi)
  type(t_spherical) :: m_angular
  


  ! local potential
  ! ---------------
  ! local potential in Hamiltonian substituting the exact electron-electron
  ! and switches off the Coulomb interaciont (nocoulomb=.false.)
  type(t_spline),save :: m_modelpot


  ! Absorbing
  ! ---------
  ! m_absorb: method used to absorb wavefunction at boundary
  !           = "CAP" : use CAP [default]
  !           = "ECS" : use smooth exterior complex scaling
  ! m_Rabsorb : start radius of the complex absorbing potential (CAP)
  !             position exterior complex scaling begins
  !             [atomic units]
  ! m_cap_strength : strength of CAP
  ! m_cs_angle     : angle of complex scaling
  ! m_cs_smooth    : smoothness of exterior complex scaling 
  character(clen),save :: m_absorb="CAP"
  real(dp),save   :: m_Rabsorb=40.D0         &
                    ,m_cap_strength = 3.D-3  &
                    ,m_cs_angle = 45.D0      &
                    ,m_cs_smooth = ONE
  ! m_absorb       : "SPL", uses splitting function for the absorption
  ! m_split_r      : radius where the splitting function is rising to 1 (R_c)
  ! m_split_smooth : smoothness of splitting (Delta)
  ! m_split_dt     : time step where the splitting is performed
  real(dp)        :: m_split_r
  real(dp)        :: m_split_smooth
  real(dp)        :: m_split_dt   
  logical         :: m_split_write

  ! T-Surf method for calculating the electron spectrum
  ! ----------------------------------------------------
  ! m_tsurf_r  : radius of t-surf sphere (reajusted to closed grid point)
  !! m_tsurf_ir : grid point corresponding to m_tsurf_r 
  logical, save  :: m_tsurf   = .false.
  real(dp),save  :: m_tsurf_r = 0.0
  !integer,save   :: m_tsurf_ir= -1


  ! HF-specific
  ! ----------------------
  ! m_niter   : max. number of iteration (-1 : no limit)
  ! m_hfstep  : weight of suggested orbital set when updating orbitals 
  !             within HF iteration
  ! m_convE   : energy convergence criterion
  ! m_convWF  : wavefunction convergence criterion
  integer,save  :: m_niter = 50
  real(dp),save :: m_hfstep = 0.8D0    &
                  ,m_convWF = 1.D-10   &
                  ,m_convE  = 1.D-08
  

  ! grid information of SCF procedure (can differ from system grid)
  ! SCF does not need large radii as needed for HHG runs. In which way the grid
  ! is reduced is defined in m_gridsscf.
  !
  ! m_rmaxscf : max. radius of SCF grid (used for finding occupied HF orbitals)
  real(dp),save  :: m_rmaxscf = 20.D0

  !
  ! for high L vlaues the diagonalization can run into troubles
  ! this can be prevented by ignoring small radii where l(l+1)/r^2 is becomes 
  ! large
  ! m_rminHighL: min. radius that is consider by the diagonalization of H
  !              for L>m_rminLmin
  ! m_rminHighL:  mininimum L where the region r<m_rmaxHighL is ignored
  real(dp),save ::  m_rminHighL = 5.D-2
  integer,save  ::  m_LminHighL = 80


  ! cut-off 
  ! ------------------
  ! m_nmax : max. radial quantum number in the system (defined via ecut)
  ! m_nmaxl: max. radial quan. number for each ang. mom. L
  ! m_lmax : total max. ang. mom. (defined via input)
  ! m_lprx : beyond this ang. mom. Coulomb approx. (1/r) is used
  ! m_lsum : max. ang. mom. change of Coulomb operator
  integer :: m_nmax,m_lmax,m_lprx,m_lsum
  integer, allocatable :: m_nmaxl(:)
  ! m_llimit1p,m_llimit2p : lower limit of matrix element magnitude
  ! m_ecut  : max. orbital energy
  real(dp) :: m_llimit1p,m_llimit2p
  real(dp) :: m_ecut 



  ! time propagation 
  ! ---------------------------
  ! m_tmax : time to be propagated  [atomic units]
  ! m_dt   : propagation time steps [atomic units]
  ! m_dtprint  : time step of storing exp. val., density matrix,... to file
  ! m_propmode : propagation scheme (time differencing,Runge-Kutta 4,...)
  !        = 0 : Euler method
  !        = 1 : improved Euler method
  !        = 11: 2nd order finiti differencing
  !        = 2 : 4th order Runge-Kutta
  real(dp) :: m_tmax,m_dt
  real(dp) :: m_dtprint   
  integer  :: m_propmode

  ! for Lanczos-Arnoldi propagation rank of hessenberg matrix
  integer  :: m_lanev

  ! --------------
  ! INPUT / OUTPUT
  ! --------------

  ! backup for wave function during time propagation
  type(t_backup),save :: m_backup

  ! m_fout        : root file name of output files generated during 
  !                 the time propagation
  !                 only file suffix ending changes (.ev , .idm.1 , .wfct, etc.)
  character(LEN=clen) :: m_fout
  
  ! UNIT/Pipe/file ID
  ! ------------------
  ! UNIT = 999    : file ID for wavefunction backup during propagation
  ! m_fIDev       : file ID of file containing exp. val. (*.ev)
  ! m_fIDrho1     : file ID of file containing corrected IDM (*.idm.1)
  ! m_fIDrho2     : file ID of file containing IDM correction (*.idm.2)
  ! m_fIDrho2l+XX : file ID of files containing L-resolved IDM correction (*.idm.2_LXXX)
  ! m_fIDwfct     : file ID of file containing alpha coefficients (*.wfct)
  ! m_fIDtsurf    : file ID of file containing tsurf information (*.tsurf)
  integer,parameter :: m_fIDev       = 1000    &
                      ,m_fIDrho1     = 1001    &
                      ,m_fIDrho2     = 1002    &
                      ,m_fIDwfct     = 1003    &
                      ,m_fIDwfctsplit= 1004    &       
                      ,m_fIDtsurf    = 1200    &       
                      ,m_fIDcorr     = 1010    &
                      ,m_fIDrho2l    = 1100       

  ! file names (= m_fout + ending)
  ! m_fdens  : root of file name of spatial density  (name = m_fdens_T*)
  character(clen) ::  m_fev        = ""   &
                     ,m_frho1      = ""   &
                     ,m_frho2      = ""   &
                     ,m_fwfct      = ""   &
                     ,m_fwfctsplit = ""   &
                     ,m_ftsurf     = ""   &
                     ,m_fcorr      = ""   &
                     ,m_fdens      = ""   &
                     ,m_fpes       = ""   

  ! switches
  ! m_storeEV  : list of expectation values that needs to be calculated
  !              elements can be: "z","zion","zacc","mom"
  !              elements are separatede by ":"
  ! m_ev       : logical switches determine if expectation value should be 
  !              calculated
  !              m_ev(1) = dipole moment
  !              m_ev(2) = ionic dipole moment
  !              m_ev(3) = momentum operator
  !              m_ev(4) = dipole acceleration
  character(clen),save :: m_storeEV = "z:zion:zacc:mom"
  logical,save :: m_ev(4) = .false.  
  ! m_storeIDMcapL: store detailed CAP absorption (ang. mom. resolved)
  !                 in  corresponding file
  ! m_storeWfct   : store alpha coeffcient in corresponding file

  logical,save :: m_storeWfct = .false.       &
                 ,m_storeIDMcapL = .false.
  
  ! m_corrfct     : calculate correlation function <HF|zG(t,0)z|HF>
  logical,save :: m_corrfct = .false.
  
  ! m_probe    : do a probe step i*m_probe_E * z|Psi(t_init)> 
  !              at the beginning of propagation
  ! m_probe_E  : strength of probe step
  logical,save   :: m_probe = .false.
  real(dp),save  :: m_probe_E = ZERO
  

  ! Diagonalize CIS Hamiltonian
  ! -----------------------------
  ! m_DiagCIS   :  diagonalize the full CIS Hamiltonian
  logical :: m_DiagCIS=.false.

  ! m_diagnev   : number of smallest eigenstates
  integer :: m_diagnev = 0

  ! m_diagField(1) : smallest Field
  ! m_diagField(2) : Field step size
  ! m_diagField(3) : largest Field
  real(dp) :: m_diagField(3)=ZERO


  ! directories
  ! -----------
  ! m_matrix_folder : folder containing matrix files
  ! m_orbital_folder: folder containing orbitals+energy files
  character(clen),save :: m_matrix_folder  = ""
  character(clen),save :: m_orbital_folder = ""

  ! m_matrix_format : format matrix is stored in file
  !                   = "bin" : store in binary format
  !                             -> extra file *.info for each matrix 
  !                                is generated containing header infos
  !                   = "txt" : store in human readable ASCII 
  ! store matrices in binary or ASCII format
  ! -> if binary format is choosen, an exra file *.info is created
  !    for each matrix containing the header informations
  character(clen),save :: m_matrix_format  = "bin"
  
  ! m_storeorbU: store u_[n,l](r) separately
  logical,save :: m_storeOrbU = .false.


  ! CIS states that should be used to prepare the initial CIS state
  ! m_refstates_rootname : root name of files
  ! m_refstates_segment : defines range of numbers the file names should end with
  !                       format of defining ranges is similar to m_actoccorb
  !                       e.g. 1-10:22:32 
  !                       files names are m_refstates_rootname//'1' and so on
  ! m_refstates_mode : defines how these reference state should influence the initial state,
  !                    which is already defined via corrfct or backup.
  !                     = 'add' : add these states to initial state
  !                     = 'sub' : substract these states from the initial state
  !                     = 'lim' : limit the predefiend initial states to these states (remove the rest)
  !                     = 'only': use only the superosition of reference states 
  !                               as initial state
  !                     = '','null' : don't use reference states
  ! m_refstates_use : states if ref. states should be used (internal parameter)
  !                   = .false.: only for m_refstates_mode='null' [default]
  !                   = .true. : otherwise
  character(clen) :: m_refstates_rootname
  character(clen) :: m_refstates_segment
  type(t_string), allocatable  :: m_refstates_file(:)
  character(clen) :: m_refstates_mode
  logical ::         m_refstates_use


  ! orbital files
  ! ----------------------
  ! -> up to m_fSOcoupl all other file names are fixed
  ! m_focc     : occupation file
  ! m_forbU    : wavefunction file of u_[n,l](r)
  ! m_forb     : wavefunction file of A^k_[n,l]
  ! m_fenergy    : orbital energy file
  ! m_fenergy_new: orbital energy file (for overwitting selected orbital energies)
  character(LEN=clen) :: m_fenergy_new
  character(LEN=clen),save,target :: m_focc    = "occ"
  character(LEN=clen),save,target :: m_forbu   = "orbu"
  character(LEN=clen),save,target :: m_forb    = "orb"
  character(LEN=clen),save,target :: m_fenergy = "energy"

  ! matrix files
  ! ------------
  ! m_folap   : overlap matrix
  ! m_fdipsymm: dipole moment (complex symmetric)
  ! m_fdipherm: dipole moment (hermitian)
  ! m_fdipacc : dipole acceleration
  ! m_fmomsymm: mometum operator in z-direction (complex symmetric)
  ! m_fmomherm: mometum operator in z-direction (hermitian)
  !
  ! m_fcouocc : Coulomb ineraction (v_ijkl)
  ! m_fcoudir : Coulomb ineraction (v_ajib)
  ! m_fcouexc : Coulomb ineraction (v_ajbi)
  character(LEN=clen),save,target :: m_fcouocc  = "coulomb-occ"
  character(LEN=clen),save,target :: m_fcoudir  = "coulomb-dir"
  character(LEN=clen),save,target :: m_fcouexc  = "coulomb-exc"
  character(LEN=clen),save,target :: m_folap    = "overlap"
  character(LEN=clen),save,target :: m_fsplit   = "split"
  character(LEN=clen),save,target :: m_ftsurfmat= "tsurf"
  character(LEN=clen),save,target :: m_fdipsymm = "dipole-symm"
  character(LEN=clen),save,target :: m_fdipherm = "dipole-herm"
  character(LEN=clen),save,target :: m_fdipacc  = "accel-herm"
  character(LEN=clen),save,target :: m_fmomsymm = "momen-symm"
  character(LEN=clen),save,target :: m_fmomherm = "momen-herm"

  
  ! pulse
  ! -------
  ! pulse objects (see module pulse_type for more infos)
  !  m_efield : E(t) 
  !  m_afield : A(t) 
  !  m_fieldtype : defines wether E(t) or A(t) is provided
  !          = E : E(t)  [default]
  !          = A : A(t)
  ! m_npulse : number of pulses defined
  integer :: m_npulse
  type(t_pulse), allocatable, target :: m_efield(:),m_afield(:)
  character, allocatable :: m_fieldtype(:)



  ! symmetries
  ! ----------
  ! m_useMsymm states if M symmetries is used to store matrix elements
  !  -> for 1p1h matrices : only m=m'=0 entries are stored
  !  -> for Coulomb (direct) matrices   : only m,m'>=0 entries are stored
  !  -> for Coulomb (exchange) matrices : only m>=0 entries are stored 
  !                                       (m=0: m'>=0; m/=0: m' no restriction)
  ! m_useSymm : use Coulomb matrix index symmetry
  !             -> v_pqrs = v_rqps = v_rspq
  logical,save :: m_useMsymm = .true.  &
                 ,m_useSymm  = .true.


  ! m_coulombmode: approximations made to the H_1 residual Coulomb interaction
  !              = "all"  : no approximation [default]
  !              = "intra": only intrachannel interactions
  !              = "inter": only interchannel interactions
  !              = "none" : no residual Coulomb interaction 
  !              = "slater": Hatree-Slater where exchange part is approximated
  !
  ! logical switches for faster access
  !  m_nocoulomb = true <-> m_coulombmode == "none"
  !  m_intercoupl = true/false <->  m_coulombmode == "inter"/"intra"
  character(clen),save:: m_coulombmode="all"
  logical,save :: m_intercoupl = .true.  &
                 ,m_nocoulomb  = .false. &
                 ,m_intracoupl = .true.

  ! spin-orbit coupling
  ! ---------------------
  ! m_SOcoupl    : spin-orbital coupling in occupied orbitals
  logical,save :: m_SOcoupl=.false.


  ! form of light-mater interaction 
  ! -> can have the values : dipole, velocity
  !    -> length   = E(t) * r.e_polarization
  !    -> velocity = A(t) p.e_polarization 
  character(clen),save :: m_LMgauge=""
  character(clen),parameter :: m_vgauge="velocity"
  character(clen),parameter :: m_lgauge="length"

  ! Visualization parameter
  ! --------------------------
  ! angular grid informations of the angles theta and phi, energy resolution 
  ! for elec. spectrum / elec. density
  !   -> index structure : 1. entry initial angle in degree
  !                        2. entry final angle in degree
  !                        3. number of points 
  real(dp),target :: m_theta(3) = ZERO , m_phi(3) = ZERO, m_energy(3)
  ! m_rskip : print only every m_rskip radial grid point
  ! m_tskip : print only every m_tskip time of the wavefunction
  integer, target  :: m_rskip=1  , m_tskip=1


  ! OPENMP
  ! ------
  ! m_omp_nthreads : number of threads
  !                  = 0 : system default is used (normally no. of cpu)
  integer, save :: m_omp_nthreads = 0


  ! Photoelectron Spectrum
  ! -----------------------
  ! m_pes : photoelectron spectrum as function of E,theta,phi,ionization channel i
  !         ->  m_pes(E,theta,phi,i)
  !         energy and angular resolution are defined via m_theta,m_phi,m_energy
  real(dp), allocatable :: m_pes(:,:,:,:)
  
end module VarSystem
