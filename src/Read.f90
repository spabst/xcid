module Read
  !
  !**PURPOSE
  !  Subroutines for reading orbital and energy files as well as 
  !  occupation file and the spin-orbit file. 
  !
  !  Subroutines for reading matrices are in the module
  !  ReadMatrices.   
  !
  !**DESCRIPTION
  !  -----------
  !  The following subroutines (sbr) and functions (fct) are part of 
  !  this module:
  !   -> (sbr) readsystempara : Read system parameter from formatted input
  !                             file and flags. 
  !                             Also do some post-read assignment.
  !   -> (sbr) readorb        : Read orbital wavefunctions from file and 
  !                             store them in the globally accessable 
  !                             module array "m_orbital".
  !   -> (sbr) readenergy     : Read orbital energies from file and 
  !                             store them in the globally accessable 
  !                             module array "m_orbital".
  !   -> (sbr) readoccupied   : Read occupation information and assign
  !                             occupied orbitals. Furthermore, set all
  !                             all occupied orbital related variables.
  !
  !  -> (sbr) read_backup :  read backup file which contains information about
  !                          the wfct and the IDM for a certain point in time
  !
  !**AUTHOR
  !  ------
  !  (c) and written by Stefan Pabst
  !
  !**VERSION
  !  -------
  !  svn info: $Id: Read.f90 2073 2015-10-10 20:42:26Z spabst $
  !-------------------------------------------------------------
  !
  use constants
  implicit none

contains

  subroutine readsystempara(a_input)
    !
    !  Cannot use local name assignment or local pointers to rename 
    !  module parameters used in namelist.
    !  These features are not supported with GFORTRAN 4.4.
    !  GFORTRAN 4.6 supports pointer and allocatable argument in namelist.
    !  Code is conform with GFORTRAN 4.4.
    !
    use varsystem
    use VarWfct
    use general, only : postprocess


    use inout,only : flaginput, find_openpipe
    use pulse, only : t_pulse
    implicit none

    integer, parameter :: npulse=10  ! need to be m_npulse (set in defaultvalue) !!
    character(clen),intent(in) :: a_input

    ! local pointer for input file
    ! ---------------------------------------------------

    ! system
    real(dblprec)   :: charge         
    logical ::         SOcoupl      
    character(clen) :: fenergy_new    &
                      ,coulombmode    &
                      ,actoccorb      &
                      ,orbformat      &
                      ,matrix_format  &
                      ,LMgauge        &
                      ,storeEV     
    ! grid
    integer         :: npoints, fem_nintpts
    real(dblprec)   :: rmax           &
                      ,zeta     

    logical         :: grid_rdfiles
    character(clen) :: grid_type     &
                      ,grid_files


    character(clen) :: absorb       
    real(dblprec)   :: Rabsorb      
    real(dblprec)   :: cap_strength   &
                      ,cs_angle       &
                      ,cs_smooth      &
                      ,split_r        &
                      ,split_smooth   &
                      ,tsurf_r
                      
    logical         :: split_write    &
                      ,tsurf

    !time and propagation
    integer         :: propmode       &
                      ,lanev   
    real(dblprec)   :: tmax           &
                      ,dt             &    
                      ,dtprint        &
                      ,split_dt

    ! output
    character(clen) :: fout      
    logical         :: storeIDMcapL   &
                      ,storewfct      &
                      ,storeOrbU       

    ! correlation function, initial 1-photon kick
    logical         :: corrfct     &  
                      ,probe       
    real(dblprec)   :: probe_E  
     
    ! folder name
    character(clen) :: matrix_folder  &
                      ,orbital_folder 

    ! limits
    integer         :: lmax        & 
                      ,lprx       
    real(dblprec)   :: ecut        &
                      ,llimit1p    &
                      ,llimit2p    

    ! HF
    integer         :: nelec      &
                      ,niter      
    real(dblprec)   :: convWF     &
                      ,convE      &
                      ,hfstep     &
                      ,rmaxscf    
    real(dblprec)   :: rminHighL      
    integer         :: LminHighL    
        
    ! m_modelpot object
    character(clen) :: modelpot_file 
    logical         :: modelpot_active 
    
    !Diagonalization
    logical         :: diagCIS
    integer         :: diagnev
    real(dblprec)   :: diagField(3)

    ! wfct backup
    character(clen) :: backup_file   
    logical         :: backup_read          &
                      ,backup_write         &
                      ,backup_append        &
                      ,backup_actorbfile 

    ! reference states for initial state (which will be propagated)
    character(clen) :: refstates_rootname  &
                      ,refstates_segment   &
                      ,refstates_mode

    real(dblprec)   :: backup_dt      


    ! efield object
    real(dblprec) ::   field_strength(npulse)     & 
                      ,field_Duration(npulse)     &  
                      ,field_t0(npulse)           &  
                      ,field_Frequency(npulse)    &
                      ,field_CEP(npulse)         
    character(clen) :: field_file(npulse)        
    logical ::         field_rdfile(npulse)       &
                      ,field_smooth(npulse)       &
                      ,field_atomicunits(npulse) 
    character       :: field_type(npulse)

    ! visualization of density
    ! angular grid
    real(dblprec)   :: phi(3)      &
                      ,theta(3)    &
                      ,energy(3)
    ! radial skipping factor
    integer         :: rskip       &
                      ,tskip  
    ! OPENMP
    integer         :: omp_nthreads

    ! ----------------------------------------------------------------
    logical :: ex
    integer :: fid
    ! ----------------------------------------------------------------

    namelist/inputpara/                                              &
         charge,SOcoupl,fenergy_new,LMgauge                          &
         ,rmax,npoints,fem_nintpts,zeta                              &
         ,grid_files,grid_rdfiles,grid_type                          &
         ,actoccorb,orbformat                                        &
         ,modelpot_file,modelpot_active,coulombmode                  &
         ,nelec,niter,convWF,convE,hfstep,rmaxscf                    &
         ,absorb,Rabsorb,cap_strength,cs_angle,cs_smooth             &
         ,split_r,split_smooth,split_dt,split_write                  &
         ,tsurf,tsurf_r                                              &
         ,field_strength,field_duration,field_frequency,field_cep    &
         ,field_file,field_rdfile,field_smooth,field_atomicunits     &
         ,field_t0,field_type                                        &
         ,tmax,dt,dtprint,propmode,lanev                             &
         ,storeEV,storeIDMcapL,storeWfct,storeOrbU,corrfct,fout      &
         ,matrix_format,matrix_folder,orbital_folder                 &
         ,lmax,lprx,ecut,llimit1p,llimit2p                           &
         ,rminHighL,LminHighL                                        &
         ,backup_file,backup_write,backup_read,backup_dt             &
         ,backup_actorbfile,backup_append                            &
         ,diagCIS,diagnev,diagField                                  &
         ,refstates_rootname,refstates_segment,refstates_mode        &
         ,probe,probe_E                                              &
         ,theta,phi,energy,rskip,tskip                               &
         ,omp_nthreads
   
   
    ! -------------------------

    ! check no. of pulses is consistent 
    if ( npulse /= m_npulse )  stop " ERROR(readsystempara):  default number of pulses is wrong"

    call assign_local

    inquire(file=a_input,exist=ex)
    if (ex) then
      fid = find_openpipe()
      open(fid,file=a_input,status="old",action="read")
      read(fid,nml=inputpara)
      close(fid)
    else
      write(STDERR,*) " WARNING : input file is not given / does not exist"      
    end if    

    ! check for flags and create new temporary input and read it in
    ! s.t. parameter stated in the flags overwrite the values from the
    ! input file
    ! NOTE: flaginput creates a pipe of status "scratch" (no file name)
    !       , content is lost after pipe is closed
    fid = find_openpipe()
    call flaginput("inputpara",fid,ex)
    if ( ex ) then
      read(fid,nml=inputpara)
    end if

    inquire(fid,exist=ex)
    if ( ex )   close(fid,status="delete")


    call assign_global
    call postprocess

  contains

    subroutine assign_local
      use VarSystem
      implicit none
 
      charge             = m_nuccharge     
      SOcoupl            = m_SOcoupl       
      fenergy_new        = m_fenergy_new
      coulombmode        = m_coulombmode   
      actoccorb          = m_actoccorb      
      orbformat          = m_orbformat
      matrix_format      = m_matrix_format
      LMgauge            = m_LMgauge
                         
      npoints            = m_npoints
      fem_nintpts        = m_fem_nintpts
      rmax               = m_rmax          
      zeta               = m_zeta
      grid_rdfiles       = m_grid_rdfiles
      grid_files         = m_grid_files
      grid_type          = m_grid_type
                         
      absorb             = m_absorb        
      Rabsorb            = m_Rabsorb       
      cap_strength       = m_cap_strength  
      cs_angle           = m_cs_angle      
      cs_smooth          = m_cs_smooth  
      split_r            = m_split_r
      split_smooth       = m_split_smooth
      split_dt           = m_split_dt
      split_write        = m_split_write   
      tsurf_r            = m_tsurf_r
      tsurf              = m_tsurf
                         
      tmax               = m_tmax          
      dt                 = m_dt            
      dtprint            = m_dtprint       
      propmode           = m_propmode  
      lanev              = m_lanev    
                         
      fout               = m_fout          
      storeev            = m_storeev       
      storeIDMcapL       = m_storeIDMcapL  
      storewfct          = m_storewfct     
      storeOrbU          = m_storeOrbU     
                         
      corrfct            = m_corrfct       
      probe              = m_probe         
      probe_E            = m_probe_E
                         
      matrix_folder      = m_matrix_folder
      orbital_folder     = m_orbital_folder
                         
      lmax               = m_lmax          
      lprx               = m_lprx          
      ecut               = m_ecut          
      llimit1p           = m_llimit1p      
      llimit2p           = m_llimit2p      
                         
      nelec              = m_nelec         
      niter              = m_niter         
      convWF             = m_convWF        
      convE              = m_convE         
      hfstep             = m_hfstep        
      rmaxscf            = m_rmaxscf       
      rminHighL          = m_rminHighL     
      LminHighL          = m_LminHighL     

      field_type         = m_fieldtype
      field_strength     = m_efield%strength
      field_Duration     = m_efield%Duration
      field_t0           = m_efield%t0
      field_Frequency    = m_efield%Frequency
      field_CEP          = m_efield%CEP
      field_rdfile       = m_efield%rdfile
      field_file         = m_efield%file
      field_smooth       = m_efield%smooth
      field_atomicunits  = m_efield%atomicunits

      modelpot_file      = m_modelpot%file
      modelpot_active    = m_modelpot%active

      backup_file        = m_backup%file_read
      backup_read        = m_backup%read
      backup_append      = m_backup%append
      backup_actorbfile  = m_backup%actorbfile
      backup_write       = m_backup%write
      backup_dt          = m_backup%dt
      
      diagcis            = m_diagcis
      diagnev            = m_diagnev
      diagfield          = m_diagfield
      
      refstates_rootname = m_refstates_rootname
      refstates_segment  = m_refstates_segment
      refstates_mode     = m_refstates_mode

      phi                = m_phi
      theta              = m_theta
      energy             = m_energy
      rskip              = m_rskip
      tskip              = m_tskip
      
      omp_nthreads       = m_omp_nthreads
    end subroutine assign_local
    
    
    subroutine assign_global
      use VarSystem
      implicit none
 
      m_nuccharge          = charge            
      m_SOcoupl            = SOcoupl           
      m_fenergy_new        = fenergy_new
      m_coulombmode        = coulombmode       
      m_actoccorb          = actoccorb 
      m_orbformat          = orbformat
      m_matrix_format      = matrix_format
      m_LMgauge            = LMgauge           
                                               
      m_npoints            = npoints
      m_fem_nintpts        = fem_nintpts
      m_rmax               = rmax              
      m_zeta               = zeta              
      m_grid_rdfiles       = grid_rdfiles
      m_grid_files         = grid_files
      m_grid_type          = grid_type
                                               
      m_absorb             = absorb            
      m_Rabsorb            = Rabsorb           
      m_cap_strength       = cap_strength      
      m_cs_angle           = cs_angle          
      m_cs_smooth          = cs_smooth 
      m_split_r            = split_r
      m_split_smooth       = split_smooth
      m_split_dt           = split_dt   
      m_split_write        = split_write
      m_tsurf_r            = tsurf_r
      m_tsurf              = tsurf
                                               
      m_tmax               = tmax              
      m_dt                 = dt                
      m_dtprint            = dtprint           
      m_propmode           = propmode 
      m_lanev              = lanev         
                                               
      m_fout               = fout              
      m_storeev            = storeev           
      m_storeIDMcapL       = storeIDMcapL      
      m_storewfct          = storewfct         
      m_storeOrbU          = storeOrbU         
                                               
      m_corrfct            = corrfct           
      m_probe              = probe             
      m_probe_E            = probe_E           
                                               
      m_matrix_folder      = matrix_folder
      m_orbital_folder     = orbital_folder
                                               
      m_lmax               = lmax              
      m_lprx               = lprx              
      m_ecut               = ecut              
      m_llimit1p           = llimit1p          
      m_llimit2p           = llimit2p          
                                               
      m_nelec              = nelec             
      m_niter              = niter             
      m_convWF             = convWF            
      m_convE              = convE             
      m_hfstep             = hfstep            
      m_rmaxscf            = rmaxscf           
      m_rminHighL          = rminHighL         
      m_LminHighL          = LminHighL         
                                               
      m_fieldtype          = field_type
      m_efield%strength    = field_strength
      m_efield%Duration    = field_Duration   
      m_efield%t0          = field_t0         
      m_efield%Frequency   = field_Frequency  
      m_efield%CEP         = field_CEP        
      m_efield%rdfile      = field_rdfile     
      m_efield%file        = field_file       
      m_efield%smooth      = field_smooth     
      m_efield%atomicunits = field_atomicunits
                                               
      m_modelpot%file      = modelpot_file     
      m_modelpot%active    = modelpot_active   
                                               
      m_backup%file_read   = backup_file       
      m_backup%read        = backup_read       
      m_backup%append      = backup_append     
      m_backup%actorbfile  = backup_actorbfile 
      m_backup%write       = backup_write      
      m_backup%dt          = backup_dt    

      m_diagcis            = diagcis
      m_diagnev            = diagnev
      m_diagfield          = diagfield
      
      m_refstates_rootname = refstates_rootname
      m_refstates_segment  = refstates_segment
      m_refstates_mode     = refstates_mode

      m_phi                = phi
      m_theta              = theta
      m_energy             = energy
      m_rskip              = rskip
      m_tskip              = tskip
      
      m_omp_nthreads       = omp_nthreads
    end subroutine assign_global

  end subroutine readsystempara





  subroutine readorb(a_forb,a_orb,a_grid,ao_lread)
    ! read 1-particle orbitals in grid representation
    ! up to lread (lread = lmax when lread is not specified in input)
    ! 
    ! Note: length of txt can limit the number of values that can be read
    !       lines longer than txt will not be fully read
    !
    use varsystem,only : m_lmax,m_nelec
    use varwfct, only  : t_orbital,m_occnl,m_ioccnl
    use orbital,only : init_orbital
    use basics,only  : countsymbol
    use grid_type
    implicit none

    character(clen),intent(in) :: a_forb
    type(t_orbital), allocatable,intent(inout) :: a_orb(:,:)
    type(t_grid),target, intent(in)    :: a_grid
    integer,intent(in),optional :: ao_lread
    
    logical :: ex
    integer :: l,n,i,last,lr,num,nbasis,ibasis
    integer :: nelec
    real :: junk
    real(dblprec),allocatable :: orbr(:),orbi(:)
    character(99999) :: txt  


    lr=m_lmax
    if (present(ao_lread)) lr=ao_lread
    if (lr<0.or.lr>m_lmax) &
       stop " error(readorb): lread has an invalid value"

    ! re-initialize grid (incl. roots and A2u)
    if ( .not.allocated(a_grid%r) ) then
      stop " ERROR(readorb): grid is not set"
    end if
    nbasis = a_grid%nbasis

    allocate(orbr(0:nbasis-1),orbi(0:nbasis-1))

    ! orbital field must be allocated before 
    !  -> wfct array will be allocated late s.t. only field that are necessary
    !     are allocated
    if ( .not.allocated(a_orb) ) call init_orbital(a_orb,0) ! 0 = no wfct

    
    inquire(file=a_forb,exist=ex)
    if (.not.ex) stop " error (readorb): real orbital file doesn't exist"
    open(30,file=a_forb,status="old",action="read")
    
    do l=0,lr
      write(STDOUT,"(3X,a,i3,a)",advance="yes") "l =",l," ..."
      flush(STDOUT)

      orbr=ZERO
      orbi=ZERO
      
      ! read block with angular momentum l
      ibasis = 0
      num = 0
      last = 0 
      lp_lines:do while (last==0 .and. ibasis<nbasis)

         ! get line and check it
         txt = ""
         read(30,'(a)',iostat=last) txt
         if ( last /= 0 ) exit

         txt = adjustl(txt)
         if (trim(txt) == "" .or. txt(1:1)=="#") cycle
         
         if ( len(txt)-len_trim(txt) < 100 ) then
           write(STDERR,*) "WARNING(readorb): line reaches max. length of &
                                              &99,999 characters"
         end if

         if (num==0) then
           num = countsymbol(txt,"Ee")
           num = num/2  ! substract r column and get complex number pairs
           num = min(num,nbasis)
         end if


         ! next coefficient 
         ibasis = ibasis+1
         read(txt,*) (orbr(n),orbi(n),n=0,num-1)

         do n=0,num-1
           ! allocate only wfct, which are given in file
           if (.not.allocated(a_orb(n,l)%wfct))  call init_orbital(a_orb,1,n,l)
           
           a_orb(n,l)%wfct(ibasis) = orbr(n) + IMG * orbi(n)
         end do

      end do lp_lines
      
      ! assign grid
      do n=0,num-1
        a_orb(n,l)%grid => a_grid
      end do

    end do

    close(30)
    write(STDOUT,*)


    ! set occupied orbital infos
    nelec=0
    do i=1,m_ioccnl
      n = m_occnl(i,1)
      l = m_occnl(i,2)
      a_orb(n,l)%occ = min(4*l+2,m_nelec-nelec)
      nelec = nelec + a_orb(n,l)%occ
    end do

  end subroutine readorb




  subroutine readenergy(a_fen,ao_ecut)
    !
    !**PURPOSE
    !  read energy file
    !  
    !**DESCRIPTION  
    !  for tdcis nmax has the meaning of nbasis which is stored in m_grid
    !  -> energy file needs to be read twice to first determine nbasis
    !     in order to initialize m_orbital
    !
    !**INPUT
    !
    !  a_fen   : IN 
    !            name of energy file
    !  ao_ecut : IN - OPTIONAL
    !            max. real part of energy to be read 
    !            (assume energies are ordered w.r.t real part of energy)
    !            default : all eneergies are read
    !
    use varsystem,only : m_lmax,m_grid,m_SOcoupl,m_nmax,m_nmaxl,m_ecut
    use varwfct,only : m_lomax,m_orbital
    use orbital,only : init_orbital
    implicit none

    character(LEN=clen),intent(in) :: a_fen
    real(dblprec), intent(in), optional :: ao_ecut
    
    character(clen) :: txt
    logical :: ex

    integer :: l,n,ioread
    real(dblprec) :: enr,eni



    ex=.false.
    inquire(file=a_fen,exist=ex)
    if (.not.ex) stop " energy orbital file doesn't exist"

    open(31,file=a_fen,status="old",action="read")

    ! orbital field must be allocated before 
    !   mode = 0 : don't allo wfct
    ! -> if m_grid%nbasis not defined and first nmax has to be found
    if (.not.allocated(m_orbital))  call init_orbital(m_orbital,0) 

    ! find nmax to set nbasis
    if ( size(m_orbital)<1 ) then
      ioread = 0
      m_nmax=-1
      do while (ioread==0)
        read(31,'(a)',iostat=ioread) txt
        if ( ioread /= 0 ) exit
        txt = trim(adjustl(txt))

        if ( txt(1:1) == "#" .or. trim(txt) == "" ) cycle

        read(txt,*) n,l,enr
        m_nmax = max(m_nmax,n)
      end do
      
      rewind(31)
      if ( m_nmax<0 )  stop " ERROR(readenergy): no active orbital found"
      m_grid%nbasis = m_nmax+1  ! n starts from 0
      m_nmax = -1
      
      call init_orbital(m_orbital,0)
      if ( size(m_orbital)<1 )  then
        stop " ERROR(readenergy): orbital object could not be initialized"
      end if

    end if



    if ( size(m_orbital,1) < m_grid%nbasis )    &
      stop " ERROR(readenergy): m_orbital size is inconsistent (n index) "
    if ( size(m_orbital,2) < m_lmax+1 )    &
      stop " ERROR(readenergy): m_orbital size is inconsistent (l index) "

    ioread = 0
    do while (ioread==0)
      read(31,'(a)',iostat=ioread) txt
      if ( ioread /= 0 ) exit
      txt = trim(adjustl(txt))

      if ( txt(1:1) == "#" .or. trim(txt) == "" ) cycle

      read(txt,*) n,l,enr,eni

      if ( n>=m_grid%nbasis .or. l>max(m_lomax,m_lmax) ) cycle
      if ( present(ao_ecut) ) then  
        if ( enr > ao_ecut ) exit  ! energy cut-off reached -> stop reading
      end if

      m_orbital(n,l)%energy = enr + IMG*eni
      ! set spin orbital energies to non-relativistic one
      !  -> will be overwritten if spin orbital energies are specefied in 
      !     the spin orbital fil.
      m_orbital(n,l)%energy_so = enr + IMG*eni
    end do
    close(31)


    ! find max. n of the system
    ! -------------------------
    ! could be preset in readoccupied
    if (allocated(m_nmaxl)) deallocate(m_nmaxl)
    allocate(m_nmaxl(0:m_lmax))
    m_nmaxl=-1
    
    do l=0,m_lmax
      do n=0,m_grid%nbasis-1

        ! ASSUMING the ORBITAL ENERGIES INCREASE MONOTONICALLY with n.
        ! This assumption is necessary in the case not all orbital energies
        ! (especially for high n) are given. Then the orbital energies would
        ! be 0 < m_ecut
        ! ADDITIONALY orbital energies are skipped when they are 0.0 + 0.0*I
        if ( real(m_orbital(n,l)%energy) > m_ecut ) exit
        if ( abs(m_orbital(n,l)%energy) == zero ) cycle
    
        m_nmaxl(l)=max(m_nmaxl(l),n)

      end do
    end do

    ! total max. radial number (probably from a low ang. mom. l)
    m_nmax=maxval(m_nmaxl)

    ! overwrite selected orbital energies if file fenergy_new present
    ! ---------------------------------------------------------------
    call read_fenergy_new

  contains

    subroutine read_fenergy_new
      ! read orbital energies from file fenergy_new
      ! to overwrite orbital energies
      ! in case of spin-orbit: sub-shell energies are set (otherwise sub-shells are degenerate)
      !
      ! file format is always the spin-orbit format: n,l,j,real energy,imag. energy
      ! column for j value is iognored in case of no spin-orbit
      !
      use inout,only : inquireopen
      use varsystem,only : m_fenergy_new
      use varwfct,only : m_nomax

      integer :: i,is,nfield,ifield,iprev,inext
      integer :: n,l
      real :: j
      character(LEN=clen) :: txt
      type(t_string),allocatable :: switches(:)
      real(dblprec) :: er,ei
      logical :: exists


      ! check if file exist
      call inquireopen(exists,311,m_fenergy_new,"read")
      if ( .not.exists ) then
        if ( len_trim(adjustl(m_fenergy_new)) == 0 ) then
          write(STDERR,'(a)') " NOTE: no additional energy file (fenergy_new) is provided&
              &; file will be skipped"
        else
          write(STDERR,'(a)') " WARNING: energy file (fenergy_new) doesn't exist&
              &; file will be skipped"
        end if
        return  ! file doesn't exit => return 
      end if

      open(311,file=m_fenergy_new,status="old",action="read")
      write(*,'(2a)') "  -> reading energy_new file... ",trim(m_fenergy_new)

      ! loop through file
      is=0
      lp_read:do while (is==0)

        read(311,'(a)',iostat=is) txt
        if ( is /= 0 ) exit
        txt=trim(adjustl(txt))  ! delete first and last blanks

        ! skip comment/empty lines
        if (index(txt,"#")==1) cycle
        if (len_trim(txt)==0) cycle

        ! get n,l,j,energy
        ! ----------------------------------
        ! loop through line and save each element
        ! blanks are element separators
        nfield=len_trim(txt)
        allocate(switches(nfield))
        switches(:)%txt=" "
        i=0
        lp_line:do 
           ! exit when nothing is left in line
           if (trim(txt)==" ") exit          
           i=i+1

           ! get position of next entry
           iprev=verify(txt," ",.false.) ! begin of entry
           inext=iprev+scan(txt(iprev+1:)," ",.false.) ! end of entry

           ! index & save entry
           switches(i)%txt=trim(txt(iprev:inext-1))
           
           ! cut read entry away
           txt=txt(inext:)
        end do lp_line

        ! find n,l,j and store orbital energy
        ! assign values
        read(switches(1)%txt,*) n
        read(switches(2)%txt,*) l
        read(switches(3)%txt,*) j
        
        ! find energy column 
        er=ZERO
        ei=ZERO
        read(switches(4)%txt,*) er 
        if (len_trim(switches(5)%txt)/=0) read(switches(5)%txt,*) ei

        ! assign energy
        if ( m_SOcoupl .and. abs(j-l)==HALF ) then
          m_orbital(n,l)%energy_so(int(j+HALF)-l) = er + IMG * ei
        else
          m_orbital(n,l)%energy = er + IMG*ei
          m_orbital(n,l)%energy_so =  m_orbital(n,l)%energy
        end if
        
        deallocate(switches)
     end do lp_read
     close(311)
   end subroutine read_fenergy_new

  end subroutine readenergy


  subroutine readoccupied(a_focc)
    !
    !**DESCRIPTION
    !  ------------
    !  read in occupation file and assign corresponding parameters
    !  that carry occupied orbital information 
    !  ( incl. m_fmoact arrays that defined active occupied orbitals)
    !
    !**FILE FORMAT
    !  -----------
    !  file structure
    !  1. column: radial n number
    !  2. column: angular momentum l
    !
    !
    use varwfct,only : m_ioccnl,m_occnl,m_nomax,m_lomax,m_nomaxl       &
                      ,m_ioccall,m_fmoact,m_fmoact_SO

    use varsystem,only : m_SOcoupl,m_lmax,m_nmaxl,m_nmax,m_lsum,m_lprx
  
    implicit none

    character(clen),intent(in) :: a_focc
    character(clen) :: txt
    logical :: da
    integer :: is,i

    m_nomax=-1
    m_lomax=-1
    m_ioccall=0

    inquire(file=a_focc,exist=da)
    if (.not.da) then
       print *, "occupied file : ",trim(a_focc)
       stop " occupied orbital file doesn't exist"
    end if
    open(14,file=a_focc,status="old",action="read")
    
    ! find file length
    m_ioccnl=0
    is = 0
    do while (is==0)
      txt = ""
      read(14,'(a)',iostat=is) txt
      if ( is /= 0 ) exit
      txt = trim(adjustl(txt))

      if ( txt(1:1) == "#" .or. trim(txt) == "" ) cycle
      
      m_ioccnl=m_ioccnl+1
    end do
    rewind(14)
    if (m_ioccnl==0) stop " error(readoccupied): occupied field is empty"

    if (allocated(m_occnl)) deallocate(m_occnl)
        
    allocate(m_occnl(m_ioccnl,2))
    m_occnl=-1

    ! read in entire file
    i = 1
    is = 0
    do while (is==0 .and. i<=m_ioccnl)
      read(14,'(a)',iostat=is)  txt
      if ( is /= 0 ) exit

      txt = trim(adjustl(txt))
      if ( txt(1:1) == "#" .or. trim(txt) == "" ) cycle
      read(txt,*) m_occnl(i,1),m_occnl(i,2)
      i = i + 1
    end do 
    close(14)


    ! find parameter for occupied orbitals
    do i=1,m_ioccnl    
       ! maximal radial and ang. mom. quantum number
       m_nomax = max(m_nomax,m_occnl(i,1))
       m_lomax = max(m_lomax,m_occnl(i,2))
       m_ioccall = m_ioccall + 2*m_occnl(i,2) + 1
    end do
    if (m_SOcoupl) m_ioccall = 2*m_ioccall

    ! radial quantum number for each angular momentum l
    if (allocated(m_nomaxl)) deallocate(m_nomaxl)
    allocate(m_nomaxl(0:m_lomax))
    m_nomaxl=0

    do i=1,m_ioccnl ! max. occupied n for each l
      m_nomaxl(m_occnl(i,2)) = max( m_occnl(i,1) , m_nomaxl(m_occnl(i,2)) )
    end do

    ! set lsum for coulomb matrix calculation
    if (m_lsum<0) m_lsum = m_lomax + m_lprx
    m_lsum = min( m_lsum , m_lomax + m_lprx )


    ! set up field defining which are active occupied orbitals (fmoact(n,l,m))
    ! [ and fmoact_so(n,l,m_j-.5,j-l+.5) when SOcoupl ] 
    if (allocated(m_fmoact)) deallocate(m_fmoact)
    if (allocated(m_fmoact_so)) deallocate(m_fmoact_so)
    allocate(m_fmoact(0:m_nomax,0:m_lomax,-m_lomax:m_lomax))
    m_fmoact    = .false.

    if ( m_SOcoupl) then
      allocate(m_fmoact_so(0:m_nomax,0:m_lomax,0:m_lomax,0:1))
      m_fmoact_so = .false.
    end if

  end subroutine readoccupied



  subroutine read_backup(a_file,a_this)
    !
    !**PURPOSE
    !  -------
    !  Read backuped wavefunction
    !
    !**DESCRIPTION
    !  ------------
    !  Read wavefunction and IDM infos and store them in a_this
    !
    !**INPUT/OUTPUT
    !  ------------
    !  a_file    : CHARACTER(:) - IN
    !              file to be read
    !
    !  a_this    : TYPE(T_WFCT) - OUT
    !              wavefunction object containing all infos
    !
    use VarSystem, only : m_SOcoupl
    use VarWfct,   only : t_wfct,m_ioccact
    use General,   only : idocc
    use basics,    only : getelements
    use basics_append, only : append_allo
    use inout,     only : find_openpipe
    implicit none

    character(*), intent(in) :: a_file
    type(t_wfct)  :: a_this


    integer :: fid
    type(t_string), allocatable :: orbline(:)
    character(clen) :: txt
    character(9999*clen) :: txtaux  ! for long lines [e.g. "##IDM" line ]
    integer :: stat

    ! nlmj format
    ! 1. index: 1 - N <- radial quantum number
    !           2 - L
    !           3 - M_L or M_J-0.5 (w/o or w/ spin-orbit)
    !           4 - -1  or J - 0.5
    !           5 - act. orbital ID
    ! 2. index: number of tuples
    integer :: nlmj(5)
    integer, allocatable :: orb(:,:)

    ! act : active orbital information
    ! found_act : active occupied orbitals information found
    ! found_time : time information found
    logical :: act,found_act,found_time,found_idm,found_HF

    real(dblprec) :: aux1,aux2
    integer       :: i,j,k,idchannel


    ! if file doesn't exist just skip this routine and do nothing
    inquire(file=a_file,exist=act)
    if ( .not.act ) then
      write(STDOUT,*)  "WARNING(backup_file): no backup file ("//trim(a_file)//") found."
      return
    end if

    a_this%alpha_0   = ZERO
    a_this%alpha_ai  = ZERO
    a_this%idm       = ZERO
    a_this%time      = ZERO

    if ( size(a_this%idm,1)<m_ioccact .or. size(a_this%idm,2)<m_ioccact )     &
      stop " ERROR(read_backup): IDM array is too small"


    stat = 0
    found_act  = .false.
    found_time = .false.
    found_HF   = .false.
    found_IDM  = .false.


    ! -----------
    ! read header
    ! -----------
    ! -> to find lines with orbital information
    fid = find_openpipe()
    open(fid,file=a_file)


    lp_header:do while (stat==0)

      read(fid,'(a)',iostat=stat) txt
      if ( stat/=0 ) exit

      txt=adjustl(txt)

      if ( len_trim(txt)==0 ) cycle  ! empty line
      ! no comment or blank line => data section => stop reading
      if ( txt(1:1)/="#" ) exit


      ! orbital information
      ! ------------------------
      ! - only orbital information lines get it through to this point
      if_orb:if ( txt(1:6) == "##ORB " ) then
        act = .FALSE.
        call getelements(orbline,txt," ")

        read(orbline(2)%txt,*) nlmj(1)
        read(orbline(3)%txt,*) nlmj(2)
        read(orbline(4)%txt,*) aux1
        read(orbline(5)%txt,*) aux2
        read(orbline(8)%txt,*) act
        read(orbline(9)%txt,*) nlmj(5)
        nlmj(1) = nlmj(1) - nlmj(2) - 1  ! n = np - l -1


        ! check that format is OK
        if ( mod(2*aux1,1.0)/= 0 .or. mod(2*aux2,1.0)/=0 ) then
          write(STDERR,*) " WARNING(assg_act_orb_file): orbital information&
                                                      & has invalid format"
          write(STDERR,*) "  -> line reads : ",trim(txt)
          cycle
        end if

        ! check if spin orbital setting is the same as in the program
        if ( m_SOcoupl ) then
           if ( mod(4*aux1*aux2,2.0)/=1.0 )           &
              stop " ERROR(assg_act_orb_file): spin orbitals should be used"
          nlmj(3) = int(aux2-0.5)
          nlmj(4) = int(aux1-0.5)
        else
          if ( mod(aux2,1.0)/=0.0 .or. aux1/=0.0 )   &
              stop " ERROR(assg_act_orb_file): spin orbitals shouldn't be used"
          nlmj(3) = int(aux2)
          nlmj(4) = -1
        end if

        ! assign active orbitals
        if_act:if ( act ) then
          if ( nlmj(5) < 1 )  stop " ERROR(read_backup): act. orbital ID is invalid (<1)"
          found_act = .true.

          call append_allo( orb , reshape(nlmj,(/5,1/)) )
          i=size(orb,2)

          ! orbitals should be ordered w.r.t. active orbital ID
          ! starting with 1 and increasing by 1
          if ( orb(5,i) /= i )  stop &
              " ERROR(read_backup): act.  orbital ID is not ordered (as 1,2,3...)"

          ! find mapping of active orbitals in backup file
          ! to active orbitals in this program
          if (m_SOcoupl) then
            orb(5,i) = idocc(2,orb(1,i),orb(2,i),2*orb(3,i)+1,2*orb(4,i)+1)
          else
            orb(5,i) = idocc(2,orb(1,i),orb(2,i),orb(3,i))
          end if
        end if if_act
      end if if_orb



      ! IDM CAP-correction information
      ! -------------------------------
      if_idm:if ( txt(1:5) == "##IDM " ) then

        if ( .not.found_act )   &
          stop " ERROR(read_backup): no IDM because no act. occ. orbitals found"

        ! line could be quit long so use longer text field vaux
        backspace(fid)
        read(fid,'(a)') txtaux

        call getelements(orbline,txtaux," ")
        if ( size(orbline) /= 2*size(orb,2)**2 + 1 )    &
          stop " ERROR(read_backup): no. of IDM elements is inconsistent with no. of act. occ. orbitals"

        ! loop though IDM matrix
        k=2
        do i=1,size(orb,2)
          do j=1,size(orb,2)
            read(orbline(k)%txt,*) aux1
            k = k +1
            read(orbline(k)%txt,*) aux2
            k = k +1

            ! previously (in file) act. occ. orb. is now frozen (this run)
            if ( orb(5,i)<1 .or. orb(5,j)<1 ) cycle

            ! assign IDM correction to current IDM matrix
            a_this%idmcorr(orb(5,i),orb(5,j)) = aux1 + IMG*aux2
          end do
        end do

        found_IDM = .true.
      end if if_idm


      ! time information
      ! -----------------
      if_time:if ( txt(1:4) == "##T " ) then
        if ( found_time ) stop " ERROR(read_backup): more than 1 time information found"

        call getelements(orbline,txt," ")
        read(orbline(5)%txt,*) a_this%time
        found_time = .true.
      end if if_time

    end do lp_header
    backspace(fid)


    if ( stat > 0 )  stop " ERROR(read_backup): no data was found in the backup file"
    if ( stat < 0 )  stop " ERROR(read_backup): error in reading the backup file"

    if ( .not.found_time ) stop " ERROR(read_backup): no time information"
    if ( .not.found_act  ) stop " ERROR(read_backup): no orbital information"
    if ( .not.found_IDM  ) then
      write(STDERR,*)  " WARNING(read_backup): no CAP-corrected IDM information"
    end if



    ! -----------
    !  Read Data
    ! -----------
    stat = 0
    nlmj = 0
    lp_data:do while (stat==0)

      read(fid,'(a)',iostat=stat) txt
      if ( stat /= 0 ) exit  ! end of file or reading error
      txt = adjustl(txt)

      ! empty or comment line
      if ( len_trim(txt) == 0 .or. txt(1:1)=="#" ) cycle

      ! read data line
      read(txt,*)  nlmj(1:4),aux1,aux2

      ! skip line if n>nmax
      if ( nlmj(1) > ubound(a_this%alpha_ai,1) ) cycle
      ! skip rest of file if l>lmax (assume l is monotonically increasing)
      if ( nlmj(2) > ubound(a_this%alpha_ai,2) ) exit

      ! HF ground
      if ( all(nlmj(1:4)==-1) ) then
        if ( found_HF ) write(STDERR,*) " WARNING(read_backup): several HF coefficients"
        a_this%alpha_0 = aux1 + IMG*aux2
        found_HF = .true.
        cycle
      end if

      ! 1p-1h configuraton
      i = nlmj(4)
      if ( orb(5,i)<1 ) cycle  ! this previous act. occ. orbital is now frozen

      if ( .not.m_SOcoupl .and. nlmj(3)==0 ) then
        idchannel = idocc( 3 , orb(1,i) , orb(2,i) , orb(3,i))
      else if ( m_SOcoupl ) then
        idchannel = idocc( 3 , orb(1,i) , orb(2,i) , 2*orb(3,i)+1 &
                             , 2*orb(4,i)+1 , nlmj(3) )
      else
        stop " ERROR(read_backup): ms_a/=0 isn't allowed without spin orbitals"
      end if
      if ( idchannel < 1 ) stop "  ERROR(read_backup): ID of 1p1h-class is <1"

      ! store wfct
      a_this%alpha_ai( nlmj(1) , nlmj(2) , idchannel ) = aux1 + IMG * aux2

    end do lp_data

    ! close data pipe
    close(fid)
  end subroutine read_backup

end module Read


