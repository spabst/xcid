 module Grid
  !
  !**PURPOSE
  !  -------
  !  The MODULE GRID contains routines and functions that act on 
  !  grid object types defined in module grid_type.
  !
  !**DESCRIPTION
  !  -----------
  !  The following subroutines (sbr) and functions (fct) are part of 
  !  this module:
  !
  !  -> (sbr) initialize_grid: high-level routine to setup grid (for any grid type)
  !  -> (sbr) rgrid_init     : Generates radial grids
  !  -> (sbr) rgridscf_init  : Generates truncated radial grids for SCF routine
  !**AUTHOR
  !  ------
  !  (c) and written by Stefan Pabst
  !
  !**VERISON
  !  -------
  ! Version info: $Id: Grid.f90 1711 2015-05-22 18:44:19Z spabst $
  !-----------------------------------------------------------------------
  !
  use Constants, dp=>dblprec
  use grid_type
  implicit none 


contains

  subroutine initialize_grid(a_grid)
    !
    !  high level routine that generates the grid (and does all intermediate steps to achieve this)
    ! 
    use varsystem,only : m_grid_type,m_grid_files,m_grid_rdfiles     &
                        ,m_nmax,m_nmaxl,m_npoints,m_fem_nintpts      &
                        ,m_Rabsorb,m_cs_angle,m_cs_smooth            &
                        ,m_fout
    use varmatrix, only : m_fem_ov,m_femdvr_trafo
    use legendre,only  : Legendre_P
    use EigSlv, only   : eigensolver
    use reduce_lapack, only : cholesky_decomp, reduce_decomposed
    use analytic_integrator, only : int_caller
    use inout
    implicit none

    type(t_grid), intent(inout) :: a_grid

    real(dp), allocatable :: eigval(:),fem_ovup(:,:)
    real(dp) :: cs(3)
    integer :: i,info
    integer :: fid,n
    type(t_grid), pointer :: this => null()


    call rgrid_init(a_grid,ao_type=m_grid_type,ao_files=m_grid_files)
    ! for DVR grid the following will be reassigned after diagonalization of r in FEM-DVR_Hamil.f90
    m_nmax  = a_grid%nbasis
    m_nmaxl = m_nmax
    m_npoints = a_grid%npoints

    ! --------------------------------------------------------------------
    if ( a_grid%type=="FEM" )  then
      ! initialize PSG subgrid to perform FEM integration
      ! only root points need to be defined for this grid

      ! exception: the pointer FEM_intgrid does not point to an existing object
      if ( associated(a_grid%FEM_intgrid) )  deallocate(a_grid%FEM_intgrid)
      allocate(a_grid%FEM_intgrid)

      call rgrid_init(a_grid%FEM_intgrid,ao_type="PSG",ao_nr=m_fem_nintpts,ao_absorb="CAP",ao_endpts=.true.)
      a_grid%FEM_intgrid%A2u = Legendre_P( a_grid%FEM_intgrid%npoints-1 , a_grid%FEM_intgrid%root )
      a_grid%FEM_intgrid%r   = a_grid%FEM_intgrid%root
    end if      
       

    ! --------------------------------------------------------------------
    if ( a_grid%type=="DVR")  then
      
      ! DVR_grid: grid points defining the FEM functions
      ! --------------
      if ( associated(a_grid%DVR_grid) )  deallocate(a_grid%DVR_grid)
      allocate(a_grid%DVR_grid)
      call rgrid_init(a_grid%DVR_grid,ao_type="FEM",ao_nr=a_grid%npoints-2,ao_absorb="CAP",ao_endpts=.false.)
      a_grid%DVR_grid%r   = a_grid%r
      a_grid%DVR_grid%A2u = ONE

      ! build FEM integration grid
      ! --------------------------
      ! exception: the pointer FEM_intgrid does not point to an existing object
      this => a_grid%DVR_grid
      if ( associated(this%FEM_intgrid) )  deallocate(this%FEM_intgrid)
      allocate(this%FEM_intgrid)
   
      call rgrid_init(this%FEM_intgrid,ao_type="PSG",ao_nr=m_fem_nintpts,ao_absorb="CAP",ao_endpts=.true.)
      this%FEM_intgrid%A2u = Legendre_P( this%FEM_intgrid%npoints-1 , this%FEM_intgrid%root )
      this%FEM_intgrid%r   = this%FEM_intgrid%root
      this => null()


      ! build DVR grid
      ! --------------
      ! build m_femdvr_trafo
      allocate( m_femdvr_trafo(a_grid%nbasis,a_grid%nbasis), eigval(a_grid%nbasis) )
      m_femdvr_trafo = ZERO
      eigval = ZERO
      call build_femdvr_trafo(m_femdvr_trafo,eigval,a_grid)
         
      ! modify grid
      if ( allocated(a_grid%r) ) deallocate(a_grid%r)
      if ( allocated(a_grid%A2u) ) deallocate(a_grid%A2u)
      if ( allocated(a_grid%root) ) deallocate(a_grid%root)
      allocate( a_grid%r(a_grid%nbasis) )
      allocate( a_grid%A2u(a_grid%nbasis) )
      a_grid%r(:) = eigval(:)
      a_grid%A2u(:) = ONE

      a_grid%npoints = size(eigval)
      m_nmax  = a_grid%nbasis
      m_nmaxl = m_nmax
      m_npoints = a_grid%npoints

      ! DVR: store in A2u the volume elements dr for radial integration when
      !      the spatial representation and not the DVR-representation is used
      ! volume element dr_i = (r_{i+1}-r_(i-1})/2
      ! at the boundaries: dr_i = +/-(r_{i+/-1}-r_(i})/2      
      forall(i=2:a_grid%npoints-1) a_grid%A2u(i) = (a_grid%r(i+1)-a_grid%r(i-1))/TWO
      a_grid%A2u(1) = (a_grid%r(2)-a_grid%r(1))/TWO
      a_grid%A2u(a_grid%npoints) = (a_grid%r(a_grid%npoints)-a_grid%r(a_grid%npoints-1))/TWO
      
      ! change ECS grid
      if ( a_grid%absorbing=="ECS")  then
        cs = (/ m_Rabsorb,m_cs_angle,m_cs_smooth /)
        call a_grid%init_cplxscale(cs)

        ! include  1/sqrt(F'(r)) factor in A2u mapping
        !  -> see Eq. 10 in C. Buth, PRA 75, 033412 (2007)
        a_grid%A2u = a_grid%A2u / sqrt(a_grid%cs%F(:,1) )
      end if
    end if 



    fid = find_openpipe()
    call opendir(fid, file=trim(m_fout)//".grid")
    write(fid, *) "# grid points, ECS path (if ECS is used)"
    do i = 1,a_grid%npoints
      write(fid, "(ES17.7)", advance='no') a_grid%r(i)
      if ( allocated(a_grid%cs) ) then
        write(fid, "(8ES17.7)", advance='no')(a_grid%cs%F(i,n),n=0,3)
      end if
      write(fid,*)
    end do

    if ( associated(a_grid%DVR_grid) )  then
      write(fid, '(2/,a)')  "# FEM reference grid points (used to build FEMDVR grid)"
      do i = 1,a_grid%DVR_grid%npoints
        write(fid, "(ES17.7)", advance='yes') a_grid%DVR_grid%r(i)
      end do
    end if
    close(fid)


  end subroutine initialize_grid



  subroutine  rgrid_init(a_this,ao_type,ao_nr,ao_rmax,ao_zeta,ao_absorb,ao_cs,ao_files,ao_endpts)
    !
    ! initialize radial grid
    ! use system-wide parameter in varsystem when parameters
    ! nr,rmax,zeta are not specified
    !
    !
    ! OUTPUT
    ! ------
    !  a_this - TYPE(T_GRID) - INOUT 
    !           grid object
    !
    ! INPUT
    ! -----
    !  type - CHARACTER - IN - OPTIONAL
    !         type of grid (FEM or PSG=pseudo sepctral grid)
    !          default : PSG
    !
    !  nr   - INTEGER - IN - OPTIONAL
    !         number of grid points (r=0,1 are omitted)
    !
    !  rmax - REAL(DP) - IN - OPTIONAL 
    !         max. radius
    !
    !  zeta - REAL(DP) - IN - OPTONAL
    !         mapping parameter
    !
    !            r(x) = rmax/2*zeta * (1+x)/(1-x+zeta)
    !
    ! absorb - CHARACTER - IN - OPTIONAL
    !          absorbing method used  [default=m_absorb] 
    !            = CAP  : CAP
    !            = ECS  : smooth exterior complex scaling
    !
    !   cs   - REAL(3) - IN - OPTIONAL
    !          array of complex scaling parameters
    !            cs(1) - radius of exterior complex scaling
    !            cs(2) - angle of complex rotation
    !            cs(3) - smoothness factor
    !
    !  endpts  - include end points for PSG method
    !
    use varsystem,only : m_zeta,m_grid,m_rmax                        &
                        ,m_Rabsorb,m_cs_angle,m_cs_smooth,m_absorb   &
                        ,m_cap_strength,m_npoints  
    use legendre,only  : generate_LGL_points,Legendre_P
    use inout, only    : flength,find_openpipe, readfile_fct
    use basics, only   : getelements
    use basics_append, only : append_allo
    implicit none

    type(t_grid)                 :: a_this
    integer,intent(in),optional  :: ao_nr
    real(dp),intent(in),optional :: ao_zeta,ao_rmax
    character(3),intent(in),optional :: ao_absorb,ao_type
    real(dp),intent(in),optional :: ao_cs(3)
    character(clen), intent(in), optional :: ao_files
    logical, intent(in), optional :: ao_endpts

    integer  :: i,np,nr
    real(dp) :: zeta,rmax,cs(3)
    character(3) :: absorb,gtype
    character(clen) :: files
    logical :: endpts
    real(dp) :: aux
    
    ! read in grid files
    integer :: fid
    type(t_string), allocatable :: filelist(:)
    real(dp), allocatable :: raux(:),raux2(:)


    ! get parameters
    nr = m_npoints
    if (present(ao_nr)) nr = ao_nr
   
    rmax = m_rmax
    if (present(ao_rmax)) rmax = ao_rmax

    zeta = m_zeta
    if (present(ao_zeta)) zeta = ao_zeta

    cs = (/ m_Rabsorb,m_cs_angle,m_cs_smooth /)
    if ( present(ao_cs)) cs = ao_cs

    absorb = adjustl(m_absorb)
    if ( present(ao_absorb) ) absorb = ao_absorb

    gtype = "PSG"
    if ( present(ao_type) ) gtype = ao_type

    files = ""
    if ( present(ao_files) ) files = ao_files

    endpts = .false.
    if ( present(ao_endpts) ) endpts = ao_endpts

    if_gridtype:if ( gtype=="PSG" .or. (gtype/="PSG" .and. len_trim(files)==0) ) then
      ! grid points are defined by input parameters and the Moebius mapping

      select case(gtype)
      case('PSG')
        if ( nr<2 ) stop " ERROR(rgrid_init): number of PSG grid points is < 2"
      case('FEM','DVR')
        if ( nr<0 ) stop " ERROR(rgrid_init): number of FEM reference grid points is < 0"
      end select

      call init_grid_type(a_this,nr) 
      a_this%type = gtype
      a_this%zeta  = zeta
      a_this%rmax  = rmax
      a_this%rmin  = ZERO
      a_this%absorbing = absorb

      ! get Legendre Roots of P'_(N+1), N=a_nr
      call generate_LGL_points( a_this%root , nr )
    
      ! mapping between [-1:1]->[0:r_max]
      ! r(x) = L * zeta/2 * (1+x)/(1-x+zeta)
      a_this%r = rmax*zeta*HALF * (ONE+a_this%root) / (ONE-a_this%root+zeta)


      if ( gtype=="PSG" ) then
        a_this%nbasis  = a_this%npoints
        a_this%norm  = sqrt((nr+1)*(nr+2)/TWO )

        ! mapping : A^k_nl -> u_{nl}(x_k) = P_N(x_k) / sqrt( r'(x) ) * A^k_nl
        !  -> see Eq. 43 in L. Greenman. PRA 82, 023406 (2010)
        a_this%A2u = Legendre_P( nr+1 , a_this%root )  &
                   * sqrt( TWO/(rmax*zeta*(TWO+zeta)) ) * (ONE-a_this%root+zeta)
        
      end if
      
      ! add end points
      if ( endpts .or. ( (gtype=="FEM" .or. gtype=="DVR") .and. len_trim(files)==0) ) then

        a_this%npoints = a_this%npoints + 2 
        a_this%nbasis  = a_this%npoints

        call append_allo(a_this%r , (/ZERO/) , ao_append=.false. )
        call append_allo(a_this%r , (/rmax/) , ao_append=.true. )
    
        call append_allo(a_this%root , (/-ONE/) , ao_append=.false. )
        call append_allo(a_this%root , (/+ONE/) , ao_append=.true. )

        call append_allo(a_this%A2u , (/sqrt(TWO/rmax/zeta*(TWO+zeta))/) , ao_append=.false. )
        call append_allo(a_this%A2u , (/sqrt(TWO/rmax*zeta/(TWO+zeta))/) , ao_append=.true. )

      end if

      if ( gtype=="FEM" .or. gtype == "DVR" )  then
        a_this%A2u = ONE
        a_this%nbasis = 3*a_this%npoints-2
        a_this%norm   = ONE
      end if

     
    else if ( (gtype=="FEM" .or. gtype=="DVR") .and. len_trim(files)>0 ) then
      ! grid points may be defined by files

      ! read grid files and assign them to m_grids
      call getelements(filelist,files," ")
      if ( .not.allocated(filelist) )  stop "ERROR(rgrid_init): no grid files provided"

      ! read subgrids and merge them into one grid
      do i=1,size(filelist)
      
        nr = flength( filelist(i)%txt , "E", "#") ! ao_mode="E" , ao_com="#" )
        if ( nr==0 )  cycle  ! file is empty or doesn't exist
        if ( allocated(raux2) )  deallocate(raux2)
        allocate(raux2(nr))
        raux2 = ZERO

        fid = find_openpipe()
        open(fid,file=filelist(i)%txt)
        call readfile_fct(fid,raux2,ao_com="#")
        close(fid)

        if ( i==1 ) then
          call append_allo(raux,raux2)
        else
          call append_allo(raux,raux2(2:))
        end if
      end do

      if ( .not.allocated(raux) .or. size(raux)<2 ) then
        stop " ERROR(grid): no or only one grid point found in grid files"
      end if

      ! number of grid points of merged overall grid 
      ! -> (remove double counting of boundary points)
      ! ----------------------------------------------      
      nr = size(raux)
      call init_grid_type(a_this,nr)
      
      a_this%type = gtype
      a_this%r    = raux
      a_this%nbasis = 3*a_this%npoints-2
      a_this%norm  = ONE
      
      a_this%zeta  = ZERO
      a_this%rmax  = a_this%r(nr)
      a_this%rmin  = a_this%r(1)
      a_this%absorbing = absorb

    end if if_gridtype


    ! complex scaling
    ! ----------------
    if ( a_this%absorbing=="ECS" .and. gtype /= "DVR" )  then
      ! DVR:  ECS will be evaluated later;  first new grid points need to be find
      call a_this%init_cplxscale(cs)
      
      ! CAP strength is solely defined by H(cs_theta) - H(cs_theta=0)
      m_CAP_strength = ONE  

      ! include  1/sqrt(F'(r)) factor in A2u mapping
      !  -> see Eq. 10 in C. Buth, PRA 75, 033412 (2007)
      a_this%A2u = a_this%A2u / sqrt( a_this%cs%F(:,1) )
    end if

  end subroutine rgrid_init




  subroutine rgridscf_init(a_grid,a_rmaxscf)
    !
    !define radial grid for the SCF routine
    !
    ! INPUT
    ! -----
    !  grid   - grid object to be intitiated
    !
    ! rmaxscf - max. radius for SCF routine 
    !
    use varsystem,only : m_grid
    implicit none

    type(t_grid), intent(inout) :: a_grid
    real(dp),intent(in) :: a_rmaxscf
    
    integer :: npointscf
   


    ! check that system-wide grid is initialized
    if (.not.allocated(m_grid%r) ) then
      ! set system-wide grid
      call rgrid_init(m_grid)
    end if

    npointscf = minval(minloc(abs(m_grid%r-a_rmaxscf)))

    call init_grid_type(a_grid,npointscf)

    a_grid%type  = m_grid%type
    a_grid%npoints = npointscf
    select case(m_grid%type)
    case("PSG")
      a_grid%nbasis  = a_grid%npoints
    case("FEM")
      a_grid%nbasis  = 3*a_grid%npoints-2
    case("DVR")
      a_grid%nbasis  = a_grid%npoints
    end select
    a_grid%norm  = m_grid%norm
    a_grid%rmax  = a_rmaxscf
    a_grid%rmin  = m_grid%rmin
    a_grid%zeta  = m_grid%zeta
    a_grid%absorbing  = m_grid%absorbing

    a_grid%r    = m_grid%r(1:npointscf)
    if ( m_grid%type /= "DVR" ) then
      a_grid%root = m_grid%root(1:npointscf)
      a_grid%A2u  = m_grid%A2u(1:npointscf)
    else
      a_grid%root = ONE
      a_grid%A2u  = ONE
    end if
    
    a_grid%parent => m_grid  ! parent grid
   

    ! complex scaling
    ! ---------------
    if ( a_grid%absorbing == "ECS" ) then
!      if ( .not. associated(m_grid%cs) )   &
      if ( .not. allocated(m_grid%cs) )   &
        stop " ERROR(init_rgridscf): complex scaling object &
              &in m_grid is not defined"

      ! copy part of the complex scaling field of m_grid to a_grid 
      allocate(a_grid%cs)
      a_grid%cs = m_grid%cs

      deallocate(a_grid%cs%F)
      allocate(a_grid%cs%F(npointscf,0:3))
      a_grid%cs%F = m_grid%cs%F(1:npointscf,0:3)
    end if

  end subroutine rgridscf_init



  !----------------------------------------------------



  subroutine build_femdvr_trafo(a_matrix, a_eigval, a_grid)

    use varmatrix, only : m_fem_ov
    use EigSlv, only   : eigensolver
    use analytic_integrator, only : int_caller
    use f95_precision
    use lapack95
    implicit none

    real(dp),intent(inout)   :: a_matrix(:,:), a_eigval(:)
    type(t_grid), intent(in) :: a_grid

    real(dp), allocatable :: fem_ov(:,:)
    integer :: info


    a_matrix = ZERO

    if ( allocated(m_fem_ov) )   deallocate(m_fem_ov)
    allocate( m_fem_ov(a_grid%nbasis,a_grid%nbasis) )
    m_fem_ov = ZERO
    
    if ( size(a_matrix) /= size(m_fem_ov) )  then
      stop " ERROR(build_femdvr_trafo): trafo array is not allocated"
    end if

    ! creates radial position operator r
    call int_caller(a_matrix, a_grid, a_grid%rmin, a_grid%rmax, .false., 1)
    
    ! initialise overlap
    call int_caller(m_fem_ov, a_grid, a_grid%rmin, a_grid%rmax, .false., 0)
    
    allocate( fem_ov(a_grid%nbasis,a_grid%nbasis) )
    fem_ov = m_fem_ov


    ! diagonalize radial position operator
    ! -> SYGVD should be faster when eigenvectors are wanted  (results are a little different)
    ! -> SYGV give exactly the same result as Arina's implementation
    call SYGV(a_matrix,fem_ov,a_eigval,jobz="V",info=info)  
    
  end subroutine build_femdvr_trafo

end module Grid
