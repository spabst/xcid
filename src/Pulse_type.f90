module pulse_type
  !
  !**PURPOSE
  !  -------
  !  Defines pulse object
  !
  !!**AUTHOR
  !  ------
  !  (c) and written by Stefan Pabst
  !
  !**VERISON
  !  -------
  ! Version info: $Id: Pulse_type.f90 1718 2015-05-25 14:36:59Z spabst $
  !-----------------------------------------------------------------------
  !
  use Constants, dp=>dblprec
  implicit none

  type :: t_pulse
    ! Gaussian profile parameters
    ! ---------------------------
    ! para_ref : states to which function F_n(t) the Gauss parameters stored 
    !            here refer to. Specifically, 
    !
    !                     (-1)^n  d^n/dt^n F_n(t) = pulse(t)  for n=-1,0,1
    !
    !            Here, n<0 refers to integration and n>0 to differentiation.
    !            Default :  n=0 
    integer ::   para_ref = 0
    
    ! duration : FWHM pulse duration 
    ! frequency: pulse carrier frequency 
    ! Emax     : max. electric field
    ! CEP      : carrier envelope phase
    ! t0       : center of the pulse
    real(dp) ::  duration  = ZERO  &
                ,frequency = ZERO  &
                ,strength  = ZERO  &
                ,CEP       = ZERO  &
                ,t0        = ZERO


    ! pulse file
    ! -----------
    ! rdfile : read pulse from pulse profile file
    ! smooth : let the ends of the given pulse go smoothly to 0.0
    ! atomicunits : all pulse properties (radius,duration, and E-field) 
    !                 are given in atomic units
    logical :: rdfile = .false.     &
              ,smooth = .false.     &
              ,atomicunits = .true.

    ! file  : name of pulse profile file
    character(LEN=clen) :: file=""
    ! time   : time points in pulse profile file
    ! field  : (electric) field profile 
    real(dp),allocatable :: time(:),field(:)

    ! spline auxiliary arrays to interpolate pulse profile
    real(dp),allocatable :: spline(:,:)
  end type t_pulse

end module pulse_type
