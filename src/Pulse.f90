module Pulse
  !
  !**PURPOSE
  !  -------
  !  The MODULE PULSE contains routines and functions related to 
  !  electric field pulse.
  !
  !**DESCRIPTION
  !  -----------
  !  The following subroutines (sbr) and functions (fct) are part of 
  !  this module:
  !   -> (fct) GetPulse(fd)   : calculate field strength at a specific time
  !   -> (sbr) read_pulsefile : read pulse file and initialize spline arrays
  !   -> (sbr) read_pulse     : initialize pulse (read pulse from file if needed)
  !
  !**AUTHOR
  !  ------
  !  (c) and written by Stefan Pabst
  !
  !**VERISON
  !  -------
  ! Version info: $Id: Pulse.f90 2172 2015-12-11 03:06:19Z spabst $
  !-----------------------------------------------------------------------
  !
  use Constants, dp=>dblprec
  use pulse_type
  implicit none

  interface GetPulse
    module procedure  GetPulse_all,GetPulse_one
  end interface GetPulse


contains

  elemental function GetPulse_one( a_time , a_pulse , ao_mode , ao_fact ) Result(field)
    !
    !**PURPOSE
    !  -------
    !  calculate the electric field at time "time"
    ! 
    !**DESCRIPTION
    !  -----------
    ! convertion for transforming FWHM from time to energy width
    !  ->  pulse-FWHM[a.u.] = 4*ln(2)/dE[a.u.] 
    !                       = 27.21*ln(16) / dE[eV]
    !      pulse-FWHM[as]   = 24.19*27.21*ln(16)/dE[eV]
    !                       = 1820/dE[eV]
    ! 
    !**INPUT/OUTPUT
    !  ------------
    !   a_time   : time argument
    !   a_pulse  : pulse object containing all information
    !   ao_mode  : calculate 
    !        = 0 : f(x)   [default]
    !        = 1 : f'(x) 
    !        =-1 : F(x)
    !              where f(x) = exp(-a*x^2) cos(w*x+c) and F(x) is the integral.
    !              Only used when pulse is provided by parameters and not by
    !              a file.
    !   ao_fact : additional multiplication factor
    !               = 1 [default]
    !
    !
    use basics, only : errorFct
    implicit none

    real(dp), intent(in)      :: a_time
    type(t_pulse), intent(in) :: a_pulse
    integer, intent(in), optional :: ao_mode
    real(dp), intent(in), optional :: ao_fact

    real(dp) :: field,t
    real(dp) :: stren,freq,cep,dur
    complex(dp) :: z,expc

    integer :: mode
    real(dp) :: fact


    field=ZERO
    
    ! optional arguments
    mode = a_pulse%para_ref
    if ( present(ao_mode) ) mode = ao_mode
    
    fact = ONE
    if ( present(ao_fact) ) fact = ao_fact


    t = a_time - a_pulse%t0  ! center pulse around t0

    if (.not.a_pulse%rdfile)  then
      stren = a_pulse%strength
      freq  = a_pulse%frequency
      cep   = a_pulse%CEP
      dur   = a_pulse%duration / TWO / sqrt(log(TWO))  ! E-field FWHM

      ! magnitude is zero -> no need to calculate anything
      if ( stren*fact == ZERO ) return 

      select case ( mode ) 
      case(1)
        field = -stren*freq * sin(freq*t+cep)
        if ( dur /= ZERO ) then
          field = field - stren*TWO/dur**2 * t*cos(freq*t+cep)
          field = field * exp( -(t/dur)**2 )
        end if

        field = -field  ! E(t) = -A'(t)
        
      case(0)
        field = stren * cos(freq*t+cep)
        if ( dur /= ZERO ) then
          field = field * exp( -(t/dur)**2 )
        end if
        
      case(-1)
        if ( dur /= ZERO ) then
          expc = exp(-IMG*cep)
          z = ( t + IMG*dur**2*freq/2. )/dur

          ! pass exponent of rescale factor to errorFct to prevent
          ! overflow of numbers
          field = stren*sqrt(pi)*dur/TWO  &
                  * real( expc*errorFct(z,ao_fac=-(freq*dur/2.)**2) )
          !OLD ! crashed for multicycle pulses
          !OLD field = stren*exp(-freq**2*dur**2/4.)*sqrt(pi)*dur/TWO  &
          !OLD        * real( expc*errorFct(z) )
        else
          if ( freq /= ZERO ) then
            field = stren/freq * sin(freq*t+cep)
          else
            field = stren*cos(cep) * t
          end if
        end if

        field = -field  ! A(t) = -int_dt' E(t')
      end select

    else 
      ! spline interpolation from field specified in the pulse file
      field = get_pulseform(t)

    end if

    field = field * fact
    if (abs(field)<1.d-99) field = ZERO 


  contains

    pure function get_pulseform(a_time) Result(res)
      ! interpolate electric field with the help of the spline arrays
      use SplineInterpol, only : eval_spline
      implicit none

      real(dp) :: res
      real(dp),intent(in)  :: a_time
      integer :: nline

      res = ZERO

      if ( .not.(allocated(a_pulse%field) .and. allocated(a_pulse%spline)) &
         )  return 
      
      nline = size(a_pulse%field)
      res = eval_spline(a_time,a_pulse%time,a_pulse%field,a_pulse%spline)   
      if ( a_time>a_pulse%time(nline) .or. a_time<a_pulse%time(1) ) res = ZERO
    end function get_pulseform
    
  end function GetPulse_one



  pure function GetPulse_all( a_time , a_pulse , ao_mode , ao_fact ) Result(field)
    !
    !**PURPOSE
    !  -------
    !  calculate the overall field consisting of several subfields
    ! 
    !**DESCRIPTION
    !  -----------
    !  see GetPulse_one
    !
    implicit none

    real(dp), intent(in)      :: a_time
    type(t_pulse), intent(in) :: a_pulse(:)
    integer, intent(in), optional :: ao_mode
    real(dp), intent(in), optional :: ao_fact

    real(dp) :: field

    field = sum(GetPulse_one(a_time,a_pulse,ao_mode,ao_fact))
  end function GetPulse_all

  ! ---------------------------------------------------------------


  subroutine read_pulsefile( a_pulse )
    !
    ! Read pulse from file and make spline interpolation
    !     
    use inout,  only         : readfile,flength
    use SplineInterpol, only : init_spline
    implicit none
    
    type(t_pulse) :: a_pulse

    logical :: ex
    integer,parameter :: id=37
    integer :: ioread,nline,iline
    character(LEN=clen) :: txt
    real(dp) :: aux,grad,dt


    ! check existance of pulse file
    inquire(file=a_pulse%file,exist=ex)
    if (.not.ex) &
         stop " error (initPulse):  pulse input file doesn't exist!!!"
       
    ! find length
    nline = flength(a_pulse%file,"E","#")
    if ( nline ==0 ) &
         stop " error (initPulse): pulse file is empty!!"
    
    
    ioread=0
    iline=0
    if (a_pulse%smooth) then
       ! add 3 points at the beginning and end to make a smooth curve
       ! s.t. E -> 0
       nline=nline+2*3
       iline=3
    end if
    
    if (allocated(a_pulse%field)) deallocate(a_pulse%field)
    if (allocated(a_pulse%time))   deallocate(a_pulse%time)
    if (allocated(a_pulse%spline)) deallocate(a_pulse%spline)

    allocate(a_pulse%field(nline),a_pulse%time(nline),a_pulse%spline(nline,3))
    a_pulse%field  = ZERO
    a_pulse%time   = ZERO
    a_pulse%spline = ZERO
   
  
    ! read file
    ! ---------
    open(id,file=a_pulse%file,status="old",action="read")
    call readfile( id, a_pulse%time , a_pulse%field )
    close(id)



    ! smooth curves
    ! -------------
    if ( a_pulse%smooth ) then

       ! set E=0 E'=0 at end points
       ! end points are twice as far away as for a linear extrapolation
       
       ! shift data by 3 entries to the right
       a_pulse%time(4:nline-3) = a_pulse%time(1:nline-6)
       a_pulse%time(1:3) = ZERO
       a_pulse%field(4:nline-3) = a_pulse%field(1:nline-6)
       a_pulse%field(1:3) = ZERO

       ! start point
       a_pulse%field(1)=ZERO
       dt = a_pulse%time(5)-a_pulse%time(4)
       grad = a_pulse%field(5)-a_pulse%field(4)
       a_pulse%field(2) = a_pulse%field(4)/8
       a_pulse%field(3) = a_pulse%field(4)/2
       ! time step
       aux = max(abs(a_pulse%field(4))*dt/(abs(grad)+1e-10)/2,dt)
       aux = min(10*dt,aux)

       ! if gradient has wrong direction, make way twice as long
       if (grad*a_pulse%field(4)<ZERO) aux = aux*2 
       
       a_pulse%time(3) = a_pulse%time(4) - 1*aux
       a_pulse%time(2) = a_pulse%time(4) - 2*aux
       a_pulse%time(1) = a_pulse%time(4) - 3*aux

       ! end point
       a_pulse%field(nline)=ZERO
       dt = a_pulse%time(nline-3)-a_pulse%time(nline-4)
       grad = a_pulse%field(nline-3)-a_pulse%field(nline-4)
       a_pulse%field(nline-2) = a_pulse%field(nline-3)/2
       a_pulse%field(nline-1) = a_pulse%field(nline-3)/8
       ! time step
       aux = max(abs(a_pulse%field(nline-3))*dt/(abs(grad)+1e-10)/2,dt)
       aux = min(10*dt,aux)
       ! if gradient has wrong direction, make way twice as long
       if (grad*a_pulse%field(nline-3)>ZERO) aux = aux*2 
       
       a_pulse%time(nline-2) = a_pulse%time(nline-3) + 1*aux
       a_pulse%time(nline-1) = a_pulse%time(nline-3) + 2*aux
       a_pulse%time(nline)   = a_pulse%time(nline-3) + 3*aux
    end if
 
    if ( .not.a_pulse%atomicunits ) then
       ! convert time : as -> atomic units
       a_pulse%time(:) = a_pulse%time(:) * attosec2au
       ! convert E-field : V/m -> a.u.
       a_pulse%field = a_pulse%field * V_m2au 
    end if

    ! make spline interpolation
    call init_spline( a_pulse%time , a_pulse%field , a_pulse%spline )
  end subroutine read_pulsefile


  subroutine read_pulse
    use varsystem, only : m_efield,m_afield,m_fieldtype,m_npulse
    use splineinterpol, only : init_spline , eval_spline
    use basics, only : derivative1
    implicit none

    real(dp) :: dt
    integer :: i,ip


    do ip=1,m_npulse
      select case ( m_fieldtype(ip) )
      case("E")
        m_efield(ip)%para_ref = 0
        m_afield(ip)%para_ref = -1
      case("A")
        m_efield(ip)%para_ref = +1
        m_afield(ip)%para_ref = 0
      end select
 
 
      if ( .not.m_efield(ip)%rdfile ) then    
         if ( m_efield(ip)%frequency < ZERO ) stop " ERROR: pulse frequency < 0"
         if ( m_efield(ip)%Duration  < ZERO ) stop " ERROR: pulse width < 0"
         
      else
         write(STDOUT,'(2a)') " reading electric pulse file... ",trim(m_efield(ip)%file)
         
         select case ( m_fieldtype(ip) )
         case("E")
           call read_pulsefile(m_efield(ip))
           m_afield(ip) = m_efield(ip)
           m_afield(ip)%smooth = .false.
           dt = m_efield(ip)%time(2)-m_efield(ip)%time(1)
 
           ! A(t) = -int_t0^t E(t') dt'
           forall(i=1:size(m_afield(ip)%field))
             !OLD m_afield%field(i) = -sum( m_efield%field(2:i) ) * dt
             m_afield(ip)%field(i) = -eval_spline(m_efield(ip)%time(i)   &
                                                 ,m_efield(ip)%time      &
                                                 ,m_efield(ip)%field     &
                                                 ,m_efield(ip)%spline    &
                                                 ,ao_mode=-1)
           end forall
           ! spline interpolation
           call init_spline( m_afield(ip)%time , m_afield(ip)%field , m_afield(ip)%spline )
 
 
         case("A")
           call read_pulsefile(m_afield(ip))
           m_efield(ip) = m_afield(ip)
           m_efield(ip)%smooth = .false.
           dt = m_afield(ip)%time(2)-m_afield(ip)%time(1)
 
           ! E(t) = - d_t A(t)
           m_efield(ip)%field = -derivative1( m_afield(ip)%field , dt )
 
           ! spline interpolation
           call init_spline( m_efield(ip)%time , m_efield(ip)%field , m_efield(ip)%spline )
 
         case default
           write(*,*) " ERROR(postprocess): field_type=",m_fieldtype(ip)  &
                   ," is invalid (valid: E,A)"
           stop
 
         end select
      end if
    end do
  end subroutine read_pulse


end module Pulse
